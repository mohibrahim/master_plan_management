<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phases', function (Blueprint $table) {
            
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('name');
            $table->dateTime('supposed_start_date')->nullable();
            $table->dateTime('actually_start_date')->nullable();
            $table->dateTime('supposed_end_date')->nullable();
            $table->dateTime('actually_end_date')->nullable();
            $table->double('percentage_of_completion')->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();

            $table->bigInteger('creator_id')->unsigned()->nullable();
            $table->bigInteger('last_updater_id')->unsigned()->nullable();

            $table->foreign('creator_id')->references('id')->on('users')->onDelete('SET NULL');
            $table->foreign('last_updater_id')->references('id')->on('users')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phases');
    }
}
