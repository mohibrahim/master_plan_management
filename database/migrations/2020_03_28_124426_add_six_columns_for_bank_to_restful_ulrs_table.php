<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSixColumnsForBankToRestfulUlrsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('restful_urls', function (Blueprint $table) {
            $table->text('bank_api_store')->nullable();
            $table->text('bank_api_update')->nullable();
            $table->text('bank_api_destroy')->nullable();
            $table->text('bank_api_show')->nullable();
            $table->text('bank_api_index')->nullable();
            $table->dateTime('bank_sync_date_time')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('restful_urls', function (Blueprint $table) {
            $table->dropColumn('bank_api_store');
            $table->dropColumn('bank_api_update');
            $table->dropColumn('bank_api_destroy');
            $table->dropColumn('bank_api_show');
            $table->dropColumn('bank_api_index');
            $table->dropColumn('bank_sync_date_time');
        });
    }
}
