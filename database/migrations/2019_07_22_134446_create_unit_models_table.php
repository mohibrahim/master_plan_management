<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unit_models', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('code')->unique()->nullable();
            $table->string('type');
            $table->string('business_type')->nullable();
            $table->integer('built_up_area')->nullable();
            $table->string('land_area')->nullable();
            $table->smallInteger('number_of_floors')->unsigned()->nullable();
            $table->smallInteger('number_of_balconies')->unsigned()->nullable();
            $table->smallInteger('number_of_bedrooms')->unsigned()->nullable();
            $table->smallInteger('number_of_bathrooms')->unsigned()->nullable();
            $table->smallInteger('number_of_kitchens')->unsigned()->nullable();
            $table->smallInteger('number_of_elevators')->unsigned()->nullable();
            $table->smallInteger('number_of_entries')->unsigned()->nullable();
            $table->smallInteger('number_of_exits')->unsigned()->nullable();
            $table->smallInteger('number_of_shops')->unsigned()->nullable();
            $table->smallInteger('number_of_courts')->unsigned()->nullable();
            $table->string('finishing_type')->nullable();
            $table->boolean('has_garden')->nullable();
            $table->integer('garden_area')->unsigned()->nullable();
            $table->boolean('has_pool')->nullable();
            $table->integer('pool_area')->unsigned()->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();

            $table->bigInteger('creator_id')->unsigned()->nullable();
            $table->bigInteger('last_updater_id')->unsigned()->nullable();

            $table->foreign('creator_id')->references('id')->on('users')->onDelete('SET NULL');
            $table->foreign('last_updater_id')->references('id')->on('users')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unit_models');
    }
}
