<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddThirtyTwoColumnsToRestfulUrlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('restful_urls', function (Blueprint $table) {
            $table->text('project_api_store')->nullable();
            $table->text('project_api_update')->nullable();
            $table->text('project_api_destroy')->nullable();
            $table->text('project_api_show')->nullable();
            $table->text('phase_api_store')->nullable();
            $table->text('phase_api_update')->nullable();
            $table->text('phase_api_destroy')->nullable();
            $table->text('phase_api_show')->nullable();
            $table->text('zone_api_store')->nullable();
            $table->text('zone_api_update')->nullable();
            $table->text('zone_api_destroy')->nullable();
            $table->text('zone_api_show')->nullable();
            $table->text('block_api_store')->nullable();
            $table->text('block_api_update')->nullable();
            $table->text('block_api_destroy')->nullable();
            $table->text('block_api_show')->nullable();
            $table->text('unit_model_api_store')->nullable();
            $table->text('unit_model_api_update')->nullable();
            $table->text('unit_model_api_destroy')->nullable();
            $table->text('unit_model_api_show')->nullable();
            $table->text('unit_api_store')->nullable();
            $table->text('unit_api_update')->nullable();
            $table->text('unit_api_destroy')->nullable();
            $table->text('unit_api_show')->nullable();
            $table->text('customer_api_store')->nullable();
            $table->text('customer_api_update')->nullable();
            $table->text('customer_api_destroy')->nullable();
            $table->text('customer_api_show')->nullable();
            $table->text('reservation_api_store')->nullable();
            $table->text('reservation_api_update')->nullable();
            $table->text('reservation_api_destroy')->nullable();
            $table->text('reservation_api_show')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('restful_urls', function (Blueprint $table) {
            $table->dropColumn('project_api_store');
            $table->dropColumn('project_api_update');
            $table->dropColumn('project_api_destroy');
            $table->dropColumn('project_api_show');
            $table->dropColumn('phase_api_store');
            $table->dropColumn('phase_api_update');
            $table->dropColumn('phase_api_destroy');
            $table->dropColumn('phase_api_show');
            $table->dropColumn('zone_api_store');
            $table->dropColumn('zone_api_update');
            $table->dropColumn('zone_api_destroy');
            $table->dropColumn('zone_api_show');
            $table->dropColumn('block_api_store');
            $table->dropColumn('block_api_update');
            $table->dropColumn('block_api_destroy');
            $table->dropColumn('block_api_show');
            $table->dropColumn('unit_model_api_store');
            $table->dropColumn('unit_model_api_update');
            $table->dropColumn('unit_model_api_destroy');
            $table->dropColumn('unit_model_api_show');
            $table->dropColumn('unit_api_store');
            $table->dropColumn('unit_api_update');
            $table->dropColumn('unit_api_destroy');
            $table->dropColumn('unit_api_show');
            $table->dropColumn('customer_api_store');
            $table->dropColumn('customer_api_update');
            $table->dropColumn('customer_api_destroy');
            $table->dropColumn('customer_api_show');
            $table->dropColumn('reservation_api_store');
            $table->dropColumn('reservation_api_update');
            $table->dropColumn('reservation_api_destroy');
            $table->dropColumn('reservation_api_show');
        });
    }
}
