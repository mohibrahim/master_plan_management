<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestfulUrlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restful_urls', function (Blueprint $table) {
            //weak entity
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->text('api_authorization')->nullable();
            $table->text('project_api_index')->nullable();
            $table->text('phase_api_index')->nullable();
            $table->text('zone_api_index')->nullable();
            $table->text('block_api_index')->nullable();
            $table->text('unit_model_api_index')->nullable();
            $table->text('unit_api_index')->nullable();
            $table->text('customer_api_index')->nullable();
            $table->text('reservation_api_index')->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restful_urls');
    }
}
