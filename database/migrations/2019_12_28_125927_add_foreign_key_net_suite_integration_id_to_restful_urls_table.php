<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyNetSuiteIntegrationIdToRestfulUrlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('restful_urls', function (Blueprint $table) {
            $table->bigInteger('net_suite_integration_id')->unsigned();
            $table->foreign('net_suite_integration_id')->references('id')->on('net_suite_integrations')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('restful_urls', function (Blueprint $table) {
            $table->dropForeign('restful_urls_net_suite_integration_id_foreign');
            $table->dropColumn('net_suite_integration_id');
        });
    }
}
