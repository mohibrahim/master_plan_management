<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->string('customer_code')->nullable();
            $table->string('vat_registration_number')->nullable();
            $table->date('vat_registration_expiry')->nullable();
            $table->string('secondary_phone')->nullable();
            $table->string('alt_phone')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->dropColumn('customer_code');
            $table->dropColumn('vat_registration_number');
            $table->dropColumn('vat_registration_expiry');
            $table->dropColumn('secondary_phone');
            $table->dropColumn('alt_phone');
        });
    }
}
