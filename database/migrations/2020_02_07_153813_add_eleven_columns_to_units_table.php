<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddElevenColumnsToUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('units', function (Blueprint $table) {
            $table->string('land_area')->nullable();
            $table->smallInteger('number_of_floors')->unsigned()->nullable();
            $table->smallInteger('number_of_balconies')->unsigned()->nullable();
            $table->smallInteger('number_of_bedrooms')->unsigned()->nullable();
            $table->smallInteger('number_of_bathrooms')->unsigned()->nullable();
            $table->smallInteger('number_of_kitchens')->unsigned()->nullable();
            $table->smallInteger('number_of_elevators')->unsigned()->nullable();
            $table->boolean('has_garden')->nullable();
            $table->integer('garden_area')->unsigned()->nullable();
            $table->boolean('has_pool')->nullable();
            $table->integer('pool_area')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('units', function (Blueprint $table) {
            $table->dropColumn('land_area');
            $table->dropColumn('number_of_floors');
            $table->dropColumn('number_of_balconies');
            $table->dropColumn('number_of_bedrooms');
            $table->dropColumn('number_of_bathrooms');
            $table->dropColumn('number_of_kitchens');
            $table->dropColumn('number_of_elevators');
            $table->dropColumn('has_garden');
            $table->dropColumn('garden_area');
            $table->dropColumn('has_pool');
            $table->dropColumn('pool_area');
        });
    }
}
