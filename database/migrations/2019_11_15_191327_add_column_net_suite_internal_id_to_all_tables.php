<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnNetSuiteInternalIdToAllTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('blocks', function (Blueprint $table) {
            $table->string('net_suite_internal_id')->nullable();
        });
        Schema::table('catalogs', function (Blueprint $table) {
            $table->string('net_suite_internal_id')->nullable();
        });
        Schema::table('customers', function (Blueprint $table) {
            $table->string('net_suite_internal_id')->nullable();
        });
        Schema::table('permissions', function (Blueprint $table) {
            $table->string('net_suite_internal_id')->nullable();
        });
        Schema::table('phases', function (Blueprint $table) {
            $table->string('net_suite_internal_id')->nullable();
        });
        Schema::table('project_files', function (Blueprint $table) {
            $table->string('net_suite_internal_id')->nullable();
        });
        Schema::table('project_user_assignments', function (Blueprint $table) {
            $table->string('net_suite_internal_id')->nullable();
        });
        Schema::table('projects', function (Blueprint $table) {
            $table->string('net_suite_internal_id')->nullable();
        });
        Schema::table('reservations', function (Blueprint $table) {
            $table->string('net_suite_internal_id')->nullable();
        });
        Schema::table('roles', function (Blueprint $table) {
            $table->string('net_suite_internal_id')->nullable();
        });
        Schema::table('unit_models', function (Blueprint $table) {
            $table->string('net_suite_internal_id')->nullable();
        });
        Schema::table('units', function (Blueprint $table) {
            $table->string('net_suite_internal_id')->nullable();
        });
        Schema::table('users', function (Blueprint $table) {
            $table->string('net_suite_internal_id')->nullable();
        });
        Schema::table('zones', function (Blueprint $table) {
            $table->string('net_suite_internal_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('blocks', function (Blueprint $table) {
            $table->dropColumn('net_suite_internal_id');
        });
        Schema::table('catalogs', function (Blueprint $table) {
            $table->dropColumn('net_suite_internal_id');
        });
        Schema::table('customers', function (Blueprint $table) {
            $table->dropColumn('net_suite_internal_id');
        });
        Schema::table('permissions', function (Blueprint $table) {
            $table->dropColumn('net_suite_internal_id');
        });
        Schema::table('phases', function (Blueprint $table) {
            $table->dropColumn('net_suite_internal_id');
        });
        Schema::table('project_files', function (Blueprint $table) {
            $table->dropColumn('net_suite_internal_id');
        });
        Schema::table('project_user_assignments', function (Blueprint $table) {
            $table->dropColumn('net_suite_internal_id');
        });
        Schema::table('projects', function (Blueprint $table) {
            $table->dropColumn('net_suite_internal_id');
        });
        Schema::table('reservations', function (Blueprint $table) {
            $table->dropColumn('net_suite_internal_id');
        });
        Schema::table('roles', function (Blueprint $table) {
            $table->dropColumn('net_suite_internal_id');
        });
        Schema::table('unit_models', function (Blueprint $table) {
            $table->dropColumn('net_suite_internal_id');
        });
        Schema::table('units', function (Blueprint $table) {
            $table->dropColumn('net_suite_internal_id');
        });
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('net_suite_internal_id');
        });
        Schema::table('zones', function (Blueprint $table) {
            $table->dropColumn('net_suite_internal_id');
        });
    }
}
