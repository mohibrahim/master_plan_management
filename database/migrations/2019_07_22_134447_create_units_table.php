<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('units', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('code')->nullable();
            $table->string('status')->nullable();
            $table->string('land_number')->nullable();
            $table->string('construction_status')->nullable();
            $table->decimal('booking_amount', 8, 2)->nullable();
            $table->decimal('minimum_booking_amount', 8, 2)->nullable();
            $table->string('currency')->nullable();
            $table->smallInteger('percentage_of_completion')->nullable()->unsigned();
            $table->text('developer_name')->nullable();
            $table->boolean('is_for_sale')->nullable();
            $table->text('sale_details')->nullable();
            $table->decimal('sale_price', 18,2)->nullable()->unsigned();
            $table->dateTime('date_of_sale')->nullable();
            $table->boolean('is_for_rent')->nullable();
            $table->datetime('rent_from')->nullable();
            $table->datetime('rent_to')->nullable();
            $table->string('rent_price')->nullable();
            $table->text('rent_details')->nullable();
            $table->string('unit_account_number')->nullable();
            $table->string('direction')->nullable();
            $table->string('floor_number')->nullable();
            $table->text('address')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->string('electricity_meter_number')->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();

            $table->bigInteger('creator_id')->unsigned()->nullable();
            $table->bigInteger('last_updater_id')->unsigned()->nullable();

            $table->foreign('creator_id')->references('id')->on('users')->onDelete('SET NULL');
            $table->foreign('last_updater_id')->references('id')->on('users')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('units');
    }
}
