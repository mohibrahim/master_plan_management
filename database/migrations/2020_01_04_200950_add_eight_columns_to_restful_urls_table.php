<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEightColumnsToRestfulUrlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('restful_urls', function (Blueprint $table) {
            $table->dateTime('project_sync_date_time')->nullable();
            $table->dateTime('phase_sync_date_time')->nullable();
            $table->dateTime('zone_sync_date_time')->nullable();
            $table->dateTime('block_sync_date_time')->nullable();
            $table->dateTime('unit_model_sync_date_time')->nullable();
            $table->dateTime('unit_sync_date_time')->nullable();
            $table->dateTime('customer_sync_date_time')->nullable();
            $table->dateTime('reservation_sync_date_time')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('restful_urls', function (Blueprint $table) {
            $table->dropColumn('project_sync_date_time');
            $table->dropColumn('phase_sync_date_time');
            $table->dropColumn('zone_sync_date_time');
            $table->dropColumn('block_sync_date_time');
            $table->dropColumn('unit_model_sync_date_time');
            $table->dropColumn('unit_sync_date_time');
            $table->dropColumn('customer_sync_date_time');
            $table->dropColumn('reservation_sync_date_time');
        });
    }
}
