<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_files', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('title')->nullable();
            $table->string('status')->nullable();
            $table->string('type');
            $table->text('notes')->nullable();
            $table->timestamps();

            $table->bigInteger('creator_id')->unsigned()->nullable();
            $table->bigInteger('last_updater_id')->unsigned()->nullable();

            $table->foreign('creator_id')->references('id')->on('users')->onDelete('SET NULL');
            $table->foreign('last_updater_id')->references('id')->on('users')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_files');
    }
}
