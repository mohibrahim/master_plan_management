<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyCatalogIdAsCatalogCoverImageToProjectFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_files', function (Blueprint $table) {
            $table->bigInteger('catalog_cover_image_id')->unsigned()->nullable();
            $table->foreign('catalog_cover_image_id')->references('id')->on('catalogs')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_files', function (Blueprint $table) {
            $table->dropForeign('project_files_catalog_cover_image_id_foreign');
            $table->dropColumn('catalog_cover_image_id');
        });
    }
}
