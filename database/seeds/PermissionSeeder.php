<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->permissions();
        $this->users();
        $this->roles();
        $this->ProjectFiles();
        $this->catalogs();
        $this->projects();
        $this->phases();
        $this->zones();
        $this->blocks();
        $this->UnitModels();
        $this->Units();
        $this->customers();
        $this->reservations();
        $this->projectUserAssignments();
        $this->netSuiteIntegration();
        $this->banks();
    }

    private function permissions()
    {
        DB::table('permissions')->insert([
            'title' => 'permissions',
            'name' => 'store_permissions',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'permissions',
            'name' => 'show_permissions',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'permissions',
            'name' => 'show_permissions_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'permissions',
            'name' => 'index_permissions',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'permissions',
            'name' => 'index_permissions_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'permissions',
            'name' => 'update_permissions',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'permissions',
            'name' => 'update_permissions_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'permissions',
            'name' => 'destroy_permissions',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'permissions',
            'name' => 'destroy_permissions_multi_tenancy',
            'created_at' => now(),
        ]);
    }

    private function users()
    {
        DB::table('permissions')->insert([
            'title' => 'users',
            'name' => 'store_users',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'users',
            'name' => 'show_users',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'users',
            'name' => 'show_users_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'users',
            'name' => 'index_users',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'users',
            'name' => 'index_users_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'users',
            'name' => 'update_users',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'users',
            'name' => 'update_users_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'users',
            'name' => 'destroy_users',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'users',
            'name' => 'destroy_users_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'users',
            'name' => 'index_users_not_verified_email',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'users',
            'name' => 'verify_user_email',
            'created_at' => now(),
        ]);
    }

    private function roles()
    {
        DB::table('permissions')->insert([
            'title' => 'roles',
            'name' => 'store_roles',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'roles',
            'name' => 'show_roles',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'roles',
            'name' => 'show_roles_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'roles',
            'name' => 'index_roles',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'roles',
            'name' => 'index_roles_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'roles',
            'name' => 'update_roles',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'roles',
            'name' => 'update_roles_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'roles',
            'name' => 'destroy_roles',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'roles',
            'name' => 'destroy_roles_multi_tenancy',
            'created_at' => now(),
        ]);
    }

    private function projectFiles()
    {
        DB::table('permissions')->insert([
            'title' => 'project_files',
            'name' => 'store_project_files',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'project_files',
            'name' => 'show_project_files',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'project_files',
            'name' => 'show_project_files_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'project_files',
            'name' => 'index_project_files',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'project_files',
            'name' => 'index_project_files_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'project_files',
            'name' => 'update_project_files',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'project_files',
            'name' => 'update_project_files_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'project_files',
            'name' => 'destroy_project_files',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'project_files',
            'name' => 'destroy_project_files_multi_tenancy',
            'created_at' => now(),
        ]);
    }

    public function catalogs()
    {
        DB::table('permissions')->insert([
            'title' => 'catalogs',
            'name' => 'store_catalogs',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'catalogs',
            'name' => 'show_catalogs',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'catalogs',
            'name' => 'show_catalogs_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'catalogs',
            'name' => 'index_catalogs',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'catalogs',
            'name' => 'index_catalogs_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'catalogs',
            'name' => 'update_catalogs',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'catalogs',
            'name' => 'update_catalogs_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'catalogs',
            'name' => 'destroy_catalogs',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'catalogs',
            'name' => 'destroy_catalogs_multi_tenancy',
            'created_at' => now(),
        ]);
    }

    public function projects()
    {
        DB::table('permissions')->insert([
            'title' => 'projects',
            'name' => 'store_projects',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'projects',
            'name' => 'show_projects',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'projects',
            'name' => 'show_projects_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'projects',
            'name' => 'index_projects',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'projects',
            'name' => 'index_projects_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'projects',
            'name' => 'update_projects',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'projects',
            'name' => 'update_projects_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'projects',
            'name' => 'destroy_projects',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'projects',
            'name' => 'destroy_projects_multi_tenancy',
            'created_at' => now(),
        ]);
    }

    public function phases()
    {
        DB::table('permissions')->insert([
            'title' => 'phases',
            'name' => 'store_phases',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'phases',
            'name' => 'show_phases',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'phases',
            'name' => 'show_phases_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'phases',
            'name' => 'index_phases',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'phases',
            'name' => 'index_phases_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'phases',
            'name' => 'update_phases',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'phases',
            'name' => 'update_phases_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'phases',
            'name' => 'destroy_phases',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'phases',
            'name' => 'destroy_phases_multi_tenancy',
            'created_at' => now(),
        ]);
    }

    public function zones()
    {
        DB::table('permissions')->insert([
            'title' => 'zones',
            'name' => 'store_zones',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'zones',
            'name' => 'show_zones',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'zones',
            'name' => 'show_zones_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'zones',
            'name' => 'index_zones',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'zones',
            'name' => 'index_zones_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'zones',
            'name' => 'update_zones',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'zones',
            'name' => 'update_zones_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'zones',
            'name' => 'destroy_zones',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'zones',
            'name' => 'destroy_zones_multi_tenancy',
            'created_at' => now(),
        ]);
    }
    public function blocks()
    {
        DB::table('permissions')->insert([
            'title' => 'blocks',
            'name' => 'store_blocks',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'blocks',
            'name' => 'show_blocks',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'blocks',
            'name' => 'show_blocks_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'blocks',
            'name' => 'index_blocks',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'blocks',
            'name' => 'index_blocks_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'blocks',
            'name' => 'update_blocks',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'blocks',
            'name' => 'update_blocks_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'blocks',
            'name' => 'destroy_blocks',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'blocks',
            'name' => 'destroy_blocks_multi_tenancy',
            'created_at' => now(),
        ]);
    }

    public function unitModels()
    {
        DB::table('permissions')->insert([
            'title' => 'unit_models',
            'name' => 'store_unit_models',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'unit_models',
            'name' => 'show_unit_models',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'unit_models',
            'name' => 'show_unit_models_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'unit_models',
            'name' => 'index_unit_models',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'unit_models',
            'name' => 'index_unit_models_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'unit_models',
            'name' => 'update_unit_models',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'unit_models',
            'name' => 'update_unit_models_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'unit_models',
            'name' => 'destroy_unit_models',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'unit_models',
            'name' => 'destroy_unit_models_multi_tenancy',
            'created_at' => now(),
        ]);
    }

    public function units()
    {
        DB::table('permissions')->insert([
            'title' => 'units',
            'name' => 'store_units',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'units',
            'name' => 'show_units',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'units',
            'name' => 'show_units_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'units',
            'name' => 'index_units',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'units',
            'name' => 'index_units_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'units',
            'name' => 'update_units',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'units',
            'name' => 'update_units_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'units',
            'name' => 'destroy_units',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'units',
            'name' => 'destroy_units_multi_tenancy',
            'created_at' => now(),
        ]);
    }

    public function customers()
    {
        DB::table('permissions')->insert([
            'title' => 'customers',
            'name' => 'store_customers',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'customers',
            'name' => 'show_customers',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'customers',
            'name' => 'show_customers_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'customers',
            'name' => 'index_customers',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'customers',
            'name' => 'index_customers_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'customers',
            'name' => 'update_customers',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'customers',
            'name' => 'update_customers_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'customers',
            'name' => 'destroy_customers',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'customers',
            'name' => 'destroy_customers_multi_tenancy',
            'created_at' => now(),
        ]);
    }

    public function reservations()
    {
        DB::table('permissions')->insert([
            'title' => 'reservations',
            'name' => 'store_reservations',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'reservations',
            'name' => 'show_reservations',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'reservations',
            'name' => 'show_reservations_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'reservations',
            'name' => 'index_reservations',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'reservations',
            'name' => 'index_reservations_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'reservations',
            'name' => 'update_reservations',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'reservations',
            'name' => 'update_reservations_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'reservations',
            'name' => 'destroy_reservations',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'reservations',
            'name' => 'destroy_reservations_multi_tenancy',
            'created_at' => now(),
        ]);
    }

    public function projectUserAssignments()
    {
        DB::table('permissions')->insert([
            'title' => 'project_user_assignments',
            'name' => 'store_project_user_assignments',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'project_user_assignments',
            'name' => 'show_project_user_assignments',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'project_user_assignments',
            'name' => 'show_project_user_assignments_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'project_user_assignments',
            'name' => 'index_project_user_assignments',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'project_user_assignments',
            'name' => 'index_project_user_assignments_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'project_user_assignments',
            'name' => 'update_project_user_assignments',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'project_user_assignments',
            'name' => 'update_project_user_assignments_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'project_user_assignments',
            'name' => 'destroy_project_user_assignments',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'project_user_assignments',
            'name' => 'destroy_project_user_assignments_multi_tenancy',
            'created_at' => now(),
        ]);
    }

    public function netSuiteIntegration()
    {
        DB::table('permissions')->insert([
            'title' => 'net_suite_integrations',
            'name' => 'store_net_suite_integrations',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'net_suite_integrations',
            'name' => 'show_net_suite_integrations',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'net_suite_integrations',
            'name' => 'show_net_suite_integrations_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'net_suite_integrations',
            'name' => 'index_net_suite_integrations',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'net_suite_integrations',
            'name' => 'index_net_suite_integrations_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'net_suite_integrations',
            'name' => 'update_net_suite_integrations',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'net_suite_integrations',
            'name' => 'update_net_suite_integrations_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'net_suite_integrations',
            'name' => 'destroy_net_suite_integrations',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'net_suite_integrations',
            'name' => 'destroy_net_suite_integrations_multi_tenancy',
            'created_at' => now(),
        ]);
    }

    public function banks()
    {
        DB::table('permissions')->insert([
            'title' => 'banks',
            'name' => 'store_banks',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'banks',
            'name' => 'show_banks',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'banks',
            'name' => 'show_banks_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'banks',
            'name' => 'index_banks',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'banks',
            'name' => 'index_banks_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'banks',
            'name' => 'update_banks',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'banks',
            'name' => 'update_banks_multi_tenancy',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'banks',
            'name' => 'destroy_banks',
            'created_at' => now(),
        ]);
        DB::table('permissions')->insert([
            'title' => 'banks',
            'name' => 'destroy_banks_multi_tenancy',
            'created_at' => now(),
        ]);
    }
}
