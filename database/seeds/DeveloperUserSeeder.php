<?php

use App\Role;
use App\User;
use App\Permission;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DeveloperUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $moh = new User();
        $moh->name = "Mohammed Ibrahim Fawzy";
        $moh->email = "mohibrahimqop@gmail.com";
        $moh->password = bcrypt("password");
        $moh->email_verified_at = now();
        $moh->creator_id = 1;
        $moh->save();

        $admin = new User();
        $admin->name = "Admin";
        $admin->email = "admin@azdan.net";
        $admin->password = bcrypt("admin@123");
        $admin->email_verified_at = now();
        $admin->creator_id = 1;
        $admin->save();

        $salesman = new User();
        $salesman->name = 'salesman1';
        $salesman->email = "sales1@azdan.net";
        $salesman->password = bcrypt("111111");
        $salesman->email_verified_at = now();
        $salesman->creator_id = 1;
        $salesman->save();



        $roleDeveloper = new Role();
        $roleDeveloper->name = "Developer";
        $roleDeveloper->save();

        $roleAdmin = new Role();
        $roleAdmin->name = "Super Admin";
        $roleAdmin->save();

        $roleSalesman = new Role();
        $roleSalesman->name = "Sales Rep";
        $roleSalesman->save();



        $moh->update(['master_role_id'=>$roleDeveloper->id]);
        $admin->update(['master_role_id'=>$roleAdmin->id]);
        $salesman->update(['master_role_id'=>$roleSalesman->id]);



        $moh->roles()->attach($roleDeveloper);
        $admin->roles()->attach($roleAdmin);
        $salesman->roles()->attach($roleSalesman);


        $permissions = Permission::all();

        $roleDeveloper->permissions()->attach($permissions->pluck('id'));
        $roleAdmin->permissions()->attach($permissions->pluck('id'));
        $roleSalesman->permissions()->attach($permissions->pluck('id'));



        DB::table('permissions')->update(['creator_id'=>1]);
        DB::table('roles')->update(['creator_id'=>1]);
    }
}
