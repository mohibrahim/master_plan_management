<?php

use Illuminate\Database\Seeder;

class ProjectUserAssignmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new PermissionSeeder())->ProjectUserAssignments();
    }
}
