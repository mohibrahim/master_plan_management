<?php

use Illuminate\Database\Seeder;

class NetSuiteIntegrationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new PermissionSeeder())->netSuiteIntegration();
    }
}
