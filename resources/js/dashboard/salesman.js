salesmanDashboard = function () {

    //Cache DOM
    let $mapImage = null;
    let showProjectTrigger = $(".show-project");
    let projectWrapper = $("#project-wrapper");
    let projectSpinner = null;


    //Bind Events
    showProjectTrigger.on('click', showProject);

    function showProject(event) {
        event.preventDefault();
        projectSpinner = $("#" + $(this).data('project-spinner-id'));

        //Cache DOM
        let project = $(this).data('project');

        //Bind Events
        $("#main-upper-navbar-up-down-btn").trigger('click', ['slideUp']);//toggle main navbar;


        //Clear project Wrapper
        projectWrapper.fadeOut('slow', function () {
            $(this).empty();
        });

        //Getting project data by ajax
        $.ajax({
            url: "/salesman_dashboard/get_selected_project",
            method: "GET",
            contentType: "application/json",
            dataType: "json",
            data: { "project_id": project.id },
            success: whenSuccess,
            beforeSend: function () {
                projectSpinner.toggleClass('d-none').toggleClass('d-flex');
            }

        });


        function whenSuccess(response) {
            projectSpinner.toggleClass('d-none').toggleClass('d-flex');
            projectWrapper.fadeIn('fast', function () {
                $(this).append(response.html).fadeIn('fast', function () {
                    //CACHE DOM
                    $mapImage = $('#map-image');
                    masterPlanImageAreaHighlightAndFilter(project);// js/models/projects/miscellaneous
                    masterPlanPreviewUnitAsModal(project);
                    filterCollapseToggle();
                    scrollTopProjectWrapper();
                    refreshMasterPlanWhenModalClosed(project);
                    centerScroll();
                });

            });
        }

        function centerScroll() {
            //Cache DOM
            let overFlowWrapper = $('#over-flow-wrapper');
            let overFlowWrapperWidth = overFlowWrapper.width();
            let mapImageWidth = $mapImage.width();
            overFlowWrapper.animate({
                scrollLeft: (mapImageWidth-overFlowWrapperWidth)/2
            }, 1000);

            // scrollLeft((mapImageWidth-overFlowWrapperWidth)/2);
        }

        function scrollTopProjectWrapper() {
            let projectWrapperOffsetTop = projectWrapper.offset().top;
            $('html, body').animate({
                scrollTop: projectWrapperOffsetTop
            }, 1000);
        }


        function filterCollapseToggle() {
            //CACHE DOME
            let filterCollapseButton = $("#filter-collapse-button");
            let columnFilterCollapse = $("#colum-filer-collapse");
            let columnMasterPlanImageHolder = $("#colum-master-plan-image-holder");
            let chevronIconElement = $("#chevron-icon-element");

            //BIND EVENTS
            filterCollapseButton.on('click', collapseFilterToggle);


            function collapseFilterToggle() {
                if (!columnMasterPlanImageHolder.hasClass('col')) {
                    columnFilterCollapse.fadeToggle(300, function () {
                        columnMasterPlanImageHolder.toggleClass("col-lg-10").toggleClass("col");
                        columnMasterPlanImageHolder.toggleClass("col-xl-10");
                    });

                }
                if (columnMasterPlanImageHolder.hasClass('col')) {
                    columnMasterPlanImageHolder.toggleClass("col-lg-10").toggleClass("col");
                    columnMasterPlanImageHolder.toggleClass("col-xl-10");
                    columnFilterCollapse.fadeToggle(300);
                }

                chevronIconElement = $("#chevron-icon-element");//re-cache chevron-icon-element
                chevronIconElement.toggleClass("fa-angle-double-right").toggleClass("fa-angle-double-left");
            }
        }


        function refreshMasterPlanWhenModalClosed(project) {
            //Cache DOM
            let unitModal = $("#unit-modal");
            let filteringButton = $("#filtering-btn");
            // let projectId = filteringButton.data('filtered-project-id');
            // let mapImage = $("#map-image");
            // let mapElement = $("#map-element");
            //Bind Events
            unitModal.on('hidden.bs.modal', () => {
                // masterPlanImageAreaHighlightAndFilter(project);
                filteringButton.trigger('click');

                // $.ajax({
                //     url: '/salesman_dashboard/get_units_areas_as_html_tags',
                //     method: 'GET',
                //     contentType: 'application/json',
                //     data: { "project_id": projectId },
                //     dataType: 'text',
                //     success: function (results) {
                //         mapElement.empty().append(results);
                //         mapImage.maphilight();
                //         masterPlanPreviewUnitAsModal(project);
                //     },
                // });


            });
        }


        masterPlanPreviewUnitAsModal = function (project) {
            let viewUnitAsModal = $("#view-unit-as-modal");

            if (viewUnitAsModal.val() == "true") {
                let areaElement = $(".my-selected-area");

                areaElement.on('click', function (event) {
                    event.preventDefault();
                    let unitData = $(this).data('unit-details');
                    getUnitRelatedCatalogsLinks(unitData.id);
                    feedModalWithData(unitData, project);
                });
            }



            function getUnitRelatedCatalogsLinks(unitId) {
                //Cache Dom
                let modal_unit_project_catalogs_show_links = $("#modal-unit-project-catalogs-links");
                let catalogMessages = $("#catalog-messages").data('catalog-messages');

                //Bind Events
                // EMPTY


                $.ajax({
                    url: "/salesman_dashboard/get_unit_related_catalogs_links",
                    method: "GET",
                    contentType: "application/json",
                    dataType: "json",
                    data: { "unit_id": unitId },
                    success: whenSuccess,
                    beforeSend: function () {
                        modal_unit_project_catalogs_show_links.html(catalogMessages.loading_translation);

                    },
                    error: function() {
                        modal_unit_project_catalogs_show_links.html(catalogMessages.fail_to_load_translation);
                    }

                });

                function whenSuccess(results) {

                    let htmlTags = results.map((catalog, key) => {
                        let structure = '';
                        structure += `
                        <div class="container">
                            <h4>${catalog.catalog_name}</h3>
                            <div class="row">
                                <div class="col-6 text-center">
                                    <h5> ${catalogMessages.image_translation} </h5>
                                    <ul>`;
                        catalog.catalog_files.forEach((file) => {
                            if (file.type == 'image') {
                                structure +=    `<li class="text-truncate">
                                                    <a href="${catalogMessages.preview_file+'?file_name='+file.name}" target="_blank">
                                                        ${file.title}
                                                    </a>
                                                </li>`;
                            }
                        });
                        structure += `</ul>
                                </div>

                                <div class="col-6 text-center">
                                    <h5> ${catalogMessages.pdf_translation} </h5>
                                    <ul>`;
                        catalog.catalog_files.forEach((file) => {
                            if (file.type == 'pdf') {
                                structure +=    `<li class="text-truncate">
                                                    <a href="${catalogMessages.preview_file+'?file_name='+file.name}" target="_blank">
                                                        ${file.title}
                                                    </a>
                                                </li>`;
                            }
                        });
                        structure += `</ul>
                                </div>
                            </div>
                        </div>`;
                        return structure;
                    });


                    modal_unit_project_catalogs_show_links.html(htmlTags);
                    // modal_unit_project_catalogs_show_links.html(results.links);
                }

            }


            function feedModalWithData(unitData, project) {
                let modal_title = $("#modal-title");
                let modal_unit_code = $("#modal-unit-code");
                let modal_unit_status = $("#modal-unit-status");
                let modal_unit_land_number = $("#modal-unit-land-number");
                let modal_unit_land_area = $("#modal-unit-land-area");
                let modal_unit_percentage_of_completion = $("#modal-unit-percentage-of-completion");
                let modal_unit_developer_name = $("#modal-unit-developer-txt");
                let modal_unit_sale_price = $("#modal-unit-sale-price");
                let modal_unit_notes = $("#modal-unit-notes");
                let show_pdf_link_as_button = $("#modal-unit-show-pdf-link-as-button");
                let modal_unit_project_catalogs_show_links = $("#modal-unit-project-catalogs-links");

                modal_title.text(unitData.code + " | " + project.name);
                modal_unit_code.html(unitData.unit_show_link);
                modal_unit_status.html(unitData.suitable_status + " " + unitData.reservation_details + " " + unitData.reserve_the_unit_link);
                modal_unit_land_number.text(unitData.land_number);
                modal_unit_land_area.text(unitData.land_area);
                modal_unit_percentage_of_completion.text(unitData.percentage_of_completion);
                modal_unit_developer_name.text(unitData.developer_name);
                modal_unit_sale_price.text(unitData.sale_price);
                modal_unit_notes.text(unitData.notes);
                show_pdf_link_as_button.html(unitData.show_pdf_link_as_button);
                modal_unit_project_catalogs_show_links.html(unitData.related_catalogs_links);
            }
        }


    }

}








