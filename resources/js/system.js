isEmptyOrSpaces = function(str){
    return str === null || str.match(/^ *$/) !== null;
}


optional = function (arg1, arg2)
{
    if (arg1 !== null && !(typeof arg1 === 'undefined')) {
        return arg1[arg2];
    } else {
        return "";
    }
}

onlyOneClick = function () {
    $(".only-one-click").on ('click', function(){
        $(this).attr('disabled', 'disabled');
    });
}

// selecting project and his phases Ajax-ly and use in _form 
selectProjectAndPhase = function (modelName, showSelectPhase, showSelectZone, showSelectBlock) {
    let phaseId = $('#phase-id');
    let zoneId = $('#zone-id');
    let blockId = $('#block-id');

    let phaseNamePlaceholder = $('#phase-name-placeholder');
    let zoneNamePlaceholder = $('#zone-name-placeholder');
    let blockNamePlaceholder = $('#block-name-placeholder');

    let oldSelectedProjectName = $('#old-selected-project-name');
    let oldSelectedPhaseName = $('#old-selected-phase-name');
    let oldSelectedZoneName = $('#old-selected-zone-name');
    let oldSelectedBlockName = $('#old-selected-block-name');

    $('#project-id').select2({
        minimumInputLength: 1,
        placeholder: $('#project-name-placeholder').val(),
        allowClear: true,
        ajax: {
            delay: 500,
            url: '/'+modelName+'_miscellaneous/get_project_names_ids',
            dataType: 'json'
        },
    });

    $('#project-id').on('select2:select', function (event) {
        oldSelectedProjectName.val(event.params.data.text);

        if (showSelectPhase) {
            phaseId.empty();
            phaseId.append(`<option value="">${phaseNamePlaceholder.val()}</option>`);
            $.each(event.params.data.phases, function (id, name) {
                phaseId.append(`<option value="${id}">${name}</option>`);
            });
        }
        if (showSelectZone) {
            zoneId.empty();
            zoneId.append(`<option value="">${zoneNamePlaceholder.val()}</option>`);
            $.each(event.params.data.zones, function (id, name) {
                zoneId.append(`<option value="${id}">${name}</option>`);
            });
        }
        if (showSelectBlock) {
            blockId.empty();
            blockId.append(`<option value="">${blockNamePlaceholder.val()}</option>`);
            $.each(event.params.data.blocks, function (id, name) {
                blockId.append(`<option value="${id}">${name}</option>`);
            });
        }
    });


    if (showSelectPhase) {
        phaseId.on('change', function () {
            oldSelectedPhaseName.val(phaseId.children('option:selected').text());
        });
    }
    if (showSelectZone) {
        zoneId.on('change', function () {
            oldSelectedZoneName.val(zoneId.children('option:selected').text());
        });
    }
    if (showSelectBlock) {
        blockId.on('change', function () {
            oldSelectedBlockName.val(blockId.children('option:selected').text());
        });
    }
}

upAndDownMainUpperNavbar = function() {
    //Cache DOM
    let mainUpperNavbar = $("#main-upper-navbar");
    let mainUpperNavbarUpDownButton = $("#main-upper-navbar-up-down-btn");
    let chevronWrapper = $("#chevron-wrapper");
    //Bind Events
    mainUpperNavbarUpDownButton.on('click', upDownToggle);

    function upDownToggle(event, p1)
    {
        //ReCache DOM
        chevronWrapper = $("#chevron-wrapper");
        if (p1 == 'slideUp') {
            chevronWrapper.removeClass('fa-chevron-up').addClass('fa-chevron-down');
            mainUpperNavbar.slideUp();
        } else {
            if (chevronWrapper.hasClass('fa-chevron-up')) {
                chevronWrapper.removeClass('fa-chevron-up').addClass('fa-chevron-down');
            } else {
                chevronWrapper.removeClass('fa-chevron-down').addClass('fa-chevron-up');
            }
            mainUpperNavbar.slideToggle();
        }
    }
}
