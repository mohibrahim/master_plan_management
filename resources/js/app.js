/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

// window.Vue = require('vue');

// /**
//  * The following block of code may be used to automatically register your
//  * Vue components. It will recursively scan this directory for the Vue
//  * components and automatically register them with their "basename".
//  *
//  * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
//  */

// // const files = require.context('./', true, /\.vue$/i);
// // files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);

// /**
//  * Next, we will create a fresh Vue application instance and attach it to
//  * the page. Then, you may begin adding components to this application
//  * or customize the JavaScript scaffolding to fit your unique needs.
//  */

// const app = new Vue({
//     el: '#app',
// });


require('./system');
require('./models/users/index');
require('./models/users/_form');

require('./models/project_files/index');

require('./models/catalogs/index');
require('./models/catalogs/_form');

require('./models/projects/index');
require('./models/projects/_form');
require('./models/projects/show');
require('./models/projects/miscellaneous');

require('./models/phases/index');
require('./models/phases/_form');

require('./models/zones/index');
require('./models/zones/_form');

require('./models/blocks/index');
require('./models/blocks/_form');

require('./models/unit_models/index');
require('./models/unit_models/_form');

require('./models/units/index');
require('./models/units/_form');
require('./models/units/show/pdf/zone_to_project');
require('./models/units/show/pdf/zone');

require('./models/customers/index');
require('./models/customers/_form');

require('./models/reservations/index');
require('./models/reservations/_form');

require('./models/project_user_assignments/index');
require('./models/project_user_assignments/_form');

require('./models/net_suite_integrations/index');

require('./dashboard/salesman');
