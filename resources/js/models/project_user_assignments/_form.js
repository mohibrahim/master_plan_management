projectUserAssignmentSelectProjectId = function () {

    let projectFormGroup = $('#project-form-group');

    $('#project-id').select2({
        minimumInputLength: 1,
        placeholder: $('#project-name-placeholder').val(),
        allowClear: true,
        ajax: {
            delay: 500,
            url: '/project_user_assignments_miscellaneous/get_project_names_ids',
            dataType: 'json'
        },
    });

    $('#project-id').on('select2:select', function (event) {
        
        let selectedOptions = $("#project-id option:selected");
        $(".old-selected-projects-names").remove();
        $.each(selectedOptions, function(key, value){
            let hiddenOldSelectedProjectName = `<input type="hidden" name="old_selected_projects_names[]" class="old-selected-projects-names" value="${$(value).text()}">`;
            projectFormGroup.append(hiddenOldSelectedProjectName);
        });
        
    });
}

projectUserAssignmentSelectEmployeeId = function () {

    let oldSelectedEmployeeName = $('#old-selected-employee-name');

    $('#employee-id').select2({
        minimumInputLength: 1,
        placeholder: $('#employee-name-placeholder').val(),
        ajax: {
            delay: 500,
            url: '/project_user_assignments_miscellaneous/get_employee_names_ids',
            dataType: 'json'
        },
    });

    $('#employee-id').on('select2:select', function (event) {
        oldSelectedEmployeeName.val(event.params.data.text);
    });
}