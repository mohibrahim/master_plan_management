searchingOnProjectFilesAjax = function () {
    //Cache Dom

    var searchingButton = $("#searching-button");
    var searchingInput = $("#searching-input");
    var csrfToken = $("[name='csrf-token']").attr('content');

    //Cache Events
    searchingButton.on('click', search);
    searchingInput.on('keydown', checkEnterKey);

    function search(e) {
        if (!isEmptyOrSpaces(searchingInput.val())) {
           
            $.ajax({
                type: 'POST',
                url: '/users_miscellaneous/index_search',
                dataType: 'JSON',
                data: {
                    '_token': csrfToken,
                    'keyword': searchingInput.val()
                },
                beforeSend: beforeSend,
                complete: complete,
                success: function (results) {
                    dataTable.clear();
                    $.each(results, function (key, user) {
                        dataTable.row.add([
                            key + 1,
                            user.id,
                            `<a href="/users/${user.id}">${user.name}</a>`,
                            user.email,
                            user.mobile_number,
                            optional(user.roles[0],'name'),
                            user.created_at,
                            optional(user.creator, 'name'),
                        ]);
                    });
                    dataTable.draw();
                },
                error: function(){
                    console.log('error');
                },
            });
        } else {
            searchingButton.text("برجاء إدخال الكلمة المراد البحث عنها...").addClass("btn-danger").fadeOut().fadeIn();
        }
    }

    function checkEnterKey(e) {
        if (e.keyCode == 13) {
            search();
        }
    }

    function beforeSend() {
        searchingButton.text("جــاري البحث...").fadeOut().fadeIn();
    }

    function complete() {
        searchingButton.text("ابحث").removeClass("btn-danger");
    }
    
}