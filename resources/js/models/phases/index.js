searchingOnPhasesAjax = function () {
    //Cache Dom

    var searchingButton = $("#searching-button");
    var searchingButtonRequired = $("#searching-button-required");
    var searchingButtonSearch = $("#searching-button-search");
    var searchingButtonSearching = $("#searching-button-searching");
    var searchingInput = $("#searching-input");
    var csrfToken = $("[name='csrf-token']").attr('content');

    //Cache Events
    searchingButton.on('click', search);
    searchingInput.on('keydown', checkEnterKey);

    function search(e) {
        if (!isEmptyOrSpaces(searchingInput.val())) {

            $.ajax({
                type: 'POST',
                url: '/phases_miscellaneous/index_search',
                dataType: 'JSON',
                data: {
                    '_token': csrfToken,
                    'keyword': searchingInput.val()
                },
                beforeSend: beforeSend,
                complete: complete,
                success: function (results) {
                    dataTable.clear();
                    $.each(results, function (key, phase) {
                        dataTable.row.add([
                            key + 1,
                            `<a href="/projects/${optional(phase,'project').id}">${optional(phase,'project').name}</a>`,
                            phase.id,
                            `<a href="/phases/${phase.id}">${phase.name}</a>`,
                            phase.percentage_of_completion,
                            phase.created_at,
                            optional(phase.creator, 'name'),
                        ]);
                    });
                    dataTable.draw();
                },
                error: function(){
                    console.log('error');
                },
            });
        } else {
            searchingButton.text(searchingButtonRequired.val()).addClass("btn-danger").fadeOut().fadeIn();
        }
    }

    function checkEnterKey(e) {
        if (e.keyCode == 13) {
            search();
        }
    }

    function beforeSend() {
        searchingButton.text(searchingButtonSearching.val()).fadeOut().fadeIn();
    }

    function complete() {
        searchingButton.text(searchingButtonSearch.val()).removeClass("btn-danger");
    }

}

selectSyncOnPhases = function () {
    let netSuiteId = $("#net-suite-id");
    let netSuiteIdSyncButton = $("#net-suite-id-sync-button");
    netSuiteId.on('change', handelSelectNetSuiteUrl)

    function handelSelectNetSuiteUrl(e) {
        netSuiteIdSyncButton.attr('href', '/net_suite_integrations_miscellaneous/sync_phases_data/' + e.target.value).toggleClass('disabled');
    }
}
