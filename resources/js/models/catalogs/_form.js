/*
|--------------------------------------------------------------------------
| Cover image
|--------------------------------------------------------------------------
|
*/
deleteCoverImage = function () {
    //Cache DOM
    let coverImage = $("#cover_image");
    let projectFileId = coverImage.attr('value');
    let deleteImageButton = $("#delete_image_button");
    let deleteImageButtonWrapper = $("#delete_image_button_wrapper");

    //Bind Events
    deleteImageButton.on('click', deleteTheImage);

    function deleteTheImage() {
        $(this).addClass("spinner-border");
        $.ajax({
            type: "GET",
            dataType: 'json',
            url: '/catalogs_miscellaneous/delete_file_ajax/' + projectFileId,
            success: function (results) {

                coverImage.attr('src', '/images/helper_images/no_image.png');
                deleteImageButtonWrapper.hide();
            },
        });
    }
}

/*
|--------------------------------------------------------------------------
| Catalog images
|--------------------------------------------------------------------------
|
*/
catalogAddInputFileImageRow = function () {
    //Variables
    let rowNumber = 1;
    //Cache Dom


    let inputFileImageRowAdderButton = $("#catalog-input-file-images-row-adder");
    let inputFilesImagesWrapper = $("#catalog-inputs-files-images-wrapper");
    let noImagePng = $('#no-image-png').val();

    //Bind Events
    inputFileImageRowAdderButton.on('click', addRow);

    function addRow() {
        let catalogImagesImageNumber = $("#catalog-images-image-number").val();
        let catalogImagesImageTitle = $("#catalog-images-image-title").val();
        let catalogImagesImageTitlePlaceholder = $("#catalog-images-image-title-placeholder").val();
        let catalogImagesImageStatus = $("#catalog-images-image-status").val();
        let catalogImagesImageStatusPlaceholder = $("#catalog-images-image-status-placeholder").val();

        let structure =
            `<div class="border rounded mb-2 p-2">
            <button type="button" class="close text-danger" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="row">
                <div class="col-md-12 ">
                    <div class="form-group">
                        <img src="${noImagePng}" id="mirror-image-${rowNumber}" class="rounded m-2 d-block float-right" width="100">
                        <label class="m-2" for="catalog-upload-image-${rowNumber}">${catalogImagesImageNumber}: ${rowNumber}</label>
                        <input name="catalog_images[]" type="file" id="catalog-upload-image-${rowNumber}" class="form-control m-2" placeholder="Example input">
                    </div>
                    <div class="form-group">
                        <label class="m-2" for="catalog-images-title-${rowNumber}">${catalogImagesImageTitle}</label>
                        <input type="text" name="catalog_images_titles[]"  id="catalog-images-title-${rowNumber}" class="form-control m-2" placeholder="${catalogImagesImageTitlePlaceholder}">
                    </div>
                    <div class="form-group">
                        <label class="m-2" for="catalog-image-status-${rowNumber}">${catalogImagesImageStatus}</label>
                        <input type="text" name="catalog_images_statuses[]"  id="catalog-image-status-${rowNumber}" class="form-control m-2" placeholder="${catalogImagesImageStatusPlaceholder}">
                    </div>
                </div>
            </div>
        </div>`;
        inputFilesImagesWrapper.append(structure);
        catalogUploadImageMirror(rowNumber);
        catalogRemoveInputFileImageRow();
        rowNumber++;
    }

    catalogUploadImageMirror = function (rowNumber) {
        //Cache DOM
        let mirrorImage = $("#mirror-image-" + rowNumber);
        let catalogUploadImage = $("#catalog-upload-image-" + rowNumber);
        let catalogImageTitle = $("#catalog-images-title-" + rowNumber);

        mirrorImageWhenUploadAndSetTitle(mirrorImage, catalogUploadImage, catalogImageTitle);

    }

    catalogRemoveInputFileImageRow = function () {
        //Cache DOM
        let closeButton = $(".close");
        //Bind Events
        closeButton.on('click', deleteRow);

        function deleteRow() {
            let parentElement = $(this).parent();
            parentElement.fadeOut(function () {
                $(this).remove();
                rowNumber--;
            });
        }
    }


}

mirrorImageWhenUploadAndSetTitle = function (imageTag, inputFile, inputTitle) {
    //Cache DOM
    let mirrorImage = imageTag;
    let catalogUploadImage = inputFile;
    let catalogImageTitle = inputTitle;

    //Bind Events
    catalogUploadImage.on('change', reflectImage);

    function reflectImage() {
        catalogUploadImage = this;

        if (catalogUploadImage.files && catalogUploadImage.files[0]) {
            let file = catalogUploadImage.files[0];
            if (file.type == 'image/jpeg' || file.type == 'image/png') {
                let fileName = file.name;
                let reader = new FileReader();
                reader.onload = function (e) {
                    mirrorImage.attr('src', e.target.result);
                }
                reader.readAsDataURL(catalogUploadImage.files[0]);


                catalogImageTitle.val((fileName.split('.')[0]));

            } else {
                $(catalogUploadImage).val('');
                alert('برجاء اختيار ملف آخر حيث أن الملف المختار ليس بصورة');
            }
        }

    }
}

/*
|--------------------------------------------------------------------------
| Catalog PDF files
|--------------------------------------------------------------------------
|
*/

catalogAddInputFilePdfRow = function () {
    //Variables
    let rowNumber = 1;
    //Cache Dom


    let inputFilePdfRowAdderButton = $("#catalog-input-file-pdf-row-adder");
    let inputFilesPdfWrapper = $("#catalog-inputs-files-pdfs-wrapper");

    //Bind Events
    inputFilePdfRowAdderButton.on('click', addRow);

    function addRow() {
        let catalogFilesPdfNu = $("#catalog-files-pdf-number").val();
        let catalogFilesPdfTitle = $("#catalog-files-pdf-title").val();
        let catalogFilesPdfTitlePlaceholder = $("#catalog-files-pdf-title-placeholder").val();
        let catalogFilesPdfStatus = $("#catalog-files-pdf-status").val();
        let catalogFilesPdfStatusPlaceholder = $("#catalog-files-pdf-status-placeholder").val();

        let structure =
            `<div class="border rounded mb-2 p-2">
            <button type="button" class="close text-danger" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="row">
                <div class="col-md-12 ">
                    <div class="form-group">
                        <label class="m-2" for="catalog-upload-pdf-${rowNumber}">${catalogFilesPdfNu}: ${rowNumber}</label>
                        <input name="catalog_pdfs[]" type="file" id="catalog-upload-pdf-${rowNumber}" class="form-control m-2" placeholder="Example input">
                    </div>
                    <div class="form-group">
                        <label class="m-2" for="catalog-pdfs-titles-${rowNumber}">${catalogFilesPdfTitle}</label>
                        <input name="catalog_pdfs_titles[]" type="text" id="catalog-pdfs-titles-${rowNumber}" class="form-control m-2" placeholder="${catalogFilesPdfTitlePlaceholder}">
                    </div>
                    <div class="form-group">
                        <label class="m-2" for="catalog-pdfs-status-${rowNumber}">${catalogFilesPdfStatus}</label>
                        <input type="text" name="catalog_pdfs_statuses[]"  id="catalog-pdfs-status-${rowNumber}" class="form-control m-2" placeholder="${catalogFilesPdfStatusPlaceholder}">
                    </div>
                </div>
            </div>
        </div>`;



        inputFilesPdfWrapper.append(structure);
        catalogRemoveInputFilePdfRow();
        validateAndSetFileTitleFile(rowNumber);
        rowNumber++;
    }



    catalogRemoveInputFilePdfRow = function () {
        //Cache DOM
        let closeButton = $(".close");
        //Bind Events
        closeButton.on('click', deleteRow);

        function deleteRow() {
            let parentElement = $(this).parent();
            parentElement.fadeOut(function () {
                $(this).remove();
                rowNumber--;
            });
        }
    }


    /**
     * Validate selected file
     */
    validateAndSetFileTitleFile = function (rowNumber) {
        //Cache DOM
        let cataloInputFilePdf = $("#catalog-upload-pdf-" + rowNumber);
        let cataloInputTitlePdf = $("#catalog-pdfs-titles-" + rowNumber);
        //Bind Events
        cataloInputFilePdf.on('change', function () {
            let files = this.files;
            if (files && files[0]) {
                let file = files[0];
                if (file.type != 'application/pdf') {
                    cataloInputFilePdf.val('');
                    alert('برجاء اختيار ملف آخر حيث أن الملف المختار ليس pdf');
                } else {
                    cataloInputTitlePdf.val(file.name.split('.')[0]);
                }

            }

        });
    }


}

/*
|--------------------------------------------------------------------------
| Delete Catalog images and his cover separately
|--------------------------------------------------------------------------
|
*/
deleteImage = function () {
    //Cache DOM
    let catalogFileLink = $(".delete-catalog-file-link");


    //Bind Events
    catalogFileLink.on('click', deleteTheImage);

    function deleteTheImage() {
        //ReCache Dom
        catalogFileWrapper = $(this).parents('.delete-catalog-file-wrapper');
        projectFileId = $(this).data("catalog-file-id");
        $(this).addClass("spinner-border");


        $.ajax({
            type: "GET",
            dataType: 'json',
            url: '/catalogs_miscellaneous/delete_file_ajax/' + projectFileId,
            success: function (results) {
                catalogFileWrapper.fadeOut(function () {
                    catalogFileWrapper.remove();
                });
            },
        });
    }
}

