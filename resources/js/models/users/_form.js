deletePersonalImage = function () {
    //Cache DOM
    var personalImage = $("#personal_image");
    var projectFileId = personalImage.attr('value');
    var deleteImageButton = $("#delete_image_button");
    var deleteImageButtonWrapper = $("#delete_image_button_wrapper");
    
    //Bind Events
    deleteImageButton.on('click', deleteTheImage);
    
    function deleteTheImage()
    {
        $.ajax({
            type: "GET",
            dataType: 'json',
            url: '/users_miscellaneous/delete_personal_image_ajax/'+projectFileId,
            success: function(results){
                console.log('success');
                personalImage.attr('src', '/images/helper_images/no_image.png');
                deleteImageButtonWrapper.hide();
            },
        });
    }
}