searchingOnUnitModelsAjax = function () {
    //Cache Dom

    var searchingButton = $("#searching-button");
    var searchingButtonRequired = $("#searching-button-required");
    var searchingButtonSearch = $("#searching-button-search");
    var searchingButtonSearching = $("#searching-button-searching");
    var searchingInput = $("#searching-input");
    var csrfToken = $("[name='csrf-token']").attr('content');

    //Cache Events
    searchingButton.on('click', search);
    searchingInput.on('keydown', checkEnterKey);

    function search(e) {
        if (!isEmptyOrSpaces(searchingInput.val())) {

            $.ajax({
                type: 'POST',
                url: '/unit_models_miscellaneous/index_search',
                dataType: 'JSON',
                data: {
                    '_token': csrfToken,
                    'keyword': searchingInput.val()
                },
                beforeSend: beforeSend,
                complete: complete,
                success: function (results) {
                    dataTable.clear();
                    $.each(results, function (key, unit_model) {
                        dataTable.row.add([
                            key + 1,
                            unit_model.id,
                            `<a href="/unit_models/${unit_model.id}">${unit_model.name}</a>`,
                            unit_model.type,
                            unit_model.created_at,
                            optional(unit_model.creator, 'name'),
                        ]);
                    });
                    dataTable.draw();
                },
                error: function(){
                    console.log('error');
                },
            });
        } else {
            searchingButton.text(searchingButtonRequired.val()).addClass("btn-danger").fadeOut().fadeIn();
        }
    }

    function checkEnterKey(e) {
        if (e.keyCode == 13) {
            search();
        }
    }

    function beforeSend() {
        searchingButton.text(searchingButtonSearching.val()).fadeOut().fadeIn();
    }

    function complete() {
        searchingButton.text(searchingButtonSearch.val()).removeClass("btn-danger");
    }

}

selectSyncOnUnitModels = function () {
    let netSuiteId = $("#net-suite-id");
    let netSuiteIdSyncButton = $("#net-suite-id-sync-button");
    netSuiteId.on('change', handelSelectNetSuiteUrl)

    function handelSelectNetSuiteUrl(e) {
        netSuiteIdSyncButton.attr('href', '/net_suite_integrations_miscellaneous/sync_unit_models_data/' + e.target.value).toggleClass('disabled');
    }
}
