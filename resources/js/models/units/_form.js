selectUnitModelForUnit = function() {
    let oldSelectedUnitModelName = $('#old-selected-unit-model-name');

    $('#unit-model-id').select2({
        minimumInputLength: 1,
        placeholder: $('#unit-model-name-placeholder').val(),
        ajax: {
            delay: 500,
            url: '/units_miscellaneous/get_unit_model_names_ids',
            dataType: 'json'
        },
        templateResult: formatRepo,
    });

    $('#unit-model-id').on('select2:select', function (event) {
        oldSelectedUnitModelName.val(event.params.data.text);
    });;

    function formatRepo(repo) {
        
        if (repo.loading) {
            return repo.text
        }
        let container = `
            <div>${repo.text}</div>
            <div>${repo.type}</div>
            <div>${repo.code}</div>
        `;

        return $.parseHTML(container);
    }
}