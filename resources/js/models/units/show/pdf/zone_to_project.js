unitShowPdfZoneToProject = function (p5) {
    let $dataHolder = $("#data-holder");
    let unitDataJson = $dataHolder.data("unit-data-as-json");
    let zoneCords = $.map(unitDataJson.unitZoneCoords.split(','), function (value) { return parseInt(value) });
    let $canvasWrapper = $("#zone-to-project-canvas-wrapper");

    let masterPlanImageUrl = unitDataJson.unitMasterPlanImageUrl;
    let masterPlanImageWidth = unitDataJson.unitMasterPlanImageWidth;
    let masterPlanImageHeight = unitDataJson.unitMasterPlanImageHeight;
    let zoneName = unitDataJson.zoneName;

    let masterPlanImage;
    let scalingRatio = 0;
    let newMasterPlanImageWidth = 0;
    let newMasterPlanImageHeight = 0;

    p5.setup = function () {
        resizeMasterPlanImage();
        canvas = p5.createCanvas(newMasterPlanImageWidth, newMasterPlanImageHeight);
        canvas.parent('zone-to-project-canvas-wrapper')
        masterPlanImage = p5.loadImage(masterPlanImageUrl);
        p5.frameRate(1);
    }

    function resizeMasterPlanImage() {
        let canvasWrapperWidth = $canvasWrapper.innerWidth() - 20;
        if (masterPlanImageWidth != 0) {
            //setting Scaling ratio
            scalingRatio = ((canvasWrapperWidth * 100) / masterPlanImageWidth);
            newMasterPlanImageWidth = canvasWrapperWidth;
            newMasterPlanImageHeight = (canvasWrapperWidth * masterPlanImageHeight) / masterPlanImageWidth;
        }
    }

    p5.draw = function () {
        p5.image(masterPlanImage, 0, 0, newMasterPlanImageWidth, newMasterPlanImageHeight);
        p5.fill(10, 10, 10, 150);
        p5.strokeWeight(2);
        p5.stroke("#C92A2A");

        p5.beginShape();
        for (let i = 0; i < zoneCords.length; i += 2) {
            p5.vertex(
                scaleUpOrDown(scalingRatio, zoneCords[i]),
                scaleUpOrDown(scalingRatio, zoneCords[i + 1])
            );
        }
        p5.beginContour();
        p5.vertex(-4, masterPlanImageHeight);
        p5.vertex(masterPlanImageWidth, masterPlanImageHeight);
        p5.vertex(masterPlanImageWidth, -4);
        p5.vertex(-4, -4);
        p5.endContour(p5.CLOSE);
        p5.endShape(p5.CLOSE);

        p5.noStroke();
        p5.textSize(20);
        p5.fill(255, 255, 255);
        p5.text(('Zone: ' + zoneName), 10, (newMasterPlanImageHeight - 20));
    }

    function scaleUpOrDown(ratio, scalableValue) {
        return parseInt(((ratio * scalableValue) / 100));
    }
}
