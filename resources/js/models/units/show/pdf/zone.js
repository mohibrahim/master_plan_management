unitShowPdfZone = function (p5) {
    let $canvasWrapper = $("#zone-canvas-wrapper");
    let canvasWrapperWidth = $canvasWrapper.innerWidth();

    let $dataHolder = $("#data-holder");
    let unitDataJson = $dataHolder.data("unit-data-as-json");
    let zoneName = unitDataJson.zoneName;

    let loadedMasterPlanImage;
    let masterPlanImageWidth = 0;
    let masterPlanImageHeight = 0;
    let masterPlanImageUrl = '';

    let newMasterPlanImageWidth = 0;
    let newMasterPlanImageHeight = 0;

    let zoneCoords = [];
    let zoneWidth = 0;
    let zoneHeight = 0;
    let newZoneWidth = 0;
    let newZoneHeight = 0;

    let unitCords = [];
    let loadedUnitMapPinImage;
    let unitMapPinUrl = '';

    const drawingZoneOffset = 0;

    let dX = 0;
    let dY = 0;
    let dWidth = 0;
    let dHeight = 0;
    let sX = 0;
    let sY = 0;
    let sWidth = 0;
    let sHeight = 0;

    let unitPinCoords = [0, 0];

    let leastX = 0;
    let leastY = 0;
    let greatestX = 0;
    let greatestY = 0;

    let newNewZoneWidthRatioToZoneWidth = 0;

    function initializing() {
        setMasterPlanImageUrl();
        setMasterPlanImageWidth();
        setMasterPlanImageHeight();

        setUnitCoords();
        setUnitMapPinUrl();

        setZoneCoords();
        setGreatestX();
        setGreatestY();
        setLeastX();
        setLeastY();
        setNewZoneWidth();
        setZoneWidth();
        setZoneHeight();
        setNewZoneHeight();

        setDrawingZoneImage();

        setNewMasterPlanImageWidth();
        setNewMasterPlanImageHeight();

        setNewZoneWidthRatioToZoneWidth();
    }

    //P5 drawing
    p5.setup = function () {
        initializing();
        canvas = p5.createCanvas(newZoneWidth, newZoneHeight);
        canvas.parent('zone-canvas-wrapper');
        loadedMasterPlanImage = p5.loadImage(masterPlanImageUrl);
        loadedUnitMapPinImage = p5.loadImage(unitMapPinUrl);
        p5.background(0);
        p5.frameRate(1);

        unitPinCoords = calculatePinPosition();
    }

    p5.draw = function () {
        p5.image(
            loadedMasterPlanImage,
            dX,
            dY,
            dWidth,
            dHeight,
            sX,
            sY,
            sWidth,
            sHeight
        );

        p5.fill(10, 10, 10, 150);
        p5.strokeWeight(2);
        p5.stroke("#C92A2A");
        p5.beginShape();
        for (let i = 0; i <= zoneCoords.length; i += 2) {
            if (i == 0) {
                continue;
            }
            p5.vertex(...getNewZoneXY(zoneCoords[i], zoneCoords[i + 1]));
        }
        p5.beginContour();
        p5.vertex(0, newZoneHeight);
        p5.vertex(newZoneWidth, newZoneHeight);
        p5.vertex(newZoneWidth, 0);
        p5.vertex(0, 0);
        p5.endContour(p5.CLOSE);
        p5.endShape(p5.CLOSE);

        p5.image(loadedUnitMapPinImage, unitPinCoords[0], unitPinCoords[1]);
        p5.noStroke();
        p5.textSize(20);
        p5.fill(255, 255, 255);
        p5.text(('Zone: ' + zoneName), 10, (newZoneHeight - 20));
    }

    function getGreatestX(arrayOfXY) {
        let greatX = 0;
        let y = 0;
        for (let i = 0; i < arrayOfXY.length; i += 2) {
            if (arrayOfXY[i] > greatX) {
                greatX = arrayOfXY[i];
                y = arrayOfXY[i + 1];
            }
        }
        return [greatX, y];
    }

    function getGreatestY(arrayOfXY) {
        let x = 0;
        let greatY = 0;
        for (let i = 1; i < arrayOfXY.length; i += 2) {
            if (arrayOfXY[i] > greatY) {
                x = arrayOfXY[i - 1];
                greatY = arrayOfXY[i];
            }
        }
        return [x, greatY];
    }

    function getLeastX(arrayOfXY) {
        let leastX = 1000000;
        let y = 0;
        for (let i = 0; i < arrayOfXY.length; i += 2) {
            if (arrayOfXY[i] < leastX) {
                leastX = arrayOfXY[i];
                y = arrayOfXY[i + 1];
            }
        }
        return [leastX, y];
    }

    function getLeastY(arrayOfXY) {
        let x = 0;
        let leastY = 1000000;
        for (let i = 1; i < arrayOfXY.length; i += 2) {
            if (arrayOfXY[i] < leastY) {
                x = arrayOfXY[i - 1];
                leastY = arrayOfXY[i];
            }
        }
        return [x, leastY];
    }

    function toArrayOfIntegers(stringNumbers, separator) {
        return $.map(stringNumbers.split(separator), function (value) { return parseInt(value) });
    }

    function calculatePinPosition() {

        let newX = 0;
        let newY = 0;

        //   masterPlanImageWidth              newMasterPlanImageWidth
        // ---------------------------    =     ------------------------
        //         oldUnitX                             newUnitX
        let oldZoneLeastX = leastX[0];
        let oldUnitX = unitCords[0];
        let newUnitX = (newMasterPlanImageWidth * oldUnitX) / masterPlanImageWidth;

        //     masterPlanImageWidth              newMasterPlanImageWidth
        // -------------------------------   =  -----------------------------
        //        oldZoneLeastX                        newZoneLeastX

        let newZoneLeastX = (newMasterPlanImageWidth * oldZoneLeastX) / masterPlanImageWidth;

        newX = (newUnitX - newZoneLeastX);

        //   masterPlanImageHeight              newMasterPlanImageHeight
        // ---------------------------    =     ------------------------
        //         oldUnitY                             newUnitY
        let oldZoneLeastY = leastY[1];
        let oldUnitY = unitCords[1];
        let newUnitY = (newMasterPlanImageHeight * oldUnitY) / masterPlanImageHeight;

        //     masterPlanImageHeight              newMasterPlanImageHeight
        // -------------------------------   =  -----------------------------
        //        oldZoneLeastY                        newZoneLeastY

        let newZoneLeastY = (newMasterPlanImageHeight * oldZoneLeastY) / masterPlanImageHeight;

        newY = (newUnitY - newZoneLeastY);

        return [newX - 15, newY - 45];
    }

    function setDrawingZoneImage() {
        dX = 0;
        dY = 0;
        dWidth = newZoneWidth;
        dHeight = newZoneHeight;
        sX = leastX[0] - drawingZoneOffset;
        sY = leastY[1] - drawingZoneOffset;
        sWidth = zoneWidth;
        sHeight = zoneHeight;
    }

    function setUnitMapPinUrl() {
        unitMapPinUrl = unitDataJson.unitMapPinUrl;
    }

    function setUnitCoords() {
        unitCords = toArrayOfIntegers(unitDataJson.unitCoords, ',');
    }

    function setZoneCoords() {
        zoneCoords = toArrayOfIntegers(unitDataJson.unitZoneCoords, ',');
    }

    function setGreatestX() {
        greatestX = getGreatestX(zoneCoords);
    }

    function setGreatestY() {
        greatestY = getGreatestY(zoneCoords);
    }

    function setLeastX() {
        leastX = getLeastX(zoneCoords);
    }

    function setLeastY() {
        leastY = getLeastY(zoneCoords);
    }

    function setMasterPlanImageUrl() {
        masterPlanImageUrl = unitDataJson.unitMasterPlanImageUrl;
    }

    function setMasterPlanImageWidth() {
        masterPlanImageWidth = unitDataJson.unitMasterPlanImageWidth;
    }

    function setMasterPlanImageHeight() {
        masterPlanImageHeight = unitDataJson.unitMasterPlanImageHeight;
    }

    function setNewZoneWidth() {
        newZoneWidth = canvasWrapperWidth;
    }

    function setZoneWidth() {
        zoneWidth = greatestX[0] - leastX[0];
    }

    function setZoneHeight() {
        zoneHeight = greatestY[1] - leastY[1];
    }

    function setNewZoneHeight() {
        // zoneWidth           zoneHeight
        // ---------    =   -------------------
        //  newZoneWidth       newZoneHeight
        newZoneHeight = (newZoneWidth * zoneHeight) / zoneWidth;
    }

    function setNewMasterPlanImageWidth() {
        // masterPlanImageWidth         newMasterPlanImageWidth
        // ---------------------- =  ----------------------------
        // zoneWidth                        newZoneWidth
        newMasterPlanImageWidth = (newZoneWidth * masterPlanImageWidth) / zoneWidth;
    }

    function setNewMasterPlanImageHeight() {
        // masterPlanImageHeight         newMasterPlanImageHeight
        // ---------------------- =  ----------------------------
        // zoneHeight                   newZoneHeight
        newMasterPlanImageHeight = (masterPlanImageHeight * newZoneHeight) / zoneHeight;
    }

    function setNewZoneWidthRatioToZoneWidth() {
        //    newZoneWidth            newNewZoneWidthRatioToZoneWidth
        // -------------------------   =  ----------------------------------------------
        //      zoneWidth                      100
        newNewZoneWidthRatioToZoneWidth = (100 * newZoneWidth) / zoneWidth;
    }

    function getNewZoneXY(vertexX, vertexY) {
        // Inflating or reducing points
        let ratioOfX = newNewZoneWidthRatioToZoneWidth / 100;
        vertexX = vertexX * ratioOfX;
        let ratioOfY = newNewZoneWidthRatioToZoneWidth / 100;
        vertexY = vertexY * ratioOfY;

        // Shifting origins coordinates
        let shiftXAmount = leastX[0] * ratioOfX;
        vertexX = vertexX - shiftXAmount;
        let shiftYAmount = leastY[1] * ratioOfY;
        vertexY = vertexY - shiftYAmount;
        return [vertexX, vertexY];
    }


}
