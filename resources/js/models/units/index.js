searchingOnUnitsAjax = function () {
    //Cache Dom

    var searchingButton = $("#searching-button");
    var searchingButtonRequired = $("#searching-button-required");
    var searchingButtonSearch = $("#searching-button-search");
    var searchingButtonSearching = $("#searching-button-searching");
    var searchingInput = $("#searching-input");
    var csrfToken = $("[name='csrf-token']").attr('content');
    let pagination = $(".pagination");

    let sliderRange = $( "#slider-range" );
    let sliderRangeMinInitial = sliderRange.data('min');
    let sliderRangeMaxInitial = sliderRange.data('max');
    let lang = $('html').attr('lang');


    // Function Body
    jQuerySlider();



    //Cache Events
    searchingButton.on('click', search);
    searchingInput.on('keydown', checkEnterKey);


    function search(e) {
        if (!isEmptyOrSpaces(searchingInput.val())) {
            console.log(sliderRange.data('min'), sliderRange.data('max'));
            $.ajax({
                type: 'POST',
                url: '/units_miscellaneous/index_search',
                dataType: 'JSON',
                data: {
                    '_token': csrfToken,
                    'keyword': searchingInput.val(),
                    'sale_price_range_min': sliderRange.data('min'),
                    'sale_price_range_max': sliderRange.data('max'),
                },
                beforeSend: beforeSend,
                complete: complete,
                success: function (results) {
                    dataTable.clear();
                    $.each(results, function (key, unit) {
                        dataTable.row.add([
                            key + 1,
                            `<a href="/projects/${optional(unit,'project').id}">${optional(unit,'project').name}</a>`,
                            (unit.zone)?(`<a href="/zones/${unit.zone.id}" target="_blank"> ${unit.zone.name}</a>`):(''),
                            (unit.unit_model)?(`<a href="/unit_models/${unit.unit_model.id}" target="_blank"> ${unit.unit_model.name}</a>`):(''),
                            unit.sale_price_presentation,
                            unit.id,
                            `<a href="/units/${unit.id}">${unit.code}</a>`,
                            unit.status,
                            unit.created_at,
                            optional(unit.creator, 'name'),
                        ]);
                    });
                    dataTable.draw();
                    pagination.hide();
                },
                error: function(){
                    console.log('error');
                },
            });
        } else {
            searchingButton.text(searchingButtonRequired.val()).addClass("btn-danger").fadeOut().fadeIn();
        }
    }

    function checkEnterKey(e) {
        if (e.keyCode == 13) {
            search();
        }
    }

    function beforeSend() {
        searchingButton.text(searchingButtonSearching.val()).fadeOut().fadeIn();
    }

    function complete() {
        searchingButton.text(searchingButtonSearch.val()).removeClass("btn-danger");
    }

    function jQuerySlider() {
        let min = (lang == 'en')?('From: '):('من: ');
        let max = (lang == 'en')?('To: '):('إلى: ');

        sliderRange.slider({
            range: true,
            min: 0,
            max: sliderRangeMaxInitial,
            values: [ sliderRangeMinInitial, sliderRangeMaxInitial ],
            slide: function( event, ui ) {
                $( "#amount" ).val( min + ui.values[ 0 ] + " - "+ max + ui.values[ 1 ] );
                sliderRange.data('min', ui.values[ 0 ]);
                sliderRange.data('max', ui.values[ 1 ]);
            }
            });

            $( "#amount" ).val( min + sliderRange.slider( "values", 0 ) +
            " - " + max + sliderRange.slider( "values", 1 ) );
    }

}

selectSyncOnUnits = function () {
    let netSuiteId = $("#net-suite-id");
    let netSuiteIdSyncButton = $("#net-suite-id-sync-button");
    netSuiteId.on('change', handelSelectNetSuiteUrl)

    function handelSelectNetSuiteUrl(e) {
        netSuiteIdSyncButton.attr('href', '/net_suite_integrations_miscellaneous/sync_units_data/' + e.target.value).toggleClass('disabled');
    }
}
