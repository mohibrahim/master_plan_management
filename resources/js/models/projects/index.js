searchingOnProjectsAjax = function () {
    //Cache Dom

    var searchingButton = $("#searching-button");
    var searchingButtonRequired = $("#searching-button-required");
    var searchingButtonSearch = $("#searching-button-search");
    var searchingButtonSearching = $("#searching-button-searching");
    var searchingInput = $("#searching-input");
    var csrfToken = $("[name='csrf-token']").attr('content');

    //Cache Events
    searchingButton.on('click', search);
    searchingInput.on('keydown', checkEnterKey);

    function search(e) {
        if (!isEmptyOrSpaces(searchingInput.val())) {

            $.ajax({
                type: 'POST',
                url: '/projects_miscellaneous/index_search',
                dataType: 'JSON',
                data: {
                    '_token': csrfToken,
                    'keyword': searchingInput.val()
                },
                beforeSend: beforeSend,
                complete: complete,
                success: function (results) {
                    dataTable.clear();
                    $.each(results, function (key, project) {
                        dataTable.row.add([
                            key + 1,
                            project.project_code,
                            `<a href="/projects/${project.id}">${project.name}</a>`,
                            project.percentage_of_completion,
                            project.created_at,
                            optional(project.creator, 'name'),
                        ]);
                    });
                    dataTable.draw();
                },
                error: function () {
                    console.log('error');
                },
            });
        } else {
            searchingButton.text(searchingButtonRequired.val()).addClass("btn-danger").fadeOut().fadeIn();
        }
    }

    function checkEnterKey(e) {
        if (e.keyCode == 13) {
            search();
        }
    }

    function beforeSend() {
        searchingButton.text(searchingButtonSearching.val()).fadeOut().fadeIn();
    }

    function complete() {
        searchingButton.text(searchingButtonSearch.val()).removeClass("btn-danger");
    }

}

selectSyncOnProjects = function () {
    let netSuiteId = $("#net-suite-id");
    let netSuiteIdSyncButton = $("#net-suite-id-sync-button");
    netSuiteId.on('change', handelSelectNetSuiteUrl)

    function handelSelectNetSuiteUrl(e) {
        netSuiteIdSyncButton.attr('href', '/net_suite_integrations_miscellaneous/sync_projects_data/' + e.target.value).toggleClass('disabled');
    }
}

