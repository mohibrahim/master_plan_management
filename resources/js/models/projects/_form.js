deleteMasterPlanImage = function () {
    //Cache DOM
    let coverImage = $("#cover_image");
    let projectFileId = coverImage.attr('value');
    let deleteImageButton = $("#delete_image_button");
    let deleteImageButtonWrapper = $("#delete_image_button_wrapper");

    //Bind Events
    deleteImageButton.on('click', deleteTheImage);

    function deleteTheImage() {
        $.ajax({
            type: "GET",
            dataType: 'json',
            url: '/catalogs_miscellaneous/delete_image_ajax/' + projectFileId,
            success: function (results) {
                console.log('success');
                coverImage.attr('src', '/images/helper_images/no_image.png');
                deleteImageButtonWrapper.hide();
            },
        });
    }
}


masterPlanImageMirror = function () {
    //Cache DOM
    let mirrorImage = $("#mirror-image");
    let masterPlanUploadImage = $("#master-plan-upload-image");

    mirrorImageWhenUpload(mirrorImage, masterPlanUploadImage);
}

deleteLogo = function () {
    //CACHE DOM
    let $deleteLogoButton = $('#delete-logo');
    let $LogoWrapper = $('#logo-wrapper');

    //BIND EVENTS
    $deleteLogoButton.on('click', deleteTheImage);

    //FUNCTION BODY


    //INNER FUNCTIONS

    function deleteTheImage(event) {
        $.ajax({
            type:"get",
            url:"/projects_miscellaneous/delete_logo",
            data:{
                "logo": $(this).data("logo-name")
            },
            success: function(){
                $LogoWrapper.empty().fadeOut();
            },
        });
    }
}

