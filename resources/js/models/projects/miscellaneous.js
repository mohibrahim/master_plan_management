masterPlanImageAreaHighlightAndFilter = function (project) {

    //Cache DOM
    let overFlowWrapper = $('#over-flow-wrapper');
    let $maphilight = $("#map-image");
    let mapElement = $("#map-element");
    let filteringButton = $("#filtering-btn");
    let zoneIdSelectElement = $("#zone-id-select-element");
    let unitModelIdSelectElement = $("#unit-model-id-select-element");
    let unitStatusSelectElement = $("#unit-status-select-element");
    let inBlockRadioButton = $("#in-block-radio-button");
    let filteringProjectButtonName = $("#filtering-project-button-name");
    let filteringProjectButtonNameWhenLoading = $("#filtering-project-button-name-when-loading");

    //Bind Events
    filteringButton.on('click', filterMasterPlanImage);

    //1st time page load
    $maphilight.maphilight();


    function filterMasterPlanImage() {
        let projectId = $(this).data("filtered-project-id");

        $.ajax({
            method: "GET",
            dataType: "json",
            contentType: "application/json",
            url: "/projects_miscellaneous/filter_master_plan_image",
            data: {
                'project_id': projectId,
                'zone_id': zoneIdSelectElement.val(),
                'unit_model_id': unitModelIdSelectElement.val(),
                'unit_status': unitStatusSelectElement.val(),
                'in_block': inBlockRadioButton.is(':checked'),
            },
            beforeSend: beforeSend,
            success: renderResponse
        });

        function renderResponse(result, status, xhr) {

            //if (result.length > 0) {
            //Clear old areas
            mapElement.children().remove();

            //Add new areas
            $.each(result.units, function (key, value) {
                let area = `<area type="button" class="my-selected-area" id="area-${key}" shape="poly"
											coords="${value.html_area_tag_coords}"
											data-maphilight='{"stroke":true, "strokeColor":"000000","strokeWidth":2,"fillColor":"${value.status_color}","fillOpacity":0.7,"alwaysOn":true}'
                                            href="/units/${value.id}" alt=""
                                            data-toggle="modal" data-target="#unit-modal" data-unit-details=\'${JSON.stringify(value)}\'>`;
                mapElement.append(area);
            });
            //reRender
            $maphilight.maphilight();



            filteringButton.text(filteringProjectButtonName.val());
            masterPlanPreviewUnitAsModal(project);
            alignZoneRight(result.zone_lowest_x);

        }


        function beforeSend() {
            filteringButton.text(filteringProjectButtonNameWhenLoading.val());
        }

        function alignZoneRight(lowestX) {
            if (lowestX != 0) {
                overFlowWrapper.scrollLeft(lowestX);
            }
            if (lowestX == 0) {
                let overFlowWrapperWidth = overFlowWrapper.width();
                let mapImageWidth = $maphilight.width();
                overFlowWrapper.scrollLeft((mapImageWidth-overFlowWrapperWidth)/2);
            }

        }
    }



}
