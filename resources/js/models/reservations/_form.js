reservationSelectUnitId = function () {

    /*
    |--------------------------------------------------------------------------
    | Cache DOM
    |--------------------------------------------------------------------------
    */
    let unitId = $('#unit-id');
    let oldSelectedUnitCode = $('#old-selected-unit-code');
    let bookingAmountForUnit = $("#booking-amount-for-unit");
    let minimumBookingAmountForUnit = $("#minimum-booking-amount-for-unit");
    let bookingCurrencyForUnit = $(".booking-currency-for-unit");

    let oldBookingAmountForUnit = $("#old-booking-amount-for-unit");
    let oldMinimumBookingAmountForUnit = $("#old-minimum-booking-amount-for-unit");
    let oldBookingCurrencyForUnit = $("#old-booking-currency-for-unit");
    /*
    |--------------------------------------------------------------------------
    | Bind Events
    |--------------------------------------------------------------------------
    */
    unitId.on('select2:select', unitIdSelected)
    /*
    |--------------------------------------------------------------------------
    | Function body
    |--------------------------------------------------------------------------
    */
    unitId.select2({
        minimumInputLength: 1,
        placeholder: $('#unit-code-placeholder').val(),
        ajax: {
            delay: 500,
            url: '/reservations_miscellaneous/get_unit_codes_ids',
            dataType: 'json'
        },
    });

    //setting the old values if validation fails
    if (oldBookingAmountForUnit.val()) {
        bookingAmountForUnit.text(oldBookingAmountForUnit.val());
    }
    if (oldMinimumBookingAmountForUnit.val()) {
        minimumBookingAmountForUnit.text(oldMinimumBookingAmountForUnit.val());
    }
    if (oldBookingCurrencyForUnit.val()) {
        bookingCurrencyForUnit.text(oldBookingCurrencyForUnit.val());
    }
    /*
    |--------------------------------------------------------------------------
    | Functions
    |--------------------------------------------------------------------------
    */


    function unitIdSelected(event) {
        oldSelectedUnitCode.val(event.params.data.text);

        bookingAmountForUnit.text(event.params.data.booking_amount);
        oldBookingAmountForUnit.val(event.params.data.booking_amount);
        minimumBookingAmountForUnit.text(event.params.data.minimum_booking_amount);
        oldMinimumBookingAmountForUnit.val(event.params.data.minimum_booking_amount);
        bookingCurrencyForUnit.text(event.params.data.currency);
        oldBookingCurrencyForUnit.val(event.params.data.currency);
    };
}

reservationSelectCustomerId = function () {

    let oldSelectedCustomerName = $('#old-selected-customer-name');

    $('#customer-id').select2({
        minimumInputLength: 1,
        placeholder: $('#customer-name-placeholder').val(),
        ajax: {
            delay: 500,
            url: '/reservations_miscellaneous/get_customer_names_ids',
            dataType: 'json'
        },
    });

    $('#customer-id').on('select2:select', function (event) {
        oldSelectedCustomerName.val(event.params.data.text);
    });
}

checkBooking = function () {
    /*
    |--------------------------------------------------------------------------
    | Cache DOM
    |--------------------------------------------------------------------------
    */
    let booking = $("#booking");
    let paymentWrapper = $("#payment-wrapper");
    let paidAmount = $("#paid-amount");
    let remainingAmount = $("#remaining-amount");
    /*
    |--------------------------------------------------------------------------
    | Bind Events
    |--------------------------------------------------------------------------
    */
    booking.on('change', bookingChanged);
    /*
    |--------------------------------------------------------------------------
    | Function body
    |--------------------------------------------------------------------------
    */

    if (booking.prop('checked')) {
        paymentWrapper.removeClass('d-none');
    }

    /*
    |--------------------------------------------------------------------------
    | Functions
    |--------------------------------------------------------------------------
    */

    function bookingChanged(event) {
        if (booking.prop('checked')) {
            paymentWrapper.removeClass('d-none');
        }
        if (!booking.prop('checked')) {
            paymentWrapper.addClass('d-none');
            paidAmount.val('');
            remainingAmount.val('');
        }
    }

}

setBookingDateByToday = function () {
    /*
    |--------------------------------------------------------------------------
    | Cache DOM
    |--------------------------------------------------------------------------
    */
    let bookingDate = $("#booking-date");
    let todayDate = $("#today-date");
    /*
    |--------------------------------------------------------------------------
    | Bind Events
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | Function body
    |--------------------------------------------------------------------------
    */

    if (!bookingDate.val()) {
        bookingDate.val(todayDate.val());
    }

    /*
    |--------------------------------------------------------------------------
    | Functions
    |--------------------------------------------------------------------------
    */

}


selectBankForReservation = function () {
    //CACHE DOM
    let bankId = $('#bank-id');
    let oldBankId = $('#old-bank-id');
    let oldBankIdText = $('#old-bank-id-text');
    let bankIdPlaceholder = $('#bank-id-placeholder');

    //BIND EVENTS
    bankId.on('select2:select', select2Select);

    //FUNCTION BODY

    bankId.select2({
        minimumInputLength: 1,
        placeholder: bankIdPlaceholder.val(),
        allowClear: true,
        // language: "ar",
        // dir: "rtl",
        ajax: {
            delay: 500,
            url: '/reservations_miscellaneous/get_banks_names_ids',
            dataType: 'json'
        },
        templateResult: formatRepo,
    });

    //when validation fails
    if (oldBankIdText.val() && oldBankId.val()) {
        bankId.empty().append('<option value="' + oldBankId.val() + '">' + oldBankIdText.val() + '</option>');
    }

    //INNER FUNCTIONS

    function select2Select(event) {
        oldBankIdText.val(event.params.data.text);
    }

    function formatRepo(repo) {

        if (repo.loading) {
            return repo.text
        }
        let container = `
            <h5><strong>${repo.text}</strong></h5>
        `;

        return $.parseHTML(container);
    }
}

