<?php

return [
    'id' => 'Block ID',
    'name' => 'Name',
    'name_placeholder' => 'Enter block name',
    'notes' => 'Notes',
    'notes_placeholder' => ' Enter the notes',

    'project_name' => 'Project',
    'project_name_placeholder' => ' Choose project',
    'zone_name' => 'Zone name',
    'zone_name_placeholder' => ' Choose zone name',


    'created_at' => 'Creation date',
    'updated_at' => 'Last updated date',
    'creator_name' => 'Creator name',
    'last_updater_name' => 'Last update done by',
    'save' => 'Save',
    'add_block' => 'Add new block',
    'edit_block' => 'Edit the block',
    'delete_block' => 'Delete the block',
    'add_catalog' => 'Add new catalog to the block',
    'all_blocks' => 'All blocks',
    'searching_on_block' => 'Searching for block',
    'search' => 'Search',
    'searching_word' => 'Searching word',
    'confirm_delete_message' => 'Do you want to delete this block',

    'searching_button_searching' => 'Searching...',
    'searching_button_search' => 'Search',
    'searching_button_required' => 'Please enter the word that you want to searching for...',

    'block_sync_create_message' => 'Added blocks: ',
    'block_sync_update_message' => 'Blocks that have been updated: ',
    'block_sync_delete_message' => 'Blocks deleted: ',

];
