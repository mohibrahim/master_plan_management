<?php

return [
    'dashboard' => 'Dashboard',
    'app_name' => config('app.name'),
    'login' => 'Login',
    'unit' => 'Unit | ',
];
