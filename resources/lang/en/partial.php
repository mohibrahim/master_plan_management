<?php

return [
    'confirm_delete' => 'Confirm Delete',
    'warning' => 'Warning!',
    'close' => 'Close',
    'delete' => 'Delete',
    'boolean_placeholder' => 'Choose Yes or No',
    'yes' => 'Yes',
    'no' => 'No',
];
