<?php

return [
    'id' => 'Phase ID',
    'name' => 'Name',
    'name_placeholder' => ' Enter Name',
    'supposed_start_date' => 'Scheduled start date',
    'supposed_start_date_placeholder' => ' Enter Scheduled start date',
    'actually_start_date' => 'The actual start date',
    'actually_start_date_placeholder' => ' Enter the actual start date',
    'supposed_end_date' => 'Scheduled completion date',
    'supposed_end_date_placeholder' => ' Enter Scheduled completion date',
    'actually_end_date' => 'The actual end date',
    'actually_end_date_placeholder' => ' Enter The actual end date',
    'percentage_of_completion' => 'Percentage of completion',
    'percentage_of_completion_placeholder' => 'Enter Percentage of completion',
    'notes' => 'Notes',
    'notes_placeholder' => ' Enter your notes',

    'project_name' => 'Project',
    'project_name_placeholder' => ' Enter Project',


    'created_at' => 'Creation date',
    'updated_at' => 'Last updated date',
    'creator_name' => 'Creator name',
    'last_updater_name' => 'Last update done by',
    'save' => 'Save',
    'add_phase' => 'Add new phase',
    'edit_phase' => 'Edit the phase',
    'delete_phase' => 'Delete the phase',
    'add_catalog' => 'Add new catalog to the phase',
    'all_phases' => 'All phases',
    'searching_on_phase' => 'Searching on a phase',
    'search' => 'Search',
    'searching_word' => 'Searching word',
    'confirm_delete_message' => 'Do you want to delete the phase',

    'searching_button_searching' => 'Searching...',
    'searching_button_search' => 'Search',
    'searching_button_required' => 'Please enter the word that you want to searching for...',

    'phase_sync_create_message' => 'Added phases: ',
    'phase_sync_update_message' => 'Phases that have been updated: ',
    'phase_sync_delete_message' => 'Phases deleted: ',

];
