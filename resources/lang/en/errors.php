<?php

return [
    '403'=> 'Sorry, you are not authorized to access this page',
    '404'=> 'Sorry, the page to be access was not exists',
    '500'=> 'Sorry, there is an error in the program',
    '503'=> 'Oops! In case of maintenance, we will return as soon as possible',
];