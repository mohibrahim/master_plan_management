<?php

return [
    'id' => 'Zone ID',
    'name' => 'Name',
    'name_placeholder' => ' Enter Name',
    'notes' => 'Notes',
    'notes_placeholder' => ' Enter your notes',

    'project_name' => 'Project',
    'project_name_placeholder' => ' Choose Project',
    'phase_name' => 'Phase',
    'html_area_tag_coords'=> 'The coordinates of the area on the master plan',
    'html_area_tag_coords_placeholder'=> 'Choose The coordinates of the area on the master plan',
    'phase_name_placeholder' => ' Choose Phase',


    'created_at' => 'Creation date',
    'updated_at' => 'Last updated date',
    'creator_name' => 'Creator name',
    'last_updater_name' => 'Updater name',
    'save' => 'Save',
    'add_zone' => 'Add new zone',
    'edit_zone' => 'Edit the zone',
    'delete_zone' => 'Delete the zone',
    'add_catalog' => 'Add new catalog for the zone',
    'all_zones' => 'All zones',
    'searching_on_zone' => 'Searching on zone',
    'search' => 'Search',
    'searching_word' => 'Searching word',
    'confirm_delete_message' => 'Do you want to delete the zone',

    'searching_button_searching' => 'Searching...',
    'searching_button_search' => 'Search',
    'searching_button_required' => 'Please enter the word that you want to searching for...',

    'zone_sync_create_message' => 'Added zones: ',
    'zone_sync_update_message' => 'Zones that have been updated: ',
    'zone_sync_delete_message' => 'Zones deleted: ',

];
