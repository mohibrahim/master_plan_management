<?php

return [
    'id' => 'Bank ID',
    'name' => 'Name',
    'name_placeholder' => ' Enter Name',
    'notes' => 'Notes',
    'notes_placeholder' => ' Enter your notes',


    'created_at' => 'Creation date',
    'updated_at' => 'Last updated date',
    'save' => 'Save',
    'add_bank' => 'Add new bank',
    'edit_bank' => 'Edit the bank',
    'delete_bank' => 'Delete the bank',
    'add_catalog' => 'Add new catalog for the bank',
    'all_banks' => 'All banks',
    'searching_on_bank' => 'Searching on bank',
    'search' => 'Search',
    'searching_word' => 'Searching word',
    'confirm_delete_message' => 'Do you want to delete the bank',

    'searching_button_searching' => 'Searching...',
    'searching_button_search' => 'Search',
    'searching_button_required' => 'Please enter the word that you want to searching for...',

    'bank_sync_create_message' => 'Added banks: ',
    'bank_sync_update_message' => 'Banks that have been updated: ',
    'bank_sync_delete_message' => 'Banks deleted: ',

];
