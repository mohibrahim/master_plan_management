<?php

return [
    'login' => 'Login',
    'logout' => 'Logout',
    'register'=>'Register',
    'app_name'=> config('app.name'),
    'home'=>'Home',
    'user_system'=> 'User System',
    'users'=>'Users',
    'index_users'=>'Show all users',
    'create_user'=>'Add new user',
    'verify_users_emails'=>'Verify users emails',
    'roles'=>'Rules',
    'create_role'=>'Add new Role',
    'index_roles'=>'Show all roles',
    'permissions'=>'Permissions',
    'create_permission'=>'Add new permission',
    'index_permissions'=>'Show all permissions',

    'project_files' => 'Project files',
    'project_files_files' => 'files ',
    'index_project_files'=>'All project files',
    'create_project_files'=>'Add new project file',

    'catalogs' => 'Catalogs',
    'index_catalogs'=>'All catalogs',
    'create_catalog'=>'Add new catalog',

    'projects' => 'Projects',
    'index_projects'=>'All projects',
    'create_project'=>'Add new project',

    'phases' => 'Phases',
    'index_phases'=>'All phases',
    'create_phase'=>'Add new phase',

    'zones' => 'Zones',
    'index_zones'=>'All zones',
    'create_zone'=>'Add new zone',

    'unit_models' => 'Units models',
    'index_unit_models'=>'All units models',
    'create_unit_model'=>'Add new unit model',

    'units' => 'Units',
    'index_units'=>'All units',
    'create_unit'=>'Add new unit',

    'customers' => 'Customers',
    'index_customers'=>'All customers',
    'create_customer'=>'Add new customer',

    'reservations' => 'Reservations',
    'index_reservations'=>'Show all reservations',
    'create_reservation'=>'Add new reservation',

    'project_user_assignments' => 'Assigning users to projects',
    'index_project_user_assignments'=>'All assignments',
    'create_project_user_assignment'=>'Assign user to project',

    'blocks' => 'Blocks',
    'index_blocks'=>'Show all blocks',
    'create_block'=>'Add new block',

    'net_suite_integrations' => 'NetSuite Integrations',
    'index_net_suite_integrations'=>'Show all netSuites integrations',
    'create_net_suite_integration'=>'Add new netSuite integration',

];
