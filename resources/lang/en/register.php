<?php

return [
    'name'=>'Name',
    'email'=>'E-Mail Address',
    'password'=>'Password',
    'password_confirm'=> 'Confirm Password',
    'remember_me'=>'Remember Me',
    'forgot_your_password'=>'Forgot Your Password?',
    'login'=>'Login',
    'register'=>'Register',
];