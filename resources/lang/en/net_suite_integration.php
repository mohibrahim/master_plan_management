<?php

return [
    'id' => 'NetSuite integration code',
    'name' => 'NetSuite integration title',
    'name_placeholder' => ' Enter netSuite integration title',

    'api'=> ' API for Net suite integration',
    'project_restful'=>'For Projects',
    'phase_restful'=>'For Phases',
    'zone_restful'=>'For Zones',
    'block_restful'=>'For Blocks',
    'unit_model_restful'=>'For Unit Models',
    'unit_restful'=>'For Units',
    'customer_restful'=>'For Customers',
    'bank_restful'=>'For Banks',
    'reservation_restful'=>'For Reservations',

    'api_authorization'=>'Authorization key',
    'api_authorization_placeholder'=>'Enter authorization key',

    'project_api_index'=>' Project index URL',
    'project_api_index_placeholder'=>'Enter project index url',

    'phase_api_index'=>' Phase index URL',
    'phase_api_index_placeholder'=>'Enter phase index url',

    'zone_api_index'=>' Zone index URL',
    'zone_api_index_placeholder'=>'Enter zone index url',

    'block_api_index'=>' Block index URL',
    'block_api_index_placeholder'=>'Enter block index url',

    'unit_model_api_index'=>' Unit model index URL',
    'unit_model_api_index_placeholder'=>'Enter unit model index url',

    'unit_api_index'=>' Unit index URL',
    'unit_api_index_placeholder'=>'Enter unit index url',

    'customer_api_index'=>' Customer index URL',
    'customer_api_index_placeholder'=>'Enter customer index url',
    'customer_api_store'=>' Customer store URL',
    'customer_api_store_placeholder'=>'Enter customer store url',
    'customer_api_update'=>' Customer update URL',
    'customer_api_update_placeholder'=>'Enter customer update url',
    'customer_api_destroy'=>' Customer delete URL',
    'customer_api_destroy_placeholder'=>'Enter customer delete url',

    'bank_api_index'=>' Bank index URL',
    'bank_api_index_placeholder'=>'Enter bank index url',

    'reservation_api_index'=>' Reservation index URL',
    'reservation_api_index_placeholder'=>'Enter reservation index url',
    'reservation_api_store'=>' Reservation store URL',
    'reservation_api_store_placeholder'=>'Enter reservation store url',
    'reservation_api_update'=>' Reservation update URL',
    'reservation_api_update_placeholder'=>'Enter reservation update url',
    'reservation_api_destroy'=>' Reservation delete URL',
    'reservation_api_destroy_placeholder'=>'Enter reservation delete url',


    'notes' => 'Notes',
    'notes_placeholder' => ' Enter your notes',
    'created_at' => 'Creation date',
    'updated_at' => 'Last updated date',
    'creator_name' => 'Creator name',
    'last_updater_name' => 'Last update done by',
    'save' => 'Save',
    'add_net_suite_integration' => 'Add a new netSuite integration',



    'edit_net_suite_integration' => 'Edit the netSuite integration',
    'delete_net_suite_integration' => 'Delete the netSuite integration',
    'projects' => 'Projects',
    'api_show'=> 'API',
    'entity'=>'Entity',
    'url'=>'URL',
    'last_syncing_date_time'=>'Last syncing date time',
    'synchronize'=>'Synchronization',


    'all_net_suite_integrations' => 'All netSuites integrations',
    'filtering' => 'Filtering',
    'filtering_when_loading' => 'Loading... ',
    'searching_on_net_suite_integration' => 'Searching on netSuite integration',
    'search' => 'Search',
    'searching_word' => 'Searching word',
    'confirm_delete_message' => 'Do you want to delete the netSuite integration?',

    'searching_button_searching' => 'Searching...',
    'searching_button_search' => 'Search',
    'searching_button_required' => 'Please enter the word to search for...',

    'select_net_suite_integration' => 'Sync With NetSuite Integration',
    'select_net_suite_integration_placeholder' => 'Select NetSuite Integration',

];
