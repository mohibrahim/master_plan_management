<?php

return [
    'id'=> 'Assignment ID',

    'project_name'=> 'Projects names',
    'project_name_placeholder'=> 'Choose projects names',
    'employee_name'=> 'Employee name',
    'employee_name_placeholder'=> 'Choose employee name',
    'notes' => 'Notes',
    'notes_placeholder' => ' Enter your notes',


    'created_at' => 'Creation date',
    'updated_at' => 'Last updated date',
    'creator_name' => 'Creator name',
    'last_updater_name' => 'Last update done by',
    'save' => 'Save',
    'add_project_user_assignment' => 'Assign a projects to user',
    'edit_project_user_assignment' => 'Edit assignment',
    'delete_project_user_assignment' => 'Delete user projects assignments',
    'all_project_user_assignments' => 'All project assignments',
    'searching_on_project_user_assignment' => 'Searching on projects assignments',
    'search' => 'Search',
    'searching_word' => 'Searching word',
    'confirm_delete_message' => 'Do you want to delete the projects assignment',

    'searching_button_searching' => 'Searching...',
    'searching_button_search' => 'Search',
    'searching_button_required' => 'Please enter the word that you want to searching for...',



];
