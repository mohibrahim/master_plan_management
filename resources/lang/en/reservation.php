<?php

return [
    'id' => 'Reservation ID',
    'unit_code' => 'Unit',
    'unit_code_placeholder' => 'Choose Unit',
    'customer_name' => 'Customer Name',
    'customer_name_placeholder' => 'Choose Customer Name',
    'bank_id' => 'Bank',
    'bank_id_placeholder' => 'Choose Bank Name',

    'customer_phone' => 'Customer Phone',
    'customer_national_id' => 'Customer National ID',

    'add_new_customer' => 'Add New Customer',

    'booking_date' => 'Booking Date',
    'booking_date_placeholder' => 'Choose Booking Date',
    'bank_account_details' => 'Bank Account Details',
    'document_number' => 'Document Number',
    'document_number_placeholder' => ' Enter Document Number',
    'net_suite_internal_id' => 'Netsuite Integration number',
    'net_suite_internal_id_placeholder' => ' Enter Netsuite Integration number',

    'expiry_date' => 'Expiry Date',
    'expiry_date_placeholder' => 'Choose The Expiry Date',
    'status' => 'Status',
    'status_placeholder' => 'Enter Status',
    'booking_status' => 'Booking Status',
    'booking_status_placeholder' => 'Enter Booking Status',
    'reason' => 'Win/loss Reason',
    'reason_placeholder' => 'Enter Reason',

    'booking' => 'Booking',
    'paid' => 'Paid',
    'no_prepayment' => 'No Prepayment',

    'paid_amount' => 'Paid Amount',
    'paid_amount_placeholder' => 'Enter The Paid Amount',
    'remaining_amount' => 'Remaining Amount',
    'remaining_amount_placeholder' => 'Enter Remaining Amount',

    'bank_account_details_placeholder' => 'Enter Bank Account Details',
    'notes' => 'Reservation Notes',
    'notes_placeholder' => 'Enter Your Notes',


    'created_at' => 'Creation Date',
    'updated_at' => 'Last Updated Date',
    'net_suite_created_at' => 'Creation Date on NetSuite',
    'net_suite_updated_at' => 'Last Updated Date on NetSuite',
    'sales_representative_name' => 'Sales Representative',
    'creator_name' => 'Creator Name',
    'last_updater_name' => 'Last Update Done By',
    'save' => 'Save',
    'add_reservation' => 'Add New Reservation',
    'edit_reservation' => 'Edit The Reservation',
    'delete_reservation' => 'Delete The Reservation',
    'add_catalog' => 'Attach Files For Booking',
    'all_reservations' => 'All Reservations',
    'searching_on_reservation' => 'البحث عن الحجز',
    'search' => 'Search',
    'searching_word' => 'Searching Word',
    'confirm_delete_message' => 'Do You Want To Delete The Reservation',
    'reservation_files' => 'Attached Files',

    'searching_button_searching' => 'Searching',
    'searching_button_search' => 'Search',
    'searching_button_required' => 'Please Enter The Word That You Want To Searching For',

    'reservation_details' => 'Reservation Details',

    'reserved_before_unique_validation' => 'This Unit Is Already Booked And No New Reservation Can Be Added',
    'minimum_booking_amount' => 'The Minimum Booking Amount For This Unit Is Not Specified Please Refer To The Creator of this unit on the system to assign it',
    'paid_amount_less_than_minimum_booking_amount' => 'The Paid Amount For Booking Is Less Than The Minimum Unit booking amount',

    'reservation_sync_create_message' => 'Added reservations: ',
    'reservation_sync_update_message' => 'Reservations that have been updated: ',
    'reservation_sync_delete_message' => 'Reservations deleted: ',

];
