<?php

return [
    'id'=> 'كود الحجز',
    'unit_code'=> 'كود الوحدة',
    'unit_code_placeholder'=> 'اختار كود الوحدة',
    'customer_name'=> 'اسم العميل',
    'customer_name_placeholder'=> 'اختار اسم العميل',
    'bank_id'=> 'البنك',
    'bank_id_placeholder'=> 'اختار اسم البنك',

    'customer_phone'=> 'رقم تليفون العميل',
    'customer_national_id'=> 'رقم الهوية الشخصية للعميل',

    'add_new_customer' => 'إضافة عميل جديد',

    'id' => 'رقم الحجز',
    'booking_date' => 'تاريخ الحجز',
    'booking_date_placeholder' => ' ادخل تاريخ الحجز',
    'bank_account_details' => 'تفاصيل الحساب البنكي',
    'bank_account_details_placeholder' => ' ادخل تفاصيل الحساب البنكي',
    'document_number' => 'رقم المستند',
    'document_number_placeholder' => ' ادخل رقم المستند',
    'net_suite_internal_id' => 'رقم دمج نتسويت',
    'net_suite_internal_id_placeholder' => ' ادخل رقم دمج نتسويت',

    'expiry_date' => 'تاريخ الانتهاء',
    'expiry_date_placeholder' => ' ادخل تاريخ الانتهاء',
    'status' => 'الحالة',
    'status_placeholder' => ' ادخل الحالة',
    'booking_status' => 'حالة الحجز',
    'booking_status_placeholder' => ' ادخل حالة الحجز',
    'reason' => 'الفوز / الخسارة السبب',
    'reason_placeholder' => ' ادخل السبب',

    'booking' => 'الحجز مقدًا',
    'paid' => 'تم دفع مقدم',
    'no_prepayment' => 'لم يتم دفع مقدم',

    'paid_amount' => 'المبلغ المدفوع',
    'paid_amount_placeholder' => ' ادخل المبلغ المدفوع',
    'remaining_amount' => 'المبلغ المتبقي',
    'remaining_amount_placeholder' => ' ادخل المبلغ المتبقي',

    'notes' => 'الملاحظات',
    'notes_placeholder' => ' ادخل الملاحظات',


    'created_at' => 'تاريخ الإضافة',
    'updated_at' => 'تاريخ آخر تعديل',
    'net_suite_created_at' => 'تاريخ الإضافة على نتسويت',
    'net_suite_updated_at' => 'تاريخ آخر تعديل على نتسويت',
    'sales_representative_name' => 'مندوب مبيعات',
    'creator_name' => 'تمت الإضافة بوسطة',
    'last_updater_name' => 'آخر تعديل تم بوسطة',
    'save' => 'حفظ',
    'add_reservation' => 'إضافة حجز جديد',
    'edit_reservation' => 'تعديل الحجز',
    'delete_reservation' => 'حذف الحجز',
    'add_catalog' => 'إرفاق ملفات للحجز',
    'all_reservations' => 'كل الحجوزات',
    'searching_on_reservation' => 'البحث عن الحجز',
    'search' => 'البحث',
    'searching_word' => 'كلمة البحث',
    'confirm_delete_message' => 'هل ترغب في حذف الحجز',
    'reservation_files' => 'الملفات المرفقة',

    'searching_button_searching' => 'جاري البحث..',
    'searching_button_search' => 'ابحث',
    'searching_button_required' => 'برجاء إدخال الكلمة المراد البحث عنها...',

    'reservation_details' => 'تفاصيل الحجز',

    'reserved_before_unique_validation' => 'هذه الوحدة محجوزة بالفعل ولا يمكن إضافة حجز جديد',
    'minimum_booking_amount' => 'لم يتم تحديد الحد الادنى لمقدم الدفع لهذه الوحدة. برجاء الرجوع لمنشيء هذه الوحدة على النظام ليقوم بتعينه',
    'paid_amount_less_than_minimum_booking_amount' => 'المبلغ المدفوع لحجز أقل من الحد الأدنى لمبلغ حجز الوحدة',

    'reservation_sync_create_message' => 'الحجوزات التي تم إضافتها:  ',
    'reservation_sync_update_message' => 'الحجوزات التي تم تحديثها:  ',
    'reservation_sync_delete_message' => 'الحجوزات التي تم حذفها:  ',

];
