<?php

return [
    'id'=> 'كود التعين',

    'project_name'=> 'اسماء المشاريع',
    'project_name_placeholder'=> 'اختار اسماء المشاريع',
    'employee_name'=> 'اسم الموظف',
    'employee_name_placeholder'=> 'اختار اسم الموظف',
    'notes' => 'الملاحظات',
    'notes_placeholder' => ' ادخل الملاحظات',


    'created_at' => 'تاريخ الإضافة',
    'updated_at' => 'تاريخ آخر تعديل',
    'creator_name' => 'تمت الإضافة بوسطة',
    'last_updater_name' => 'آخر تعديل تم بوسطة',
    'save' => 'حفظ',
    'add_project_user_assignment' => 'إضافة تعين جديد',
    'edit_project_user_assignment' => 'تعديل التعين',
    'delete_project_user_assignment' => 'حذف تعينات الموظف',
    'all_project_user_assignments' => 'كل التعينات',
    'searching_on_project_user_assignment' => 'البحث عن التعين',
    'search' => 'البحث',
    'searching_word' => 'كلمة البحث',
    'confirm_delete_message' => 'هل ترغب في حذف التعينات للموظف',

    'searching_button_searching' => 'جاري البحث..',
    'searching_button_search' => 'ابحث',
    'searching_button_required' => 'برجاء إدخال الكلمة المراد البحث عنها...',

    'project_user_assignment_details' => 'تفاصيل التعين',


];
