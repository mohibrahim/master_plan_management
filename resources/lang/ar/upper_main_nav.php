<?php

return [
    'login' => 'تسجيل الدخول',
    'logout' => 'تسجيل خروج',
    'register'=>'الاشتراك',
    'app_name'=> config('app.name_arabic'),
    'home'=>'الرئيسية',
    'user_system'=> 'مستخدمي النظام',
    'users'=>'المستخدمين',
    'index_users'=>'عرض كل المستخدمين',
    'create_user'=>'إضافة مستخدم جديد',
    'verify_users_emails'=>'تفعيل المستخدمين الذين لم يتم التحقق من البريد الإلكتروني الخاص بهم',
    'roles'=>'الأدوار',
    'create_role'=>'إضافة دور جديد',
    'index_roles'=>'عرض كل الأدوار',
    'permissions'=>'صلاحيات',
    'create_permission'=>'إضافة صلاحية جديد',
    'index_permissions'=>'عرض كل صلاحيات',

    'project_files' => 'ملفات مشروعات',
    'project_files_files' => 'الملفات ',
    'index_project_files'=>'عرض ملفات مشروعات',
    'create_project_files'=>'إضافة ملف جديد',

    'catalogs' => 'الكتالوجات',
    'index_catalogs'=>'عرض الكتالوجات',
    'create_catalog'=>'إضافة كتالوج جديد',

    'projects' => 'المشروعات',
    'index_projects'=>'عرض المشروعات',
    'create_project'=>'إضافة مشروع جديد',

    'phases' => 'المراحل',
    'index_phases'=>'عرض المراحل',
    'create_phase'=>'إضافة مرحلة جديد',

    'zones' => 'المناطق',
    'index_zones'=>'عرض المناطق',
    'create_zone'=>'إضافة منطقة جديد',

    'unit_models' => 'نماذج الوحدات',
    'index_unit_models'=>'عرض نماذج الوحدات',
    'create_unit_model'=>'إضافة نموذج وحدة جديد',

    'units' => 'الوحدات',
    'index_units'=>'عرض الوحدات',
    'create_unit'=>'إضافة وحدة جديد',

    'customers' => 'العملاء',
    'index_customers'=>'عرض العملاء',
    'create_customer'=>'إضافة عميل جديد',

    'reservations' => 'الحجوزات',
    'index_reservations'=>'عرض الحجوزات',
    'create_reservation'=>'إضافة حجز جديد',

    'project_user_assignments' => 'تعين الموظفين على المشاريع',
    'index_project_user_assignments'=>'عرض تعينات الموظفين على المشاريع',
    'create_project_user_assignment'=>' تعين موظف على المشروعات',

    'blocks' => 'القطع',
    'index_blocks'=>'عرض القطع',
    'create_block'=>'إضافة قطعة جديد',

    'net_suite_integrations' => 'دمج النتسويت',
    'index_net_suite_integrations'=>'عرض دمج النتسويت',
    'create_net_suite_integration'=>'إضافة دمج نتسويت جديد',
];
