<?php

return [
    'id' => 'كود المنطقة',
    'name' => 'اسم المنطقة',
    'name_placeholder' => ' ادخل اسم المنطقة',
    'notes' => 'الملاحظات',
    'notes_placeholder' => ' ادخل الملاحظات',

    'project_name' => 'اسم المشروع',
    'project_name_placeholder' => ' اختار اسم المشروع',
    'phase_name' => 'اسم المرحلة',
    'html_area_tag_coords'=> 'احداثيات المنطقة على المخطط',
    'html_area_tag_coords_placeholder'=> 'اختار احداثيات المنطقة على المخطط',
    'phase_name_placeholder' => ' اختار اسم المرحلة',


    'created_at' => 'تاريخ الإضافة',
    'updated_at' => 'تاريخ آخر تعديل',
    'creator_name' => 'تمت الإضافة بوسطة',
    'last_updater_name' => 'آخر تعديل تم بوسطة',
    'save' => 'حفظ',
    'add_zone' => 'إضافة منطقة جديد',
    'edit_zone' => 'تعديل المنطقة',
    'delete_zone' => 'حذف المنطقة',
    'add_catalog' => 'إضافة كتالوج جديد للمنطقة',
    'all_zones' => 'كل المناطق',
    'searching_on_zone' => 'البحث عن المنطقة',
    'search' => 'البحث',
    'searching_word' => 'كلمة البحث',
    'confirm_delete_message' => 'هل ترغب في حذف المنطقة',

    'searching_button_searching' => 'جاري البحث..',
    'searching_button_search' => 'ابحث',
    'searching_button_required' => 'برجاء إدخال الكلمة المراد البحث عنها...',

    'zone_sync_create_message' => 'المناطق التي تم إضافتها:  ',
    'zone_sync_update_message' => 'المناطق التي تم تحديثها:  ',
    'zone_sync_delete_message' => 'المناطق التي تم حذفها:  ',

];
