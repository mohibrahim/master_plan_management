<?php

return [
    'dashboard' => 'لوحة التحكم',
    'app_name' => config('app.name_arabic'),
    'login' => 'تسجيل دخول',
    'unit' => 'الوحدة | ',
];
