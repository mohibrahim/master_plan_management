<?php

return [
    'confirm_delete' => 'تأكيد الحذف',
    'warning' => 'تحذير!',
    'close' => 'إغــلاق',
    'delete' => 'حــذف',
    'boolean_placeholder' => 'اختار نعم أو لا',
    'yes' => 'نعم',
    'no' => 'لا',
];
