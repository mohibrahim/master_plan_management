<?php

return [
    'id' => 'كود النتسويت',
    'name' => 'عنوان النتسويت',
    'name_placeholder' => ' ادخل عنوان النتسويت',

    'api'=> ' وجهة تحديث التطبيق للنتسويت',
    'project_restful'=>'للمشاريع',
    'phase_restful'=>'للمراحل',
    'zone_restful'=>'للمناطق',
    'block_restful'=>'للقطع',
    'unit_model_restful'=>'لنماذج الوحدات',
    'unit_restful'=>'للوحدات',
    'customer_restful'=>'للعملاء',
    'bank_restful'=>'البنوك',
    'reservation_restful'=>'للحجوزات',

    'api_authorization'=>' رمز الإذن',
    'api_authorization_placeholder'=>'ادخل رمز الإذن',

    'project_api_index'=>' رابط فهرس المشروعات',
    'project_api_index_placeholder'=>'ادخل رابط فهرس المشروعات',

    'phase_api_index'=>' رابط فهرس المراحل',
    'phase_api_index_placeholder'=>'ادخل رابط فهرس المراحل',

    'zone_api_index'=>' رابط فهرس المناطق',
    'zone_api_index_placeholder'=>'ادخل رابط فهرس المناطق',

    'block_api_index'=>' رابط فهرس القطع',
    'block_api_index_placeholder'=>'ادخل رابط فهرس القطع',

    'unit_model_api_index'=>' رابط فهرس نماذج الوحدات',
    'unit_model_api_index_placeholder'=>'ادخل رابط فهرس نماذج الوحدات',

    'unit_api_index'=>' رابط فهرس الوحدات',
    'unit_api_index_placeholder'=>'ادخل رابط فهرس الوحدات',

    'customer_api_index'=>' رابط فهرس العملاء',
    'customer_api_index_placeholder'=>'ادخل رابط فهرس العملاء',
    'customer_api_store'=>' رابط إضافة العملاء',
    'customer_api_store_placeholder'=>'ادخل رابط إضافة العملاء',
    'customer_api_update'=>' رابط تعديل العملاء',
    'customer_api_update_placeholder'=>'ادخل رابط تعديل العملاء',
    'customer_api_destroy'=>' رابط حذف العميل',
    'customer_api_destroy_placeholder'=>'ادخل رابط حذف العميل',

    'bank_api_index'=>' رابط فهرس البنوك',
    'bank_api_index_placeholder'=>'ادخل رابط فهرس البنوك',

    'reservation_api_index'=>' رابط فهرس الحجوزات',
    'reservation_api_index_placeholder'=>'ادخل رابط فهرس الحجوزات',
    'reservation_api_store'=>' رابط إضافة الحجوزات',
    'reservation_api_store_placeholder'=>'ادخل رابط إضافة الحجوزات',
    'reservation_api_update'=>' رابط تعديل الحجوزات',
    'reservation_api_update_placeholder'=>'ادخل رابط تعديل الحجوزات',
    'reservation_api_destroy'=>' رابط حذف الحجز',
    'reservation_api_destroy_placeholder'=>'ادخل رابط حذف الحجز',





    'notes' => 'الملاحظات',
    'notes_placeholder' => ' ادخل الملاحظات',
    'created_at' => 'تاريخ الإضافة',
    'updated_at' => 'تاريخ آخر تعديل',
    'creator_name' => 'تمت الإضافة بوسطة',
    'last_updater_name' => 'آخر تعديل تم بوسطة',
    'save' => 'حفظ',
    'add_net_suite_integration' => 'إضافة مطور عقاري جديد',


    'edit_net_suite_integration' => 'تعديل النتسويت',
    'delete_net_suite_integration' => 'حذف النتسويت',
    'projects' => 'المشروعات',
    'api_show'=> ' وجهة تحديث التطبيق',
    'entity'=>'الكيان',
    'url'=>'الرابط',
    'last_syncing_date_time'=>'تاريخ آخر وقت للمزامنة',
    'synchronize'=>'مزامنة',


    'all_net_suite_integrations' => 'كل المطورين العقاريين',
    'filtering' => 'ترشيح',
    'filtering_when_loading' => 'تحميل... ',
    'searching_on_net_suite_integration' => 'البحث عن النتسويت',
    'search' => 'البحث',
    'searching_word' => 'كلمة البحث',
    'confirm_delete_message' => 'هل ترغب في حذف النتسويت',

    'searching_button_searching' => 'جاري البحث..',
    'searching_button_search' => 'ابحث',
    'searching_button_required' => 'برجاء إدخال الكلمة المراد البحث عنها...',

    'select_net_suite_integration' => 'المزامنة مع دمج النتسويت',
    'select_net_suite_integration_placeholder' => 'اختار دمج النتسويت ',

];
