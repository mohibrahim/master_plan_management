<?php

return [
    'id' => 'كود المرحلة',
    'name' => 'اسم المرحلة',
    'name_placeholder' => ' ادخل اسم المرحلة',
    'supposed_start_date' => 'تاريخ البدء المقرر',
    'supposed_start_date_placeholder' => ' ادخل تاريخ البدء المقرر',
    'actually_start_date' => 'تاريخ البدء الفعلي',
    'actually_start_date_placeholder' => ' ادخل تاريخ البدء الفعلي',
    'supposed_end_date' => 'تاريخ الانتهاء المقرر',
    'supposed_end_date_placeholder' => ' ادخل تاريخ الانتهاء المقرر',
    'actually_end_date' => 'تاريخ الانتهاء الافعلي',
    'actually_end_date_placeholder' => ' ادخل تاريخ الانتهاء الفعلي',
    'percentage_of_completion' => 'النسبة المئوية للإتمام',
    'percentage_of_completion_placeholder' => ' ادخل النسبة المئوية للإتمام',
    'notes' => 'الملاحظات',
    'notes_placeholder' => ' ادخل الملاحظات',

    'project_name' => 'اسم المشروع',
    'project_name_placeholder' => ' اختار اسم المشروع',


    'created_at' => 'تاريخ الإضافة',
    'updated_at' => 'تاريخ آخر تعديل',
    'creator_name' => 'تمت الإضافة بوسطة',
    'last_updater_name' => 'آخر تعديل تم بوسطة',
    'save' => 'حفظ',
    'add_phase' => 'إضافة مرحلة جديد',
    'edit_phase' => 'تعديل المرحلة',
    'delete_phase' => 'حذف المرحلة',
    'add_catalog' => 'إضافة كتالوج جديد للمرحلة',
    'all_phases' => 'كل المراحل',
    'searching_on_phase' => 'البحث عن المرحلة',
    'search' => 'البحث',
    'searching_word' => 'كلمة البحث',
    'confirm_delete_message' => 'هل ترغب في حذف المرحلة',

    'searching_button_searching' => 'جاري البحث..',
    'searching_button_search' => 'ابحث',
    'searching_button_required' => 'برجاء إدخال الكلمة المراد البحث عنها...',

    'phase_sync_create_message' => 'المراحل التي تم إضافتها:  ',
    'phase_sync_update_message' => 'المراحل التي تم تحديثها:  ',
    'phase_sync_delete_message' => 'المراحل التي تم حذفها:  ',

];
