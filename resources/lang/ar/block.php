<?php

return [
    'id' => 'كود القطعة',
    'name' => 'اسم القطعة',
    'name_placeholder' => ' ادخل اسم القطعة',
    'notes' => 'الملاحظات',
    'notes_placeholder' => ' ادخل الملاحظات',

    'project_name' => 'اسم المشروع',
    'project_name_placeholder' => ' اختار اسم المشروع',
    'zone_name' => 'اسم المنطقة',
    'zone_name_placeholder' => ' اختار اسم المنطقة',


    'created_at' => 'تاريخ الإضافة',
    'updated_at' => 'تاريخ آخر تعديل',
    'creator_name' => 'تمت الإضافة بوسطة',
    'last_updater_name' => 'آخر تعديل تم بوسطة',
    'save' => 'حفظ',
    'add_block' => 'إضافة قطعة جديد',
    'edit_block' => 'تعديل القطعة',
    'delete_block' => 'حذف القطعة',
    'add_catalog' => 'إضافة كتالوج جديد للقطعة',
    'all_blocks' => 'كل القطع',
    'searching_on_block' => 'البحث عن القطعة',
    'search' => 'البحث',
    'searching_word' => 'كلمة البحث',
    'confirm_delete_message' => 'هل ترغب في حذف القطعة',

    'searching_button_searching' => 'جاري البحث..',
    'searching_button_search' => 'ابحث',
    'searching_button_required' => 'برجاء إدخال الكلمة المراد البحث عنها...',

    'block_sync_create_message' => 'القطع التي تم إضافتها:  ',
    'block_sync_update_message' => 'القطع التي تم تحديثها:  ',
    'block_sync_delete_message' => 'القطع التي تم حذفها:  ',

];
