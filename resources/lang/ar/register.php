<?php

return [
    'name'=>'الاسم',
    'email'=>'عنوان بريد الكتروني',
    'password'=>'كلمة السر',
    'password_confirm'=>'تأكيد كلمة السر',
    'remember_me'=>'تذكرنى',
    'forgot_your_password'=>'نسيت رقمك السري؟',
    'login'=>'تسجيل دخول',
    'register'=>'الإشتراك',
];