<?php

return [
    'id' => 'كود البنك',
    'name' => 'اسم البنك',
    'name_placeholder' => ' ادخل اسم البنك',
    'notes' => 'الملاحظات',
    'notes_placeholder' => ' ادخل الملاحظات',


    'created_at' => 'تاريخ الإضافة',
    'updated_at' => 'تاريخ آخر تعديل',
    'save' => 'حفظ',
    'add_bank' => 'إضافة بنك جديد',
    'edit_bank' => 'تعديل البنك',
    'delete_bank' => 'حذف البنك',
    'add_catalog' => 'إضافة كتالوج جديد للبنك',
    'all_banks' => 'كل البنوك',
    'searching_on_bank' => 'البحث عن البنك',
    'search' => 'البحث',
    'searching_word' => 'كلمة البحث',
    'confirm_delete_message' => 'هل ترغب في حذف البنك',

    'searching_button_searching' => 'جاري البحث..',
    'searching_button_search' => 'ابحث',
    'searching_button_required' => 'برجاء إدخال الكلمة المراد البحث عنها...',

    'bank_sync_create_message' => 'البنوك التي تم إضافتها:  ',
    'bank_sync_update_message' => 'البنوك التي تم تحديثها:  ',
    'bank_sync_delete_message' => 'البنوك التي تم حذفها:  ',

];
