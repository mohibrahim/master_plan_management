<?php

return [
    'id' => 'رقم العميل',
    'name' => 'اسم العميل',
    'name_placeholder' => ' ادخل اسم العميل',
    'phone' => 'رقم تليفون العميل الأول',
    'phone_placeholder' => 'ادخل رقم تليفون العميل الأول',
    'email' => 'البريد الإلكتروني العميل',
    'email_placeholder' => ' ادخل البريد الإلكتروني العميل',
    'address' => 'عنوان العميل',
    'address_placeholder' => ' ادخل عنوان العميل',
    'national_id' => 'رقم الهوية الوطنية للعميل',
    'national_id_placeholder' => ' ادخل رقم الهوية الوطنية للعميل',
    'notes' => 'الملاحظات',
    'notes_placeholder' => ' ادخل الملاحظات',

    'customer_code' => 'كود العميل',
    'customer_code_placeholder' => ' ادخل كود العميل',
    'vat_registration_number' => 'رقم السجل الضريبي',
    'vat_registration_number_placeholder' => ' ادخل رقم السجل الضريبي',
    'vat_registration_expiry' => 'تاريخ إنتهاء السجل الضريبي',
    'vat_registration_expiry_placeholder' => ' ادخل تاريخ إنتهاء السجل الضريبي',
    'secondary_phone' => 'رقم تليفون العميل الثاني',
    'secondary_phone_placeholder' => 'ادخل رقم تليفون العميل الثاني',
    'alt_phone' => 'رقم التليفون البديل',
    'alt_phone_placeholder' => ' ادخل رقم التليفون البديل',



    'created_at' => 'تاريخ الإضافة',
    'updated_at' => 'تاريخ آخر تعديل',
    'creator_name' => 'تمت الإضافة بوسطة',
    'last_updater_name' => 'آخر تعديل تم بوسطة',
    'save' => 'حفظ',
    'add_customer' => 'إضافة عميل جديد',
    'edit_customer' => 'تعديل العميل',
    'delete_customer' => 'حذف العميل',
    'reserved_units' => 'الوحدات المحجوزة',
    'reservation_link' => 'رابط الحجز',
    'unit_code' => 'كود الوحدة',
    'all_customers' => 'كل العملاء',
    'searching_on_customer' => 'البحث عن العميل',
    'search' => 'البحث',
    'searching_word' => 'كلمة البحث',
    'confirm_delete_message' => 'هل ترغب في حذف العميل',

    'searching_button_searching' => 'جاري البحث..',
    'searching_button_search' => 'ابحث',
    'searching_button_required' => 'برجاء إدخال الكلمة المراد البحث عنها...',

    'customer_sync_create_message' => 'العملاء الذين تم إضافتهم:  ',
    'customer_sync_update_message' => 'العملاء الذين تم تحديثهم:  ',
    'customer_sync_delete_message' => 'العملاء الذين تم حذفهم:  ',

];
