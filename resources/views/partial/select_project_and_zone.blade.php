<div class="card my-3">
    <div class="card-body">

        <div class="form-group">
            <span style="color:#f47373">*</span>
            <label for="project-id">{{__($modelName.'.project_name')}}</label>
            <select class="form-control" name="project_id" id="project-id">
                @if (old('project_id') && old('old_selected_project_name'))
                    <option value="{{old('project_id')}}">{{old('old_selected_project_name')}}</option>
                @elseif (!empty($$modelName->selected_project_name_id))
                    <option value="{{$$modelName->selected_project_name_id['id']}}">{{$$modelName->selected_project_name_id['name']}}</option>
                @endif
            </select>
            <input type="hidden" id="project-name-placeholder" value="{{__($modelName.'.project_name_placeholder')}}">
            <input type="hidden" name="old_selected_project_name" id="old-selected-project-name" value="">
        </div>

        @if($showSelectZone)
            <div class='form-group'>
                <span style='color:#f47373'>*</span>
                {!!Form::label('zone_id', __($modelName.'.zone_name'))!!}
                {!!Form::select('zone_id',$$modelName->selectedProjectZonesNamesIds, null,['id'=>'zone-id','class'=>'form-control', 'placeholder'=>__($modelName.'.zone_name_placeholder')]) !!}
                <input type="hidden" id="zone-name-placeholder" value="{{__($modelName.'.zone_name_placeholder')}}">
                <input type="hidden" name="old_selected_zone_name" id="old-selected-zone-name" value="">
            </div>
        @endif

    </div>
</div>