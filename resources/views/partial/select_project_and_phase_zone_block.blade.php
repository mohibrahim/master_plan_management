<div class="card border-secondary my-3">
    <div class="card-body text-secondary">
        <h5 class="card-title">{{__('unit.unit_classification')}}</h5>
        <div class="row">



            <div class="col-md-4 col-lg-4">

                <div class="form-group">
                    <span style="color:#f47373">*</span>
                    <label for="project-id">{{__($modelName.'.project_name')}}</label>
                    <select class="form-control" name="project_id" id="project-id">
                        @if (old('project_id') && old('old_selected_project_name'))
                            <option value="{{old('project_id')}}">{{old('old_selected_project_name')}}</option>
                        @elseif (!empty($$modelName->selected_project_name_id))
                            <option value="{{$$modelName->selected_project_name_id['id']}}">{{$$modelName->selected_project_name_id['name']}}</option>
                        @endif
                    </select>
                    <input type="hidden" id="project-name-placeholder" value="{{__($modelName.'.project_name_placeholder')}}">
                    <input type="hidden" name="old_selected_project_name" id="old-selected-project-name" value="">
                </div>

                @if($showSelectBlock)
                    <div class='form-group'>
                        {!!Form::label('block_id', __($modelName.'.block_name'))!!}
                        {!!Form::select('block_id',$$modelName->selectedProjectBlocksNamesIds, null,['id'=>'block-id','class'=>'form-control', 'placeholder'=>__($modelName.'.block_name_placeholder')]) !!}
                        <input type="hidden" id="block-name-placeholder" value="{{__($modelName.'.block_name_placeholder')}}">
                        <input type="hidden" name="old_selected_block_name" id="old-selected-block-name" value="">
                    </div>
                @endif

            </div>

            <div class="col-md-4 col-lg-4">
                @if($showSelectPhase)
                    <div class='form-group'>
                        <span style='color:#f47373'>*</span>
                        {!!Form::label('phase_id', __($modelName.'.phase_name'))!!}
                        {!!Form::select('phase_id',$$modelName->selectedProjectPhasesNamesIds, null,['id'=>'phase-id','class'=>'form-control', 'placeholder'=>__($modelName.'.phase_name_placeholder')]) !!}
                        <input type="hidden" id="phase-name-placeholder" value="{{__($modelName.'.phase_name_placeholder')}}">
                        <input type="hidden" name="old_selected_phase_name" id="old-selected-phase-name" value="">
                    </div>
                @endif
                @if($showSelectUnitModel)
                    <div class="form-group">
                        <span style="color:#f47373">*</span>
                        <label for="unit-model-id">{{__('unit.unit_model_name')}}</label>
                        <select class="form-control" name="unit_model_id" id="unit-model-id">
                            @if (old('unit_model_id') && old('old_selected_unit_model_name'))
                                <option value="{{old('unit_model_id')}}">{{old('old_selected_unit_model_name')}}</option>
                            @elseif (!empty($unit->selected_unit_model_name_id))
                                <option value="{{$unit->selected_unit_model_name_id['id']}}">{{$unit->selected_unit_model_name_id['name']}}</option>
                            @endif
                        </select>
                        <input type="hidden" id="unit-model-name-placeholder" value="{{__('unit.unit_model_name_placeholder')}}">
                        <input type="hidden" name="old_selected_unit_model_name" id="old-selected-unit-model-name" value="">
                    </div>
                @endif

            </div>

            <div class="col-md-4 col-lg-4">

                @if($showSelectZone)
                    <div class='form-group'>
                        <span style='color:#f47373'>*</span>
                        {!!Form::label('zone_id', __($modelName.'.zone_name'))!!}
                        {!!Form::select('zone_id',$$modelName->selectedProjectZonesNamesIds, null,['id'=>'zone-id','class'=>'form-control', 'placeholder'=>__($modelName.'.zone_name_placeholder')]) !!}
                        <input type="hidden" id="zone-name-placeholder" value="{{__($modelName.'.zone_name_placeholder')}}">
                        <input type="hidden" name="old_selected_zone_name" id="old-selected-zone-name" value="">
                    </div>
                @endif

            </div>

            <div class="col">
                <div class='form-group'>
                    {!!Form::label('html-area-tag-coords', __('unit.html_area_tag_coords'))!!}
                    {!!Form::textarea('html_area_tag_coords', null, ['id'=> 'html-area-tag-coords', 'class'=>'form-control', 'rows'=>'5',
                    'placeholder'=>__('unit.html_area_tag_coords_placeholder')])!!}
                </div>
            </div>
        </div>

    </div>
</div>
