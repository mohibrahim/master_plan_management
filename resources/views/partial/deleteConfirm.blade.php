<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">{{__('partial.confirm_delete')}}</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
					<strong>{{__('partial.warning')}}</strong> {{ $message}}, <span style="color:red;">{{$name}}</span>.<br><br>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('partial.close')}}</button>
					<form method="POST" action="{{action($route,['id'=>$id])}}">
						<input type="hidden" name="_method" value="DELETE">
						<input type="hidden" name="_token" value="{{csrf_token()}}">
						<button class="btn btn-danger" type="submit" style="float: right;">{{__('partial.delete')}}</button>
					</form>
			</div>
		</div>
	</div>
</div>