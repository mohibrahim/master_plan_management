@extends('layouts.app')
@section('title')
	{{__('block.edit_block').' | '.$block->name}}
@endsection
@section('content')
	<div class="container-fluid">
		<div class="col">
			<div class="card card-info">
				<div class="card-heading">
					<h3 class="card-header text-center">{{$block->name}}</h3>
				</div>
				<div class="card-body ">
					@include('errors.list')
					{!! Form::model($block, ['action' => ['BlockController@update', 'id'=>$block->id], 'files'=>true,
					'method'=>'put'])!!}
					@include('blocks._form')
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
@endsection

