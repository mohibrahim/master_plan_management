@extends('layouts.app')

@section('title')
	{{$block->name}}
@endsection

@section('content')
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h2 class="text-center">{{$block->name}}</h2>
			</div>
			<div class="card-body ">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-4 text-left">
							<img src="{{url('images/helper_images/block_cover.jpg')}}" class="rounded w-100 border border-default mb-3" alt="">
							<div class=" row text-muted">
								<div class="col small">
									{{__('block.created_at')}}:
								</div>
								<div class="col small">
									{{$block->created_at}}
								</div>
							</div>
							<div class=" row text-muted">
								<div class="col small">
									{{__('block.creator_name')}}:
								</div>
								<div class="col small">
									{{optional($block->creator)->name}}
								</div>
							</div>
							<div class=" row text-muted">
								<div class="col small">
									{{__('block.updated_at')}}:
								</div>
								<div class="col small">
									{{$block->updated_at}}
								</div>
							</div>
							<div class=" row text-muted">
								<div class="col small">
									{{__('block.last_updater_name')}}:
								</div>
								<div class="col small">
									{{optional($block->lastUpdater)->name}}
								</div>
							</div>
							<div class="row mt-3">
								<div class="col">
									@if(in_array('update_blocks', $permissions))
										<a href="{{ action('BlockController@edit', ['id'=>$block->id]) }}" class="btn btn-sm btn-warning btn-block"><i class="fas fa-edit"></i> {{__('block.edit_block')}}
										</a>
									@endif
									@if(in_array('destroy_blocks', $permissions))
										<td><button type="button" class="btn btn-sm btn-danger btn-block" data-toggle="modal" data-target="#myModal"><i class="far fa-trash-alt"></i> {{__('block.delete_block')}} </button></td>
									@endif
									@if(	in_array('store_catalogs', $permissions) &&
											in_array('store_blocks', $permissions)
										)
										<a href="{{ action('CatalogController@create', ['model_associated_type'=>'block_id', 'model_associated_type_id'=>$block->id]) }}" class="btn btn-sm btn-primary btn-block"><i class="fas fa-images"></i> {{__('block.add_catalog')}}
										</a>
									@endif
								</div>

							</div>
						</div>
						<div class="col">

							<div class="container-fluid">
								<div class="row">
									<div class="col">
										<table class="table">
											<tbody>
												<tr>
													<th scope="row" class="border-0">{{__('block.project_name')}}</th>
													<td class="border-0">{!!optional($block->project)->project_show_link!!}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('block.zone_name')}}</th>
													<td class="border-0">{!!optional($block->zone)->zone_show_link!!}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('block.id')}}</th>
													<td class="border-0">{{$block->id}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('block.name')}}</th>
													<td class="border-0">{{$block->name}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('block.notes')}}</th>
													<td class="border-0">{{$block->notes}}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<hr>
							</div>

						</div>
					</div>


					<div class="row mt-3">
						<div class="col">
							<div class="card">
								<div class="card-body">
									<h4 class="font-weight-bold text-center m-3"> الكتالوجات </h4>

									<table class="table table-hover" id="catalogs">
										<thead>
											<tr>
												<th>#</th>
												<th>{{__('catalog.id')}}</th>
												<th>{{__('catalog.name')}}</th>
												<th>{{__('catalog.description')}}</th>
												<th>{{__('catalog.created_at')}}</th>
												<th>{{__('catalog.creator_name')}}</th>
											</tr>
										</thead>
										<tbody>
											@foreach($block->catalogs as $catalogKey => $catalog)
												<tr>
													<td>{{$catalogKey+1}}</td>
													<td>{{$catalog->id}}</td>
													<td>
														<a href="{{action('CatalogController@show', ['id'=>$catalog->id])}}">
															{{$catalog->name}}
														</a>
													</td>
													<td>{{$catalog->description}}</td>
													<td>{{$catalog->created_at}}</td>
													<td>{{optional($catalog->creator)->name}}</td>

												</tr>
											@endforeach
										</tbody>
									</table>
									<input id="data-table-search" type="hidden" value="{{__('data_table.search')}}">
									<input id="data-table-zero-record" type="hidden" value="{{__('data_table.zeroRecords')}}">
								</div>
							</div>
						</div>
					</div>

					<div class="row mt-3">
						<div class="col">
							<div class="card">
								<div class="card-body">
									<h4 class="font-weight-bold text-center m-3"> الوحدات </h4>

									<table class="table table-hover" id="units">
										<thead>
											<tr>
												<th>#</th>
												<th>{{__('unit.project_name')}}</th>
												<th>{{__('unit.id')}}</th>
												<th>{{__('unit.code')}}</th>
												<th>{{__('unit.status')}}</th>
												<th>{{__('unit.created_at')}}</th>
												<th>{{__('unit.creator_name')}}</th>
											</tr>
										</thead>
										<tbody>
											@foreach($block->units as $unitKey => $unit)
												<tr>
													<td>{{$unitKey+1}}</td>
													<td>
														<a href="{{action('ProjectController@show', ['id'=>optional($unit->project)->id])}}">
															{{optional($unit->project)->name}}
														</a>
													</td>
													<td>{{$unit->id}}</td>
													<td>
														<a href="{{action('UnitController@show', ['id'=>$unit->id])}}">
															{{$unit->code}}
														</a>
													</td>
													<td>{{$unit->status}}</td>
													<td>{{$unit->created_at}}</td>
													<td>{{optional($unit->creator)->name}}</td>

												</tr>
											@endforeach
										</tbody>
									</table>
									<input id="data-table-search" type="hidden" value="{{__('data_table.search')}}">
									<input id="data-table-zero-record" type="hidden" value="{{__('data_table.zeroRecords')}}">
								</div>
							</div>
						</div>
					</div>


				</div>
			</div>
		</div>

	</div>

	@include('partial.deleteConfirm',['name'=>$block->name,
									  'id'=> $block->id,
									  'message'=> __('block.confirm_delete_message'),
									  'route'=>'BlockController@destroy'])
@endsection
