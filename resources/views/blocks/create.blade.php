@extends('layouts.app')
@section('title')
	{{__('block.add_block')}}
@endsection
	@section('content')
	<div class="container-fluid">
		<div class="col">
			<div class="card card-info">
				<div class="card-heading">
					<h3 class="card-header text-center">{{__('block.add_block')}}</h3>
				</div>
				<div class="card-body ">
					@include('errors.list')
					{!! Form::open(['action' => ['BlockController@store'], 'files'=>true, 'method'=>'POST'])!!}
						@include('blocks._form')
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
@endsection
