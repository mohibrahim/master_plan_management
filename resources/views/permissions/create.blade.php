@extends('layouts/app')
@section('title')
	Create Permission
@endsection
@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="card">
			<div class="card-header">
				Add New Permission
			</div>
				<div class="card-body">
					@include('errors.list')
					<form class="" action="{{ action('PermissionController@store') }}" method="post">
						{{ csrf_field() }}
						@include('permissions._form')
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection()
