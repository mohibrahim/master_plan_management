@extends('layouts.app')
@section('title')
    All Permission
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    All Permissions
                </div>
                <div class="card-body ">

                    <table class="table table-hover">
                        <thead>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Name</th>
                        </thead>
                        <tbody>
                            @foreach($permissions as $permission)
                                <tr>
                                    <td>{{$permission->id}}</td>
                                    <td>{{$permission->title}}</td>
                                    <td><a href="{{ action('PermissionController@show',['id'=>$permission->id]) }}" >{{$permission->name}}</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>


                </div>
            </div>

        </div>
    </div>
@endsection
