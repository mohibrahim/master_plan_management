<div class="form-group">
	<label for="title">Title:</label>
	<input type="text" name="title" value="{{ isset($permission)?($permission->title):('') }}" class="form-control" placeholder="Enter Permission Title">
</div>

<div class="form-group">
	<label for="name">Name:</label>
	<input type="text" name="name" value="{{ isset($permission)?($permission->name):('') }}" class="form-control" placeholder="Enter Permission Name">
</div>

<div class="form-group">
  <label for="descriptions">Descriptions</label>
  <textarea class="form-control" name="descriptions" id="descriptions" rows="3" placeholder="Enter Permissions descriptions.">{{ isset($permission)?($permission->descriptions):('') }}</textarea>
</div>

<div class="form-group">
	<input type="submit" name="" value="Save" class="btn btn-primary">
</div>
