@extends('layouts.app')
@section('title')
	Edit Permission
@endsection
@section('content')
	<div class="row">
	<div class="col-md-12">
		<div class="card ">
		<div class="card-header">
			Update Permission
		</div>
			<div class="card-body ">
				@include('errors.list')
				<form class="form" action="{{action('PermissionController@update', [ 'id'=>$permission->id])}}" method="POST">
					{{ csrf_field() }}
					<input type="hidden" name="_method" value="PATCH">
					@include('permissions._form')
				</form>
			</div>
		</div>

	</div>
</div>
@endsection
