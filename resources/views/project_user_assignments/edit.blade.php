@extends('layouts.app')
@section('title')
	{{__('project_user_assignment.edit_project_user_assignment')}}
@endsection
@section('content')
	<div class="container-fluid">
		<div class="col">
			<div class="card card-info">
				<div class="card-heading">
					<h3 class="card-header text-center">{{$projectUserAssignment->id}}</h3>
				</div>
				<div class="card-body ">
					@include('errors.list')
					{!! Form::model($projectUserAssignment, ['action' => ['ProjectUserAssignmentController@update', 'id'=>$projectUserAssignment->id], 'files'=>true,
					'method'=>'put'])!!}
					@include('project_user_assignments._form')
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
@endsection

