@extends('layouts.app')
@section('title')
	{{__('project_user_assignment.all_project_user_assignments')}}
@endsection
@section('content')
	<div class="col-md-12">
		<div class="card">

			<div class="card-header">
				{{__('project_user_assignment.all_project_user_assignments')}}
			</div>
				<div class="card-body">
					<div class="form-inline">
						<div class="input-group mb-2 mr-sm-2">
							<label class="m-2" for="searching-input">{{__('project_user_assignment.searching_on_project_user_assignment')}}: </label>
							<input type="text" class="form-control" id="searching-input" placeholder="{{__('project_user_assignment.searching_word')}}" autofocus>
						</div>

						<button id="searching-button" type="button" class="btn btn-primary mb-2">{{__('project_user_assignment.search')}}</button>
						<input id="searching-button-required" type="hidden" value="{{__('project_user_assignment.searching_button_required')}}">
						<input id="searching-button-searching" type="hidden" value="{{__('project_user_assignment.searching_button_searching')}}">
						<input id="searching-button-search" type="hidden" value="{{__('project_user_assignment.searching_button_search')}}">
					</div>
					<hr>
					<table class="table table-hover" id="project-user-assignment">
						<thead>
							<tr>
								<th>#</th>
								<th>{{__('project_user_assignment.project_name')}}</th>
								<th>{{__('project_user_assignment.employee_name')}}</th>
								<th>{{__('project_user_assignment.id')}}</th>
								<th>{{__('project_user_assignment.created_at')}}</th>
								<th>{{__('project_user_assignment.creator_name')}}</th>
							</tr>
						</thead>
						<tbody>
							@foreach($projectUserAssignments as $projectUserAssignmentKey => $projectUserAssignment)
								<tr>
									<td>{{$projectUserAssignmentKey+1}}</td>
									<td>{{optional($projectUserAssignment->project)->name}}</td>
									<td>{{optional($projectUserAssignment->employee)->name}}</td>
									<td>
										<a href="{{action('ProjectUserAssignmentController@show', ['id'=>$projectUserAssignment->id])}}">
											{{$projectUserAssignment->id}}
										</a>
									</td>
									<td>{{$projectUserAssignment->created_at}}</td>
									<td>{{optional($projectUserAssignment->creator)->name}}</td>

								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="d-flex justify-content-center">
					{{$projectUserAssignments->links()}}
				</div>
			</div>

			<input id="data-table-search" type="hidden" value="{{__('data_table.search')}}">
			<input id="data-table-zero-record" type="hidden" value="{{__('data_table.zeroRecords')}}">
	</div>
@endsection

@section('head')
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css"> -->
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
@endsection
@section('js_footer')
	<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
	<script>
		$(document).ready(function () {
			dataTable = $('#project-user-assignment').DataTable({
				"paging":   false,
				"info":     false,
				"language": {
					"search": $("#data-table-search").val(),
					"zeroRecords": $("#data-table-zero-record").val(),
				},
			});
			searchingOnProjectUserAssignmentsAjax();
		});
	</script>
@endsection
