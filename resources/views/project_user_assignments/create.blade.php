@extends('layouts.app')
@section('title')
	{{__('project_user_assignment.add_project_user_assignment')}}
@endsection
	@section('content')
	<div class="container-fluid">
		<div class="col">
			<div class="card card-info">
				<div class="card-heading">
					<h3 class="card-header text-center">{{__('project_user_assignment.add_project_user_assignment')}}</h3>
				</div>
				<div class="card-body ">
					@include('errors.list')
					{!! Form::open(['action' => ['ProjectUserAssignmentController@store'], 'files'=>true, 'method'=>'POST'])!!}
						@include('project_user_assignments._form')
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
@endsection
