@extends('layouts.app')

@section('title')
	{{$projectUserAssignment->id}}
@endsection

@section('content')
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h2 class="text-center">{{$projectUserAssignment->id}}</h2>
			</div>
			<div class="card-body ">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-4 text-left">
							<img src="{{url('images/helper_images/assignments.jpg')}}" class="rounded w-100 border border-default mb-3" alt="">
							<div class=" row text-muted">
								<div class="col small">
									{{__('project_user_assignment.created_at')}}:
								</div>
								<div class="col small">
									{{$projectUserAssignment->created_at}}
								</div>
							</div>
							<div class=" row text-muted">
								<div class="col small">
									{{__('project_user_assignment.creator_name')}}:
								</div>
								<div class="col small">
									{{optional($projectUserAssignment->creator)->name}}
								</div>
							</div>
							<div class=" row text-muted">
								<div class="col small">
									{{__('project_user_assignment.updated_at')}}:
								</div>
								<div class="col small">
									{{$projectUserAssignment->updated_at}}
								</div>
							</div>
							<div class=" row text-muted">
								<div class="col small">
									{{__('project_user_assignment.last_updater_name')}}:
								</div>
								<div class="col small">
									{{optional($projectUserAssignment->lastUpdater)->name}}
								</div>
							</div>
							<div class="row mt-3">
								<div class="col">
									@if(in_array('update_project_user_assignments', $permissions))
										<a href="{{ action('ProjectUserAssignmentController@edit', ['id'=>$projectUserAssignment->id]) }}" class="btn btn-sm btn-warning btn-block"><i class="fas fa-edit"></i> {{__('project_user_assignment.edit_project_user_assignment')}}
										</a>
									@endif
									@if(in_array('destroy_project_user_assignments', $permissions))
										<td><button type="button" class="btn btn-sm btn-danger btn-block" data-toggle="modal" data-target="#myModal"><i class="far fa-trash-alt"></i> {{__('project_user_assignment.delete_project_user_assignment')}} </button></td>
									@endif
								</div>

							</div>
						</div>
						<div class="col">

							<div class="container-fluid">
								<div class="row">
									<div class="col">
										<table class="table">
											<tbody>
												<tr>
													<th scope="row" class="border-0">{{__('project_user_assignment.project_name')}}</th>
													<td class="border-0">
													@php
														$array = $projectUserAssignment->selected_projects_names_ids;
														end($array);
														$endKey = key($array);
													@endphp
														@foreach ($array as $projectId => $projectName)
															<a href="{{action('ProjectController@show', ['id'=>$projectId])}}">
																{{$projectName}}
															</a>
															@if($endKey != $projectId)
															|
															@endif
														@endforeach
													</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('project_user_assignment.employee_name')}}</th>
													<td class="border-0">
														<a href="{{action('UserController@show', ['id'=>optional($projectUserAssignment->employee)->id])}}">
															{{optional($projectUserAssignment->employee)->name}}
														</a>
													</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('project_user_assignment.id')}}</th>
													<td class="border-0">{{$projectUserAssignment->id}}</td>
												</tr>

												<tr>
													<th scope="row" class="border-0">{{__('project_user_assignment.notes')}}</th>
													<td class="border-0">{{$projectUserAssignment->notes}}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<hr>
							</div>

						</div>
					</div>

				</div>
			</div>
		</div>

	</div>

	@include('partial.deleteConfirm',['name'=>optional($projectUserAssignment->employee)->name,
									  'id'=> $projectUserAssignment->id,
									  'message'=> __('project_user_assignment.confirm_delete_message'),
									  'route'=>'ProjectUserAssignmentController@destroy'])
@endsection
