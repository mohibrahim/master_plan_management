<div class="container-fluid border">
	<div class="row">
		<div class="col">


			<div class="card my-3">
				<div class="card-body">
					<div class="form-group" id="project-form-group">
						<span style="color:#f47373">*</span>
						<label for="project-id">{{__('project_user_assignment.project_name')}}</label>
						<select class="form-control" name="project_id[]" id="project-id" multiple="multiple">
							@if (old('project_id') && old('old_selected_projects_names'))
								@foreach(old('old_selected_projects_names') as $oldSelectProjectNameKey => $oldSelectProjectName)
									<option value="{{old('project_id')[$oldSelectProjectNameKey]}}" selected>{{$oldSelectProjectName}}</option>
								@endforeach
							@elseif (!empty($projectUserAssignment->selected_projects_names_ids))
								@foreach ($projectUserAssignment->selected_projects_names_ids as $projectId => $projectName)
									<option value="{{$projectId}}" selected>{{$projectName}}</option>
								@endforeach
							@endif
						</select>
						<input type="hidden" id="project-name-placeholder" value="{{__('project_user_assignment.project_name_placeholder')}}">



					</div>
				</div>
			</div>

			<div class="card my-3">
				<div class="card-body">
					<div class="form-group">
						<span style="color:#f47373">*</span>
						<label for="employee-id">{{__('project_user_assignment.employee_name')}}</label>
						<select class="form-control" name="employee_id" id="employee-id">
							@if (old('employee_id') && old('old_selected_employee_name'))
								<option value="{{old('employee_id')}}">{{old('old_selected_employee_name')}}</option>
							@elseif (!empty($projectUserAssignment->selected_employee_name_id))
								<option value="{{$projectUserAssignment->selected_employee_name_id['id']}}">{{$projectUserAssignment->selected_employee_name_id['name']}}</option>
							@endif
						</select>
						<input type="hidden" id="employee-name-placeholder" value="{{__('project_user_assignment.employee_name_placeholder')}}">
						<input type="hidden" name="old_selected_employee_name" id="old-selected-employee-name" value="">
					</div>
				</div>
			</div>


			<div class='form-group'>
				{!!Form::label('notes', __('project_user_assignment.notes'))!!}
				{!!Form::textarea('notes', null, ['class'=>'form-control', 'rows'=>'5',
				'placeholder'=>__('project_user_assignment.notes_placeholder')])!!}
			</div>
		</div>

	</div>
</div>

<div class="form-group">
	{!!Form::submit(__('reservation.save'), ['class'=>'btn btn-primary form-control'])!!}
</div>


@section('head')
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
@endsection
@section('js_footer')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
	<script>
		$(document).ready(function () {
			projectUserAssignmentSelectProjectId();
			projectUserAssignmentSelectEmployeeId();
		});
	</script>
@endsection
