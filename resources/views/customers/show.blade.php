@extends('layouts.app')

@section('title')
	{{$customer->name}}
@endsection

@section('content')
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h2 class="text-center">{{$customer->name}}</h2>
			</div>
			<div class="card-body ">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-4 text-left">
							<img src="{{url('images/helper_images/customer_cover.jpg')}}" class="rounded w-75 border border-default" alt="">
							<div class=" row text-muted">
								<div class="col small">
									{{__('customer.created_at')}}:
								</div>
								<div class="col small">
									{{$customer->created_at}}
								</div>
							</div>
							<div class=" row text-muted">
								<div class="col small">
									{{__('customer.creator_name')}}:
								</div>
								<div class="col small">
									{{optional($customer->creator)->name}}
								</div>
							</div>
							<div class=" row text-muted">
								<div class="col small">
									{{__('customer.updated_at')}}:
								</div>
								<div class="col small">
									{{$customer->updated_at}}
								</div>
							</div>
							<div class=" row text-muted">
								<div class="col small">
									{{__('customer.last_updater_name')}}:
								</div>
								<div class="col small">
									{{optional($customer->lastUpdater)->name}}
								</div>
							</div>
							<div class="row mt-3">
								<div class="col">
									@if(in_array('update_customers', $permissions))
										<a href="{{ action('CustomerController@edit', ['id'=>$customer->id]) }}" class="btn btn-sm btn-warning"><i class="fas fa-edit"></i> {{__('customer.edit_customer')}}
										</a>
									@endif
									@if(in_array('destroy_customers', $permissions))
										<td><button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#myModal"><i class="far fa-trash-alt"></i> {{__('customer.delete_customer')}} </button></td>
									@endif
								</div>

							</div>
						</div>
						<div class="col">

							<div class="container-fluid">
								<div class="row">
									<div class="col">
										<table class="table">
											<tbody>
												<tr>
													<th scope="row" class="border-0">{{__('customer.customer_code')}}</th>
													<td class="border-0">{{$customer->customer_code}}</td>
                                                </tr>
												<tr>
													<th scope="row" class="border-0">{{__('customer.name')}}</th>
													<td class="border-0">{{$customer->name}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('customer.phone')}}</th>
													<td class="border-0">{{$customer->phone}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('customer.secondary_phone')}}</th>
													<td class="border-0">{{$customer->secondary_phone}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('customer.alt_phone')}}</th>
													<td class="border-0">{{$customer->alt_phone}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('customer.email')}}</th>
													<td class="border-0">{{$customer->email}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('customer.address')}}</th>
													<td class="border-0">{{$customer->address}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('customer.national_id')}}</th>
													<td class="border-0">{{$customer->national_id}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('customer.vat_registration_number')}}</th>
													<td class="border-0">{{$customer->vat_registration_number}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('customer.vat_registration_expiry')}}</th>
													<td class="border-0">{{$customer->vat_registration_expiry}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('customer.notes')}}</th>
													<td class="border-0">{{$customer->notes}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('customer.reserved_units')}}</th>
													<td class="border-0">
														<table class="table">
															<thead>
																<tr>
																	<th>#</th>
																	<th>{{__('customer.reservation_link')}}</th>
																	<th>{{__('customer.unit_code')}}</th>
																</tr>
															</thead>
															<tbody>
																@foreach($customer->reservations as $reservationKey=>$reservation)
																<tr>
																	<td scope="row">{{$reservationKey +1}}</td>
																	<td>{!!$reservation->reservation_show_link!!}</td>
																	<td>{!!optional($reservation->unit)->unit_show_link!!}</td>
																</tr>
																@endforeach

															</tbody>
														</table>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<hr>
							</div>

						</div>
					</div>

				</div>
			</div>
		</div>

	</div>

	@include('partial.deleteConfirm',['name'=>$customer->name,
									  'id'=> $customer->id,
									  'message'=> __('customer.confirm_delete_message'),
									  'route'=>'CustomerController@destroy'])
@endsection
