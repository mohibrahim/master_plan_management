@extends('layouts.app')
@section('title')
	{{__('customer.add_customer')}}
@endsection
	@section('content')
	<div class="container-fluid">
		<div class="col">
			<div class="card card-info">
				<div class="card-heading">
					<h3 class="card-header text-center">{{__('customer.add_customer')}}</h3>
				</div>
				<div class="card-body ">
					@include('errors.list')
					{!! Form::open(['action' => ['CustomerController@store'], 'files'=>true, 'method'=>'POST'])!!}
						@include('customers._form')
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
@endsection
