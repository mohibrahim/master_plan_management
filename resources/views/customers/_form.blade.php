<div class="container-fluid border">
	<div class="row">
		<div class="col">

			<div class="form-group">
				<span style="color:#f47373">*</span>
				{!!Form::label('name', __('customer.name'))!!}
				{!!Form::text('name', null,['class'=>'form-control', 'placeholder'=>__('customer.name_placeholder')]) !!}
			</div>
			<div class="form-group">
				<span style="color:#f47373">*</span>
				{!!Form::label('customer_code', __('customer.customer_code'))!!}
				{!!Form::text('customer_code', null,['class'=>'form-control', 'placeholder'=>__('customer.customer_code_placeholder')]) !!}
			</div>
			<div class="form-group">
				<span style="color:#f47373">*</span>
				{!!Form::label('phone', __('customer.phone'))!!}
				{!!Form::text('phone', null,['class'=>'form-control', 'placeholder'=>__('customer.phone_placeholder')]) !!}
			</div>
			<div class="form-group">
				{!!Form::label('secondary_phone', __('customer.secondary_phone'))!!}
				{!!Form::text('secondary_phone', null,['class'=>'form-control', 'placeholder'=>__('customer.secondary_phone_placeholder')]) !!}
			</div>
			<div class="form-group">
				{!!Form::label('alt_phone', __('customer.alt_phone'))!!}
				{!!Form::text('alt_phone', null,['class'=>'form-control', 'placeholder'=>__('customer.alt_phone_placeholder')]) !!}
			</div>
			<div class="form-group">
				{!!Form::label('email', __('customer.email'))!!}
				{!!Form::text('email', null,['class'=>'form-control', 'placeholder'=>__('customer.email_placeholder')]) !!}
			</div>

			<div class='form-group'>
				{!!Form::label('address', __('customer.address'))!!}
				{!!Form::textarea('address', null, ['class'=>'form-control', 'rows'=>'5',
				'placeholder'=>__('customer.address_placeholder')])!!}
            </div>

			<div class='form-group'>
				{!!Form::label('national_id', __('customer.national_id'))!!}
				{!!Form::text('national_id', null, ['class'=>'form-control', 'rows'=>'5',
				'placeholder'=>__('customer.national_id_placeholder')])!!}
			</div>
			<div class='form-group'>
				{!!Form::label('vat_registration_number', __('customer.vat_registration_number'))!!}
				{!!Form::text('vat_registration_number', null, ['class'=>'form-control', 'rows'=>'5',
				'placeholder'=>__('customer.vat_registration_number_placeholder')])!!}
            </div>
            <div class='form-group'>
                {!!Form::label('vat_registration_expiry', __('customer.vat_registration_expiry'))!!}
                {!!Form::date('vat_registration_expiry', null,['class'=>'form-control', 'placeholder'=>__('customer.vat_registration_expiry_placeholder')]) !!}
            </div>

			<div class='form-group'>
				{!!Form::label('notes', __('customer.notes'))!!}
				{!!Form::textarea('notes', null, ['class'=>'form-control', 'rows'=>'5',
				'placeholder'=>__('customer.notes_placeholder')])!!}
			</div>
		</div>

	</div>
</div>

<div class="form-group">
	{!!Form::submit(__('customer.save'), ['class'=>'btn btn-primary form-control'])!!}
</div>
