@extends('layouts.app')
@section('title')
	{{__('customer.edit_customer')}}
@endsection
@section('content')
	<div class="container-fluid">
		<div class="col">
			<div class="card card-info">
				<div class="card-heading">
					<h3 class="card-header text-center">{{$customer->name}}</h3>
				</div>
				<div class="card-body ">
					@include('errors.list')
					{!! Form::model($customer, ['action' => ['CustomerController@update', 'id'=>$customer->id], 'files'=>true,
					'method'=>'put'])!!}
					@include('customers._form')
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
@endsection

