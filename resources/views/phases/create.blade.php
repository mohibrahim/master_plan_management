@extends('layouts.app')
@section('title')
	{{__('phase.add_phase')}}
@endsection
	@section('content')
	<div class="container-fluid">
		<div class="col">
			<div class="card card-info">
				<div class="card-heading">
					<h3 class="card-header text-center">{{__('phase.add_phase')}}</h3>
				</div>
				<div class="card-body ">
					@include('errors.list')
					{!! Form::open(['action' => ['PhaseController@store'], 'files'=>true, 'method'=>'POST'])!!}
						@include('phases._form')
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
@endsection
