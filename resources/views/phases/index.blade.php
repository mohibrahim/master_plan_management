@extends('layouts.app')
@section('title')
	{{__('phase.all_phases')}}
@endsection
@section('content')
	<div class="col-md-12">
		<div class="card">

			<div class="card-header">
				{{__('phase.all_phases')}}
			</div>
				<div class="card-body">
                    <div class="container">
                        <div class="row">
                            <div class="col">

                                <div class="form-inline">
                                    <div class="input-group mb-2 mr-sm-2">
                                        <label class="m-2" for="searching-input">{{__('phase.searching_on_phase')}}: </label>
                                        <input type="text" class="form-control" id="searching-input" placeholder="{{__('phase.searching_word')}}" autofocus>
                                    </div>

                                    <button id="searching-button" type="button" class="btn btn-primary mb-2">{{__('phase.search')}}</button>
                                    <input id="searching-button-required" type="hidden" value="{{__('phase.searching_button_required')}}">
                                    <input id="searching-button-searching" type="hidden" value="{{__('phase.searching_button_searching')}}">
                                    <input id="searching-button-search" type="hidden" value="{{__('phase.searching_button_search')}}">
                                </div>

                            </div>
                            <div class="col-3">
                                <div class="float-right">
                                    <div class='form-group '>
                                        {!!Form::label('net-suite-id', __('net_suite_integration.select_net_suite_integration'))!!}
                                        {!!Form::select('a', $netSuitNamesIds, null,['id'=>'net-suite-id', 'class'=>'form-control custom-select', 'placeholder'=>__('net_suite_integration.select_net_suite_integration_placeholder')]) !!}
                                    </div>
                                    <div class="form-group">
                                        <a href="#" type="button" id="net-suite-id-sync-button" class="btn btn-primary btn-block disabled">
                                            <i class="fas fa-sync-alt"></i>
                                        </a>

                                    </div>
                                </div>
                                <span class="clearfix"></span>
                            </div>
                        </div>
                    </div>

					<hr>
					<table class="table table-hover" id="phases">
						<thead>
							<tr>
								<th>#</th>
								<th>{{__('phase.project_name')}}</th>
								<th>{{__('phase.id')}}</th>
								<th>{{__('phase.name')}}</th>
								<th>{{__('phase.percentage_of_completion')}}</th>
								<th>{{__('phase.created_at')}}</th>
								<th>{{__('phase.creator_name')}}</th>
							</tr>
						</thead>
						<tbody>
							@foreach($phases as $phaseKey => $phase)
								<tr>
									<td>{{$phaseKey+1}}</td>
									<td>
										<a href="{{action('ProjectController@show', ['id'=>optional($phase->project)->id])}}">
											{{optional($phase->project)->name}}
										</a>
									</td>
									<td>{{$phase->id}}</td>
									<td>
										<a href="{{action('PhaseController@show', ['id'=>$phase->id])}}">
											{{$phase->name}}
										</a>
									</td>
									<td>{{$phase->percentage_of_completion}}</td>
									<td>{{$phase->created_at}}</td>
									<td>{{optional($phase->creator)->name}}</td>

								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="d-flex justify-content-center">

					{{$phases->links()}}
				</div>
			</div>

			<input id="data-table-search" type="hidden" value="{{__('data_table.search')}}">
			<input id="data-table-zero-record" type="hidden" value="{{__('data_table.zeroRecords')}}">
	</div>
@endsection

@section('head')
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css"> -->
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
@endsection
@section('js_footer')
	<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
	<script>
		$(document).ready(function () {
			dataTable = $('#phases').DataTable({
				"paging":   false,
				"info":     false,
				"language": {
					"search": $("#data-table-search").val(),
					"zeroRecords": $("#data-table-zero-record").val(),
				},
			});
            searchingOnPhasesAjax();
            selectSyncOnPhases();
		});
	</script>
@endsection
