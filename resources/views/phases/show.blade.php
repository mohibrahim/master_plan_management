@extends('layouts.app')

@section('title')
	{{$phase->name}}
@endsection

@section('content')
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h2 class="text-center">{{$phase->name}}</h2>
			</div>
			<div class="card-body ">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-4 text-left">
							<img src="{{url('images/helper_images/phase_cover.png')}}" class="rounded w-100 border border-default mb-3" alt="">
							<div class=" row text-muted">
								<div class="col small">
									{{__('phase.created_at')}}:
								</div>
								<div class="col small">
									{{$phase->created_at}}
								</div>
							</div>
							<div class=" row text-muted">
								<div class="col small">
									{{__('phase.creator_name')}}:
								</div>
								<div class="col small">
									{{optional($phase->creator)->name}}
								</div>
							</div>
							<div class=" row text-muted">
								<div class="col small">
									{{__('phase.updated_at')}}:
								</div>
								<div class="col small">
									{{$phase->updated_at}}
								</div>
							</div>
							<div class=" row text-muted">
								<div class="col small">
									{{__('phase.last_updater_name')}}:
								</div>
								<div class="col small">
									{{optional($phase->lastUpdater)->name}}
								</div>
							</div>
							<div class="row mt-3">
								<div class="col">
									@if(in_array('update_phases', $permissions))
										<a href="{{ action('PhaseController@edit', ['id'=>$phase->id]) }}" class="btn btn-sm btn-warning btn-block"><i class="fas fa-edit"></i> {{__('phase.edit_phase')}}
										</a>
									@endif
									@if(in_array('destroy_phases', $permissions))
										<td><button type="button" class="btn btn-sm btn-danger btn-block" data-toggle="modal" data-target="#myModal"><i class="far fa-trash-alt"></i> {{__('phase.delete_phase')}} </button></td>
									@endif
									@if(	in_array('store_catalogs', $permissions) &&
										in_array('store_phases', $permissions)
									)
										<a href="{{ action('CatalogController@create', ['model_associated_type'=>'phase_id', 'model_associated_type_id'=>$phase->id]) }}" class="btn btn-sm btn-primary btn-block"><i class="fas fa-images"></i> {{__('phase.add_catalog')}}
										</a>
									@endif
								</div>

							</div>
						</div>
						<div class="col">

							<div class="container-fluid">
								<div class="row">
									<div class="col">
										<table class="table">
											<tbody>
												<tr>
													<th scope="row" class="border-0">{{__('phase.project_name')}}</th>
													<td class="border-0">{{optional($phase->project)->name}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('phase.id')}}</th>
													<td class="border-0">{{$phase->id}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('phase.name')}}</th>
													<td class="border-0">{{$phase->name}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('phase.supposed_start_date')}}</th>
													<td class="border-0">{{$phase->supposed_start_date}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('phase.actually_start_date')}}</th>
													<td class="border-0">{{$phase->actually_start_date}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('phase.supposed_end_date')}}</th>
													<td class="border-0">{{$phase->supposed_end_date}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('phase.actually_end_date')}}</th>
													<td class="border-0">{{$phase->actually_end_date}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('phase.percentage_of_completion')}}</th>
													<td class="border-0">{{$phase->percentage_of_completion}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('phase.notes')}}</th>
													<td class="border-0">{{$phase->notes}}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<hr>
							</div>

						</div>
					</div>
					<div class="row mt-3">

						<div class="col">
							<div class="card">
								<div class="card-body">
									<h4 class="font-weight-bold text-center m-3"> الكتالوجات </h4>

									<table class="table table-hover" id="catalogs">
										<thead>
											<tr>
												<th>#</th>
												<th>{{__('catalog.id')}}</th>
												<th>{{__('catalog.name')}}</th>
												<th>{{__('catalog.description')}}</th>
												<th>{{__('catalog.created_at')}}</th>
												<th>{{__('catalog.creator_name')}}</th>
											</tr>
										</thead>
										<tbody>
											@foreach($phase->catalogs as $catalogKey => $catalog)
												<tr>
													<td>{{$catalogKey+1}}</td>
													<td>{{$catalog->id}}</td>
													<td>
														<a href="{{action('CatalogController@show', ['id'=>$catalog->id])}}">
															{{$catalog->name}}
														</a>
													</td>
													<td>{{$catalog->description}}</td>
													<td>{{$catalog->created_at}}</td>
													<td>{{optional($catalog->creator)->name}}</td>

												</tr>
											@endforeach
										</tbody>
									</table>
									<input id="data-table-search" type="hidden" value="{{__('data_table.search')}}">
									<input id="data-table-zero-record" type="hidden" value="{{__('data_table.zeroRecords')}}">
								</div>
							</div>
						</div>


					</div>
				</div>
			</div>
		</div>

	</div>

	@include('partial.deleteConfirm',['name'=>$phase->name,
									  'id'=> $phase->id,
									  'message'=> __('phase.confirm_delete_message'),
									  'route'=>'PhaseController@destroy'])
@endsection
