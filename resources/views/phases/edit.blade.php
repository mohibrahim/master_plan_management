@extends('layouts.app')
@section('title')
	{{__('phase.edit_phase')}}
@endsection
@section('content')
	<div class="container-fluid">
		<div class="col">
			<div class="card card-info">
				<div class="card-heading">
					<h3 class="card-header text-center">{{$phase->name}}</h3>
				</div>
				<div class="card-body ">
					@include('errors.list')
					{!! Form::model($phase, ['action' => ['PhaseController@update', 'id'=>$phase->id], 'files'=>true,
					'method'=>'put'])!!}
					@include('phases._form')
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
@endsection

