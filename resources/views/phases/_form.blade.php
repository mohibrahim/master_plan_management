<div class="container-fluid border">
	<div class="row">
		<div class="col">
			

			<div class="card my-3">
				<div class="card-body">
					<div class="form-group">
						<span style="color:#f47373">*</span>
						<label for="project-id">{{__('phase.project_name')}}</label>
						<select class="form-control" name="project_id" id="project-id">
							@if (old('project_id') && old('old_selected_project_name'))
								<option value="{{old('project_id')}}">{{old('old_selected_project_name')}}</option>
							@elseif (!empty($phase->selected_project_name_id))
								<option value="{{$phase->selected_project_name_id['id']}}">{{$phase->selected_project_name_id['name']}}</option>
							@endif
						</select>
						<input type="hidden" id="project-name-placeholder" value="{{__('phase.project_name_placeholder')}}">
						<input type="hidden" name="old_selected_project_name" id="old-selected-project-name" value="">
					</div>
				</div>
			</div>
			

			<div class="form-group">
				<span style="color:#f47373">*</span>
				{!!Form::label('name', __('phase.name'))!!}
				{!!Form::text('name', null,['class'=>'form-control', 'placeholder'=>__('phase.name_placeholder')]) !!}
			</div>

			<div class="form-group">
				{!!Form::label('supposed_start_date', __('phase.supposed_start_date'))!!}
				{!!Form::date('supposed_start_date', null,['class'=>'form-control', 'placeholder'=>__('phase.supposed_start_date')]) !!}
			</div>
			
			<div class="form-group">
				{!!Form::label('actually_start_date', __('phase.actually_start_date'))!!}
				{!!Form::date('actually_start_date', null,['class'=>'form-control', 'placeholder'=>__('phase.actually_start_date')]) !!}
			</div>

			<div class="form-group">
				{!!Form::label('supposed_end_date', __('phase.supposed_end_date'))!!}
				{!!Form::date('supposed_end_date', null,['class'=>'form-control', 'placeholder'=>__('phase.supposed_end_date')]) !!}
			</div>

			<div class="form-group">
				{!!Form::label('actually_end_date', __('phase.actually_end_date'))!!}
				{!!Form::date('actually_end_date', null,['class'=>'form-control', 'placeholder'=>__('phase.actually_end_date')]) !!}
			</div>

			<div class="form-group">
				{!!Form::label('percentage_of_completion', __('phase.percentage_of_completion'))!!}
				{!!Form::text('percentage_of_completion', null,['class'=>'form-control', 'placeholder'=>__('phase.percentage_of_completion')]) !!}
			</div>
			

			<div class='form-group'>
				{!!Form::label('notes', __('phase.notes'))!!}
				{!!Form::textarea('notes', null, ['class'=>'form-control', 'rows'=>'5',
				'placeholder'=>__('phase.notes_placeholder')])!!}
			</div>
		</div>

	</div>
</div>

<div class="form-group">
	{!!Form::submit(__('phase.save'), ['class'=>'btn btn-primary form-control only-one-click'])!!}
</div>

@section('head')
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
@endsection
@section('js_footer')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
	<script>
		$(document).ready(function () {

			let oldSelectedProjectName = $('#old-selected-project-name');

			$('#project-id').select2({
				minimumInputLength: 1,
				placeholder: $('#project-name-placeholder').val(),
       			allowClear: true,
				ajax: {
					delay: 500,
					url: '/phases_miscellaneous/get_project_names_ids',
					dataType: 'json'
				},
			});

			$('#project-id').on('select2:select', function (event) {
				oldSelectedProjectName.val(event.params.data.text);
			});;


		});
	</script>
@endsection

