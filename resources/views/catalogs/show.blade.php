@extends('layouts.app')

@section('title')
	{{$catalog->name}}
@endsection

@section('content')
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h2 class="text-center">{{$catalog->name}}</h2>
			</div>
			<div class="card-body ">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-4 text-left">
							<img src="{{$catalog->cover_image_path}}" class="rounded w-100 border border-default mb-3" alt="">
							<div class=" row text-muted">
								<div class="col small">
									{{__('catalog.created_at')}}:
								</div>
								<div class="col small">
									{{$catalog->created_at}}
								</div>
							</div>
							<div class=" row text-muted">
								<div class="col small">
									{{__('catalog.creator_name')}}:
								</div>
								<div class="col small">
									{{optional($catalog->creator)->name}}
								</div>
							</div>
							<div class=" row text-muted">
								<div class="col small">
									{{__('catalog.updated_at')}}:
								</div>
								<div class="col small">
									{{$catalog->updated_at}}
								</div>
							</div>
							<div class=" row text-muted">
								<div class="col small">
									{{__('catalog.last_updater_name')}}:
								</div>
								<div class="col small">
									{{optional($catalog->lastUpdater)->name}}
								</div>
							</div>
							<div class="row mt-3">
								<div class="col">
									@if(in_array('update_catalogs', $permissions))
										<a href="{{ action('CatalogController@edit', ['id'=>$catalog->id]) }}" class="btn btn-sm btn-warning btn-block"><i class="fas fa-edit"></i> {{__('catalog.edit_catalog')}}
										</a>
									@endif
									@if(in_array('destroy_catalogs', $permissions))
										<td><button type="button" class="btn btn-sm btn-danger btn-block" data-toggle="modal" data-target="#myModal"><i class="far fa-trash-alt"></i> {{__('catalog.delete_catalog')}} </button></td>
									@endif
								</div>

							</div>
						</div>
						<div class="col">

							<div class="container-fluid">
								<div class="row">
									<div class="col">
										<table class="table">
											<tbody>

												@if(!empty($catalog->associated_model_type_data))
													<tr>
														<th scope="row" class="border-0">{{__('catalog.catalog_type')}}</th>
														<td class="border-0">
															{{__('catalog.'.$catalog->associated_model_type_data[0])}} |
															<a href="{{$catalog->associated_model_type_data[1]}}">
																{{$catalog->associated_model_type_data[2]}}
															</a>
														</td>
													</tr>
												@endif

												<tr>
													<th scope="row" class="border-0">{{__('catalog.id')}}</th>
													<td class="border-0">{{$catalog->id}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('catalog.name')}}</th>
													<td class="border-0">{{$catalog->name}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('catalog.description')}}</th>
													<td class="border-0">{{$catalog->description}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('catalog.notes')}}</th>
													<td class="border-0">{{$catalog->notes}}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<hr>
								<h4 class="text-center">{{__('catalog.catalog_files')}} </h4>
								<div class="row mt-3">
									@foreach($catalog->files as $file)
										<div class="col-lg-4 d-flex justify-content-center p-4 delete-catalog-image-wrapper">
											<div class="card" style="width: 18rem;">
												@if($file->isImage())
													<img class="card-img-top" src="{{asset($file->name)}}" alt="Card image cap">

													<div class="card-body">
														<span class="font-weight-bold">عنوان الصورة:</span> {{$file->title}}
														<span class="font-weight-bold">حالة الصورة:</span> {{$file->status}}
													</div>
												@elseif($file->isPdf())
													<img class="card-img-top" src="{{url('images/helper_images/pdf_cover.jpg')}}" alt="Card image cap">

													<div class="card-body">
														<span class="font-weight-bold">عنوان الملف:</span> {{$file->title}}
														<span class="font-weight-bold">حالة الملف:</span> {{$file->status}}
													</div>
												@endif
												<div class="card-footer bg-white text-dark text-center">
                                                    <a href="{{action('CatalogController@downloadFile', ['file_name'=>$file->name])}}" role="button" class="btn btn-primary btn-sm btn-block" data-catalog-file-id="{{$file->id}}"><i class="fas fa-download"></i> {{__('catalog.download_file')}}</a>

													<a href="{{action('CatalogController@displayFile', ['file_name'=>$file->name])}}" role="button" class="btn btn-success btn-sm btn-block" data-catalog-file-id="{{$file->id}}" target="_blank"><i class="fas fa-eye"></i> {{__('catalog.display_file')}}</a>
												</div>
											</div>
										</div>
									@endforeach

								</div>
							</div>

						</div>
					</div>

				</div>
			</div>
		</div>

	</div>

	@include('partial.deleteConfirm',['name'=>$catalog->name,
									  'id'=> $catalog->id,
									  'message'=> __('catalog.confirm_delete_message'),
									  'route'=>'CatalogController@destroy'])
@endsection


