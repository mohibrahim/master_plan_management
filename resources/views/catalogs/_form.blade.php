<div class="container">
	<div class="row">
		<div class="col">

			<div class="form-group">
				<span style="color:#f47373">*</span>
				{!!Form::label('name', __('catalog.name'))!!}
				{!!Form::text('name', null,['class'=>'form-control', 'placeholder'=>__('catalog.name_placeholder')]) !!}
			</div>

			<div class='form-group'>
				{!!Form::label('description', __('catalog.description'))!!}
				{!!Form::textarea('description', null, ['class'=>'form-control', 'rows'=>'5',
				'placeholder'=>__('catalog.description_placeholder')])!!}
			</div>

			<div class='form-group'>
				{!!Form::label('notes', __('catalog.notes'))!!}
				{!!Form::textarea('notes', null, ['class'=>'form-control', 'rows'=>'5',
				'placeholder'=>__('catalog.notes_placeholder')])!!}
			</div>
			@if (request()->route()->named('catalogs.create'))
				{!!$hiddenInput!!}
			@endif
		</div>

	</div>
</div>
<div class="container mb-3">
	<div class="row">
		<div class="col-lg-3 ">
			<div class="card ">
				<div class="card-header">
					<h5>
						{{__('catalog.cover_image')}}
					</h5>
				</div>
				<div class="card-body">

					<img src="{{$catalog->coverImagePath}}" class="card-img-top" id="cover_image" alt="Image"
						value="{{optional($catalog->coverImage)->id}}">

					@if($catalog->coverImagePath != '/images/helper_images/no_image.png')
					<div class="my-3" id="delete_image_button_wrapper">
						<button type="button" name="delete_image" class="btn btn-sm btn-danger" id="delete_image_button"><i class="far fa-trash-alt"></i> {{__('catalog.delete_cover_image')}}</button>
					</div>
					@endif

					<div class="form-group">
						{!!Form::label('cover_image', __('catalog.cover_image'))!!}
						{!!Form::file('cover_image', ['class'=>'form-control',
						'placeholder'=>__('catalog.cover_image_placeholder')])!!}
					</div>
				</div>
			</div>
		</div>

		<div class="col-lg-9">
			<div class="row">
				<div class="col">
					<div class="card ">
						<div class="card-header">
							<h5>{{__('catalog.catalog_add_images')}}</h5>
						</div>
						<div class="card-body">
							<div class="container" id="catalog-inputs-files-images-wrapper">
								<input type="hidden" id="no-image-png" value="{{url('images/helper_images/no_image.png')}}">
								<h6 class="text-secondary"><i class="far fa-lightbulb"></i> {{__('catalog.catalog_choose_suitable_images')}}.</h6>

							</div>
						</div>
						<div class="card-footer bg-white">
							<h5 class="float-right">
								<button type="button" id="catalog-input-file-images-row-adder" class="btn btn-info btn-sm"><i class="far fa-image"></i> | {{__('catalog.catalog_choose_suitable_image')}} </button>
							</h5>

						</div>
					</div>

				</div>

			</div>


			<div class="row mt-3">
				<div class="col">
					<div class="card ">
						<div class="card-header">
							<h5>{{__('catalog.catalog_add_pdfs')}}</h5>
						</div>
						<div class="card-body">
							<div class="container" id="catalog-inputs-files-pdfs-wrapper">
								<h6 class="text-secondary"><i class="far fa-lightbulb"></i> {{__('catalog.catalog_choose_suitable_pdfs')}}.</h6>

							</div>
						</div>
						<div class="card-footer bg-white">
							<h5 class="float-right">
								<button type="button" id="catalog-input-file-pdf-row-adder" class="btn btn-warning btn-sm"><i class="fas fa-file-pdf"></i> | {{__('catalog.catalog_add_pdf')}} </button>
							</h5>

						</div>
					</div>

				</div>

			</div>

			@if($catalog->files->isNotEmpty() )
				<div class="card mt-3">
					<div class="card-header">
						<h5 class="float-left">{{__('catalog.catalog_inserted_files')}}</h5>
					</div>
					<div class="card-body">
						<div class="row">
							@foreach($catalog->files as $file)
								<div class="col-lg-4 d-flex justify-content-center p-4 delete-catalog-file-wrapper">
									<div class="card" style="width: 18rem;">
										@if($file->isImage())
											<img class="card-img-top" src="{{asset($file->name)}}" alt="Card image cap">
											<div class="card-body">
												<span class="font-weight-bold">عنوان الصورة:</span> {{$file->title}}
												<span class="font-weight-bold">حالة الصورة:</span> {{$file->status}}
											</div>
										@elseif($file->isPdf())
											<img class="card-img-top" src="{{url('images/helper_images/pdf_cover.jpg')}}" alt="Card image cap">
											<div class="card-body">
												<span class="font-weight-bold">عنوان الملف:</span> {{$file->title}}
												<span class="font-weight-bold">حالة الملف:</span> {{$file->status}}
											</div>
										@endif
										<div class="card-footer bg-white text-dark"">
											<button type="button" class="btn btn-danger btn-sm delete-catalog-file-link" data-catalog-file-id="{{$file->id}}"><i class="far fa-trash-alt"></i> حذف</button>
										</div>
									</div>
								</div>
							@endforeach
						</div>
					</div>
				</div>
			@endif

			<input type="hidden" id="catalog-images-image-number" value="{{__('catalog.catalog_images_image_number')}}">
			<input type="hidden" id="catalog-images-image-title" value="{{__('catalog.catalog_images_image_title')}}">
			<input type="hidden" id="catalog-images-image-title-placeholder" value="{{__('catalog.catalog_images_image_title_placeholder')}}">
			<input type="hidden" id="catalog-images-image-status" value="{{__('catalog.catalog_images_image_status')}}">
			<input type="hidden" id="catalog-images-image-status-placeholder" value="{{__('catalog.catalog_images_image_status_placeholder')}}">
			<input type="hidden" id="catalog-files-pdf-number" value="{{__('catalog.catalog_files_pdf_number')}}">
			<input type="hidden" id="catalog-files-pdf-title" value="{{__('catalog.catalog_files_pdf_title')}}">
			<input type="hidden" id="catalog-files-pdf-title-placeholder" value="{{__('catalog.catalog_files_pdf_title_placeholder')}}">
			<input type="hidden" id="catalog-files-pdf-status" value="{{__('catalog.catalog_files_pdf_status')}}">
			<input type="hidden" id="catalog-files-pdf-status-placeholder" value="{{__('catalog.catalog_files_pdf_status_placeholder')}}">

		</div>
	</div>
</div>

<div class="form-group">
	{!!Form::submit(__('catalog.save'), ['class'=>'btn btn-primary form-control'])!!}
</div>


@section('js_footer')
	<script>
		$(document).ready(function(){
			deleteCoverImage();
			catalogAddInputFileImageRow();
			catalogAddInputFilePdfRow();
			deleteImage();
		});
	</script>
@endsection
