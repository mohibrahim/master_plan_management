@extends('layouts.app')
@section('title')
	{{__('catalog.add_catalog')}}
@endsection
	@section('content')
	<div class="container">
		<div class="col">
			<div class="card card-info">
				<div class="card-heading">
					<h3 class="card-header text-center">{{__('catalog.add_catalog')}}</h3>
				</div>
				<div class="card-body ">
					@include('errors.list')
					{!! Form::open(['action' => ['CatalogController@store'], 'files'=>true, 'method'=>'POST'])!!}
						@include('catalogs._form')
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
@endsection
