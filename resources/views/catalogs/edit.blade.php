@extends('layouts.app')
@section('title')
	{{__('catalog.edit_catalog')}}
@endsection
@section('content')
	<div class="container">
		<div class="col">
			<div class="card card-info">
				<div class="card-heading">
					<h3 class="card-header text-center">{{$catalog->name}}</h3>
				</div>
				<div class="card-body ">
					@include('errors.list')
					{!! Form::model($catalog, ['action' => ['CatalogController@update', 'id'=>$catalog->id], 'files'=>true,
					'method'=>'put'])!!}
					@include('catalogs._form')
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
@endsection

