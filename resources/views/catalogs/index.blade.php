@extends('layouts.app')
@section('title')
	{{__('catalog.all_catalogs')}}
@endsection
@section('content')
	<div class="col-md-12">
		<div class="card">

			<div class="card-header">
				{{__('catalog.all_catalogs')}}
			</div>
				<div class="card-body">
					<div class="form-inline">
						<div class="input-group mb-2 mr-sm-2">
							<label class="m-2" for="searching-input">{{__('catalog.searching_on_catalog')}}: </label>
							<input type="text" class="form-control" id="searching-input" placeholder="{{__('catalog.searching_word')}}" autofocus>
						</div>
						
						<button id="searching-button" type="button" class="btn btn-primary mb-2">{{__('catalog.search')}}</button>
						<input id="searching-button-required" type="hidden" value="{{__('catalog.searching_button_required')}}">
						<input id="searching-button-searching" type="hidden" value="{{__('catalog.searching_button_searching')}}">
						<input id="searching-button-search" type="hidden" value="{{__('catalog.searching_button_search')}}">
					</div>
					<hr>
					<table class="table table-hover" id="catalogs">
						<thead>
							<tr>
								<th>#</th>
								<th>{{__('catalog.id')}}</th>
								<th>{{__('catalog.name')}}</th>
								<th>{{__('catalog.description')}}</th>
								<th>{{__('catalog.created_at')}}</th>
								<th>{{__('catalog.creator_name')}}</th>
							</tr>
						</thead>
						<tbody>
							@foreach($catalogs as $catalogKey => $catalog)
								<tr>
									<td>{{$catalogKey+1}}</td>
									<td>{{$catalog->id}}</td>
									<td>
										<a href="{{action('CatalogController@show', ['id'=>$catalog->id])}}">
											{{$catalog->name}}
										</a>
									</td>
									<td>{{$catalog->description}}</td>
									<td>{{$catalog->created_at}}</td>
									<td>{{optional($catalog->creator)->name}}</td>

								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="d-flex justify-content-center">

					{{$catalogs->links()}}
				</div>
			</div>

			<input id="data-table-search" type="hidden" value="{{__('data_table.search')}}">
			<input id="data-table-zero-record" type="hidden" value="{{__('data_table.zeroRecords')}}">
	</div>
@endsection

@section('head')
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css"> -->
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
@endsection
@section('js_footer')
	<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
	<script>
		$(document).ready(function () {
			dataTable = $('#catalogs').DataTable({
				"paging":   false,
				"info":     false,
				"language": {
					"search": $("#data-table-search").val(),
					"zeroRecords": $("#data-table-zero-record").val(),
				},
			});
			searchingOnCatalogsAjax();
		});
	</script>
@endsection
