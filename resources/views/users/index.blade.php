@extends('layouts.app')
@section('title')
	All Users
@endsection
@section('content')
	<div class="col-md-12">
		<div class="card">

			<div class="card-header">
				All Users
			</div>
				<div class="card-body">
					<div class="form-inline">
						<div class="input-group mb-2 mr-sm-2">
							<label class="m-2" for="searching-input">{{__('user.searching_on_user')}}: </label>
							<input type="text" class="form-control" id="searching-input" placeholder="{{__('user.searching_word')}}" autofocus>
						</div>
						
						<button id="searching-button" type="button" class="btn btn-primary mb-2">{{__('user.search')}}</button>
						<input id="searching-button-required" type="hidden" value="{{__('user.searching_button_required')}}">
						<input id="searching-button-searching" type="hidden" value="{{__('user.searching_button_searching')}}">
						<input id="searching-button-search" type="hidden" value="{{__('user.searching_button_search')}}">
					</div>
					<hr>
					<table class="table table-hover" id="users">
						<thead>
							<tr>
								<th>#</th>
								<th>ID</th>
								<th>Name</th>
								<th>Email</th>
								<th>Mobile Number</th>
								<th>Role</th>
								<th>created at</th>
								<th>Creator name</th>
							</tr>
						</thead>
						<tbody>
							@foreach($users as $userKey => $user)
								<tr>
									<td>{{$userKey+1}}</td>
									<td>{{$user->id}}</td>
									<td>
										<a href="{{ action('UserController@show',['id'=>$user->id]) }}"> {{$user->name}}</a>
									</td>
									<td>{{$user->email}}</td>
									<td>{{$user->mobile_number}}</td>
									<td>
										@if($user->roles->isNotEmpty())
											@foreach ($user->roles as $key => $role)
												{{ $role->name}}<br>
											@endforeach
										@else									
											{{'No Role Yet!'}}
										@endif
									</td>
									<td>{{$user->created_at}}</td>
									<td>{{optional($user->creator)->name}}</td>

								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="d-flex justify-content-center">

					{{$users->links()}}
				</div>
			</div>

			<input id="data-table-search" type="hidden" value="{{__('data_table.search')}}">
			<input id="data-table-zero-record" type="hidden" value="{{__('data_table.zeroRecords')}}">
			
			
	</div>
@endsection

@section('head')
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css"> -->
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
@endsection
@section('js_footer')
	<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
	<script>
		$(document).ready(function () {
			dataTable = $('#users').DataTable({
				"paging":   false,
				"info":     false,
				"language": {
					"search": $("#data-table-search").val(),
					"zeroRecords": $("#data-table-zero-record").val(),
				},
			});
			searchingOnUsersAjax();
		});
	</script>
@endsection
