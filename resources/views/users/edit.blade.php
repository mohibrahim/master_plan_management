@extends('layouts.app')
@section('title')
	Edit profile
@endsection
@section('content')
	<div class="col-md-12">
		<div class="card card-info">
		<div class="card-heading">
			<h3 class="card-header text-center">{{$user->name}}</h3>
		</div>
			<div class="card-body ">
				@include('errors.list')
				{!! Form::model($user, ['action' => ['UserController@update', 'id'=>$user->id], 'files'=>true, 'method'=>'put'])!!}
					@include('users._form')
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@endsection

