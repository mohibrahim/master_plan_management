<div class="row">

	<div class="col-lg-12">

		@if (in_array('store_roles', $permissions) || in_array('update_roles', $permissions))
			<div class='form-group'>
				<span style='color:#f47373'>*</span>
				{!!Form::label('master_role_id', __('user.master_role_id'))!!}
				{!!Form::select('master_role_id', $user->user_roles_names_ids, null,['class'=>'form-control', 'placeholder'=>__('user.master_role_id_placeholder')]) !!}
			</div>
		@endif
		
		<div class="form-group">
			<span style="color:#f47373">*</span>
			{!!Form::label('name', __('user.name'))!!}
			{!!Form::text('name', null,['class'=>'form-control', 'placeholder'=>__('user.name_placeholder')]) !!}
		</div>
		
		<div class="form-group">
			<span style="color:#f47373">*</span>
			{!!Form::label('email', __('user.email'))!!}
			{!!Form::email('email', null, ['class'=>'form-control', 'placeholder'=>__('user.email_placeholder')])!!}
		</div>

		<div class="form-group">
			{!!Form::label('address', __('user.address'))!!}
			{!!Form::textarea('address', null, ['class'=>'form-control', 'rows'=>'5', 'placeholder'=>__('user.address_placeholder')])!!}
		</div>

		<div class="form-group">
			{!!Form::label('country', __('user.country'))!!}
			{!!Form::select('country', $countries, null, ['class'=>'form-control', 'placeholder'=>__('user.country_placeholder')])!!}
		</div>

		<div class='form-group'>
			{!!Form::label('date_of_birth', __('user.date_of_birth'))!!}
			{!!Form::date('date_of_birth', null,['class'=>'form-control', 'placeholder'=>__('user.date_of_birth_placeholder')]) !!}
		</div>

		<div class="form-group">
			<span style="color:#f47373">*</span>
			{!!Form::label('mobile_number', __('user.mobile_number'))!!}
			{!!Form::text('mobile_number', null, ['class'=>'form-control', 'placeholder'=>__('user.mobile_number_placeholder')])!!}
		</div>
		
		<div class="form-group">
			{!!Form::label('id_card_number', __('user.id_card_number'))!!}
			{!!Form::text('id_card_number', null, ['class'=>'form-control', 'placeholder'=>__('user.id_card_number_placeholder')])!!}
		</div>

		<div class="form-group">
			<span style="color:#f47373">*</span>
			{!!Form::label('password', __('user.password'))!!}
			{!!Form::password('password', ['class'=>'form-control', 'placeholder'=>__('user.password_placeholder')])!!}
		</div>

		<div class="form-group">
			<span style="color:#f47373">*</span>
			{!!Form::label('password_confirmation', __('user.password_confirmation'))!!}
			{!!Form::password('password_confirmation', ['class'=>'form-control', 'placeholder'=>__('user.password_confirmation_placeholder')])!!}
		</div>

		<div class="col-lg-5 ">
			<div class="card ">
				<div class="card-header">
				{{__('user.personal_image')}}
				</div>
				<div class="card-body">

					<img src="{{$user->personalImagePath}}" class="card-img-top" id="personal_image" alt="Image" value="{{optional($user->personalImage)->id}}">
					
					@if($user->personalImagePath != '/images/helper_images/no_image.png')
						<div class="my-3" id="delete_image_button_wrapper">
							<button type="button" name="delete_image" class="btn btn-sm btn-danger" id="delete_image_button">Delete the image</button>
						</div>
					@endif
					
					<div class="form-group">
						{!!Form::label('personal_image',  __('user.personal_image'))!!}
						{!!Form::file('personal_image', ['class'=>'form-control', 'placeholder'=>__('user.personal_image_placeholder')])!!}
					</div>
				</div>
			</div>
		</div>

		<div class='form-group'>
			{!!Form::label('notes', __('user.notes'))!!}
			{!!Form::textarea('notes', null, ['class'=>'form-control', 'rows'=>'5', 'placeholder'=>__('user.notes_placeholder')])!!}
		</div>

		<div class="form-group">
			{!!Form::submit(__('user.save'), ['class'=>'btn btn-primary form-control'])!!}
		</div>
	</div>
</div>

@section('js_footer')
	<script>
		$(document).ready(function(){
			deletePersonalImage();
		});
	</script>
@endsection
