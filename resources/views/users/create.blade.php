@extends('layouts.app')
@section('title')
	{{__('user.add_user')}}
@endsection
@section('content')
	<div class="col-md-12">
		<div class="card card-info">
		<div class="card-heading">
			<h3 class="card-header text-center">{{__('user.add_user')}}</h3>
		</div>
			<div class="card-body ">
				@include('errors.list')
				{!! Form::open(['action' => ['UserController@store'], 'files'=>true, 'method'=>'POST'])!!}
					@include('users._form')
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@endsection
