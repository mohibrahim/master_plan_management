@extends('layouts.app')

@section('title')
	{{$user->name}}
@endsection

@section('content')
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				{{$user->name}}
			</div>
			<div class="card-body ">
				<table class="table">
					<tbody>
						@if (in_array('store_roles', $permissions) || in_array('update_roles', $permissions))
							<tr>
								<th class="border-top-0">Master Role Name</th>
								<td class="border-top-0" scop="row">{{optional($user->masterRole)->name}}</td>
							</tr>
						@endif
						<tr>
							<th class="border-top-0">ID</th>
							<td class="border-top-0" scop="row">{{$user->id}}</td>
						</tr>
						<tr>
							<th class="border-top-0">Personal Image</th>
							<td class="border-top-0" scop="row">
								<img src="{{asset(optional($user->personalImage)->name)}}" class="img img-fluid rounded-circle" alt="" width="200">
							</td>
						</tr>
						<tr>
							<th class="border-top-0">Name</th>
							<td class="border-top-0" scop="row"> {{$user->name}}</td>
						</tr>
						<tr>
							<th class="border-top-0">Email</th>
							<td class="border-top-0" scope="row"> {{ $user->email}} </td>
						</tr>
						<tr>
							<th class="border-top-0">Address</th>
							<td class="border-top-0" scope="row"> {{ $user->address}} </td>
						</tr>
						<tr>
							<th class="border-top-0">Date of birth</th>
							<td class="border-top-0" scope="row"> {{ $user->date_of_birth}} </td>
						</tr>
						<tr>
							<th class="border-top-0">Mobile number</th>
							<td class="border-top-0" scope="row"> {{ $user->mobile_number}} </td>
						</tr>
						<tr>
							<th class="border-top-0">E-Mail verification at</th>
							<td class="border-top-0" scope="row"> {{ $user->email_verified_at}} </td>
						</tr>
						<tr>
							<th class="border-top-0">Role</th>
							<td class="border-top-0" scope="row">
								@if($user->roles->isNotEmpty())
									@foreach ($user->roles as $key => $role)
										{{ $role->name}}<br>
									@endforeach
								@else
									No Role Yet!
								@endif
							</td>
						</tr>

						<tr>
							<th class="border-top-0">Notes</th>
							<td class="border-top-0" scope="row"> {{$user->notes}} </td>
						</tr>
						<tr>
							@if(in_array('update_users', $permissions))
								<th class="border-top-0">Assign role</th>
								<td class="border-top-0" scope="row"> <a href="{{ action('RoleUserController@edit', ['id'=>$user->id]) }}"> Edit </td>
							@endif
						</tr>
						<tr>
							@if(in_array('update_users', $permissions))
								<th class="border-top-0">Edit</th>
								<td class="border-top-0" scope="row"> <a href="{{action('UserController@edit',['id'=>$user->id])}}">Eidt</a> </td>
							@endif
						</tr>
						<tr>
							@if(in_array('destroy_users', $permissions))
								<th class="border-top-0">Delete</th>
								<td class="border-top-0" scope="row"><button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#myModal"><i class="far fa-trash-alt"></i> Delete</button></td>
							@endif
						</tr>
					</tbody>
				</table>
			</div>
		</div>

	</div>

	@include('partial.deleteConfirm',['name'=>$user->name,
									  'id'=> $user->id,
									  'message'=> __('user.confirm_delete_message'),
									  'route'=>'UserController@destroy'])
@endsection
