@extends('layouts.app')
@section('title')
	{{__('net_suite_integration.add_net_suite_integration')}}
@endsection
	@section('content')
	<div class="container-fluid">
		<div class="col">
			<div class="card card-info">
				<div class="card-heading">
					<h3 class="card-header text-center">{{__('net_suite_integration.add_net_suite_integration')}}</h3>
				</div>
				<div class="card-body ">
					@include('errors.list')
					{!! Form::open(['action' => ['NetSuiteIntegrationController@store'], 'files'=> false, 'method'=>'POST'])!!}
						@include('net_suite_integrations._form')
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
@endsection
