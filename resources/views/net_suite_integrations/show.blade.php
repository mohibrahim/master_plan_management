@extends('layouts.app')

@section('title')
{{$netSuiteIntegration->name}}
@endsection

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h2 class="text-center">{{$netSuiteIntegration->name}}</h2>
            </div>
            <div class="card-body ">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-4 text-left">
                            <img src="{{url('images/helper_images/net_suite_integrations.png')}}"
                                class="rounded w-100 border border-default mb-3" alt="">
                            <div class=" row text-muted">
                                <div class="col small">
                                    {{__('net_suite_integration.created_at')}}:
                                </div>
                                <div class="col small">
                                    {{$netSuiteIntegration->created_at}}
                                </div>
                            </div>
                            <div class=" row text-muted">
                                <div class="col small">
                                    {{__('net_suite_integration.creator_name')}}:
                                </div>
                                <div class="col small">
                                    {{optional($netSuiteIntegration->creator)->name}}
                                </div>
                            </div>
                            <div class=" row text-muted">
                                <div class="col small">
                                    {{__('net_suite_integration.updated_at')}}:
                                </div>
                                <div class="col small">
                                    {{$netSuiteIntegration->updated_at}}
                                </div>
                            </div>
                            <div class=" row text-muted">
                                <div class="col small">
                                    {{__('net_suite_integration.last_updater_name')}}:
                                </div>
                                <div class="col small">
                                    {{optional($netSuiteIntegration->lastUpdater)->name}}
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col">
                                    @if(in_array('update_net_suite_integrations', $permissions))
                                    <a href="{{ action('NetSuiteIntegrationController@edit', ['id'=>$netSuiteIntegration->id]) }}"
                                        class="btn btn-sm btn-warning btn-block"><i class="fas fa-edit"></i>
                                        {{__('net_suite_integration.edit_net_suite_integration')}}
                                    </a>
                                    @endif
                                    @if(in_array('destroy_net_suite_integrations', $permissions))
                                    <td><button type="button" class="btn btn-sm btn-danger btn-block" data-toggle="modal"
                                            data-target="#myModal"><i class="far fa-trash-alt"></i>
                                            {{__('net_suite_integration.delete_net_suite_integration')}} </button></td>
                                    @endif

                                </div>

                            </div>

                        </div>
                        <div class="col">

                            <div class="row">
                                <div class="col">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <th scope="row" class="border-0">{{__('net_suite_integration.id')}}</th>
                                                <td class="border-0">{{$netSuiteIntegration->id}}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row" class="border-0">{{__('net_suite_integration.name')}}</th>
                                                <td class="border-0">{{$netSuiteIntegration->name}}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row" class="border-0">{{__('net_suite_integration.notes')}}</th>
                                                <td class="border-0">{{$netSuiteIntegration->notes}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <hr>

                            <div class="row">
                                <div class="col">
                                    <h4 class="card-title text-center">{{__('net_suite_integration.api_show')}}</h4>
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>{{__('net_suite_integration.entity')}}</th>
                                                <th>{{__('net_suite_integration.url')}}</th>
                                                <th>{{__('net_suite_integration.last_syncing_date_time')}}</th>
                                                <th>{{__('net_suite_integration.synchronize')}}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td scope="row">{{__('net_suite_integration.project_restful')}}</td>
                                                <td dir="ltr">{{str_limit($netSuiteIntegration->project_api_index, 20)}}</td>
                                                <td>
                                                    {{$netSuiteIntegration->project_sync_date_time}}
                                                </td>
                                                <td>
                                                    <a href="{{action('NetSuiteIntegrationController@syncProjectsData', ['id'=>$netSuiteIntegration->id])}}" type="button" class="btn btn-primary btn-sm">
                                                        <i class="fas fa-sync-alt"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            <span>
                                                <tr>
                                                    <td>{{__('net_suite_integration.customer_restful')}}</td>
                                                    <td>
                                                        {{__('net_suite_integration.customer_api_index')}}:<br>
                                                        <span dir="ltr">
                                                            {{str_limit($netSuiteIntegration->customer_api_index, 20)}}
                                                        </span>
                                                    </td>
                                                    <td>
                                                        {{$netSuiteIntegration->customer_sync_date_time}}
                                                    </td>
                                                    <td>
                                                        <a href="{{action('NetSuiteIntegrationController@syncCustomersData', ['id'=>$netSuiteIntegration->id])}}" type="button" class="btn btn-primary btn-sm">
                                                            <i class="fas fa-sync-alt"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="border-top-0"></td>
                                                    <td class="border-top-0">
                                                        {{__('net_suite_integration.customer_api_store')}}:<br>
                                                        <span dir="ltr">
                                                            {{str_limit($netSuiteIntegration->customer_api_store, 20)}}
                                                        </span>
                                                    </td>
                                                    <td class="border-top-0">

                                                    </td>
                                                    <td class="border-top-0"></td>
                                                </tr>
                                                <tr>
                                                    <td class="border-top-0"></td>
                                                    <td class="border-top-0">
                                                        {{__('net_suite_integration.customer_api_update')}}:<br>
                                                        <span dir="ltr">
                                                            {{str_limit($netSuiteIntegration->customer_api_update, 20)}}
                                                        </span>
                                                    </td>
                                                    <td class="border-top-0">

                                                    </td>
                                                    <td class="border-top-0"></td>
                                                </tr>
                                                <tr>
                                                    <td class="border-top-0"></td>
                                                    <td class="border-top-0">
                                                        {{__('net_suite_integration.customer_api_destroy')}}:<br>
                                                        <span dir="ltr">
                                                            {{str_limit($netSuiteIntegration->customer_api_destroy, 20)}}
                                                        </span>
                                                    </td>
                                                    <td class="border-top-0">

                                                    </td>
                                                    <td class="border-top-0"></td>
                                                </tr>
                                            </span>
                                            <tr>
                                                <td>{{__('net_suite_integration.phase_restful')}}</td>
                                                <td dir="ltr">{{str_limit($netSuiteIntegration->phase_api_index, 20)}}</td>
                                                <td>
                                                    {{$netSuiteIntegration->phase_sync_date_time}}
                                                </td>
                                                <td>
                                                    <a href="{{action('NetSuiteIntegrationController@syncPhasesData', ['id'=>$netSuiteIntegration->id])}}" type="button" class="btn btn-primary btn-sm">
                                                        <i class="fas fa-sync-alt"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>{{__('net_suite_integration.zone_restful')}}</td>
                                                <td dir="ltr">{{str_limit($netSuiteIntegration->zone_api_index, 20)}}</td>
                                                <td>
                                                    {{$netSuiteIntegration->zone_sync_date_time}}
                                                </td>
                                                <td>
                                                    <a href="{{action('NetSuiteIntegrationController@syncZonesData', ['id'=>$netSuiteIntegration->id])}}" type="button" class="btn btn-primary btn-sm">
                                                        <i class="fas fa-sync-alt"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>{{__('net_suite_integration.block_restful')}}</td>
                                                <td dir="ltr">{{str_limit($netSuiteIntegration->block_api_index, 20)}}</td>
                                                <td>
                                                    {{$netSuiteIntegration->block_sync_date_time}}
                                                </td>
                                                <td>
                                                    <a href="{{action('NetSuiteIntegrationController@syncBlocksData', ['id'=>$netSuiteIntegration->id])}}" type="button" class="btn btn-primary btn-sm">
                                                        <i class="fas fa-sync-alt"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>{{__('net_suite_integration.unit_model_restful')}}</td>
                                                <td dir="ltr">{{str_limit($netSuiteIntegration->unit_model_api_index, 20)}}</td>
                                                <td>
                                                    {{$netSuiteIntegration->unit_model_sync_date_time}}
                                                </td>
                                                <td>
                                                    <a href="{{action('NetSuiteIntegrationController@syncUnitModelsData', ['id'=>$netSuiteIntegration->id])}}" type="button" class="btn btn-primary btn-sm">
                                                        <i class="fas fa-sync-alt"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>{{__('net_suite_integration.unit_restful')}}</td>
                                                <td dir="ltr">{{str_limit($netSuiteIntegration->unit_api_index, 20)}}</td>
                                                <td>
                                                    {{$netSuiteIntegration->unit_sync_date_time}}
                                                </td>
                                                <td>
                                                    <a href="{{action('NetSuiteIntegrationController@syncUnitsData', ['id'=>$netSuiteIntegration->id])}}" type="button" class="btn btn-primary btn-sm">
                                                        <i class="fas fa-sync-alt"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>{{__('net_suite_integration.bank_restful')}}</td>
                                                <td dir="ltr">{{str_limit($netSuiteIntegration->bank_api_index, 20)}}</td>
                                                <td>
                                                    {{$netSuiteIntegration->bank_sync_date_time}}
                                                </td>
                                                <td>
                                                    <a href="{{action('NetSuiteIntegrationController@syncBanksData', ['id'=>$netSuiteIntegration->id])}}" type="button" class="btn btn-primary btn-sm">
                                                        <i class="fas fa-sync-alt"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            <span>
                                                <tr>
                                                    <td>{{__('net_suite_integration.reservation_restful')}}</td>
                                                    <td>
                                                        {{__('net_suite_integration.reservation_api_index')}}:<br>
                                                        <span dir="ltr">
                                                            {{str_limit($netSuiteIntegration->reservation_api_index, 20)}}
                                                        </span>
                                                    </td>
                                                    <td>
                                                        {{$netSuiteIntegration->reservation_sync_date_time}}
                                                    </td>
                                                    <td>
                                                        <a href="{{action('NetSuiteIntegrationController@syncReservationsData', ['id'=>$netSuiteIntegration->id])}}" type="button" class="btn btn-primary btn-sm">
                                                            <i class="fas fa-sync-alt"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="border-top-0"></td>
                                                    <td class="border-top-0">
                                                        {{__('net_suite_integration.reservation_api_store')}}:<br>
                                                        <span dir="ltr">
                                                            {{str_limit($netSuiteIntegration->reservation_api_store, 20)}}
                                                        </span>
                                                    </td>
                                                    <td class="border-top-0">

                                                    </td>
                                                    <td class="border-top-0"></td>
                                                </tr>
                                                <tr>
                                                    <td class="border-top-0"></td>
                                                    <td class="border-top-0">
                                                        {{__('net_suite_integration.reservation_api_update')}}:<br>
                                                        <span dir="ltr">
                                                            {{str_limit($netSuiteIntegration->reservation_api_update, 20)}}
                                                        </span>
                                                    </td>
                                                    <td class="border-top-0">

                                                    </td>
                                                    <td class="border-top-0"></td>
                                                </tr>
                                                <tr>
                                                    <td class="border-top-0"></td>
                                                    <td class="border-top-0">
                                                        {{__('net_suite_integration.reservation_api_destroy')}}:<br>
                                                        <span dir="ltr">
                                                            {{str_limit($netSuiteIntegration->reservation_api_destroy, 20)}}
                                                        </span>
                                                    </td>
                                                    <td class="border-top-0">

                                                    </td>
                                                    <td class="border-top-0"></td>
                                                </tr>
                                            </span>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>

    @include('partial.deleteConfirm',['name'=>$netSuiteIntegration->name,
        'id'=> $netSuiteIntegration->id,
        'message'=> __('net_suite_integration.confirm_delete_message'),
        'route'=>'NetSuiteIntegrationController@destroy'])
@endsection
