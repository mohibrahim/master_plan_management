@extends('layouts.app')
@section('title')
	{{__('net_suite_integration.edit_net_suite_integration')}}
@endsection
@section('content')
	<div class="container-fluid">
		<div class="col">
			<div class="card card-info">
				<div class="card-heading">
					<h3 class="card-header text-center">{{$netSuiteIntegration->name}}</h3>
				</div>
				<div class="card-body ">
					@include('errors.list')
					{!! Form::model($netSuiteIntegration, ['action' => ['NetSuiteIntegrationController@update', 'id'=>$netSuiteIntegration->id], 'files'=>false,
					'method'=>'put'])!!}
					@include('net_suite_integrations._form')
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
@endsection

