<div class="container-fluid border">
	<div class="row">
		<div class="col">

			<div class="form-group">
				<span style="color:#f47373">*</span>
				{!!Form::label('name', __('net_suite_integration.name'))!!}
				{!!Form::text('name', null,['class'=>'form-control', 'placeholder'=>__('net_suite_integration.name_placeholder')]) !!}
			</div>

            <div class="card mt-3">
                <div class="card-body border border-primary">
                    <h4 class="card-title text-center">{{__('net_suite_integration.api')}}</h4>
                    <div class='form-group'>
                        {!!Form::label('api_authorization', __('net_suite_integration.api_authorization'))!!}
                        {!!Form::text('api_authorization', null,['class'=>'form-control', 'placeholder'=>__('net_suite_integration.api_authorization_placeholder')]) !!}
                    </div>


                    <div class="card mt-3">
                        <div class="card-body">
                        <h5 class="mb-3">{{__('net_suite_integration.project_restful')}}</h5>
                            <div class='form-group'>
                                {!!Form::label('project_api_index', __('net_suite_integration.project_api_index'))!!}
                                {!!Form::text('project_api_index', null,['class'=>'form-control', 'placeholder'=>__('net_suite_integration.project_api_index_placeholder')]) !!}
                            </div>
                        </div>
                    </div>

                    <div class="card mt-3">
                        <div class="card-body">
                        <h5 class="mb-3">{{__('net_suite_integration.phase_restful')}}</h5>
                            <div class='form-group'>
                                {!!Form::label('phase_api_index', __('net_suite_integration.phase_api_index'))!!}
                                {!!Form::text('phase_api_index', null,['class'=>'form-control', 'placeholder'=>__('net_suite_integration.phase_api_index_placeholder')]) !!}
                            </div>
                        </div>
                    </div>

                    <div class="card mt-3">
                        <div class="card-body">
                        <h5 class="mb-3">{{__('net_suite_integration.zone_restful')}}</h5>
                            <div class='form-group'>
                                {!!Form::label('zone_api_index', __('net_suite_integration.zone_api_index'))!!}
                                {!!Form::text('zone_api_index', null,['class'=>'form-control', 'placeholder'=>__('net_suite_integration.zone_api_index_placeholder')]) !!}
                            </div>
                        </div>
                    </div>

                    <div class="card mt-3">
                        <div class="card-body">
                        <h5 class="mb-3">{{__('net_suite_integration.block_restful')}}</h5>
                            <div class='form-group'>
                                {!!Form::label('block_api_index', __('net_suite_integration.block_api_index'))!!}
                                {!!Form::text('block_api_index', null,['class'=>'form-control', 'placeholder'=>__('net_suite_integration.block_api_index_placeholder')]) !!}
                            </div>
                        </div>
                    </div>

                    <div class="card mt-3">
                        <div class="card-body">
                        <h5 class="mb-3">{{__('net_suite_integration.unit_model_restful')}}</h5>
                            <div class='form-group'>
                                {!!Form::label('unit_model_api_index', __('net_suite_integration.unit_model_api_index'))!!}
                                {!!Form::text('unit_model_api_index', null,['class'=>'form-control', 'placeholder'=>__('net_suite_integration.unit_model_api_index_placeholder')]) !!}
                            </div>
                        </div>
                    </div>

                    <div class="card mt-3">
                        <div class="card-body">
                        <h5 class="mb-3">{{__('net_suite_integration.unit_restful')}}</h5>
                            <div class='form-group'>
                                {!!Form::label('unit_api_index', __('net_suite_integration.unit_api_index'))!!}
                                {!!Form::text('unit_api_index', null,['class'=>'form-control', 'placeholder'=>__('net_suite_integration.unit_api_index_placeholder')]) !!}
                            </div>
                        </div>
                    </div>

                    <div class="card mt-3">
                        <div class="card-body">
                        <h5 class="mb-3">{{__('net_suite_integration.customer_restful')}}</h5>
                            <div class='form-group'>
                                {!!Form::label('customer_api_index', __('net_suite_integration.customer_api_index'))!!}
                                {!!Form::text('customer_api_index', null,['class'=>'form-control', 'placeholder'=>__('net_suite_integration.customer_api_index_placeholder')]) !!}
                            </div>
                            <div class='form-group'>
                                {!!Form::label('customer_api_store', __('net_suite_integration.customer_api_store'))!!}
                                {!!Form::text('customer_api_store', null,['class'=>'form-control', 'placeholder'=>__('net_suite_integration.customer_api_store_placeholder')]) !!}
                            </div>
                            <div class='form-group'>
                                {!!Form::label('customer_api_update', __('net_suite_integration.customer_api_update'))!!}
                                {!!Form::text('customer_api_update', null,['class'=>'form-control', 'placeholder'=>__('net_suite_integration.customer_api_update_placeholder')]) !!}
                            </div>
                            <div class='form-group'>
                                {!!Form::label('customer_api_destroy', __('net_suite_integration.customer_api_destroy'))!!}
                                {!!Form::text('customer_api_destroy', null,['class'=>'form-control', 'placeholder'=>__('net_suite_integration.customer_api_destroy_placeholder')]) !!}
                            </div>
                        </div>
                    </div>

                    <div class="card mt-3">
                        <div class="card-body">
                        <h5 class="mb-3">{{__('net_suite_integration.bank_restful')}}</h5>
                            <div class='form-group'>
                                {!!Form::label('bank_api_index', __('net_suite_integration.bank_api_index'))!!}
                                {!!Form::text('bank_api_index', null,['class'=>'form-control', 'placeholder'=>__('net_suite_integration.bank_api_index_placeholder')]) !!}
                            </div>
                        </div>
                    </div>

                    <div class="card mt-3">
                        <div class="card-body">
                        <h5 class="mb-3">{{__('net_suite_integration.reservation_restful')}}</h5>
                            <div class='form-group'>
                                {!!Form::label('reservation_api_index', __('net_suite_integration.reservation_api_index'))!!}
                                {!!Form::text('reservation_api_index', null,['class'=>'form-control', 'placeholder'=>__('net_suite_integration.reservation_api_index_placeholder')]) !!}
                            </div>
                            <div class='form-group'>
                                {!!Form::label('reservation_api_store', __('net_suite_integration.reservation_api_store'))!!}
                                {!!Form::text('reservation_api_store', null,['class'=>'form-control', 'placeholder'=>__('net_suite_integration.reservation_api_store_placeholder')]) !!}
                            </div>
                            <div class='form-group'>
                                {!!Form::label('reservation_api_update', __('net_suite_integration.reservation_api_update'))!!}
                                {!!Form::text('reservation_api_update', null,['class'=>'form-control', 'placeholder'=>__('net_suite_integration.reservation_api_update_placeholder')]) !!}
                            </div>
                            <div class='form-group'>
                                {!!Form::label('reservation_api_destroy', __('net_suite_integration.reservation_api_destroy'))!!}
                                {!!Form::text('reservation_api_destroy', null,['class'=>'form-control', 'placeholder'=>__('net_suite_integration.reservation_api_destroy_placeholder')]) !!}
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class='form-group mt-3'>
				{!!Form::label('notes', __('net_suite_integration.notes'))!!}
				{!!Form::textarea('notes', null, ['class'=>'form-control', 'rows'=>'5',
				'placeholder'=>__('net_suite_integration.notes_placeholder')])!!}
            </div>
		</div>

	</div>
</div>


<div class="form-group">
	{!!Form::submit(__('net_suite_integration.save'), ['class'=>'btn btn-primary form-control'])!!}
</div>



