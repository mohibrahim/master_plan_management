@extends('layouts.app')
@section('title')
	{{__('net_suite_integration.all_net_suite_integrations')}}
@endsection
@section('content')
	<div class="col-md-12">
		<div class="card">

			<div class="card-header">
				{{__('net_suite_integration.all_net_suite_integrations')}}
			</div>
				<div class="card-body">
					<div class="form-inline">
						<div class="input-group mb-2 mr-sm-2">
							<label class="m-2" for="searching-input">{{__('net_suite_integration.searching_on_net_suite_integration')}}: </label>
							<input type="text" class="form-control" id="searching-input" placeholder="{{__('net_suite_integration.searching_word')}}" autofocus>
						</div>

						<button id="searching-button" type="button" class="btn btn-primary mb-2">{{__('net_suite_integration.search')}}</button>
						<input id="searching-button-required" type="hidden" value="{{__('net_suite_integration.searching_button_required')}}">
						<input id="searching-button-searching" type="hidden" value="{{__('net_suite_integration.searching_button_searching')}}">
						<input id="searching-button-search" type="hidden" value="{{__('net_suite_integration.searching_button_search')}}">
					</div>
					<hr>
					<table class="table table-hover" id="real-estate-developers">
						<thead>
							<tr>
								<th>#</th>
								<th>{{__('net_suite_integration.id')}}</th>
                                <th>{{__('net_suite_integration.name')}}</th>
                                <th>{{__('project.created_at')}}</th>
								<th>{{__('project.creator_name')}}</th>
							</tr>
						</thead>
						<tbody>
							@foreach($netSuiteIntegrations as $netSuiteIntegrationKey => $netSuiteIntegration)
								<tr>
									<td>{{$netSuiteIntegrationKey+1}}</td>
									<td>{{$netSuiteIntegration->id}}</td>
									<td>{!! $netSuiteIntegration->net_suite_integration_show_link_name !!}</td>
									<td>{{$netSuiteIntegration->created_at}}</td>
									<td>{{optional($netSuiteIntegration->creator)->name}}</td>

								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="d-flex justify-content-center">

					{{$netSuiteIntegrations->links()}}
				</div>
			</div>

			<input id="data-table-search" type="hidden" value="{{__('data_table.search')}}">
			<input id="data-table-zero-record" type="hidden" value="{{__('data_table.zeroRecords')}}">
	</div>
@endsection

@section('head')
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css"> -->
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
@endsection
@section('js_footer')
	<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
	<script>
		$(document).ready(function () {
			dataTable = $('#real-estate-developers').DataTable({
				"paging":   false,
				"info":     false,
				"language": {
					"search": $("#data-table-search").val(),
					"zeroRecords": $("#data-table-zero-record").val(),
				},
			});
			searchingOnNetSuiteIntegrationsAjax();
		});
	</script>
@endsection
