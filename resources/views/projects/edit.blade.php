@extends('layouts.app')
@section('title')
	{{__('project.edit_project')}}
@endsection
@section('content')
	<div class="container-fluid">
		<div class="col">
			<div class="card card-info">
				<div class="card-heading">
					<h3 class="card-header text-center">{{$project->name}}</h3>
				</div>
				<div class="card-body ">
					@include('errors.list')
					{!! Form::model($project, ['action' => ['ProjectController@update', 'id'=>$project->id], 'files'=>true,
					'method'=>'put'])!!}
					@include('projects._form')
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
@endsection

