@extends('layouts.app')

@section('title')
{{$project->name}}
@endsection

@section('content')
<div class="col-md-12">
	<div class="card">
		<div class="card-header">
			<h2 class="text-center">{{$project->name}}</h2>
		</div>
		<div class="card-body ">
			<div class="container-fluid">




				<div class="row mt-3">
					<div class="col">
						<div class="card">
							<div class="card-body">
                                <form>
                                    <fieldset>
                                        <legend class="text-center"> {{__('project.filtering_units')}} </legend>

                                        <div class='form-group'>
                                            {!!Form::label('zone_id', __('project.zone_id'))!!}
                                            {!!Form::select('zone_id', $project->zones_names_ids,
                                            null,['class'=>'form-control', 'placeholder'=>__('unit.zone_name_placeholder'),
                                            'id'=>'zone-id-select-element']) !!}
                                        </div>

                                        <div class="form-group">
                                            {!!Form::label('unit_model_id', __('project.unit_model_id'))!!}
                                            {!!Form::select('unit_model_id', $project->units_models_names_ids,
                                            null,['class'=>'form-control', 'placeholder'=>__('unit.unit_model_name_placeholder'),
                                            'id'=>'unit-model-id-select-element']) !!}
                                        </div>

                                        <div class='form-group'>
                                            {!!Form::label('unit_status', __('project.unit_status'))!!}
                                            {!!Form::select('unit_status', $unitStatus, null,['class'=>'form-control',
                                            'placeholder'=>__('unit.status_placeholder'),
                                            'id'=>'unit-status-select-element']) !!}
                                        </div>
                                        <div class='form-group'>
                                            {!!Form::label('in-block-radio-button', __('project.in_block'))!!}
                                            {!!Form::checkbox('in_block',"che",false, ['id'=>'in-block-radio-button']) !!}
                                        </div>
                                        <div class="form-group">
                                            <button type="button" id="filtering-btn" class="btn btn-primary btn-block form-control" data-filtered-project-id="{{$project->id}}">{{__('project.filtering')}}</button>
                                        </div>
                                        <div class="form-group">
                                            <input type="reset" class="btn btn-primary btn-block form-control" value="{{__('project.reset_filtering')}}">
                                        </div>
                                    </fieldset>
                                </form>
							</div>
						</div>
					</div>
				</div>

				<div class="row mt-3">
					<div class="col">
						<div class="card">
							<div class="card-body">
								<legend class="text-center">{{__('project.master_plan_image')}}</legend>
								<div class="overflow-auto" id="over-flow-wrapper">

									<img src="{{asset($project->master_plan_image)}}"
										alt="" id="map-image" class="m-auto" usemap="#master-plan-test">
									<map name="master-plan-test" id="map-element">
										@foreach($project->units as $key=> $unit)
										<area class="my-selected-area" id="area-{{$key}}" shape="poly"
											coords="{{$unit->html_area_tag_coords}}"
											data-maphilight='{"stroke":true, "strokeColor":"000000","strokeWidth":2,"fillColor":"{{$unit->status_color}}","fillOpacity":0.7,"alwaysOn":true}'
											href="{{action('UnitController@show', ['id'=>$unit->id])}}" alt="">
										@endforeach
									</map>
								</div>
							</div>
						</div>
					</div>
				</div>






				<hr>
				<div class="row">
					<div class="col-md-4 text-left">
						<img src="{{asset($project->master_plan_image)}}"
							class="rounded w-100 border border-default mb-3" alt="">
						<div class=" row text-muted">
							<div class="col small">
								{{__('project.created_at')}}:
							</div>
							<div class="col small">
								{{$project->created_at}}
							</div>
						</div>
						<div class=" row text-muted">
							<div class="col small">
								{{__('project.creator_name')}}:
							</div>
							<div class="col small">
								{{optional($project->creator)->name}}
							</div>
						</div>
						<div class=" row text-muted">
							<div class="col small">
								{{__('project.updated_at')}}:
							</div>
							<div class="col small">
								{{$project->updated_at}}
							</div>
						</div>
						<div class=" row text-muted">
							<div class="col small">
								{{__('project.last_updater_name')}}:
							</div>
							<div class="col small">
								{{optional($project->lastUpdater)->name}}
							</div>
						</div>
						<div class="row mt-3">
							<div class="col">
								@if(in_array('update_projects', $permissions))
								<a href="{{ action('ProjectController@edit', ['id'=>$project->id]) }}"
									class="btn btn-sm btn-warning btn-block"><i class="fas fa-edit"></i>
									{{__('project.edit_project')}}
								</a>
								@endif
								@if(in_array('destroy_projects', $permissions))
								<td><button type="button" class="btn btn-sm btn-danger btn-block" data-toggle="modal"
										data-target="#myModal"><i class="far fa-trash-alt"></i>
										{{__('project.delete_project')}} </button></td>
								@endif
								@if(	in_array('store_catalogs', $permissions) &&
										in_array('store_projects', $permissions)
									)
								<a href="{{ action('CatalogController@create', ['model_associated_type'=>'project_id', 'model_associated_type_id'=>$project->id]) }}"
									class="btn btn-sm btn-primary btn-block"><i class="fas fa-images"></i>
									{{__('project.add_catalog')}}
								</a>
								@endif

							</div>

						</div>

					</div>
					<div class="col">

						<div class="container-fluid">
							<div class="row">
								<div class="col">
									<table class="table">
										<tbody>
                                            <tr>
                                                <th scope="row" class="border-0">{{__('project.project_code')}}</th>
                                                <td class="border-0">{{$project->project_code}}</td>
                                            </tr>
											<tr>
												<th scope="row" class="border-0">{{__('project.name')}}</th>
												<td class="border-0">{{$project->name}}</td>
											</tr>
											<tr>
												<th scope="row" class="border-0">{{__('project.logo')}}</th>
												<td class="border-0">
                                                    <img src="{{asset($project->logo)}}" width="100px" alt="{{$project->name}}">
                                                </td>
											</tr>
											<tr>
												<th scope="row" class="border-0">{{__('project.supposed_start_date')}}
												</th>
												<td class="border-0">{{$project->supposed_start_date}}</td>
											</tr>
											<tr>
												<th scope="row" class="border-0">{{__('project.actually_start_date')}}
												</th>
												<td class="border-0">{{$project->actually_start_date}}</td>
											</tr>
											<tr>
												<th scope="row" class="border-0">{{__('project.supposed_end_date')}}
												</th>
												<td class="border-0">{{$project->supposed_end_date}}</td>
											</tr>
											<tr>
												<th scope="row" class="border-0">{{__('project.actually_end_date')}}
												</th>
												<td class="border-0">{{$project->actually_end_date}}</td>
											</tr>
											<tr>
												<th scope="row" class="border-0">
													{{__('project.percentage_of_completion')}}</th>
												<td class="border-0">{{$project->percentage_of_completion}}</td>
											</tr>
											<tr>
												<th scope="row" class="border-0">{{__('project.notes')}}</th>
												<td class="border-0">{{$project->notes}}</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<hr>
						</div>

					</div>
				</div>



				<div class="row mt-3">

					<div class="col">
						<div class="card">
							<div class="card-body">
								<h4 class="font-weight-bold text-center m-3"> الكتالوجات </h4>

								<table class="table table-hover" id="catalogs">
									<thead>
										<tr>
											<th>#</th>
											<th>{{__('catalog.id')}}</th>
											<th>{{__('catalog.name')}}</th>
											<th>{{__('catalog.description')}}</th>
											<th>{{__('catalog.created_at')}}</th>
											<th>{{__('catalog.creator_name')}}</th>
										</tr>
									</thead>
									<tbody>
										@foreach($project->catalogs as $catalogKey => $catalog)
										<tr>
											<td>{{$catalogKey+1}}</td>
											<td>{{$catalog->id}}</td>
											<td>
												<a href="{{action('CatalogController@show', ['id'=>$catalog->id])}}">
													{{$catalog->name}}
												</a>
											</td>
											<td>{{$catalog->description}}</td>
											<td>{{$catalog->created_at}}</td>
											<td>{{optional($catalog->creator)->name}}</td>

										</tr>
										@endforeach
									</tbody>
								</table>
								<input id="data-table-search" type="hidden" value="{{__('data_table.search')}}">
								<input id="data-table-zero-record" type="hidden"
                                    value="{{__('data_table.zeroRecords')}}">
                                <input type="hidden" id="filtering-project-button-name" value="{{__('project.filtering')}}">
                                <input type="hidden" id="filtering-project-button-name-when-loading" value="{{__('project.filtering_when_loading')}}">
							</div>
						</div>
					</div>


				</div>

			</div>
		</div>
	</div>

</div>

@include('partial.deleteConfirm',['name'=>$project->name,
'id'=> $project->id,
'message'=> __('project.confirm_delete_message'),
'route'=>'ProjectController@destroy'])
@endsection

@section('head')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
@endsection

@section('js_footer')
<script src="{{url('js/maphilight/jquery.maphilight.js')}}"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

<script>
	$(document).ready(function () {
		masterPlanImageAreaHighlightAndFilter();

		dataTable = $('#catalogs').DataTable({
			"paging": false,
			"info": false,
			"language": {
				"search": $("#data-table-search").val(),
				"zeroRecords": $("#data-table-zero-record").val(),
			},
		});


	});
</script>
@endsection
