<div class="container-fluid border">
	<div class="row">
		<div class="col">


			<div class="form-group">
				<span style="color:#f47373">*</span>
				{!!Form::label('name', __('project.name'))!!}
				{!!Form::text('name', null,['class'=>'form-control', 'placeholder'=>__('project.name_placeholder')]) !!}
            </div>

            <div class="form-group">
				{!!Form::label('project_code', __('project.project_code'))!!}
				{!!Form::text('project_code', null,['class'=>'form-control', 'placeholder'=>__('project.project_code_placeholder')]) !!}
            </div>

            <div class="form-group">
				{!!Form::label('owner_name', __('project.owner_name'))!!}
				{!!Form::text('owner_name', null,['class'=>'form-control', 'placeholder'=>__('project.owner_name_placeholder')]) !!}
			</div>

			<div class="border border-secondary rounded p-3 mb-3">

				<div class='form-group'>
					<span style='color:#f47373'>*</span>
					{!!Form::label('master_plan_image', __('project.master_plan_image'))!!}
					{!!Form::file('master_plan_image', ['class'=>'form-control', 'placeholder'=>__('project.master_plan_image_placeholder')])!!}
                </div>

				<div class='form-group'>
					{!!Form::label('logo', __('project.logo'))!!}
					{!!Form::file('logo', ['class'=>'form-control', 'placeholder'=>__('project.logo_placeholder')])!!}
                </div>


                @if(!empty($project->logo) && ($project->logo != 'images/projects_logos/no_image.png'))
                    <div id="logo-wrapper" class="form-group border rounded p-2">
                        <img src="{{asset($project->logo)}}" alt="" width="50px">
                    <button type="button" id="delete-logo" class="btn btn-danger btn-sm" data-logo-name="{{$project->logo}}">{{__('partial.delete')}}</button>
                    </div>
                @endif

				<div class='form-group'>
                    {!!Form::label('master_plan_json', __('project.master_plan_json'))!!}
                    @if(request()->route()->named('projects.edit'))
                    <p class="text-danger">{{__('project.json_unit_creation_caution')}}</p>
                    @endif
					{!!Form::textarea('master_plan_json', null, ['class'=>'form-control', 'rows'=>'5',
					'placeholder'=>__('project.master_plan_json_placeholder')])!!}
				</div>

				<div class="border border-secondary rounded p-3 mb-3">
					<h4>{{__('project.master_plan_image_decrease_size')}}</h4>
					<div class="form-group">
						{!!Form::label('new_image_width', __('project.new_image_width'))!!}
						{!!Form::text('new_image_width', null,['class'=>'form-control', 'placeholder'=>__('project.new_image_width_placeholder')]) !!}
					</div>
				</div>

			</div>


			<div class="form-group">
				{!!Form::label('supposed_start_date', __('project.supposed_start_date'))!!}
				{!!Form::date('supposed_start_date', null,['class'=>'form-control', 'placeholder'=>__('project.supposed_start_date')]) !!}
			</div>

			<div class="form-group">
				{!!Form::label('actually_start_date', __('project.actually_start_date'))!!}
				{!!Form::date('actually_start_date', null,['class'=>'form-control', 'placeholder'=>__('project.actually_start_date')]) !!}
			</div>

			<div class="form-group">
				{!!Form::label('supposed_end_date', __('project.supposed_end_date'))!!}
				{!!Form::date('supposed_end_date', null,['class'=>'form-control', 'placeholder'=>__('project.supposed_end_date')]) !!}
			</div>

			<div class="form-group">
				{!!Form::label('actually_end_date', __('project.actually_end_date'))!!}
				{!!Form::date('actually_end_date', null,['class'=>'form-control', 'placeholder'=>__('project.actually_end_date')]) !!}
			</div>

			<div class="form-group">
				{!!Form::label('percentage_of_completion', __('project.percentage_of_completion'))!!}
				{!!Form::text('percentage_of_completion', null,['class'=>'form-control', 'placeholder'=>__('project.percentage_of_completion')]) !!}
			</div>



			<div class='form-group'>
				{!!Form::label('notes', __('project.notes'))!!}
				{!!Form::textarea('notes', null, ['class'=>'form-control', 'rows'=>'5',
				'placeholder'=>__('project.notes_placeholder')])!!}
			</div>
		</div>

	</div>
</div>


<div class="form-group">
	{!!Form::submit(__('project.save'), ['class'=>'btn btn-primary form-control'])!!}
</div>

@section('js_footer')
    <script>
        $(document).ready(function(){
            deleteLogo();
        });
    </script>
@endsection




