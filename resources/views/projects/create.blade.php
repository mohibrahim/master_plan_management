@extends('layouts.app')
@section('title')
	{{__('project.add_project')}}
@endsection
	@section('content')
	<div class="container-fluid">
		<div class="col">
			<div class="card card-info">
				<div class="card-heading">
					<h3 class="card-header text-center">{{__('project.add_project')}}</h3>
				</div>
				<div class="card-body ">
					@include('errors.list')
					{!! Form::open(['action' => ['ProjectController@store'], 'files'=>true, 'method'=>'POST'])!!}
						@include('projects._form')
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
@endsection
