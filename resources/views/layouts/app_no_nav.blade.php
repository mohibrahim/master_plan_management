<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>
        @yield('title')
    </title>

    <!-- Scripts -->
    <script src="{{ url('js/app.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ url('css/app.css') }}" rel="stylesheet">
    @yield('head')
    @if(app()->getLocale() == 'ar')
        <link href="{{ url('css/bootstrap4rtl.min.css') }}" rel="stylesheet">
    @endif
</head>

<body>
    <div id="app">
        <main class="py-4">
            @yield('content')
            @yield('js_footer')
        </main>
    </div>
</body>

</html>
