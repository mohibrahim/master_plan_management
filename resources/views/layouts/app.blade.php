<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>
        @yield('title')
    </title>

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    @yield('head')
    @if(app()->getLocale() == 'ar')
        <link href="{{ url('css/bootstrap4rtl.min.css') }}" rel="stylesheet">
    @endif

</head>
<body>
    <div id="app">

        @include('layouts.nav._upper_main_nav')

        <main class="py-4">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 offset-md-3 text-center">
                        @include('flash::message')
                    </div>
                </div>
            </div>
            @yield('content')
            @yield('js_footer')
            <script>
                upAndDownMainUpperNavbar();
            </script>
        </main>
        <div class="footer-pusher"></div>
    </div>
    <footer class="footer-pusher pt-4">
        <h6 class="text-center">{!!__('footer.all_rights_reserved')!!}</h6>
    </footer>
</body>
</html>
