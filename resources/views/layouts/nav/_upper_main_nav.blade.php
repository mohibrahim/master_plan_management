<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm" id="main-upper-navbar">
    <div class="container-fluid">
        <a class="navbar-brand" href="{{ url('/home') }}">
            <img src="{{url('images/helper_images/DHB_Logo_RGB_Green.jpg')}}" width="80px" class="d-block ml-4">
            {{-- {{ __('upper_main_nav.app_name') }} --}}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav d-flex flex-wrap">
                @include('layouts.nav.left._left')
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown mx-3">
                    @if(app()->getLocale() == 'en')
                        <a class="nav-link dropdown-toggle" href="#" id="dropdown09" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">English <img src="{{url('images/helper_images/flag-usa2x.png')}}" class="img-fluid" alt="English" width="20px"></a>
                    @endif
                    @if(app()->getLocale() == 'ar')
                        <a class="nav-link dropdown-toggle" href="#" id="dropdown09" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">العربية <img src="{{url('images/helper_images/flag-egypt2x.png')}}" class="img-fluid" alt="English" width="20px"></a>
                    @endif
                    <div class="dropdown-menu" aria-labelledby="dropdown09">
                        @if(app()->getLocale() != 'ar')
                            <a class="dropdown-item" href="{{route('switch_language', ['language'=>'ar'])}}">العربية  <img src="{{url('images/helper_images/flag-egypt2x.png')}}" class="img-fluid" alt="English" width="20px"></a>
                        @endif
                        @if(app()->getLocale() != 'en')
                            <a class="dropdown-item" href="{{route('switch_language', ['language'=>'en'])}}">English  <img src="{{url('images/helper_images/flag-usa2x.png')}}" class="img-fluid" alt="English" width="20px"></a>
                        @endif
                    </div>
                </li>
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('upper_main_nav.login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('upper_main_nav.register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                {{ __('upper_main_nav.logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
<div class="text-center">
    <button type="button" class="btn btn-outline-dark btn-sm border-0" id="main-upper-navbar-up-down-btn">
        <i class="fas fa-chevron-up" id="chevron-wrapper"></i>
    </button>
</div>
