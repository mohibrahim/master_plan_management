<li class="nav-item dropdown">
    @if(in_array('index_users',$permissions))
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-users"></i> {{__('upper_main_nav.user_system')}}
        </a>
    @endif
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">

        @if(in_array('index_users', $permissions))
            <div class="dropdown-header">{{__('upper_main_nav.users')}}</div>
            <a class="dropdown-item" href="{{ action('UserController@index') }}">{{__('upper_main_nav.index_users')}}</a>
            @if(in_array('store_users', $permissions))
                <a class="dropdown-item" href="{{ action('UserController@create') }}">{{__('upper_main_nav.create_user')}}</a>
            @endif
            @if(in_array('index_users_not_verified_email', $permissions))
                <a class="dropdown-item" href="{{ action('UserController@indexUsersNotVerifiedEmail') }}">{{__('upper_main_nav.verify_users_emails')}}</a>
            @endif
            <div class="dropdown-divider"></div>
        @endif

        @if(in_array("index_roles", $permissions))
            <div class="dropdown-header">{{__('upper_main_nav.roles')}}</div>
            @if(in_array("store_roles", $permissions))
                <a class="dropdown-item" href="{{ action('RoleController@create') }}">{{__('upper_main_nav.create_role')}}</a>
            @endif
            <a class="dropdown-item" href="{{ action('RoleController@index') }}">{{__('upper_main_nav.index_roles')}}</a>
            <div class="dropdown-divider"></div>
        @endif

        @if(in_array('index_permissions', $permissions))
            <div class="dropdown-header">{{__('upper_main_nav.permissions')}}</div>
            @if(in_array('store_permissions', $permissions))
                <a class="dropdown-item" href="{{ action('PermissionController@create') }}">{{__('upper_main_nav.create_permission')}}</a>
            @endif
            <a class="dropdown-item" href="{{ action('PermissionController@index') }}">{{__('upper_main_nav.index_permissions')}}</a>
        @endif

    </div>
</li>