<li class="nav-item dropdown">
    @if(in_array('index_catalogs',$permissions))
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-images"></i> {{__('upper_main_nav.catalogs')}}
        </a>
    @endif
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">

        @if(in_array('index_catalogs', $permissions))
            <div class="dropdown-header">{{__('upper_main_nav.catalogs')}}</div>
            <a class="dropdown-item" href="{{ action('CatalogController@index') }}">{{__('upper_main_nav.index_catalogs')}}</a>
            @if(in_array('store_catalogs', $permissions))
                <a class="dropdown-item" href="{{ action('CatalogController@create') }}">{{__('upper_main_nav.create_catalog')}}</a>
            @endif
        @endif

    </div>
</li>