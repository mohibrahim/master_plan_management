<li class="nav-item dropdown">
    @if(in_array('index_project_user_assignments',$permissions))
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-user-check"></i> {{__('upper_main_nav.project_user_assignments')}}
        </a>
    @endif
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">

        @if(in_array('index_project_user_assignments', $permissions))
            <div class="dropdown-header">{{__('upper_main_nav.project_user_assignments')}}</div>
            <a class="dropdown-item" href="{{ action('ProjectUserAssignmentController@index') }}">{{__('upper_main_nav.index_project_user_assignments')}}</a>
            @if(in_array('store_zones', $permissions))
                <a class="dropdown-item" href="{{ action('ProjectUserAssignmentController@create') }}">{{__('upper_main_nav.create_project_user_assignment')}}</a>
            @endif
        @endif

    </div>
</li>