<li class="nav-item dropdown">
    @if(in_array('index_customers',$permissions))
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-user-tag"></i></i> {{__('upper_main_nav.customers')}}
        </a>
    @endif
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">

        @if(in_array('index_customers', $permissions))
            <div class="dropdown-header">{{__('upper_main_nav.customers')}}</div>
            <a class="dropdown-item" href="{{ action('CustomerController@index') }}">{{__('upper_main_nav.index_customers')}}</a>
            @if(in_array('store_customers', $permissions))
                <a class="dropdown-item" href="{{ action('CustomerController@create') }}">{{__('upper_main_nav.create_customer')}}</a>
            @endif
        @endif

    </div>
</li>