<li class="nav-item dropdown">
    @if(in_array('index_phases',$permissions))
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fab fa-algolia"></i> {{__('upper_main_nav.phases')}}
        </a>
    @endif
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">

        @if(in_array('index_phases', $permissions))
            <div class="dropdown-header">{{__('upper_main_nav.phases')}}</div>
            <a class="dropdown-item" href="{{ action('PhaseController@index') }}">{{__('upper_main_nav.index_phases')}}</a>
            @if(in_array('store_phases', $permissions))
                <a class="dropdown-item" href="{{ action('PhaseController@create') }}">{{__('upper_main_nav.create_phase')}}</a>
            @endif
        @endif

    </div>
</li>