<li class="nav-item dropdown">
    @if(in_array('index_zones',$permissions))
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-location-arrow"></i> {{__('upper_main_nav.zones')}}
        </a>
    @endif
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">

        @if(in_array('index_zones', $permissions))
            <div class="dropdown-header">{{__('upper_main_nav.zones')}}</div>
            <a class="dropdown-item" href="{{ action('ZoneController@index') }}">{{__('upper_main_nav.index_zones')}}</a>
            @if(in_array('store_zones', $permissions))
                <a class="dropdown-item" href="{{ action('ZoneController@create') }}">{{__('upper_main_nav.create_zone')}}</a>
            @endif
        @endif

    </div>
</li>