<li class="nav-item dropdown">
    @if(in_array('index_units',$permissions))
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-building"></i> {{__('upper_main_nav.units')}}
        </a>
    @endif
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">

        @if(in_array('index_units', $permissions))
            <div class="dropdown-header">{{__('upper_main_nav.units')}}</div>
            <a class="dropdown-item" href="{{ action('UnitController@index') }}">{{__('upper_main_nav.index_units')}}</a>
            @if(in_array('store_units', $permissions))
                <a class="dropdown-item" href="{{ action('UnitController@create') }}">{{__('upper_main_nav.create_unit')}}</a>
            @endif
        @endif

    </div>
</li>