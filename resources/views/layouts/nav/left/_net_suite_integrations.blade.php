<li class="nav-item dropdown">
    @if(in_array('index_net_suite_integrations',$permissions))
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-syringe"></i> {{__('upper_main_nav.net_suite_integrations')}}
        </a>
    @endif
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">

        @if(in_array('index_net_suite_integrations', $permissions))
            <div class="dropdown-header">{{__('upper_main_nav.net_suite_integrations')}}</div>
            <a class="dropdown-item" href="{{ action('NetSuiteIntegrationController@index') }}">{{__('upper_main_nav.index_net_suite_integrations')}}</a>
            @if(in_array('store_net_suite_integrations', $permissions))
                <a class="dropdown-item" href="{{ action('NetSuiteIntegrationController@create') }}">{{__('upper_main_nav.create_net_suite_integration')}}</a>
            @endif
        @endif

    </div>
</li>
