<li class="nav-item dropdown">
    @if(in_array('index_projects',$permissions))
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-city"></i> {{__('upper_main_nav.projects')}}
        </a>
    @endif
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">

        @if(in_array('index_projects', $permissions))
            <div class="dropdown-header">{{__('upper_main_nav.projects')}}</div>
            <a class="dropdown-item" href="{{ action('ProjectController@index') }}">{{__('upper_main_nav.index_projects')}}</a>
            @if(in_array('store_projects', $permissions))
                <a class="dropdown-item" href="{{ action('ProjectController@create') }}">{{__('upper_main_nav.create_project')}}</a>
            @endif
        @endif

    </div>
</li>