<li class="nav-item dropdown">
    @if(in_array('index_project_files',$permissions))
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class=""></i> {{__('upper_main_nav.project_files')}}
        </a>
    @endif
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">

        @if(in_array('index_project_files', $permissions))
            <div class="dropdown-header">{{__('upper_main_nav.project_files_files')}}</div>
            <a class="dropdown-item" href="{{ action('ProjectFileController@index') }}">{{__('upper_main_nav.index_project_files')}}</a>
            @if(in_array('store_project_files', $permissions))
                <a class="dropdown-item" href="{{ action('ProjectFileController@create') }}">{{__('upper_main_nav.create_project_files')}}</a>
            @endif
        @endif

    </div>
</li>