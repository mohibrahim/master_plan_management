<li class="nav-item dropdown">
    @if(in_array('index_blocks',$permissions))
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-cube"></i> {{__('upper_main_nav.blocks')}}
        </a>
    @endif
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">

        @if(in_array('index_blocks', $permissions))
            <div class="dropdown-header">{{__('upper_main_nav.blocks')}}</div>
            <a class="dropdown-item" href="{{ action('BlockController@index') }}">{{__('upper_main_nav.index_blocks')}}</a>
            @if(in_array('store_blocks', $permissions))
                <a class="dropdown-item" href="{{ action('BlockController@create') }}">{{__('upper_main_nav.create_block')}}</a>
            @endif
        @endif

    </div>
</li>