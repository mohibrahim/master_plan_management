<li class="nav-item dropdown">
    @if(in_array('index_unit_models',$permissions))
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-shapes"></i> {{__('upper_main_nav.unit_models')}}
        </a>
    @endif
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">

        @if(in_array('index_unit_models', $permissions))
            <div class="dropdown-header">{{__('upper_main_nav.unit_models')}}</div>
            <a class="dropdown-item" href="{{ action('UnitModelController@index') }}">{{__('upper_main_nav.index_unit_models')}}</a>
            @if(in_array('store_unit_models', $permissions))
                <a class="dropdown-item" href="{{ action('UnitModelController@create') }}">{{__('upper_main_nav.create_unit_model')}}</a>
            @endif
        @endif

    </div>
</li>