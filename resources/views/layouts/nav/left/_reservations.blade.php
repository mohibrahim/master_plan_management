<li class="nav-item dropdown">
    @if(in_array('index_reservations',$permissions))
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-check-square"></i> {{__('upper_main_nav.reservations')}}
        </a>
    @endif
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">

        @if(in_array('index_reservations', $permissions))
            <div class="dropdown-header">{{__('upper_main_nav.reservations')}}</div>
            <a class="dropdown-item" href="{{ action('ReservationController@index') }}">{{__('upper_main_nav.index_reservations')}}</a>
            @if(in_array('store_reservations', $permissions))
                <a class="dropdown-item" href="{{ action('ReservationController@create') }}">{{__('upper_main_nav.create_reservation')}}</a>
            @endif
        @endif

    </div>
</li>