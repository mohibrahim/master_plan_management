@extends('layouts.app')
@section('title')
	{{__('unit_model.add_unit_model')}}
@endsection
	@section('content')
	<div class="container-fluid">
		<div class="col">
			<div class="card card-info">
				<div class="card-heading">
					<h3 class="card-header text-center">{{__('unit_model.add_unit_model')}}</h3>
				</div>
				<div class="card-body ">
					@include('errors.list')
					{!! Form::open(['action' => ['UnitModelController@store'], 'files'=>true, 'method'=>'POST'])!!}
						@include('unit_models._form')
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
@endsection
