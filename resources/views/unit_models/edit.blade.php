@extends('layouts.app')
@section('title')
	{{__('unit_model.edit_unit_model')}}
@endsection
@section('content')
	<div class="container-fluid">
		<div class="col">
			<div class="card card-info">
				<div class="card-heading">
					<h3 class="card-header text-center">{{$unitModel->name}}</h3>
				</div>
				<div class="card-body ">
					@include('errors.list')
					{!! Form::model($unitModel, ['action' => ['UnitModelController@update', 'id'=>$unitModel->id], 'files'=>true,
					'method'=>'put'])!!}
					@include('unit_models._form')
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
@endsection

