<div class="container-fluid border">
	<div class="row">
		<div class="col">

			<div class="form-group">
				<span style="color:#f47373">*</span>
				{!!Form::label('name', __('unit_model.name'))!!}
				{!!Form::text('name', null,['class'=>'form-control', 'placeholder'=>__('unit_model.name_placeholder')]) !!}
			</div>
			<div class="form-group">
				{!!Form::label('code', __('unit_model.code'))!!}
				{!!Form::text('code', null,['class'=>'form-control', 'placeholder'=>__('unit_model.code_placeholder')]) !!}
			</div>

			<div class='form-group'>
				<span style='color:#f47373'>*</span>
				{!!Form::label('type', __('unit_model.type'))!!}
				{!!Form::select('type',$unitModelTypes , null,['class'=>'form-control', 'placeholder'=>__('unit_model.type_placeholder')]) !!}
			</div>

			{{-- <div class='form-group'>
				{!!Form::label('business_type', __('unit_model.business_type'))!!}
				{!!Form::select('business_type',$unitModelBusinessTypes , null,['class'=>'form-control', 'placeholder'=>__('unit_model.business_type_placeholder')]) !!}
			</div> --}}

			<div class="form-group">
				{!!Form::label('built_up_area', __('unit_model.built_up_area'))!!}
				{!!Form::number('built_up_area', null,['class'=>'form-control', 'placeholder'=>__('unit_model.built_up_area_placeholder')]) !!}
			</div>

			<div class="form-group">
				{!!Form::label('land_area', __('unit_model.land_area'))!!}
				{!!Form::number('land_area', null,['class'=>'form-control', 'placeholder'=>__('unit_model.land_area_placeholder')]) !!}
			</div>

			<div class="form-group">
				{!!Form::label('number_of_floors', __('unit_model.number_of_floors'))!!}
				{!!Form::number('number_of_floors', null,['class'=>'form-control', 'placeholder'=>__('unit_model.number_of_floors_placeholder')]) !!}
			</div>

			<div class="form-group">
				{!!Form::label('number_of_balconies', __('unit_model.number_of_balconies'))!!}
				{!!Form::number('number_of_balconies', null,['class'=>'form-control', 'placeholder'=>__('unit_model.number_of_balconies_placeholder')]) !!}
			</div>

			<div class="form-group">
				{!!Form::label('number_of_bedrooms', __('unit_model.number_of_bedrooms'))!!}
				{!!Form::number('number_of_bedrooms', null,['class'=>'form-control', 'placeholder'=>__('unit_model.number_of_bedrooms_placeholder')]) !!}
			</div>

			<div class="form-group">
				{!!Form::label('number_of_bathrooms', __('unit_model.number_of_bathrooms'))!!}
				{!!Form::number('number_of_bathrooms', null,['class'=>'form-control', 'placeholder'=>__('unit_model.number_of_bathrooms_placeholder')]) !!}
			</div>

			<div class="form-group">
				{!!Form::label('number_of_kitchens', __('unit_model.number_of_kitchens'))!!}
				{!!Form::number('number_of_kitchens', null,['class'=>'form-control', 'placeholder'=>__('unit_model.number_of_kitchens_placeholder')]) !!}
			</div>

			<div class="form-group">
				{!!Form::label('number_of_elevators', __('unit_model.number_of_elevators'))!!}
				{!!Form::number('number_of_elevators', null,['class'=>'form-control', 'placeholder'=>__('unit_model.number_of_elevators_placeholder')]) !!}
			</div>

			{{-- <div class="form-group">
				{!!Form::label('number_of_entries', __('unit_model.number_of_entries'))!!}
				{!!Form::number('number_of_entries', null,['class'=>'form-control', 'placeholder'=>__('unit_model.number_of_entries_placeholder')]) !!}
			</div>

			<div class="form-group">
				{!!Form::label('number_of_exits', __('unit_model.number_of_exits'))!!}
				{!!Form::number('number_of_exits', null,['class'=>'form-control', 'placeholder'=>__('unit_model.number_of_exits_placeholder')]) !!}
			</div> --}}

			{{-- <div class="form-group">
				{!!Form::label('number_of_shops', __('unit_model.number_of_shops'))!!}
				{!!Form::number('number_of_shops', null,['class'=>'form-control', 'placeholder'=>__('unit_model.number_of_shops_placeholder')]) !!}
			</div>

			<div class="form-group">
				{!!Form::label('number_of_courts', __('unit_model.number_of_courts'))!!}
				{!!Form::number('number_of_courts', null,['class'=>'form-control', 'placeholder'=>__('unit_model.number_of_courts_placeholder')]) !!}
			</div> --}}

			<div class='form-group'>
				{!!Form::label('finishing_type', __('unit_model.finishing_type'))!!}
				{!!Form::select('finishing_type',$unitModelFinishingTypes , null,['class'=>'form-control', 'placeholder'=>__('unit_model.finishing_type_placeholder')]) !!}
			</div>

			<div class='form-group'>
				{!!Form::label('has_garden', __('unit_model.has_garden'))!!}
				{!!Form::select('has_garden',[true=>__('unit_model.yes'), false=>__('unit_model.no')] , null,['class'=>'form-control', 'placeholder'=>__('unit_model.has_garden_placeholder')]) !!}
			</div>

			<div class="form-group">
				{!!Form::label('garden_area', __('unit_model.garden_area'))!!}
				{!!Form::number('garden_area', null,['class'=>'form-control', 'placeholder'=>__('unit_model.garden_area_placeholder')]) !!}
			</div>

			<div class='form-group'>
				{!!Form::label('has_pool', __('unit_model.has_pool'))!!}
				{!!Form::select('has_pool',[true=>__('unit_model.yes'), false=>__('unit_model.no')] , null,['class'=>'form-control', 'placeholder'=>__('unit_model.has_pool_placeholder')]) !!}
			</div>

			<div class="form-group">
				{!!Form::label('pool_area', __('unit_model.pool_area'))!!}
				{!!Form::number('pool_area', null,['class'=>'form-control', 'placeholder'=>__('unit_model.pool_area_placeholder')]) !!}
			</div>

			<div class='form-group'>
				{!!Form::label('notes', __('unit_model.notes'))!!}
				{!!Form::textarea('notes', null, ['class'=>'form-control', 'rows'=>'5',
				'placeholder'=>__('unit_model.notes_placeholder')])!!}
			</div>
		</div>

	</div>
</div>

<div class="form-group">
	{!!Form::submit(__('unit_model.save'), ['class'=>'btn btn-primary form-control'])!!}
</div>

