@extends('layouts.app')

@section('title')
	{{$unitModel->name}}
@endsection

@section('content')
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h2 class="text-center">{{$unitModel->name}}</h2>
			</div>
			<div class="card-body ">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-4 text-left">
							<img src="{{url('images/helper_images/unit_model_cover.png')}}" class="rounded w-100 border border-default mb-3" alt="">
							<div class=" row text-muted">
								<div class="col small">
									{{__('unit_model.created_at')}}:
								</div>
								<div class="col small">
									{{$unitModel->created_at}}
								</div>
							</div>
							<div class=" row text-muted">
								<div class="col small">
									{{__('unit_model.creator_name')}}:
								</div>
								<div class="col small">
									{{optional($unitModel->creator)->name}}
								</div>
							</div>
							<div class=" row text-muted">
								<div class="col small">
									{{__('unit_model.updated_at')}}:
								</div>
								<div class="col small">
									{{$unitModel->updated_at}}
								</div>
							</div>
							<div class=" row text-muted">
								<div class="col small">
									{{__('unit_model.last_updater_name')}}:
								</div>
								<div class="col small">
									{{optional($unitModel->lastUpdater)->name}}
								</div>
							</div>
							<div class="row mt-3">
								<div class="col">
									@if(in_array('update_unit_models', $permissions))
										<a href="{{ action('UnitModelController@edit', ['id'=>$unitModel->id]) }}" class="btn btn-sm btn-warning btn-block"><i class="fas fa-edit"></i> {{__('unit_model.edit_unit_model')}}
										</a>
									@endif
									@if(in_array('destroy_unit_models', $permissions))
										<td><button type="button" class="btn btn-sm btn-danger btn-block" data-toggle="modal" data-target="#myModal"><i class="far fa-trash-alt"></i> {{__('unit_model.delete_unit_model')}} </button></td>
									@endif
									@if(	in_array('store_catalogs', $permissions) &&
										in_array('store_unit_models', $permissions)
									)
										<a href="{{ action('CatalogController@create', ['model_associated_type'=>'unit_model_id', 'model_associated_type_id'=>$unitModel->id]) }}" class="btn btn-sm btn-primary btn-block"><i class="fas fa-images"></i> {{__('unit_model.add_catalog')}}
										</a>
									@endif
								</div>

							</div>
						</div>
						<div class="col">

							<div class="container-fluid">
								<div class="row">
									<div class="col">
										<table class="table">
											<tbody>
												<tr>
													<th scope="row" class="border-0">{{__('unit_model.id')}}</th>
													<td class="border-0">{{$unitModel->id}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('unit_model.name')}}</th>
													<td class="border-0">{{$unitModel->name}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('unit_model.code')}}</th>
													<td class="border-0">{{$unitModel->code}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('unit_model.type')}}</th>
													<td class="border-0">{{$unitModel->type}}</td>
												</tr>
												{{-- <tr>
													<th scope="row" class="border-0">{{__('unit_model.business_type')}}</th>
													<td class="border-0">{{$unitModel->business_type}}</td>
												</tr> --}}
												<tr>
													<th scope="row" class="border-0">{{__('unit_model.built_up_area')}}</th>
													<td class="border-0">{{$unitModel->built_up_area}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('unit_model.land_area')}}</th>
													<td class="border-0">{{$unitModel->land_area}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('unit_model.number_of_floors')}}</th>
													<td class="border-0">{{$unitModel->number_of_floors}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('unit_model.number_of_balconies')}}</th>
													<td class="border-0">{{$unitModel->number_of_balconies}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('unit_model.number_of_bedrooms')}}</th>
													<td class="border-0">{{$unitModel->number_of_bedrooms}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('unit_model.number_of_bathrooms')}}</th>
													<td class="border-0">{{$unitModel->number_of_bathrooms}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('unit_model.number_of_kitchens')}}</th>
													<td class="border-0">{{$unitModel->number_of_kitchens}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('unit_model.number_of_elevators')}}</th>
													<td class="border-0">{{$unitModel->number_of_elevators}}</td>
												</tr>
												{{-- <tr>
													<th scope="row" class="border-0">{{__('unit_model.number_of_entries')}}</th>
													<td class="border-0">{{$unitModel->number_of_entries}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('unit_model.number_of_exits')}}</th>
													<td class="border-0">{{$unitModel->number_of_exits}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('unit_model.number_of_shops')}}</th>
													<td class="border-0">{{$unitModel->number_of_shops}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('unit_model.number_of_courts')}}</th>
													<td class="border-0">{{$unitModel->number_of_courts}}</td>
												</tr> --}}
												<tr>
													<th scope="row" class="border-0">{{__('unit_model.finishing_type')}}</th>
													<td class="border-0">{{$unitModel->finishing_type}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('unit_model.has_garden')}}</th>
													<td class="border-0">{{$unitModel->has_garden}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('unit_model.garden_area')}}</th>
													<td class="border-0">{{$unitModel->garden_area}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('unit_model.has_pool')}}</th>
													<td class="border-0">{{$unitModel->has_pool}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('unit_model.pool_area')}}</th>
													<td class="border-0">{{$unitModel->pool_area}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('unit_model.notes')}}</th>
													<td class="border-0">{{$unitModel->notes}}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<hr>
							</div>

						</div>
					</div>




					<div class="row mt-3">
						<div class="col">
							<div class="card">
								<div class="card-body">
									<h4 class="font-weight-bold text-center m-3"> الكتالوجات </h4>

									<table class="table table-hover" id="catalogs">
										<thead>
											<tr>
												<th>#</th>
												<th>{{__('catalog.id')}}</th>
												<th>{{__('catalog.name')}}</th>
												<th>{{__('catalog.description')}}</th>
												<th>{{__('catalog.created_at')}}</th>
												<th>{{__('catalog.creator_name')}}</th>
											</tr>
										</thead>
										<tbody>
											@foreach($unitModel->catalogs as $catalogKey => $catalog)
												<tr>
													<td>{{$catalogKey+1}}</td>
													<td>{{$catalog->id}}</td>
													<td>
														<a href="{{action('CatalogController@show', ['id'=>$catalog->id])}}">
															{{$catalog->name}}
														</a>
													</td>
													<td>{{$catalog->description}}</td>
													<td>{{$catalog->created_at}}</td>
													<td>{{optional($catalog->creator)->name}}</td>

												</tr>
											@endforeach
										</tbody>
									</table>
									<input id="data-table-search" type="hidden" value="{{__('data_table.search')}}">
									<input id="data-table-zero-record" type="hidden" value="{{__('data_table.zeroRecords')}}">
								</div>
							</div>
						</div>
					</div>


				</div>
			</div>
		</div>

	</div>

	@include('partial.deleteConfirm',['name'=>$unitModel->name,
									  'id'=> $unitModel->id,
									  'message'=> __('unit_model.confirm_delete_message'),
									  'route'=>'UnitModelController@destroy'])
@endsection
