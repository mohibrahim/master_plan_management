<div class="container-fluid border">
    <div class="row">
        <div class="col">


            <div class="card border-primary mt-3">
                <div class="card-body text-primary">
                    <h5 class="card-title">{{__('unit.primary_information')}}</h5>
                    <div class="row">

                        <div class="col-md-4 col-lg-4">
                            <div class="form-group">
                                <span style="color:#f47373">*</span>
                                {!!Form::label('code', __('unit.code'))!!}
                                {!!Form::text('code', null,['class'=>'form-control',
                                'placeholder'=>__('unit.code_placeholder')]) !!}
                            </div>

                            <div class="form-group">
                                {!!Form::label('minimum_booking_amount', __('unit.minimum_booking_amount'))!!}
                                {!!Form::number('minimum_booking_amount', null,['class'=>'form-control',
                                'placeholder'=>__('unit.minimum_booking_amount_placeholder'), 'step'=>'0.01']) !!}
                            </div>
                        </div>

                        <div class="col-md-4 col-lg-4">
                            <div class='form-group'>
                                <span style="color:#f47373">*</span>
                                {!!Form::label('status', __('unit.status'))!!}
                                {!!Form::select('status',$unitStatus , null,['class'=>'form-control',
                                'placeholder'=>__('unit.status_placeholder')]) !!}
                            </div>


                        </div>

                        <div class="col-md-4 col-lg-4">
                            <div class="form-group">
                                {!!Form::label('booking_amount', __('unit.booking_amount'))!!}
                                {!!Form::number('booking_amount', null,['class'=>'form-control',
                                'placeholder'=>__('unit.booking_amount_placeholder'), 'step'=>'0.01']) !!}
                            </div>
                        </div>
                    </div>

                </div>
            </div>


            @include('partial.select_project_and_phase_zone_block', ['modelName' => 'unit', 'showSelectZone' => true,
            'showSelectPhase' => true, 'showSelectUnitModel' => true, 'showSelectBlock' => true])
            <input type="hidden" name="selected_project_master_plan_image" id="selected-project-master-plan-image"
                value="{{$unit->selected_project_master_plan_image}}">

            <div class="card border-success mt-3">
                <div class="card-body text-success">
                    <h5 class="card-title">{{__('unit.unit_specification')}}</h5>
                    <div class="row">

                        <div class="col-md-4 col-lg-4">
                            <div class="form-group">
                                {!!Form::label('product_number', __('unit.product_number'))!!}
                                {!!Form::text('product_number', null,['class'=>'form-control',
                                'placeholder'=>__('unit.product_number_placeholder')]) !!}
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-4">
                            <div class="form-group">
                                {!!Form::label('land_price', __('unit.land_price'))!!}
                                {!!Form::text('land_price', null,['class'=>'form-control',
                                'placeholder'=>__('unit.land_price_placeholder')]) !!}
                            </div>
                        </div>

                        <div class="col-md-4 col-lg-4">
                            <div class="form-group">
                                {!!Form::label('land_number', __('unit.land_number'))!!}
                                {!!Form::text('land_number', null,['class'=>'form-control',
                                'placeholder'=>__('unit.land_number_placeholder')]) !!}
                            </div>
                        </div>

                        <div class="col-md-4 col-lg-4">
                            <div class="form-group">
                                {!!Form::label('land_area', __('unit.land_area'))!!}
                                {!!Form::text('land_area', null,['class'=>'form-control',
                                'placeholder'=>__('unit.land_area_placeholder')]) !!}
                            </div>
                        </div>

                        <div class="col-md-4 col-lg-4">
                            <div class="form-group">
                                {!!Form::label('built_up_area', __('unit.built_up_area'))!!}
                                {!!Form::text('built_up_area', null,['class'=>'form-control',
                                'placeholder'=>__('unit.built_up_area_placeholder')]) !!}
                            </div>
                        </div>

                        <div class="col-md-4 col-lg-4">
                            <div class="form-group">
                                {!!Form::label('number_of_floors', __('unit.number_of_floors'))!!}
                                {!!Form::text('number_of_floors', null,['class'=>'form-control',
                                'placeholder'=>__('unit.number_of_floors_placeholder')]) !!}
                            </div>
                        </div>

                        <div class="col-md-4 col-lg-4">
                            <div class="form-group">
                                {!!Form::label('number_of_balconies', __('unit.number_of_balconies'))!!}
                                {!!Form::text('number_of_balconies', null,['class'=>'form-control',
                                'placeholder'=>__('unit.number_of_balconies_placeholder')]) !!}
                            </div>
                        </div>

                        <div class="col-md-4 col-lg-4">
                            <div class="form-group">
                                {!!Form::label('number_of_bedrooms', __('unit.number_of_bedrooms'))!!}
                                {!!Form::text('number_of_bedrooms', null,['class'=>'form-control',
                                'placeholder'=>__('unit.number_of_bedrooms_placeholder')]) !!}
                            </div>
                        </div>

                        <div class="col-md-4 col-lg-4">
                            <div class="form-group">
                                {!!Form::label('number_of_bathrooms', __('unit.number_of_bathrooms'))!!}
                                {!!Form::text('number_of_bathrooms', null,['class'=>'form-control',
                                'placeholder'=>__('unit.number_of_bathrooms_placeholder')]) !!}
                            </div>
                        </div>

                        <div class="col-md-4 col-lg-4">
                            <div class="form-group">
                                {!!Form::label('number_of_kitchens', __('unit.number_of_kitchens'))!!}
                                {!!Form::text('number_of_kitchens', null,['class'=>'form-control',
                                'placeholder'=>__('unit.number_of_kitchens_placeholder')]) !!}
                            </div>
                        </div>

                        <div class="col-md-4 col-lg-4">
                            <div class="form-group">
                                {!!Form::label('number_of_elevators', __('unit.number_of_elevators'))!!}
                                {!!Form::text('number_of_elevators', null,['class'=>'form-control',
                                'placeholder'=>__('unit.number_of_elevators_placeholder')]) !!}
                            </div>
                        </div>

                        <div class="col-md-4 col-lg-4">
                            <div class='form-group'>
                                {!!Form::label('has_garden', __('unit.has_garden'))!!}
                                {!!Form::select('has_garden', $unitHasGarden ,
                                null,['class'=>'form-control',
                                'placeholder'=>__('unit.has_garden_placeholder')]) !!}
                            </div>
                        </div>

                        <div class="col-md-4 col-lg-4">
                            <div class="form-group">
                                {!!Form::label('garden_area', __('unit.garden_area'))!!}
                                {!!Form::text('garden_area', null,['class'=>'form-control',
                                'placeholder'=>__('unit.garden_area_placeholder')]) !!}
                            </div>
                        </div>

                        <div class="col-md-4 col-lg-4">
                            <div class='form-group'>
                                {!!Form::label('has_pool', __('unit.has_pool'))!!}
                                {!!Form::select('has_pool', $unitHasPool ,
                                null,['class'=>'form-control',
                                'placeholder'=>__('unit.has_pool_placeholder')]) !!}
                            </div>
                        </div>

                        <div class="col-md-4 col-lg-4">
                            <div class="form-group">
                                {!!Form::label('pool_area', __('unit.pool_area'))!!}
                                {!!Form::text('pool_area', null,['class'=>'form-control',
                                'placeholder'=>__('unit.pool_area_placeholder')]) !!}
                            </div>
                        </div>

                        <div class="col-md-4 col-lg-4">
                            <div class='form-group'>
                                {!!Form::label('construction_status', __('unit.construction_status'))!!}
                                {!!Form::select('construction_status',$unitConstructionStatus ,
                                null,['class'=>'form-control',
                                'placeholder'=>__('unit.construction_status_placeholder')]) !!}
                            </div>
                        </div>

                        <div class="col-md-4 col-lg-4">
                            <div class='form-group'>
                                {!!Form::label('notes', __('unit.notes'))!!}
                                {!!Form::textarea('notes', null, ['class'=>'form-control', 'rows'=>'1',
                                'placeholder'=>__('unit.notes_placeholder')])!!}
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="card border-info my-3">
                <div class="card-body text-info">
                    <h5 class="card-title">{{__('unit.development_information')}}</h5>
                    <div class="row">

                        <div class="col-md-4 col-lg-4">
                            <div class='form-group'>
                                {!!Form::label('developer_name', __('unit.developer_name'))!!}
                                {!!Form::text('developer_name', null, ['class'=>'form-control',
                                'placeholder'=>__('unit.developer_name_placeholder')])!!}
                            </div>
                        </div>

                        <div class="col-md-4 col-lg-4">
                            <div class="form-group">
                                {!!Form::label('percentage_of_completion', __('unit.percentage_of_completion'))!!}
                                {!!Form::number('percentage_of_completion', null,['class'=>'form-control',
                                'placeholder'=>__('unit.percentage_of_completion_placeholder')]) !!}
                            </div>
                        </div>

                        <div class="col-md-4 col-lg-4">

                        </div>
                    </div>

                </div>
            </div>

            <div class="card border-dark my-3">
                <div class="card-body text-dark">
                    <h5 class="card-title">{{__('unit.sales_and_pricing')}}</h5>
                    <div class="row">

                        <div class="col-md-4 col-lg-4">

                            <div class="form-group">
                                {!!Form::label('sale_price', __('unit.sale_price'))!!}
                                {!!Form::number('sale_price', null,['class'=>'form-control',
                                'placeholder'=>__('unit.sale_price_placeholder')]) !!}
                            </div>

                        </div>

                        <div class="col-md-4 col-lg-4">
                            <div class="form-group">
                                {!!Form::label('currency', __('unit.currency'))!!}
                                {!!Form::select('currency', $unitCurrencies, null,['class'=>'form-control',
                                'placeholder'=>__('unit.currency_placeholder')]) !!}
                            </div>

                        </div>

                        <div class="col-md-4 col-lg-4">

                        </div>
                    </div>

                </div>
            </div>

            {{-- <div class="form-group">
                {!!Form::label('direction', __('unit.direction'))!!}
                {!!Form::select('direction', ['north_direction'=>__('unit.north_direction'), 'south_direction'=>__('unit.south_direction')], null,['class'=>'form-control',
                'placeholder'=>__('unit.direction_placeholder')]) !!}
            </div> --}}

            {{-- <div class="form-group">
                {!!Form::label('address', __('unit.address'))!!}
                {!!Form::textarea('address', null,['class'=>'form-control', 'placeholder'=>__('unit.address_placeholder'), 'rows'=>'1',])
                !!}
            </div> --}}

            {{-- <div class='form-group'>
                {!!Form::label('sale_details', __('unit.sale_details'))!!}
                {!!Form::textarea('sale_details', null, ['class'=>'form-control', 'rows'=>'1',
                'placeholder'=>__('unit.sale_details_placeholder')])!!}
            </div> --}}

            {{-- <div class='form-group'>
                {!!Form::label('is_for_sale', __('unit.is_for_sale'))!!}
                {!!Form::select('is_for_sale',[true => __('unit.yes'), false => __('unit.no')] ,
                null,['class'=>'form-control', 'placeholder'=>__('unit.is_for_sale_placeholder')]) !!}
            </div> --}}
            {{-- <div class="form-group">
                {!!Form::label('floor_number', __('unit.floor_number'))!!}
                {!!Form::number('floor_number', null,['class'=>'form-control',
                'placeholder'=>__('unit.floor_number_placeholder')]) !!}
            </div> --}}

            {{-- <div class="form-group">
                {!!Form::label('longitude', __('unit.longitude'))!!}
                {!!Form::text('longitude', null,['class'=>'form-control',
                'placeholder'=>__('unit.longitude_placeholder')]) !!}
            </div> --}}

            {{-- <div class="form-group">
                {!!Form::label('latitude', __('unit.latitude'))!!}
                {!!Form::text('latitude', null,['class'=>'form-control',
                'placeholder'=>__('unit.latitude_placeholder')]) !!}
            </div> --}}

            {{-- <div class="form-group">
				{!!Form::label('date_of_sale', __('unit.date_of_sale'))!!}
				{!!Form::date('date_of_sale', null,['class'=>'form-control',
				'placeholder'=>__('unit.date_of_sale_placeholder')]) !!}
			</div>

			<div class='form-group'>
				{!!Form::label('is_for_rent', __('unit.is_for_rent'))!!}
				{!!Form::select('is_for_rent',[true => __('unit.yes'), false => __('unit.no')] ,
				null,['class'=>'form-control', 'placeholder'=>__('unit.is_for_rent_placeholder')]) !!}
			</div>

			<div class="form-group">
				{!!Form::label('rent_from', __('unit.rent_from'))!!}
				{!!Form::date('rent_from', null,['class'=>'form-control',
				'placeholder'=>__('unit.rent_from_placeholder')]) !!}
			</div>

			<div class="form-group">
				{!!Form::label('rent_to', __('unit.rent_to'))!!}
				{!!Form::date('rent_to', null,['class'=>'form-control', 'placeholder'=>__('unit.rent_to_placeholder')])
				!!}
			</div>

			<div class="form-group">
				{!!Form::label('rent_price', __('unit.rent_price'))!!}
				{!!Form::number('rent_price', null,['class'=>'form-control',
				'placeholder'=>__('unit.rent_price_placeholder')]) !!}
			</div>

			<div class='form-group'>
				{!!Form::label('rent_details', __('unit.rent_details'))!!}
				{!!Form::textarea('rent_details', null, ['class'=>'form-control', 'rows'=>'5',
				'placeholder'=>__('unit.rent_details_placeholder')])!!}
			</div>

			<div class="form-group">
				{!!Form::label('unit_account_number', __('unit.unit_account_number'))!!}
				{!!Form::text('unit_account_number', null,['class'=>'form-control',
				'placeholder'=>__('unit.unit_account_number_placeholder')]) !!}
			</div> --}}

            {{-- <div class="form-group">
				{!!Form::label('electricity_meter_number', __('unit.electricity_meter_number'))!!}
				{!!Form::number('electricity_meter_number', null,['class'=>'form-control',
				'placeholder'=>__('unit.electricity_meter_number_placeholder')]) !!}
			</div> --}}

        </div>

    </div>
</div>

<div class="form-group">
    {!!Form::submit(__('unit.save'), ['class'=>'btn btn-primary form-control'])!!}
</div>

@section('head')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
@endsection
@section('js_footer')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script language="javascript" src="{{url('js/jqurey_canvas_area_drawer/jquery.canvasAreaDraw.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
<script>
    $(document).ready(function () {
        selectProjectAndPhase('units', true, true, true);
        selectUnitModelForUnit();

        let masterPlanImageValue = $('#selected-project-master-plan-image').val();
        let htmlAreaTagCoords = $('#html-area-tag-coords');
        $('#project-id').on('select2:select', function (event) {
            //Cache DOM
            let masterPlanImageValue = event.params.data.masterPlanImage;
            let masterPlanImageWrapper = $('.master-plan-image-wrapper');


            if (!isEmptyOrSpaces(masterPlanImageValue)) {
                masterPlanImageWrapper.empty();
                htmlAreaTagCoords.val('');
                htmlAreaTagCoords.canvasAreaDraw({
                    imageUrl: "{{asset('')}}" + masterPlanImageValue
                });
            }
        });

        //in update state
        if (!isEmptyOrSpaces(masterPlanImageValue)) {
            htmlAreaTagCoords.canvasAreaDraw({
                imageUrl: "{{asset('')}}" + masterPlanImageValue
            });

        }

    });
</script>
@endsection
