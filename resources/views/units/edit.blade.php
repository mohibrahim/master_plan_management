@extends('layouts.app')
@section('title')
	{{__('unit.edit_unit')}}
@endsection
@section('content')
	<div class="container-fluid">
		<div class="col">
			<div class="card card-info">
				<div class="card-heading">
					<h3 class="card-header text-center">{{$unit->code}}</h3>
				</div>
				<div class="card-body ">
					@include('errors.list')
					{!! Form::model($unit, ['action' => ['UnitController@update', 'id'=>$unit->id], 'files'=>true,
					'method'=>'put'])!!}
					@include('units._form')
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
@endsection

