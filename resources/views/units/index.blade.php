@extends('layouts.app')
@section('title')
	{{__('unit.all_units')}}
@endsection
@section('content')
	<div class="col-md-12">
		<div class="card">

			<div class="card-header">
				{{__('unit.all_units')}}
			</div>
				<div class="card-body">
                    <div class="container">



                        <div class="row">
                            <div class="col">

                                <div>
                                    <div class="input-group mb-2 mr-sm-2">
                                        <label class="m-2" for="searching-input">{{__('unit.searching_on_unit')}}: </label>
                                        <input type="text" class="form-control" id="searching-input" placeholder="{{__('unit.searching_word')}}" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <p>
                                            <label for="amount">{{__('unit.searching_sale_price_range')}}</label>
                                            <input type="text" class="form-control" id="amount" readonly>
                                        </p>

                                        <div id="slider-range" data-min="{{$salePriceMin}}" data-max="{{$salePriceMax}}"></div>
                                    </div>

                                    <button id="searching-button" type="button" class="btn btn-primary mb-2">{{__('unit.search')}}</button>
                                    <input id="searching-button-required" type="hidden" value="{{__('unit.searching_button_required')}}">
                                    <input id="searching-button-searching" type="hidden" value="{{__('unit.searching_button_searching')}}">
                                    <input id="searching-button-search" type="hidden" value="{{__('unit.searching_button_search')}}">
                                </div>

                            </div>
                            <div class="col-3">
                                <div class="float-right">
                                    <div class='form-group '>
                                        {!!Form::label('net-suite-id', __('net_suite_integration.select_net_suite_integration'))!!}
                                        {!!Form::select('a', $netSuitNamesIds, null,['id'=>'net-suite-id', 'class'=>'form-control custom-select', 'placeholder'=>__('net_suite_integration.select_net_suite_integration_placeholder')]) !!}
                                    </div>
                                    <div class="form-group">
                                        <a href="#" type="button" id="net-suite-id-sync-button" class="btn btn-primary btn-block disabled">
                                            <i class="fas fa-sync-alt"></i>
                                        </a>

                                    </div>
                                </div>
                                <span class="clearfix"></span>
                            </div>
                        </div>
                    </div>
					<hr>
					<table class="table table-hover" id="units">
						<thead>
							<tr>
								<th>#</th>
                                <th>{{__('unit.project_name')}}</th>
                                <th>{{__('unit.zone_name')}}</th>
                                <th>{{__('unit.unit_model_name')}}</th>
                                <th>{{__('unit.sale_price')}}</th>


								<th>{{__('unit.id')}}</th>
								<th>{{__('unit.code')}}</th>
								<th>{{__('unit.status')}}</th>
								<th>{{__('unit.created_at')}}</th>
								<th>{{__('unit.creator_name')}}</th>
							</tr>
						</thead>
						<tbody>
							@foreach($units as $unitKey => $unit)
								<tr>
									<td>{{$unitKey+1}}</td>
									<td>
										<a href="{{action('ProjectController@show', ['id'=>optional($unit->project)->id])}}">
											{{optional($unit->project)->name}}
										</a>
									</td>
                                    <td>
                                        <a href="{{action('ZoneController@show', ['id'=>optional($unit->zone)->id])}}" target="_blank">
                                            {{optional($unit->zone)->name}}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{action('UnitModelController@show', ['id'=>optional($unit->unitModel)->id])}}" target="_blank">
                                            {{optional($unit->unitModel)->name}}
                                        </a>
                                    </td>
									<td>{{$unit->sale_price_presentation}}</td>


									<td>{{$unit->id}}</td>
									<td>
										<a href="{{action('UnitController@show', ['id'=>$unit->id])}}">
											{{$unit->code}}
										</a>
									</td>
									<td>{{$unit->status}}</td>
									<td>{{$unit->created_at}}</td>
									<td>{{optional($unit->creator)->name}}</td>

								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="d-flex justify-content-center">
					{{$units->links()}}
				</div>
			</div>

			<input id="data-table-search" type="hidden" value="{{__('data_table.search')}}">
			<input id="data-table-zero-record" type="hidden" value="{{__('data_table.zeroRecords')}}">
	</div>
@endsection

@section('head')
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css"> -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    {{-- Start slider --}}
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    {{-- End slider --}}
@endsection
@section('js_footer')

    {{-- Start slider --}}
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    {{-- End slider --}}
	<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
	<script>
		$(document).ready(function () {
			dataTable = $('#units').DataTable({
				"paging":   false,
				"info":     false,
				"language": {
					"search": $("#data-table-search").val(),
					"zeroRecords": $("#data-table-zero-record").val(),
				},
			});
            searchingOnUnitsAjax();
            selectSyncOnUnits();






		});
	</script>
@endsection
