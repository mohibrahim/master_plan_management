<div class="col">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <table class="table">
                    <tbody>
                        <tr class="table-danger">
                            <th scope="row" class="border-0">{{__('unit.project_name')}}</th>
                            <td class="border-0">{!!optional($unit->project)->project_show_link!!}</td>
                        </tr>
                        <tr class="table-danger">
                            <th scope="row" class="border-0">{{__('unit.phase_name')}}</th>
                            <td class="border-0">{!!optional($unit->phase)->phase_show_link!!}</td>
                        </tr>
                        <tr class="table-danger">
                            <th scope="row" class="border-0">{{__('unit.zone_name')}}</th>
                            <td class="border-0">{!!optional($unit->zone)->zone_show_link!!}</td>
                        </tr>
                        <tr class="table-danger">
                            <th scope="row" class="border-0">{{__('unit.block_name')}}</th>
                            <td class="border-0">{!!optional($unit->block)->block_show_link!!}</td>
                        </tr>
                        <tr class="table-danger">
                            <th scope="row" class="border-0">{{__('unit.unit_model_name')}}</th>
                            <td class="border-0">{!!optional($unit->unitModel)->unit_model_show_link!!}</td>
                        </tr>
                        <tr class="table-info">
                            <th scope="row" class="border-0">{{__('unit.id')}}</th>
                            <td class="border-0">{{$unit->id}}</td>
                        </tr>
                        <tr class="table-info">
                            <th scope="row" class="border-0">{{__('unit.code')}}</th>
                            <td class="border-0">{{$unit->code}}</td>
                        </tr>
                        <tr class="table-info">
                            <th scope="row" class="border-0">{{__('unit.status')}}</th>
                            <td class="border-0">
                                {{$unit->suitable_status}} {!! $unit->reservation_details !!}
                            </td>
                        </tr>
                        <tr class="table-info">
                            <th scope="row" class="border-0">{{__('unit.booking_amount')}}</th>
                            <td class="border-0">{{$unit->booking_amount_presentation}}</td>
                        </tr>
                        <tr class="table-info">
                            <th scope="row" class="border-0">{{__('unit.minimum_booking_amount')}}</th>
                            <td class="border-0">{{$unit->minimum_booking_amount_presentation}}</td>
                        </tr>
                        <tr class="table-success">
                            <th scope="row" class="border-0">{{__('unit.land_number')}}</th>
                            <td class="border-0">{{$unit->land_number}}</td>
                        </tr>
                        <tr class="table-success">
                            <th scope="row" class="border-0">{{__('unit.land_area')}}</th>
                            <td class="border-0">{{$unit->land_area}}</td>
                        </tr>
                        <tr class="table-success">
                            <th scope="row" class="border-0">{{__('unit.built_up_area')}}</th>
                            <td class="border-0">{{$unit->built_up_area}}</td>
                        </tr>
                        <tr class="table-success">
                            <th scope="row" class="border-0">{{__('unit_model.built_up_area')}}</th>
                            <td class="border-0">{{optional($unit->unitModel)->built_up_area}}</td>
                        </tr>
                        <tr class="table-success">
                            <th scope="row" class="border-0">{{__('unit_model.number_of_bedrooms')}}</th>
                            <td class="border-0">{{$unit->number_of_bedrooms}}</td>
                        </tr>
                        <tr class="table-success">
                            <th scope="row" class="border-0">{{__('unit_model.number_of_bathrooms')}}</th>
                            <td class="border-0">{{$unit->number_of_bathrooms}}</td>
                        </tr>
                        <tr class="table-success">
                            <th scope="row" class="border-0">{{__('unit_model.number_of_kitchens')}}</th>
                            <td class="border-0">{{$unit->number_of_kitchens}}</td>
                        </tr>
                        <tr class="table-success">
                            <th scope="row" class="border-0">{{__('unit_model.number_of_floors')}}</th>
                            <td class="border-0">{{$unit->number_of_floors}}</td>
                        </tr>
                        <tr class="table-success">
                            <th scope="row" class="border-0">{{__('unit_model.number_of_balconies')}}</th>
                            <td class="border-0">{{$unit->number_of_balconies}}</td>
                        </tr>
                        <tr class="table-success">
                            <th scope="row" class="border-0">{{__('unit_model.number_of_elevators')}}</th>
                            <td class="border-0">{{$unit->number_of_elevators}}</td>
                        </tr>
                        <tr class="table-success">
                            <th scope="row" class="border-0">{{__('unit_model.has_pool')}}</th>
                            <td class="border-0">{{$unit->has_pool}}</td>
                        </tr>
                        <tr class="table-success">
                            <th scope="row" class="border-0">{{__('unit_model.pool_area')}}</th>
                            <td class="border-0">{{$unit->pool_area}}</td>
                        </tr>
                        <tr class="table-success">
                            <th scope="row" class="border-0">{{__('unit_model.has_garden')}}</th>
                            <td class="border-0">{{$unit->has_garden}}</td>
                        </tr>
                        <tr class="table-success">
                            <th scope="row" class="border-0">{{__('unit_model.garden_area')}}</th>
                            <td class="border-0">{{$unit->garden_area}}</td>
                        </tr>
                        <tr class="table-warning">
                            <th scope="row" class="border-0">{{__('unit.percentage_of_completion')}}</th>
                            <td class="border-0">{{$unit->percentage_of_completion}}</td>
                        </tr>
                        <tr class="table-warning">
                            <th scope="row" class="border-0">{{__('unit.developer_name')}}</th>
                            <td class="border-0">{{$unit->developer_name}}</td>
                        </tr>
                        <tr class="table-warning">
                            <th scope="row" class="border-0">{{__('unit.product_number')}}</th>
                            <td class="border-0">{{$unit->product_number}}</td>
                        </tr>
                        <tr class="table-warning">
                            <th scope="row" class="border-0">{{__('unit.land_price')}}</th>
                            <td class="border-0">{{$unit->land_price_presentation}}</td>
                        </tr>
                        <tr class="table-warning">
                            <th scope="row" class="border-0">{{__('unit.notes')}}</th>
                            <td class="border-0">{{$unit->notes}}</td>
                        </tr>
                        <tr class="table-dark">
                            <th scope="row" class="border-0">{{__('unit.sale_price')}}</th>
                            <td class="border-0">{{$unit->sale_price_presentation}}</td>
                        </tr>
                        <tr class="table-dark">
                            <th scope="row" class="border-0">{{__('unit.currency')}}</th>
                            <td class="border-0">{{$unit->currency}}</td>
                        </tr>




                        {{-- <tr>
                            <th scope="row" class="border-0">{{__('unit.construction_status')}}</th>
                        <td class="border-0">{{$unit->construction_status}}</td>
                        </tr> --}}

                        {{-- <tr>
                            <th scope="row" class="border-0">{{__('unit.is_for_sale')}}</th>
                        <td class="border-0">{{$unit->is_for_sale}}</td>
                        </tr>
                        <tr>
                            <th scope="row" class="border-0">{{__('unit.sale_details')}}</th>
                            <td class="border-0">{{$unit->sale_details}}</td>
                        </tr>
                        <tr>
                            <th scope="row" class="border-0">{{__('unit.date_of_sale')}}</th>
                            <td class="border-0">{{$unit->date_of_sale}}</td>
                        </tr> --}}
                        {{-- <tr>
                            <th scope="row" class="border-0">{{__('unit.is_for_rent')}}</th>
                        <td class="border-0">{{$unit->is_for_rent}}</td>
                        </tr>
                        <tr>
                            <th scope="row" class="border-0">{{__('unit.rent_from')}}</th>
                            <td class="border-0">{{$unit->rent_from}}</td>
                        </tr>
                        <tr>
                            <th scope="row" class="border-0">{{__('unit.rent_to')}}</th>
                            <td class="border-0">{{$unit->rent_to}}</td>
                        </tr>
                        <tr>
                            <th scope="row" class="border-0">{{__('unit.rent_price')}}</th>
                            <td class="border-0">{{$unit->rent_price}}</td>
                        </tr>
                        <tr>
                            <th scope="row" class="border-0">{{__('unit.rent_details')}}</th>
                            <td class="border-0">{{$unit->rent_details}}</td>
                        </tr>
                        <tr>
                            <th scope="row" class="border-0">{{__('unit.unit_account_number')}}</th>
                            <td class="border-0">{{$unit->unit_account_number}}</td>
                        </tr> --}}
                        {{-- <tr>
                            <th scope="row" class="border-0">{{__('unit.direction')}}</th>
                        <td class="border-0">{{$unit->direction}}</td>
                        </tr>
                        <tr>
                            <th scope="row" class="border-0">{{__('unit.floor_number')}}</th>
                            <td class="border-0">{{$unit->floor_number}}</td>
                        </tr>
                        <tr>
                            <th scope="row" class="border-0">{{__('unit.address')}}</th>
                            <td class="border-0">{{$unit->address}}</td>
                        </tr>
                        <tr>
                            <th scope="row" class="border-0">{{__('unit.latitude')}}</th>
                            <td class="border-0">{{$unit->latitude}}</td>
                        </tr>
                        <tr>
                            <th scope="row" class="border-0">{{__('unit.longitude')}}</th>
                            <td class="border-0">{{$unit->longitude}}</td>
                        </tr> --}}
                        {{-- <tr>
                            <th scope="row" class="border-0">{{__('unit.electricity_meter_number')}}</th>
                        <td class="border-0">{{$unit->electricity_meter_number}}</td>
                        </tr> --}}

                    </tbody>
                </table>
            </div>
        </div>
        <hr>
    </div>
</div>
