@extends('layouts.app_no_nav')

@section('title')
    {{__('pages_titles.unit').$focusOnUnitImage->getUnit()->code}}
@endsection
@section('head')
    <style>
        body{
            -webkit-print-color-adjust:exact;
        }
        #unit-name-wrapper {
            background-color: #bf9e51;
        }
    </style>
@endsection
@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-6">
                <div class="border border-warning mx-auto text-center p-2" >
                    <img src="{{asset($unit->project->logo)}}" class="img-fluid" alt="" >
                </div>
                <hr class="border-warning">
                <div class="card mb-3 text-center border-warning">
                    <div class="card-body rounded" id="unit-name-wrapper">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-4 p-3">
                                    <span class="card-text text-white h4">{{__('unit.code')}} </span>
                                </div>
                                <div class="col-8  bg-white p-3 rounded">
                                    <span class="card-text text-center font-weight-bold h3">{{$unit->code}}</span>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
                <span id="data-holder" data-unit-data-as-json='{{$unitDataAsJson}}'></span>
                <div class="card border-warning">
                    <div class="card-body rounded p-1 text-center" id="zone-to-project-canvas-wrapper">
                    </div>
                </div>


            </div>

            <div class="col-md-6 overflow-hidden">
                <div id="focus-on-unit" class="border border-warning rounded"  data-focus-on-unit-css='{!!$focusOnUnitImage->getFocusOnUnitCss()!!}'>
                    <img id="map-pin-focus-on-unit" data-map-pin-focus-on-unit-css='{!!$focusOnUnitImage->getMapPinCss()!!}' src="{!! url('images/helper_images/mapPin.png') !!}" class="map-pin" >
                </div>
            </div>

        </div>
        <div class="row mt-3">
            <div class="col">
                <div class="card border-warning">
                    <div class="card-body rounded p-1 text-center">
                        <div id="zone-canvas-wrapper">

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('js_footer')
<script src="/js/p5.js"></script>
<script>

    $(document).ready(function(){
        let focusOnUnit = $("#focus-on-unit");
        let focusOnUnitData = focusOnUnit.data("focus-on-unit-css");
        let mapPinfocusOnUnit = $("#map-pin-focus-on-unit");
        let mapPinFocusOnUnitData = mapPinfocusOnUnit.data("map-pin-focus-on-unit-css");
        focusOnUnit.css(focusOnUnitData);
        mapPinfocusOnUnit.css(mapPinFocusOnUnitData);

        // unitShowPdfZoneToProject ->> resources\js\models\units\show\pdf\zone_to_project.js
        let zoneToProject = new p5(unitShowPdfZoneToProject);
        // unitShowPdfZone ->> resources\js\models\units\show\pdf\zone.js
        let zone = new p5(unitShowPdfZone);
    });

</script>
@endsection
