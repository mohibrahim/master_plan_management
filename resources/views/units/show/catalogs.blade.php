<div class="col">
    <div class="card">
        <div class="card-body">
            <h4 class="font-weight-bold text-center m-3"> الكتالوجات </h4>

            <table class="table table-hover" id="catalogs">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>{{__('catalog.id')}}</th>
                        <th>{{__('catalog.name')}}</th>
                        <th>{{__('catalog.description')}}</th>
                        <th>{{__('catalog.created_at')}}</th>
                        <th>{{__('catalog.creator_name')}}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($unit->catalogs as $catalogKey => $catalog)
                        <tr>
                            <td>{{$catalogKey+1}}</td>
                            <td>{{$catalog->id}}</td>
                            <td>
                                <a href="{{action('CatalogController@show', ['id'=>$catalog->id])}}">
                                    {{$catalog->name}}
                                </a>
                            </td>
                            <td>{{$catalog->description}}</td>
                            <td>{{$catalog->created_at}}</td>
                            <td>{{optional($catalog->creator)->name}}</td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
            <input id="data-table-search" type="hidden" value="{{__('data_table.search')}}">
            <input id="data-table-zero-record" type="hidden" value="{{__('data_table.zeroRecords')}}">
        </div>
    </div>
</div>
