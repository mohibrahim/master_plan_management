@extends('layouts.app')

@section('title')
	{{__('pages_titles.unit').$unit->code}}
@endsection

@section('content')
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h2 class="text-center">الوحدة | {{$unit->code}}</h2>
			</div>
			<div class="card-body ">
				<div class="container-fluid">
							<h3>{{__('unit.project_master_plan_image')}}</h3>
					<div class="row m-4">
						<div class="col d-flex justify-content-center overflow-auto">
							<img src="{{asset(optional($unit->project)->master_plan_image)}}" alt="" usemap="#master-plan-test">
							<map name="master-plan-test">
								<area class="fixed my-selected popover-dismiss" data-color="#{{$unit->status_color}}" data-id="1" shape="poly" coords="{{$unit->html_area_tag_coords}}" href="#" alt="">
							</map>
						</div>
					</div>
					<div class="row">
                        @include('units.show.metadata_and_buttons')
                        @include('units.show.attributes')
					</div>

					<div class="row mt-3">
                        @include('units.show.catalogs')
					</div>

				</div>
			</div>
		</div>

	</div>

	@include('partial.deleteConfirm',['name'=>$unit->code,
									  'id'=> $unit->id,
									  'message'=> __('unit.confirm_delete_message'),
									  'route'=>'UnitController@destroy'])
@endsection


@section('js_footer')
	<script src="{{url('js/highligh_image_area/mapoid.js')}}"></script>
	<script>
		$(document).ready(function(){
			let myMap = $("map[name=master-plan-test]");

			let areas = $(".my-selected")
			$.each(areas, function(key, value) {
				$area = $(value);
				$areaColor = $area.data('color');
				$areaId = $area.data('id');
				myMap.mapoid({
					 // width
					width : 600,

					// stroke color
					strokeColor: 'black',

					// stroke width
					strokeWidth: 1,

					// fill color
					fillColor: $areaColor,

					// 0-1
					fillOpacity: 0.6,

					// in milliseconds
					fadeTime: 500,

					// an array of selected areas
					selectedArea: false,

					// select on click
					selectOnClick: true,
					click: function(){

					},
				});

				myMap.mapoid('selectOne', $areaId, true);
			});

		});
	</script>
@endsection
