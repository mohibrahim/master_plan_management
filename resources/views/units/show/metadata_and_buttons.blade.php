<div class="col-md-4 text-left">
    <div class=" row text-muted">
        <div class="col small">
            {{__('unit.created_at')}}:
        </div>
        <div class="col small">
            {{$unit->created_at}}
        </div>
    </div>
    <div class=" row text-muted">
        <div class="col small">
            {{__('unit.creator_name')}}:
        </div>
        <div class="col small">
            {{optional($unit->creator)->name}}
        </div>
    </div>
    <div class=" row text-muted">
        <div class="col small">
            {{__('unit.updated_at')}}:
        </div>
        <div class="col small">
            {{$unit->updated_at}}
        </div>
    </div>
    <div class=" row text-muted">
        <div class="col small">
            {{__('unit.last_updater_name')}}:
        </div>
        <div class="col small">
            {{optional($unit->lastUpdater)->name}}
        </div>
    </div>
    <div class="row mt-3">
        <div class="col">
            @if(in_array('update_units', $permissions))
                <a href="{{ action('UnitController@edit', ['id'=>$unit->id]) }}" class="btn btn-sm btn-warning btn-block"><i class="fas fa-edit"></i> {{__('unit.edit_unit')}}
                </a>
            @endif
            @if(in_array('destroy_units', $permissions))
                <td><button type="button" class="btn btn-sm btn-danger btn-block" data-toggle="modal" data-target="#myModal"><i class="far fa-trash-alt"></i> {{__('unit.delete_unit')}} </button></td>
            @endif
            @if(in_array('store_catalogs', $permissions) &&
                in_array('store_units', $permissions)
                )
                <a href="{{ action('CatalogController@create', ['model_associated_type'=>'unit_id', 'model_associated_type_id'=>$unit->id]) }}" class="btn btn-sm btn-info btn-block"><i class="fas fa-images"></i> {{__('unit.add_catalog')}}
                </a>
            @endif
            @if(	in_array('store_catalogs', $permissions) &&
                    !$unit->isBooked
                )
                <a href="{{ action('ReservationController@create', ['unit_id'=>$unit->id]) }}" class="btn btn-sm btn-success btn-block"><i class="fas fa-check-square"></i> {{__('unit.reserve_unit')}}
                </a>
            @endif

            <a href="{{ action('UnitController@showAsPdf', ['unit'=>$unit->id]) }}" class="btn btn-sm btn-secondary btn-block">
                <i class="far fa-file-pdf"></i> {{__('unit.show_as_pdf')}}
            </a>
        </div>

    </div>
</div>
