@extends('layouts.app')
@section('title')
	{{__('unit.add_unit')}}
@endsection
	@section('content')
	<div class="container-fluid">
		<div class="col">
			<div class="card card-info">
				<div class="card-heading">
					<h3 class="card-header text-center">{{__('unit.add_unit')}}</h3>
				</div>
				<div class="card-body ">
					@include('errors.list')
					{!! Form::open(['action' => ['UnitController@store'], 'files'=>true, 'method'=>'POST'])!!}
						@include('units._form')
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
@endsection
