@component('mail::message')
# Hello!
Thank you for registering to Ghazala Bay Community!



### Please click the link below to verify your email address. And also Make Sure to Login with your "Credentials: EMail and Password" to complete the verification.

#### [Verify Email Address]({!!$url!!})





#### If you did not create an account, no further action is required.

###### If you’re having trouble clicking the "Verify Email Address" link, copy and paste the URL below into your web browser: <{!!$url!!}>

Thanks,<br>
{{ config('app.name') }}
@endcomponent

