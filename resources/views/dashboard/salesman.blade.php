@extends('layouts.app')
@section('title')
    {{__('pages_titles.dashboard')}}
@endsection
@section('content')
<div class="container-fluid">
    <input type="hidden" id="view-unit-as-modal" value="true">
    <input type="hidden" id="filtering-project-button-name" value="{{__('project.filtering')}}">
    <input type="hidden" id="filtering-project-button-name-when-loading" value="{{__('project.filtering_when_loading')}}">

    <h2 class="text-center mb-5">{{__('dashboard_salesman.assigned_projects')}}</h2>

    <div class="row">

        @foreach($projectUserAssignments as $projectUserAssignment)


            <div class="col-xl-3 col-lg-3 col-md-3">
                <div class="card mb-4">
                    <img src="{{optional($projectUserAssignment->project)->master_plan_image_src}}" class="img-fluid"
                        alt="...">
                    <div class="card-body shadow-sm">
                        <h5 class="card-title float-left text-center">
                            {{optional($projectUserAssignment->project)->name}}
                        </h5>

                        <div class="dropdown float-right">
                            <button class="btn btn-outline-dark btn-sm border-0" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-ellipsis-v text-secondary"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item show-project" href="#"
                                    data-project="{{$projectUserAssignment->project}}" data-project-spinner-id="project-spinner-{{optional($projectUserAssignment->project)->id}}">{{__('dashboard_salesman.view_project')}}</a>
                            </div>
                        </div>
                        <div class="clearfix">
                        </div>
                        <h6 class="card-title text-left text-secondary">
                            <span class="m-2 text-primary">{{__('project.project_units_total_count').":".optional($projectUserAssignment->project)->project_units_statistics['total_count']}}</span>
                            <span class="m-2 text-success">{{__('project.project_units_available_count').":".optional($projectUserAssignment->project)->project_units_statistics['available_count']}}</span>
                            <span class="m-2 text-warning">{{__('project.project_units_hold_count').":".optional($projectUserAssignment->project)->project_units_statistics['hold_count']}}</span>
                            <span class="m-2 text-danger">{{__('project.project_units_booked_count').":".optional($projectUserAssignment->project)->project_units_statistics['booked_count']}}</span>
                            <span class="m-2 text-secondary">{{__('project.project_units_reserved_count').":".optional($projectUserAssignment->project)->project_units_statistics['reserved_count']}}</span>
                            <span class="m-2 text-info">{{__('project.project_units_sold_count').":".optional($projectUserAssignment->project)->project_units_statistics['sold_count']}}</span>
                            <span class="m-2 text-dark">{{__('project.project_units_handed_over_count').":".optional($projectUserAssignment->project)->project_units_statistics['handed_over_count']}}</span>
                        </h6>
                        <div class="d-none align-items-center" id="project-spinner-{{optional($projectUserAssignment->project)->id}}">
                            <strong>{{__('project.project_loading')}}</strong>
                            <div class="spinner-border ml-auto" role="status" aria-hidden="true"></div>
                          </div>
                    </div>
                </div>
            </div>


        @endforeach

    </div>

    <div id="project-wrapper" style="display: none"></div>



    <!-- Modal -->
    <div class="modal fade" id="unit-modal" tabindex="-1" role="dialog"
        aria-labelledby="modal-title" aria-hidden="true">
        <div class="modal-dialog  modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-title">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">

                        <table class="table">
                            <tbody>
                                <tr>
                                    <th scope="row" class="border-0">{!!__('unit.code')!!}</th>
                                    <td class="border-0" id="modal-unit-code"></td>
                                </tr>
                                <tr>
                                    <th scope="row" class="border-0">{{__('unit.status')}}</th>
                                    <td class="border-0" id="modal-unit-status"></td>
                                </tr>
                                <tr>
                                    <th scope="row" class="border-0">{{__('unit.land_number')}}</th>
                                    <td class="border-0" id="modal-unit-land-number"></td>
                                </tr>
                                <tr>
                                    <th scope="row" class="border-0">{{__('unit.land_area')}}</th>
                                    <td class="border-0" id="modal-unit-land-area"></td>
                                </tr>
                                {{-- <tr>
                                    <th scope="row" class="border-0">{{__('unit.construction_status')}}</th>
                                    <td class="border-0" id="modal-unit-construction-status"></td>
                                </tr> --}}
                                <tr>
                                    <th scope="row" class="border-0">{{__('unit.percentage_of_completion')}}</th>
                                    <td class="border-0" id="modal-unit-percentage-of-completion"></td>
                                </tr>
                                <tr>
                                    <th scope="row" class="border-0">{{__('unit.developer_name')}}</th>
                                    <td class="border-0" id="modal-unit-developer-txt"></td>
                                </tr>
                                {{-- <tr>
                                    <th scope="row" class="border-0">{{__('unit.is_for_sale')}}</th>
                                    <td class="border-0" id="modal-unit-is-unit-for-sale"></td>
                                </tr> --}}
                                {{-- <tr>
                                    <th scope="row" class="border-0">{{__('unit.sale_details')}}</th>
                                    <td class="border-0" id="modal-unit-sale-details"></td>
                                </tr> --}}
                                <tr>
                                    <th scope="row" class="border-0">{{__('unit.sale_price')}}</th>
                                    <td class="border-0" id="modal-unit-sale-price"></td>
                                </tr>
                                {{-- <tr>
                                    <th scope="row" class="border-0">{{__('unit.date_of_sale')}}</th>
                                    <td class="border-0" id="modal-unit-date-of-sale"></td>
                                </tr>
                                <tr>
                                    <th scope="row" class="border-0">{{__('unit.is_for_rent')}}</th>
                                    <td class="border-0" id="modal-unit-is-for_rent"></td>
                                </tr>
                                <tr>
                                    <th scope="row" class="border-0">{{__('unit.rent_from')}}</th>
                                    <td class="border-0" id="modal-unit-rent-from"></td>
                                </tr>
                                <tr>
                                    <th scope="row" class="border-0">{{__('unit.rent_to')}}</th>
                                    <td class="border-0" id="modal-unit-rent-to"></td>
                                </tr>
                                <tr>
                                    <th scope="row" class="border-0">{{__('unit.rent_price')}}</th>
                                    <td class="border-0" id="modal-unit-rent-price"></td>
                                </tr>
                                <tr>
                                    <th scope="row" class="border-0">{{__('unit.rent_details')}}</th>
                                    <td class="border-0" id="modal-unit-rent-details"></td>
                                </tr>
                                <tr>
                                    <th scope="row" class="border-0">{{__('unit.unit_account_number')}}</th>
                                    <td class="border-0" id="modal-unit-account-number"></td>
                                </tr>
                                <tr>
                                    <th scope="row" class="border-0">{{__('unit.direction')}}</th>
                                    <td class="border-0" id="modal-unit-direction"></td>
                                </tr>
                                <tr>
                                    <th scope="row" class="border-0">{{__('unit.floor_number')}}</th>
                                    <td class="border-0" id="modal-unit-floor-number"></td>
                                </tr>
                                <tr>
                                    <th scope="row" class="border-0">{{__('unit.address')}}</th>
                                    <td class="border-0" id="modal-unit-address"></td>
                                </tr>
                                <tr>
                                    <th scope="row" class="border-0">{{__('unit.latitude')}}</th>
                                    <td class="border-0" id="modal-unit-latitude"></td>
                                </tr>
                                <tr>
                                    <th scope="row" class="border-0">{{__('unit.longitude')}}</th>
                                    <td class="border-0" id="modal-unit-longitude"></td>
                                </tr>
                                <tr>
                                    <th scope="row" class="border-0">{{__('unit.electricity_meter_number')}}</th>
                                    <td class="border-0" id="modal-unit-electricity-meter-number"></td>
                                </tr> --}}
                                <tr>
                                    <th scope="row" class="border-0">{{__('unit.notes')}}</th>
                                    <td class="border-0" id="modal-unit-notes"></td>
                                </tr>
                                <tr>
                                    <th scope="row" class="border-0">{{__('unit.show_as_pdf')}}</th>
                                    <td class="border-0" id="modal-unit-show-pdf-link-as-button"></td>
                                </tr>
                            </tbody>
                        </table>

                        <hr>

                        <h3 class="text-center">{{__('catalog.project_files')}}</h3>
                        <div class="list-group" id="modal-unit-project-catalogs-links">
                        </div>
                        <input id="catalog-messages" type="hidden" data-catalog-messages='{"image_translation":"{{__("catalog.images")}}","pdf_translation":"{{__("catalog.pdfs")}}","loading_translation":"{{__("catalog.loading")}}","fail_to_load_translation":"{{__("catalog.fail_to_load")}}","preview_file":"{{url("catalogs_miscellaneous/display_file/")}}"}'/>



                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('partial.close')}}</button>
                </div>
            </div>
        </div>
    </div>





@endsection

@section('js_footer')
<script src="{{url('js/maphilight/jquery.maphilight.js')}}"></script>
<script>
    $(document).ready(function () {
        salesmanDashboard();
    });
</script>
@endsection
