@extends('layouts.app')

@section('title')
	{{$zone->name}}
@endsection

@section('content')
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h2 class="text-center">{{$zone->name}}</h2>
			</div>
			<div class="card-body ">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-4 text-left">
							<img src="{{url('images/helper_images/zone_cover.jpg')}}" class="rounded w-100 border border-default mb-3" alt="">
							<div class=" row text-muted">
								<div class="col small">
									{{__('zone.created_at')}}:
								</div>
								<div class="col small">
									{{$zone->created_at}}
								</div>
							</div>
							<div class=" row text-muted">
								<div class="col small">
									{{__('zone.creator_name')}}:
								</div>
								<div class="col small">
									{{optional($zone->creator)->name}}
								</div>
							</div>
							<div class=" row text-muted">
								<div class="col small">
									{{__('zone.updated_at')}}:
								</div>
								<div class="col small">
									{{$zone->updated_at}}
								</div>
							</div>
							<div class=" row text-muted">
								<div class="col small">
									{{__('zone.last_updater_name')}}:
								</div>
								<div class="col small">
									{{optional($zone->lastUpdater)->name}}
								</div>
							</div>
							<div class="row mt-3">
								<div class="col">
									@if(in_array('update_zones', $permissions))
										<a href="{{ action('ZoneController@edit', ['id'=>$zone->id]) }}" class="btn btn-sm btn-warning btn-block"><i class="fas fa-edit"></i> {{__('zone.edit_zone')}}
										</a>
									@endif
									@if(in_array('destroy_zones', $permissions))
										<td><button type="button" class="btn btn-sm btn-danger btn-block" data-toggle="modal" data-target="#myModal"><i class="far fa-trash-alt"></i> {{__('zone.delete_zone')}} </button></td>
									@endif
									@if(	in_array('store_catalogs', $permissions) &&
											in_array('store_zones', $permissions)
										)
										<a href="{{ action('CatalogController@create', ['model_associated_type'=>'zone_id', 'model_associated_type_id'=>$zone->id]) }}" class="btn btn-sm btn-primary btn-block"><i class="fas fa-images"></i> {{__('zone.add_catalog')}}
										</a>
									@endif
								</div>

							</div>
						</div>
						<div class="col">

							<div class="container-fluid">
								<div class="row">
									<div class="col">
										<table class="table">
											<tbody>
												<tr>
													<th scope="row" class="border-0">{{__('zone.project_name')}}</th>
													<td class="border-0">{!!optional($zone->project)->project_show_link!!}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('zone.phase_name')}}</th>
													<td class="border-0">{!!optional($zone->phase)->phase_show_link!!}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('zone.id')}}</th>
													<td class="border-0">{{$zone->id}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('zone.name')}}</th>
													<td class="border-0">{{$zone->name}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('zone.notes')}}</th>
													<td class="border-0">{{$zone->notes}}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<hr>
							</div>

						</div>
					</div>


					<div class="row mt-3">
						<div class="col">
							<div class="card">
								<div class="card-body">
									<h4 class="font-weight-bold text-center m-3"> الكتالوجات </h4>

									<table class="table table-hover" id="catalogs">
										<thead>
											<tr>
												<th>#</th>
												<th>{{__('catalog.id')}}</th>
												<th>{{__('catalog.name')}}</th>
												<th>{{__('catalog.description')}}</th>
												<th>{{__('catalog.created_at')}}</th>
												<th>{{__('catalog.creator_name')}}</th>
											</tr>
										</thead>
										<tbody>
											@foreach($zone->catalogs as $catalogKey => $catalog)
												<tr>
													<td>{{$catalogKey+1}}</td>
													<td>{{$catalog->id}}</td>
													<td>
														<a href="{{action('CatalogController@show', ['id'=>$catalog->id])}}">
															{{$catalog->name}}
														</a>
													</td>
													<td>{{$catalog->description}}</td>
													<td>{{$catalog->created_at}}</td>
													<td>{{optional($catalog->creator)->name}}</td>

												</tr>
											@endforeach
										</tbody>
									</table>
									<input id="data-table-search" type="hidden" value="{{__('data_table.search')}}">
									<input id="data-table-zero-record" type="hidden" value="{{__('data_table.zeroRecords')}}">
								</div>
							</div>
						</div>
					</div>


				</div>
			</div>
		</div>

	</div>

	@include('partial.deleteConfirm',['name'=>$zone->name,
									  'id'=> $zone->id,
									  'message'=> __('zone.confirm_delete_message'),
									  'route'=>'ZoneController@destroy'])
@endsection
