@extends('layouts.app')
@section('title')
	{{__('zone.edit_zone')}}
@endsection
@section('content')
	<div class="container-fluid">
		<div class="col">
			<div class="card card-info">
				<div class="card-heading">
					<h3 class="card-header text-center">{{$zone->name}}</h3>
				</div>
				<div class="card-body ">
					@include('errors.list')
					{!! Form::model($zone, ['action' => ['ZoneController@update', 'id'=>$zone->id], 'files'=>true,
					'method'=>'put'])!!}
					@include('zones._form')
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
@endsection

