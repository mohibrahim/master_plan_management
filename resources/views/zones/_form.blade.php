<div class="container-fluid border">
	<div class="row">
		<div class="col">

			@include('partial.select_project_and_phase_zone_block', ['modelName'=>'zone', 'showSelectPhase' => true, 'showSelectZone' => false, 'showSelectUnitModel' => false, 'showSelectBlock' => false])

			<input type="hidden" name="selected_project_master_plan_image" id="selected-project-master-plan-image" value="{{$zone->selected_project_master_plan_image}}">

			<div class="form-group">
				<span style="color:#f47373">*</span>
				{!!Form::label('name', __('zone.name'))!!}
				{!!Form::text('name', null,['class'=>'form-control', 'placeholder'=>__('zone.name_placeholder')]) !!}
			</div>

			<div class='form-group'>
				{!!Form::label('notes', __('zone.notes'))!!}
				{!!Form::textarea('notes', null, ['class'=>'form-control', 'rows'=>'5',
				'placeholder'=>__('zone.notes_placeholder')])!!}
			</div>


		</div>

	</div>
</div>

<div class="form-group">
	{!!Form::submit(__('zone.save'), ['class'=>'btn btn-primary form-control'])!!}
</div>


@section('head')
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
@endsection
@section('js_footer')
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	<script language="javascript" src="{{url('js/jqurey_canvas_area_drawer/jquery.canvasAreaDraw.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
	<script>
		$(document).ready(function(){
			selectProjectAndPhase('zones', true, false, false);

			let htmlAreaTagCoords = $('#html-area-tag-coords');
			let masterPlanImageValue = $('#selected-project-master-plan-image').val();

			$('#project-id').on('select2:select', function (event) {
				//Cache DOM
				let masterPlanImageValue = event.params.data.masterPlanImage;
				let masterPlanImageWrapper = $('.master-plan-image-wrapper');

				if (!isEmptyOrSpaces(masterPlanImageValue)) {
					masterPlanImageWrapper.empty();
					htmlAreaTagCoords.val('');
					htmlAreaTagCoords.canvasAreaDraw({
						imageUrl: "{{asset('')}}"+masterPlanImageValue
					});
				}
			});

			//in update state
			if (!isEmptyOrSpaces(masterPlanImageValue)){
				htmlAreaTagCoords.canvasAreaDraw({
					imageUrl: "{{asset('')}}"+masterPlanImageValue
				});

			}
		});
	</script>
@endsection
