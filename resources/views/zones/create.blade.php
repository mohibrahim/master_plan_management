@extends('layouts.app')
@section('title')
	{{__('zone.add_zone')}}
@endsection
	@section('content')
	<div class="container-fluid">
		<div class="col">
			<div class="card card-info">
				<div class="card-heading">
					<h3 class="card-header text-center">{{__('zone.add_zone')}}</h3>
				</div>
				<div class="card-body ">
					@include('errors.list')
					{!! Form::open(['action' => ['ZoneController@store'], 'files'=>true, 'method'=>'POST'])!!}
						@include('zones._form')
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
@endsection
