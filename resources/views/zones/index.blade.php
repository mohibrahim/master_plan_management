@extends('layouts.app')
@section('title')
	{{__('zone.all_zones')}}
@endsection
@section('content')
	<div class="col-md-12">
		<div class="card">

			<div class="card-header">
				{{__('zone.all_zones')}}
			</div>
				<div class="card-body">
                    <div class="container">
                        <div class="row">
                            <div class="col">

                                <div class="form-inline">
                                    <div class="input-group mb-2 mr-sm-2">
                                        <label class="m-2" for="searching-input">{{__('zone.searching_on_zone')}}: </label>
                                        <input type="text" class="form-control" id="searching-input" placeholder="{{__('zone.searching_word')}}" autofocus>
                                    </div>

                                    <button id="searching-button" type="button" class="btn btn-primary mb-2">{{__('zone.search')}}</button>
                                    <input id="searching-button-required" type="hidden" value="{{__('zone.searching_button_required')}}">
                                    <input id="searching-button-searching" type="hidden" value="{{__('zone.searching_button_searching')}}">
                                    <input id="searching-button-search" type="hidden" value="{{__('zone.searching_button_search')}}">
                                </div>

                            </div>
                            <div class="col-3">
                                <div class="float-right">
                                    <div class='form-group '>
                                        {!!Form::label('net-suite-id', __('net_suite_integration.select_net_suite_integration'))!!}
                                        {!!Form::select('a', $netSuitNamesIds, null,['id'=>'net-suite-id', 'class'=>'form-control custom-select', 'placeholder'=>__('net_suite_integration.select_net_suite_integration_placeholder')]) !!}
                                    </div>
                                    <div class="form-group">
                                        <a href="#" type="button" id="net-suite-id-sync-button" class="btn btn-primary btn-block disabled">
                                            <i class="fas fa-sync-alt"></i>
                                        </a>

                                    </div>
                                </div>
                                <span class="clearfix"></span>
                            </div>
                        </div>
                    </div>
					<hr>
					<table class="table table-hover" id="zones">
						<thead>
							<tr>
								<th>#</th>
								<th>{{__('zone.project_name')}}</th>
								<th>{{__('zone.id')}}</th>
								<th>{{__('zone.name')}}</th>
								<th>{{__('zone.created_at')}}</th>
								<th>{{__('zone.creator_name')}}</th>
							</tr>
						</thead>
						<tbody>
							@foreach($zones as $zoneKey => $zone)
								<tr>
									<td>{{$zoneKey+1}}</td>
									<td>
										<a href="{{action('ProjectController@show', ['id'=>optional($zone->project)->id])}}">
											{{optional($zone->project)->name}}
										</a>
									</td>
									<td>{{$zone->id}}</td>
									<td>
										<a href="{{action('ZoneController@show', ['id'=>$zone->id])}}">
											{{$zone->name}}
										</a>
									</td>
									<td>{{$zone->created_at}}</td>
									<td>{{optional($zone->creator)->name}}</td>

								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="d-flex justify-content-center">

					{{$zones->links()}}
				</div>
			</div>

			<input id="data-table-search" type="hidden" value="{{__('data_table.search')}}">
			<input id="data-table-zero-record" type="hidden" value="{{__('data_table.zeroRecords')}}">
	</div>
@endsection

@section('head')
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css"> -->
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
@endsection
@section('js_footer')
	<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
	<script>
		$(document).ready(function () {
			dataTable = $('#zones').DataTable({
				"paging":   false,
				"info":     false,
				"language": {
					"search": $("#data-table-search").val(),
					"zeroRecords": $("#data-table-zero-record").val(),
				},
			});
            searchingOnZonesAjax();
            selectSyncOnZones();
		});
	</script>
@endsection
