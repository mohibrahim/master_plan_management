@extends('layouts.app')
@section('title')
	{{__('reservation.add_reservation')}}
@endsection
	@section('content')
	<div class="container-fluid">
		<div class="col">
			<div class="card card-info">
				<div class="card-heading">
					<h3 class="card-header text-center">{{__('reservation.add_reservation')}}</h3>
				</div>
				<div class="card-body ">
					@include('errors.list')
					{!! Form::open(['action' => ['ReservationController@store'], 'files'=>true, 'method'=>'POST'])!!}
						@include('reservations._form')
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
@endsection
