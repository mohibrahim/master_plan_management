<div class="container-fluid border">
	<div class="row">
		<div class="col">


			<div class="card my-3">
				<div class="card-body">
					<div class="form-group">
						<span style="color:#f47373">*</span>
						<label for="unit-id">{{__('reservation.unit_code')}}</label>
						<select class="form-control" name="unit_id" id="unit-id">
							@if (old('unit_id') && old('old_selected_unit_code'))
								<option value="{{old('unit_id')}}">{{old('old_selected_unit_code')}}</option>
							@elseif (!empty($reservation->selected_unit_code_id))
								<option value="{{$reservation->selected_unit_code_id['id']}}">{{$reservation->selected_unit_code_id['code']}}</option>
							@endif
						</select>
						<input type="hidden" id="unit-code-placeholder" value="{{__('reservation.unit_code_placeholder')}}">
                        <input type="hidden" name="old_selected_unit_code" id="old-selected-unit-code" value="{{old('old_selected_unit_code')}}">
					</div>
				</div>
			</div>

			<div class="card my-3">
				<div class="card-body">
					<div class="form-group">
						<span style="color:#f47373">*</span>
						<label for="customer-id">{{__('reservation.customer_name')}}</label>
						<a href="{{action('CustomerController@create')}}" class="mr-3" target="_blank">
							{{__('reservation.add_new_customer')}}
						</a>

						<select class="form-control" name="customer_id" id="customer-id">
							@if (old('customer_id') && old('old_selected_customer_name'))
								<option value="{{old('customer_id')}}">{{old('old_selected_customer_name')}}</option>
							@elseif (!empty($reservation->selected_customer_name_id))
								<option value="{{$reservation->selected_customer_name_id['id']}}">{{$reservation->selected_customer_name_id['name']}}</option>
							@endif
						</select>
						<input type="hidden" id="customer-name-placeholder" value="{{__('reservation.customer_name_placeholder')}}">
						<input type="hidden" name="old_selected_customer_name" id="old-selected-customer-name" value="{{old('old_selected_customer_name')}}">
					</div>
				</div>
            </div>

            <div class="card my-3">
                <div class="card-body">
                    <div class='form-group' id="group-indexation">
                        {!!Form::label('bank-id', __('reservation.bank_id'))!!}
                        <input type="hidden" name="bank_id" value="">
                        {!!Form::select('bank_id', $reservation->bank_id, null, ['id'=>'bank-id', 'class'=>'form-control', 'style'=>'width: 100%']) !!}

                        <input type="hidden" id="bank-id-placeholder" value="{{__('reservation.bank_id_placeholder')}}">
                        <input type="hidden" name="old_bank_id" id="old-bank-id" value="{{old('bank_id')}}">
                        <input type="hidden" name="old_bank_id_text" id="old-bank-id-text" value="{{old('old_bank_id_text')}}">
                    </div>
                </div>
            </div>

			<div class="form-group">
				<span style="color:#f47373">*</span>
				{!!Form::label('booking-date', __('reservation.booking_date'))!!}
                {!!Form::date('booking_date', null,[ 'id'=>'booking-date', 'class'=>'form-control', 'placeholder'=>__('reservation.booking_date_placeholder')]) !!}
                <input type="hidden" id="today-date" value="{{now()->today()->format('Y-m-d')}}">
			</div>

			<div class='form-group'>
                {!!Form::label('bank_account_details', __('reservation.bank_account_details'))!!}
				{!!Form::textarea('bank_account_details', null, ['class'=>'form-control', 'rows'=>'5',
				'placeholder'=>__('reservation.bank_account_details_placeholder')])!!}
            </div>

			<div class='form-group'>
                {!!Form::label('document_number', __('reservation.document_number'))!!}
				{!!Form::text('document_number', null, ['class'=>'form-control', 'rows'=>'5',
				'placeholder'=>__('reservation.document_number_placeholder')])!!}
            </div>

            <div class="form-group">
                {!!Form::label('expiry_date', __('reservation.expiry_date'))!!}
                {!!Form::date('expiry_date', null,['class'=>'form-control', 'placeholder'=>__('reservation.expiry_date_placeholder')]) !!}
            </div>

            <div class='form-group'>
                {!!Form::label('status ', __('reservation.status'))!!}
                {!!Form::select('status', $reservationStatus, null,['class'=>'form-control', 'placeholder'=>__('reservation.status_placeholder')]) !!}
            </div>

			<div class='form-group'>
				{!!Form::label('booking_status', __('reservation.booking_status'))!!}
				{!!Form::text('booking_status', null, ['class'=>'form-control', 'placeholder'=>__('reservation.booking_status_placeholder')])!!}
            </div>

			<div class='form-group'>
				{!!Form::label('reason', __('reservation.reason'))!!}
				{!!Form::textarea('reason', null, ['class'=>'form-control', 'rows'=>'5',
				'placeholder'=>__('reservation.reason_placeholder')])!!}
            </div>





            <div class="card border-secoundry mb-3">
                <div class="card-body text-secoundry">
                    <div class='form-group'>
                        {!!Form::label('booking', __('reservation.booking'))!!}
                        <input type="hidden" name="booking" value="0">
                        {!!Form::checkbox('booking', "1", null, ['id'=>'booking'])!!}
                    </div>
                    <div>
                        <p>
                            <small>
                                {{__('unit.booking_amount').': '}}
                                <span id="booking-amount-for-unit">
                                    {{optional($reservation->unit)->booking_amount}}
                                </span>
                                <span class="booking-currency-for-unit">
                                    {{optional($reservation->unit)->currency}}
                                </span>
                            </small>
                        </p>
                        <p>
                            <small>
                                {{__('unit.minimum_booking_amount').': '}}
                                <span id="minimum-booking-amount-for-unit">
                                    {{optional($reservation->unit)->minimum_booking_amount}}
                                </span>
                                <span class="booking-currency-for-unit">
                                    {{optional($reservation->unit)->currency}}
                                </span>
                            </small>
                        </p>

                        {!!Form::hidden('old_booking_amount_for_unit', null,['id'=>'old-booking-amount-for-unit']) !!}
                        {!!Form::hidden('old_booking_currency_for_unit', null,['id'=>'old-booking-currency-for-unit']) !!}
                        {!!Form::hidden('old_minimum_booking_amount_for_unit', null,['id'=>'old-minimum-booking-amount-for-unit']) !!}
                    </div>
                    <div class="card border-secoundry mb-3 d-none" id="payment-wrapper">
                        <div class="card-body text-secoundry">

                            <div class='form-group'>
                                {!!Form::label('paid_amount', __('reservation.paid_amount'))!!}
                                {!!Form::number('paid_amount', null, ['id' => 'paid-amount', 'class'=>'form-control', 'placeholder'=>__('reservation.paid_amount_placeholder'), 'step'=>'.01'])!!}
                            </div>

                            <div class='form-group'>
                                {!!Form::label('remaining_amount', __('reservation.remaining_amount'))!!}
                                {!!Form::number('remaining_amount', null, ['id' => 'remaining-amount', 'class'=>'form-control', 'placeholder'=>__('reservation.remaining_amount_placeholder'), 'step'=>'.01'])!!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>







			<div class='form-group'>
				{!!Form::label('notes', __('reservation.notes'))!!}
				{!!Form::textarea('notes', null, ['class'=>'form-control', 'rows'=>'5',
				'placeholder'=>__('reservation.notes_placeholder')])!!}
			</div>
		</div>

	</div>
</div>

<div class="form-group">
	{!!Form::submit(__('reservation.save'), ['class'=>'btn btn-primary form-control'])!!}
</div>


@section('head')
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
@endsection
@section('js_footer')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
	<script>
		$(document).ready(function () {

			reservationSelectUnitId();
            reservationSelectCustomerId();
            checkBooking();
            setBookingDateByToday();
            selectBankForReservation();


		});
	</script>
@endsection
