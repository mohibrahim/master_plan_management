@extends('layouts.app')
@section('title')
	{{__('reservation.edit_reservation')}}
@endsection
@section('content')
	<div class="container-fluid">
		<div class="col">
			<div class="card card-info">
				<div class="card-heading">
					<h3 class="card-header text-center">{{$reservation->name}}</h3>
				</div>
				<div class="card-body ">
					@include('errors.list')
					{!! Form::model($reservation, ['action' => ['ReservationController@update', 'id'=>$reservation->id], 'files'=>true,
					'method'=>'put'])!!}
					@include('reservations._form')
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
@endsection

