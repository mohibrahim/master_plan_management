@extends('layouts.app')

@section('title')
	{{__('reservation.id').": ".$reservation->id}}
@endsection

@section('content')
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h2 class="text-center">{{__('reservation.id').": ".$reservation->id}}</h2>
			</div>
			<div class="card-body ">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-4 text-left">
							<img src="{{url('images/helper_images/reservations_cover.png')}}" class="rounded w-100 border border-default mb-3" alt="">
							<div class=" row text-muted">
								<div class="col small">
									{{__('reservation.created_at')}}:
								</div>
								<div class="col small">
									{{$reservation->created_at}}
								</div>
							</div>
							<div class=" row text-muted">
								<div class="col small">
									{{__('reservation.creator_name')}}:
								</div>
								<div class="col small">
									{{optional($reservation->creator)->name}}
								</div>
							</div>
							<div class=" row text-muted">
								<div class="col small">
									{{__('reservation.updated_at')}}:
								</div>
								<div class="col small">
									{{$reservation->updated_at}}
								</div>
							</div>
							<div class=" row text-muted">
								<div class="col small">
									{{__('reservation.last_updater_name')}}:
								</div>
								<div class="col small">
									{{optional($reservation->lastUpdater)->name}}
								</div>
							</div>
							<div class="row mt-3">
								<div class="col">
									@if(in_array('update_reservations', $permissions))
										<a href="{{ action('ReservationController@edit', ['id'=>$reservation->id]) }}" class="btn btn-sm btn-warning btn-block"><i class="fas fa-edit"></i> {{__('reservation.edit_reservation')}}
										</a>
									@endif
									@if(in_array('destroy_reservations', $permissions))
										<td><button type="button" class="btn btn-sm btn-danger btn-block" data-toggle="modal" data-target="#myModal"><i class="far fa-trash-alt"></i> {{__('reservation.delete_reservation')}} </button></td>
									@endif
									@if(	in_array('store_catalogs', $permissions) &&
										in_array('store_reservations', $permissions)
									)
										<a href="{{ action('CatalogController@create', ['model_associated_type'=>'reservation_id', 'model_associated_type_id'=>$reservation->id]) }}"
											class="btn btn-sm btn-primary btn-block"><i class="fas fa-images"></i>
											{{__('reservation.add_catalog')}}
										</a>
									@endif
								</div>

							</div>
						</div>
						<div class="col">

							<div class="container-fluid">
								<div class="row">
									<div class="col">
										<table class="table">
											<tbody>
												<tr>
													<th scope="row" class="border-0">{{__('reservation.unit_code')}}</th>
													<td class="border-0">
														{!!optional($reservation->unit)->unit_show_link_with_code!!}
													</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('reservation.customer_name')}}</th>
													<td class="border-0">
														{!!optional($reservation->customer)->customer_show_link!!}
													</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('reservation.id')}}</th>
													<td class="border-0">{{$reservation->id}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('reservation.booking_date')}}</th>
													<td class="border-0">{{$reservation->booking_date}}</td>
												</tr>

												<tr>
													<th scope="row" class="border-0">{{__('reservation.booking')}}</th>
													<td class="border-0">{{($reservation->booking)?(__('reservation.paid')):(__('reservation.no_prepayment'))}}</td>
                                                </tr>

                                                <tr>
													<th scope="row" class="border-0">{{__('reservation.expiry_date')}}</th>
													<td class="border-0">{{$reservation->expiry_date}}</td>
                                                </tr>

                                                <tr>
													<th scope="row" class="border-0">{{__('reservation.status')}}</th>
													<td class="border-0">{{$reservation->status}}</td>
                                                </tr>

                                                <tr>
													<th scope="row" class="border-0">{{__('reservation.booking_status')}}</th>
													<td class="border-0">{{$reservation->booking_status}}</td>
                                                </tr>

                                                <tr>
													<th scope="row" class="border-0">{{__('reservation.reason')}}</th>
													<td class="border-0">{{$reservation->reason}}</td>
                                                </tr>

                                                <tr>
                                                    <th scope="row" class="border-0">{{__('reservation.paid_amount')}}</th>
                                                    <td class="border-0">{{$reservation->paid_amount_presentation.' '.optional($reservation->unit)->currency}}</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row" class="border-0">{{__('reservation.remaining_amount')}}</th>
                                                    <td class="border-0">{{$reservation->remaining_amount_presentation.' '.optional($reservation->unit)->currency}}</td>
                                                </tr>
												<tr>
													<th scope="row" class="border-0">{{__('reservation.bank_id')}}</th>
													<td class="border-0">{{optional($reservation->bank)->name}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('reservation.bank_account_details')}}</th>
													<td class="border-0">{{$reservation->bank_account_details}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('reservation.document_number')}}</th>
													<td class="border-0">{{$reservation->document_number}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('reservation.notes')}}</th>
													<td class="border-0">{{$reservation->notes}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('reservation.created_at')}}</th>
													<td class="border-0">{{$reservation->created_at}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('reservation.updated_at')}}</th>
													<td class="border-0">{{$reservation->updated_at}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('reservation.net_suite_created_at')}}</th>
													<td class="border-0">{{$reservation->net_suite_created_at}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('reservation.net_suite_updated_at')}}</th>
													<td class="border-0">{{$reservation->net_suite_updated_at}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('reservation.sales_representative_name')}}</th>
													<td class="border-0">{{$reservation->sales_representative_name}}</td>
												</tr>
												<tr>
													<th scope="row" class="border-0">{{__('reservation.net_suite_internal_id')}}</th>
													<td class="border-0">{{$reservation->net_suite_internal_id}}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<hr>
							</div>

						</div>
					</div>




					<div class="row mt-3">

						<div class="col">
							<div class="card">
								<div class="card-body">
									<h4 class="font-weight-bold text-center m-3"> {{__('reservation.reservation_files')}} </h4>

									<table class="table table-hover" id="catalogs">
										<thead>
											<tr>
												<th>#</th>
												<th>{{__('catalog.id')}}</th>
												<th>{{__('catalog.reservation_files_name')}}</th>
												<th>{{__('catalog.description')}}</th>
												<th>{{__('catalog.created_at')}}</th>
												<th>{{__('catalog.creator_name')}}</th>
											</tr>
										</thead>
										<tbody>
											@foreach($reservation->catalogs as $catalogKey => $catalog)
											<tr>
												<td>{{$catalogKey+1}}</td>
												<td>{{$catalog->id}}</td>
												<td>
													<a href="{{action('CatalogController@showReservationCatalog', ['id'=>$catalog->id])}}">
														{{$catalog->name}}
													</a>
												</td>
												<td>{{$catalog->description}}</td>
												<td>{{$catalog->created_at}}</td>
												<td>{{optional($catalog->creator)->name}}</td>

											</tr>
											@endforeach
										</tbody>
									</table>
									<input id="data-table-search" type="hidden" value="{{__('data_table.search')}}">
									<input id="data-table-zero-record" type="hidden"
										value="{{__('data_table.zeroRecords')}}">
								</div>
							</div>
						</div>


					</div>




				</div>
			</div>
		</div>

	</div>

	@include('partial.deleteConfirm',['name'=>$reservation->id,
									  'id'=> $reservation->id,
									  'message'=> __('reservation.confirm_delete_message'),
									  'route'=>'ReservationController@destroy'])
@endsection


@section('head')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
@endsection

@section('js_footer')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

<script>
	$(document).ready(function () {
		dataTable = $('#catalogs').DataTable({
			"paging": false,
			"info": false,
			"language": {
				"search": $("#data-table-search").val(),
				"zeroRecords": $("#data-table-zero-record").val(),
			},
		});


	});
</script>
@endsection


