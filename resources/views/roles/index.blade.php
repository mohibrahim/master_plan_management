@extends('layouts.app')

@section('title')
	All Roles
@endsection
			
@section('content')

	<div class="col-md-12">
		<div class="card">
		<div class="card-header">
			All Roles
		</div>
			<div class="card-body ">	
				<table class="table">
					<thead>
						<th>ID</th>
						<th>Name</th>
						<th>Descriptions</th>
					</thead>
					<tbody>
						@foreach($roles as $role)
							<tr>
								<td>{{$role->id}}</td>
								<td><a href="{{ action('RoleController@show',['id'=>$role->id]) }}"> {{$role->name}}</a></td>
								<td>{{$role->descriptions}}</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
	
	

@endsection
