@extends('layouts/app')
@section('title')
	Create Role
@endsection
@section('content')
		<div class="col-md-12">
			<div class="card">
			<div class="card-header">
				Add New Role
			</div>
				<div class="card-body">
					<!-- validation errors -->
					@include('errors.list')
					<form class="" action="{{action('RoleController@store')}}" method="post">
						{{ csrf_field() }}
						@include('roles._form')
					</form>
				</div>
			</div>
		</div>
@endsection
