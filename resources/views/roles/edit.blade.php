@extends('layouts.app')
@section('title')
	Edit Role
@endsection
@section('content')

	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					Edit Role
				</div>
				<div class="card-body ">
					<!-- validation errors -->
					@include('errors.list')
					<form class="" action="{{action('RoleController@update', ['id'=>$role->id])}}" method="post">
						<input type="hidden" name="_method" value="PATCH">
						{{ csrf_field() }}
						@include('roles._form')
					</form>
				</div>
			</div>

		</div>
	</div>
@endsection
