@extends('layouts.app')

@section('title')
	{{$role->name}}
@endsection

@section('content')	

	<div class="col-md-12">
		<div class="card">
		<div class="card-header">
			Role
		</div>
			<div class="card-body ">

				<table class="table">
					<th>ID</th>
					<th>Name</th>
					<th>Descriptions</th>
					@if(in_array('update_roles', $permissions))
						<th>Edit</th>
					@endif
					@if(in_array('destroy_roles', $permissions))
						<th>Delet</th>
					@endif
					<tr>
						<td>{{$role->id}}</td>
						<td>{{$role->name}}</td>
						<td>{{$role->descriptions}}</td>
						@if(in_array('update_roles', $permissions))
							<td><a href="{{action('RoleController@edit',['id'=>$role->id])}}"> Edit</a></td>
						@endif
						@if(in_array('destroy_roles', $permissions))
							<td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal"><i class="far fa-trash-alt"></i> Delete</button></td<button></tr>
						@endif
					</tr>

				</table>
				
				


			</div>
		</div>

	</div>

	@include('partial.deleteConfirm', ['name'=>$role->name,
		'id'=> $role->id,
		'message'=>'Are you sure you want to delete',
		'route'=>'RoleController@destroy'
	])

@endsection()

