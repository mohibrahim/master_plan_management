<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
|--------------------------------------------------------------------------
| Welcome
|--------------------------------------------------------------------------
|
*/

Route::get(
    '/',
    function () {
        return view('welcome');
    }
);

/*
|--------------------------------------------------------------------------
| Auth
|--------------------------------------------------------------------------
|
*/
// Auth::routes(['verify' => true]);
Auth::routes();
/*
|--------------------------------------------------------------------------
| Home
|--------------------------------------------------------------------------
|
*/
Route::get(
    '/home',
    'HomeController@index'
)->name('home');


/*
|--------------------------------------------------------------------------
| Select language
|--------------------------------------------------------------------------
|
*/
Route::get('/lang/{language}', 'LanguageController@switchLanguage')->name('switch_language');
/*
|--------------------------------------------------------------------------
| Roles
|--------------------------------------------------------------------------
|
*/
Route::resource('roles', 'RoleController');
/*
|--------------------------------------------------------------------------
| Permission
|--------------------------------------------------------------------------
|
*/
Route::resource('permissions', 'PermissionController');
/*
|--------------------------------------------------------------------------
| USERS
|--------------------------------------------------------------------------
|
*/

Route::resource('users', 'UserController');
Route::post('users_miscellaneous/index_search', 'UserController@search')->name('users_miscellaneous.index_search');

// Route::get(
//     'users_miscellaneous/delete_personal_image_ajax/{image_name}/{current_password}',
//     'UserController@deletePersonalImageAjax'
// )->name('users_miscellaneous.delete_personal_image_ajax');
// Route::get(
//     'users_miscellaneous/delete_personal_image_without_verification_ajax/{image_name}',
//     'UserController@deletePersonalImageWithoutVerificationAjax'
// )->name('users_miscellaneous.delete_personal_image_without_verification_ajax');
Route::get(
    'users_miscellaneous/index_users_not_verified_email',
    'UserController@indexUsersNotVerifiedEmail'
)->name('users_miscellaneous.index_users_not_verified_email');
Route::get(
    'users_miscellaneous/verify_user_email/{user}',
    'UserController@verifyUserEmail'
)->name('users_miscellaneous.verify_user_email');
Route::get(
    'users_miscellaneous/delete_personal_image_ajax/{project_file_id}',
    'UserController@deletePersonalImage'
)->name('users_miscellaneous.delete_personal_image_ajax');
/*
|--------------------------------------------------------------------------
| ROLE-USER
|--------------------------------------------------------------------------
|
*/
Route::resource('role_user', 'RoleUserController');

/*
|--------------------------------------------------------------------------
| Project Files
|--------------------------------------------------------------------------
|
*/
Route::resource('project_files', 'ProjectFileController');
Route::post(
    'project_files_miscellaneous/index_search',
    'ProjectFileController@search'
)->name('project_files_miscellaneous.index_search');

/*
|--------------------------------------------------------------------------
| Catalogs
|--------------------------------------------------------------------------
|
*/
Route::resource('catalogs', 'CatalogController');
Route::post(
    'catalogs_miscellaneous/index_search',
    'CatalogController@search'
)->name('catalogs_miscellaneous.index_search');
Route::get(
    'catalogs_miscellaneous/delete_file_ajax/{project_file_id}',
    'CatalogController@deleteFile'
)->name('catalogs_miscellaneous.delete_file_ajax');
Route::get(
    'catalogs_miscellaneous/download_file',
    'CatalogController@downloadFile'
)->name('catalogs_miscellaneous.download_file');
Route::get(
    'catalogs_miscellaneous/display_file',
    'CatalogController@displayFile'
)->name('catalogs_miscellaneous.display_file');
Route::get(
    'catalogs_miscellaneous/show_reservation_catalog/{catalog}',
    'CatalogController@showReservationCatalog'
)->name('catalogs_miscellaneous.show_reservation_catalog');
/*
|--------------------------------------------------------------------------
| Projects
|--------------------------------------------------------------------------
|
*/
Route::resource('projects', 'ProjectController');
Route::post(
    'projects_miscellaneous/index_search',
    'ProjectController@search'
)->name('projects_miscellaneous.index_search');
Route::get(
    'projects_miscellaneous/delete_image_ajax/{project_file_id}',
    'ProjectController@deleteImage'
)->name('projects_miscellaneous.delete_image_ajax');
Route::get(
    'projects_miscellaneous/filter_master_plan_image',
    'ProjectController@filterMasterPlanImage'
)->name('projects_miscellaneous.filter_master_plan_image');
Route::get('projects_miscellaneous/delete_logo', 'ProjectController@deleteLogo')->name('projects_miscellaneous.delete_logo');

/*
|--------------------------------------------------------------------------
| Phases
|--------------------------------------------------------------------------
|
*/
Route::resource('phases', 'PhaseController');
Route::post(
    'phases_miscellaneous/index_search',
    'PhaseController@search'
)->name('phases_miscellaneous.index_search');
Route::get(
    'phases_miscellaneous/get_project_names_ids',
    'PhaseController@getProjectNamesIds'
)->name('phases_miscellaneous.get_project_names_ids');
/*
|--------------------------------------------------------------------------
| Zones
|--------------------------------------------------------------------------
|
*/
Route::resource('zones', 'ZoneController');
Route::post(
    'zones_miscellaneous/index_search',
    'ZoneController@search'
)->name('zones_miscellaneous.index_search');
Route::get(
    'zones_miscellaneous/get_project_names_ids',
    'ZoneController@getProjectNamesIds'
)->name('zones_miscellaneous.get_project_names_ids');
/*
|--------------------------------------------------------------------------
| Blocks
|--------------------------------------------------------------------------
|
*/
Route::resource('blocks', 'BlockController');
Route::post(
    'blocks_miscellaneous/index_search',
    'BlockController@search'
)->name('blocks_miscellaneous.index_search');
Route::get(
    'blocks_miscellaneous/get_project_names_ids',
    'BlockController@getProjectNamesIds'
)->name('blocks_miscellaneous.get_project_names_ids');
/*
|--------------------------------------------------------------------------
| Unit Models
|--------------------------------------------------------------------------
|
*/
Route::resource('unit_models', 'UnitModelController');
Route::post(
    'unit_models_miscellaneous/index_search',
    'UnitModelController@search'
)->name('unit_models_miscellaneous.index_search');
/*
|--------------------------------------------------------------------------
| Units
|--------------------------------------------------------------------------
|
*/
Route::resource('units', 'UnitController');
Route::post(
    'units_miscellaneous/index_search',
    'UnitController@search'
)->name('units_miscellaneous.index_search');
Route::get(
    'units_miscellaneous/get_project_names_ids',
    'UnitController@getProjectNamesIds'
)->name('units_miscellaneous.get_project_names_ids');
Route::get(
    'units_miscellaneous/get_unit_model_names_ids',
    'UnitController@getUnitModelNamesIds'
)->name('units_miscellaneous.get_project_names_ids');
Route::get(
    'units_miscellaneous/show_as_pdf/{unit}',
    'UnitController@showAsPdf'
)->name('units_miscellaneous.show_as_pdf');
/*
|--------------------------------------------------------------------------
| Customers
|--------------------------------------------------------------------------
|
*/
Route::resource('customers', 'CustomerController');
Route::post(
    'customers_miscellaneous/index_search',
    'CustomerController@search'
)->name('customers_miscellaneous.index_search');

/*
|--------------------------------------------------------------------------
| Reservations
|--------------------------------------------------------------------------
|
*/
Route::resource('reservations', 'ReservationController');
Route::post(
    'reservations_miscellaneous/index_search',
    'ReservationController@search'
)->name('reservations_miscellaneous.index_search');
Route::get(
    'reservations_miscellaneous/get_unit_codes_ids',
    'ReservationController@getUnitCodesIds'
)->name('reservations_miscellaneous.get_unit_codes_ids');
Route::get(
    'reservations_miscellaneous/get_customer_names_ids',
    'ReservationController@getCustomerNamesIds'
)->name('reservations_miscellaneous.get_customer_names_ids');
Route::get(
    'reservations_miscellaneous/get_banks_names_ids',
    'ReservationController@getBanksNamesIds'
)->name('reservations_miscellaneous.get_banks_names_ids');

/*
|--------------------------------------------------------------------------
| Project User Assignment
|--------------------------------------------------------------------------
|
*/
Route::resource('project_user_assignments', 'ProjectUserAssignmentController');
Route::post(
    'project_user_assignments_miscellaneous/index_search',
    'ProjectUserAssignmentController@search'
)->name('project_user_assignments_miscellaneous.index_search');
Route::get(
    'project_user_assignments_miscellaneous/get_project_names_ids',
    'ProjectUserAssignmentController@getProjectNamesIds'
)->name('project_user_assignments_miscellaneous.get_project_names_ids');
Route::get(
    'project_user_assignments_miscellaneous/get_employee_names_ids',
    'ProjectUserAssignmentController@getEmployeeNamesIds'
)->name('project_user_assignments_miscellaneous.get_employee_names_ids');

/*
|--------------------------------------------------------------------------
| Dashboard
|--------------------------------------------------------------------------
|
*/
Route::get(
    'dashboard',
    'Dashboard\DashboardController@specifyUserDashboard'
)->name('dashboard');

/*
|--------------------------------------------------------------------------
| Developer Dashboard
|--------------------------------------------------------------------------
|
*/
Route::get('developer_dashboard/home', 'Dashboard\DeveloperDashboardController@home')
    ->name('developer_dashboard.home');
/*
|--------------------------------------------------------------------------
| Salesman Dashboard
|--------------------------------------------------------------------------
|
*/
Route::get('salesman_dashboard/home', 'Dashboard\SalesmanDashboardController@home')
    ->name('salesman_dashboard.home');
Route::get('salesman_dashboard/get_selected_project', 'Dashboard\SalesmanDashboardController@getSelectedProject')
    ->name('salesman_dashboard.get_selected_project');
Route::get('salesman_dashboard/get_unit_related_catalogs_links', 'Dashboard\SalesmanDashboardController@getUnitRelatedCatalogsLinks')
    ->name('salesman_dashboard.get_unit_related_catalogs_links');
Route::get('salesman_dashboard/get_units_areas_as_html_tags', 'Dashboard\SalesmanDashboardController@getUnitsAreasAsHtmlTags')
    ->name('salesman_dashboard.get_units_areas_as_html_tags');

/*
|--------------------------------------------------------------------------
| Net Suite Integrations
|--------------------------------------------------------------------------
|
*/
Route::resource('net_suite_integrations', 'NetSuiteIntegrationController');
Route::post(
    'net_suite_integrations_miscellaneous/index_search',
    'NetSuiteIntegrationController@search'
)->name('net_suite_integrations_miscellaneous.index_search');
Route::get('net_suite_integrations_miscellaneous/sync_projects_data/{net_suite_integration}', 'NetSuiteIntegrationController@syncProjectsData')->name('net_suite_integrations_miscellaneous.sync_projects_data');
Route::get('net_suite_integrations_miscellaneous/sync_customers_data/{net_suite_integration}', 'NetSuiteIntegrationController@syncCustomersData')->name('net_suite_integrations_miscellaneous.sync_customers_data');
Route::get('net_suite_integrations_miscellaneous/sync_phases_data/{net_suite_integration}', 'NetSuiteIntegrationController@syncPhasesData')->name('net_suite_integrations_miscellaneous.sync_phases_data');
Route::get('net_suite_integrations_miscellaneous/sync_zones_data/{net_suite_integration}', 'NetSuiteIntegrationController@syncZonesData')->name('net_suite_integrations_miscellaneous.sync_zones_data');
Route::get('net_suite_integrations_miscellaneous/sync_blocks_data/{net_suite_integration}', 'NetSuiteIntegrationController@syncBlocksData')->name('net_suite_integrations_miscellaneous.sync_blocks_data');
Route::get('net_suite_integrations_miscellaneous/sync_unit_models_data/{net_suite_integration}', 'NetSuiteIntegrationController@syncUnitModelsData')->name('net_suite_integrations_miscellaneous.sync_unit_models_data');
Route::get('net_suite_integrations_miscellaneous/sync_units_data/{net_suite_integration}', 'NetSuiteIntegrationController@syncUnitsData')->name('net_suite_integrations_miscellaneous.sync_units_data');
Route::get('net_suite_integrations_miscellaneous/sync_reservations_data/{net_suite_integration}', 'NetSuiteIntegrationController@syncReservationsData')->name('net_suite_integrations_miscellaneous.sync_reservations_data');
Route::get('net_suite_integrations_miscellaneous/sync_banks_data/{net_suite_integration}', 'NetSuiteIntegrationController@syncBanksData')->name('net_suite_integrations_miscellaneous.sync_banks_data');
