<?php

namespace App\Providers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->composeNavigationPrivileges();
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
    }

    private function composeNavigationPrivileges()
    {
        $v = [
            'layouts.nav.left._user_system',
            'layouts.nav.left._project_files',
            'layouts.nav.left._catalogs',
            'layouts.nav.left._projects',
            'layouts.nav.left._phases',
            'layouts.nav.left._zones',
            'layouts.nav.left._unit_models',
            'layouts.nav.left._units',
            'layouts.nav.left._customers',
            'layouts.nav.left._reservations',
            'layouts.nav.left._project_user_assignments',
            'layouts.nav.left._blocks',
            'layouts.nav.left._net_suite_integrations',

            'permissions.show',

            'roles.show',
            'roles.create',
            'roles.edit',

            'users.show',
            'users._form',

            'role_user._form',

            'project_files.show',

            'catalogs.show',
            'catalogs.show_reservation_catalog',

            'projects.show',

            'phases.show',

            'zones.show',

            'unit_models.show',

            'units.show.show',

            'customers.show',

            'reservations.show',

            'project_user_assignments.show',

            'blocks.show',

            'net_suite_integrations.show',

        ];
        view()->composer($v, function ($view) {

            $permissions = [];

            $aUser = Auth::user();
            if (isset($aUser)) {
                $userRoles = $aUser->roles;
                if ($userRoles->isNotEmpty()) {
                    foreach ($userRoles as $key => $role) {
                        $permissions = array_merge($permissions, $role->permissions()->pluck('name')->toArray());
                    }
                }
            }
            $view->with('permissions', $permissions);
        });
    }
}
