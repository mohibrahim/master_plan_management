<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use App\MasterPlanManagement\Services\FileUploader\FileUploader;
use App\MasterPlanManagement\Services\IndexTenancy\IndexByTenancy;
use App\MasterPlanManagement\Services\FileUploader\FileUploaderInterface;
use App\MasterPlanManagement\Services\FileUploader\FileUploaderLaravelWay;
use App\MasterPlanManagement\Services\IndexTenancy\IndexStrategyInterface;
use App\MasterPlanManagement\Services\Models\Unit\CreateUnitStrategy\CreateUnit;
use App\MasterPlanManagement\Services\Models\Unit\DeleteUnitStrategy\DeleteUnit;
use App\MasterPlanManagement\Services\Models\Unit\FilterUnits\FilterByFourTerms;
use App\MasterPlanManagement\Services\Models\Unit\UpdateUnitStrategy\UpdateUnit;
use App\MasterPlanManagement\Services\Models\User\CreateUserStrategy\CreateUser;
use App\MasterPlanManagement\Services\Models\User\UpdateUserStrategy\UpdateUser;
use App\MasterPlanManagement\Services\Models\Zone\CreateZoneStrategy\CreateZone;
use App\MasterPlanManagement\Services\Models\Zone\DeleteZoneStrategy\DeleteZone;
use App\MasterPlanManagement\Services\Models\Zone\UpdateZoneStrategy\UpdateZone;
use App\MasterPlanManagement\Services\Models\Unit\FilterUnits\FilterUnitInterface;
use App\MasterPlanManagement\Services\Models\Block\CreateBlockStrategy\CreateBlock;
use App\MasterPlanManagement\Services\Models\Block\DeleteBlockStrategy\DeleteBlock;
use App\MasterPlanManagement\Services\Models\Block\UpdateBlockStrategy\UpdateBlock;
use App\MasterPlanManagement\Services\Models\Phase\CreatePhaseStrategy\CreatePhase;
use App\MasterPlanManagement\Services\Models\Phase\DeletePhaseStrategy\DeletePhase;
use App\MasterPlanManagement\Services\Models\Phase\UpdatePhaseStrategy\UpdatePhase;
use App\MasterPlanManagement\Services\WorldCountries\ArabicEnglishCountriesResolver;
use App\MasterPlanManagement\Services\FlashMessageLanguage\FlashMessageArabicEnglish;
use App\MasterPlanManagement\Services\Integrations\Pull\CommunicationStrategy\Guzzle;
use App\MasterPlanManagement\Services\Models\User\UpdateUserStrategy\UpdateProjectFile;
use App\MasterPlanManagement\Services\WorldCountries\LanguageCountriesResolverInterface;
use App\MasterPlanManagement\Services\Models\Catalog\CreateCatalogStrategy\CreateCatalog;
use App\MasterPlanManagement\Services\Models\Catalog\DeleteCatalogStrategy\DeleteCatalog;
use App\MasterPlanManagement\Services\Models\Catalog\UpdateCatalogStrategy\UpdateCatalog;
use App\MasterPlanManagement\Services\Models\Project\DeleteProjectStrategy\DeleteProject;
use App\MasterPlanManagement\Services\Models\Project\UpdateProjectStrategy\UpdateProject;
use App\MasterPlanManagement\Services\Models\Unit\CreateUnitStrategy\CreateUnitInterface;
use App\MasterPlanManagement\Services\Models\Unit\DeleteUnitStrategy\DeleteUnitInterface;
use App\MasterPlanManagement\Services\Models\Unit\UpdateUnitStrategy\UpdateUnitInterface;
use App\MasterPlanManagement\Services\Models\User\CreateUserStrategy\CreateUserInterface;
use App\MasterPlanManagement\Services\Models\User\UpdateUserStrategy\UpdateUserInterface;
use App\MasterPlanManagement\Services\Models\Zone\CreateZoneStrategy\CreateZoneInterface;
use App\MasterPlanManagement\Services\Models\Zone\DeleteZoneStrategy\DeleteZoneInterface;
use App\MasterPlanManagement\Services\Models\Zone\UpdateZoneStrategy\UpdateZoneInterface;
use App\MasterPlanManagement\Services\Models\Catalog\HiddenInputCreator\HiddenInputCreator;
use App\MasterPlanManagement\Services\Models\Block\CreateBlockStrategy\CreateBlockInterface;
use App\MasterPlanManagement\Services\Models\Block\DeleteBlockStrategy\DeleteBlockInterface;
use App\MasterPlanManagement\Services\Models\Block\UpdateBlockStrategy\UpdateBlockInterface;
use App\MasterPlanManagement\Services\Models\Customer\CreateCustomerStrategy\CreateCustomer;
use App\MasterPlanManagement\Services\Models\Customer\DeleteCustomerStrategy\DeleteCustomer;
use App\MasterPlanManagement\Services\Models\Customer\UpdateCustomerStrategy\UpdateCustomer;
use App\MasterPlanManagement\Services\Models\Phase\CreatePhaseStrategy\CreatePhaseInterface;
use App\MasterPlanManagement\Services\Models\Phase\DeletePhaseStrategy\DeletePhaseInterface;
use App\MasterPlanManagement\Services\Models\Phase\UpdatePhaseStrategy\UpdatePhaseInterface;
use App\MasterPlanManagement\Services\FlashMessageLanguage\FlashMessageArabicEnglishInterface;
use App\MasterPlanManagement\Services\Models\UnitModel\CreateUnitModelStrategy\CreateUnitModel;
use App\MasterPlanManagement\Services\Models\UnitModel\DeleteUnitModelStrategy\DeleteUnitModel;
use App\MasterPlanManagement\Services\Models\UnitModel\UpdateUnitModelStrategy\UpdateUnitModel;
use App\MasterPlanManagement\Services\Models\User\UpdateUserStrategy\UpdateProjectFileInterface;
use App\MasterPlanManagement\Services\Models\Catalog\CreateCatalogStrategy\CreateCatalogInterface;
use App\MasterPlanManagement\Services\Models\Catalog\DeleteCatalogStrategy\DeleteCatalogInterface;
use App\MasterPlanManagement\Services\Models\Catalog\UpdateCatalogStrategy\UpdateCatalogInterface;
use App\MasterPlanManagement\Services\Models\Project\CreateProjectStrategy\CreateProjectInterface;
use App\MasterPlanManagement\Services\Models\Project\DeleteProjectStrategy\DeleteProjectInterface;
use App\MasterPlanManagement\Services\Models\Project\UpdateProjectStrategy\UpdateProjectInterface;
use App\MasterPlanManagement\Services\Integrations\Push\CommunicationStrategy\PostToClientInterface;
use App\MasterPlanManagement\Services\Models\Catalog\HiddenInputCreator\HiddenInputCreatorInterface;
use App\MasterPlanManagement\Services\Integrations\Pull\CommunicationStrategy\ClientRequestInterface;
use App\MasterPlanManagement\Services\Models\Customer\CreateCustomerStrategy\CreateCustomerInterface;
use App\MasterPlanManagement\Services\Models\Customer\DeleteCustomerStrategy\DeleteCustomerInterface;
use App\MasterPlanManagement\Services\Models\Customer\UpdateCustomerStrategy\UpdateCustomerInterface;
use App\MasterPlanManagement\Services\Models\ProjectFile\CreateProjectFileStrategy\CreateProjectFile;
use App\MasterPlanManagement\Services\Models\Reservation\CreateReservationStrategy\CreateReservation;
use App\MasterPlanManagement\Services\Models\Reservation\DeleteReservationStrategy\DeleteReservation;
use App\MasterPlanManagement\Services\Models\Reservation\UpdateReservationStrategy\UpdateReservation;
use App\MasterPlanManagement\Services\Models\Project\CreateProjectStrategy\CreateProjectWithUnitsJson;
use App\MasterPlanManagement\Services\Integrations\Push\DataSync\Customer\Create\CreateNetSuiteCustomer;
use App\MasterPlanManagement\Services\Integrations\Push\DataSync\Customer\Delete\DeleteNetSuiteCustomer;
use App\MasterPlanManagement\Services\Integrations\Push\DataSync\Customer\Update\UpdateNetSuiteCustomer;
use App\MasterPlanManagement\Services\Models\UnitModel\CreateUnitModelStrategy\CreateUnitModelInterface;
use App\MasterPlanManagement\Services\Models\UnitModel\DeleteUnitModelStrategy\DeleteUnitModelInterface;
use App\MasterPlanManagement\Services\Models\UnitModel\UpdateUnitModelStrategy\UpdateUnitModelInterface;
use App\MasterPlanManagement\Services\Integrations\Pull\ThirdPartyObjects\MappingStrategy\Listing\GoogleGson;
use App\MasterPlanManagement\Services\Integrations\Push\DataSync\Reservation\Create\CreateNetSuiteReservation;
use App\MasterPlanManagement\Services\Integrations\Push\DataSync\Reservation\Delete\DeleteNetSuiteReservation;
use App\MasterPlanManagement\Services\Integrations\Push\DataSync\Reservation\Update\UpdateNetSuiteReservation;
use App\MasterPlanManagement\Services\Models\ProjectFile\CreateProjectFileStrategy\CreateProjectFileInterface;
use App\MasterPlanManagement\Services\Models\Reservation\CreateReservationStrategy\CreateReservationInterface;
use App\MasterPlanManagement\Services\Models\Reservation\DeleteReservationStrategy\DeleteReservationInterface;
use App\MasterPlanManagement\Services\Models\Reservation\UpdateReservationStrategy\UpdateReservationInterface;
use App\MasterPlanManagement\Services\Integrations\Pull\ThirdPartyObjects\MappingStrategy\Listing\MappingInterface;
use App\MasterPlanManagement\Services\Integrations\Push\DataSync\Customer\Create\CreateCustomerThirdPartyInterface;
use App\MasterPlanManagement\Services\Integrations\Push\DataSync\Customer\Delete\DeleteCustomerThirdPartyInterface;
use App\MasterPlanManagement\Services\Integrations\Push\DataSync\Customer\Update\UpdateCustomerThirdPartyInterface;
use App\MasterPlanManagement\Services\Integrations\Push\DataSync\Reservation\Create\CreateReservationThirdPartyInterface;
use App\MasterPlanManagement\Services\Integrations\Push\DataSync\Reservation\Delete\DeleteReservationThirdPartyInterface;
use App\MasterPlanManagement\Services\Integrations\Push\DataSync\Reservation\Update\UpdateReservationThirdPartyInterface;
use App\MasterPlanManagement\Services\Models\NetSuiteIntegration\CreateNetSuiteIntegrationStrategy\CreateNetSuiteIntegration;
use App\MasterPlanManagement\Services\Models\NetSuiteIntegration\UpdateNetSuiteIntegrationStrategy\UpdateNetSuiteIntegration;
use App\MasterPlanManagement\Services\Models\ProjectUserAssignment\CreateProjectUserAssignmentStrategy\CreateProjectUserAssignment;
use App\MasterPlanManagement\Services\Models\ProjectUserAssignment\UpdateProjectUserAssignmentStrategy\UpdateProjectUserAssignment;
use App\MasterPlanManagement\Services\Models\NetSuiteIntegration\CreateNetSuiteIntegrationStrategy\CreateNetSuiteIntegrationInterface;
use App\MasterPlanManagement\Services\Models\NetSuiteIntegration\UpdateNetSuiteIntegrationStrategy\UpdateNetSuiteIntegrationInterface;
use App\MasterPlanManagement\Services\Models\ProjectUserAssignment\CreateProjectUserAssignmentStrategy\CreateProjectUserAssignmentInterface;
use App\MasterPlanManagement\Services\Models\ProjectUserAssignment\UpdateProjectUserAssignmentStrategy\UpdateProjectUserAssignmentInterface;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Schema::defaultStringLength(191);

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        app()->bind(IndexStrategyInterface::class, IndexByTenancy::class);
        app()->bind(LanguageCountriesResolverInterface::class, ArabicEnglishCountriesResolver::class);
        app()->bind(FlashMessageArabicEnglishInterface::class, FlashMessageArabicEnglish::class);
        app()->bind(CreateUserInterface::class, CreateUser::class);
        app()->bind(UpdateUserInterface::class, UpdateUser::class);
        app()->bind(CreateProjectFileInterface::class, CreateProjectFile::class);
        app()->bind(UpdateProjectFileInterface::class, UpdateProjectFile::class);
        // app()->bind(FileUploaderInterface::class, FileUploader::class);
        app()->bind(FileUploaderInterface::class, FileUploaderLaravelWay::class);
        app()->bind(CreateCatalogInterface::class, CreateCatalog::class);
        app()->bind(UpdateCatalogInterface::class, UpdateCatalog::class);
        // app()->bind(CreateProjectInterface::class, CreateProject::class);
        app()->bind(CreateProjectInterface::class, CreateProjectWithUnitsJson::class);
        app()->bind(UpdateProjectInterface::class, UpdateProject::class);
        app()->bind(DeleteProjectInterface::class, DeleteProject::class);
        app()->bind(CreatePhaseInterface::class, CreatePhase::class);
        app()->bind(UpdatePhaseInterface::class, UpdatePhase::class);
        app()->bind(DeletePhaseInterface::class, DeletePhase::class);
        app()->bind(CreateZoneInterface::class, CreateZone::class);
        app()->bind(UpdateZoneInterface::class, UpdateZone::class);
        app()->bind(DeleteZoneInterface::class, DeleteZone::class);
        app()->bind(CreateUnitModelInterface::class, CreateUnitModel::class);
        app()->bind(UpdateUnitModelInterface::class, UpdateUnitModel::class);
        app()->bind(DeleteUnitModelInterface::class, DeleteUnitModel::class);
        app()->bind(CreateUnitInterface::class, CreateUnit::class);
        app()->bind(UpdateUnitInterface::class, UpdateUnit::class);
        app()->bind(DeleteUnitInterface::class, DeleteUnit::class);
        app()->bind(CreateCustomerInterface::class, CreateCustomer::class);
        app()->bind(UpdateCustomerInterface::class, UpdateCustomer::class);
        app()->bind(CreateReservationInterface::class, CreateReservation::class);
        app()->bind(UpdateReservationInterface::class, UpdateReservation::class);
        app()->bind(HiddenInputCreatorInterface::class, HiddenInputCreator::class);
        app()->bind(DeleteCatalogInterface::class, DeleteCatalog::class);
        app()->bind(DeleteReservationInterface::class, DeleteReservation::class);
        app()->bind(DeleteCustomerInterface::class, DeleteCustomer::class);
        app()->bind(CreateProjectUserAssignmentInterface::class, CreateProjectUserAssignment::class);
        app()->bind(UpdateProjectUserAssignmentInterface::class, UpdateProjectUserAssignment::class);
        app()->bind(CreateBlockInterface::class, CreateBlock::class);
        app()->bind(UpdateBlockInterface::class, UpdateBlock::class);
        app()->bind(DeleteBlockInterface::class, DeleteBlock::class);
        app()->bind(CreateNetSuiteIntegrationInterface::class, CreateNetSuiteIntegration::class);
        app()->bind(UpdateNetSuiteIntegrationInterface::class, UpdateNetSuiteIntegration::class);
        app()->bind(FilterUnitInterface::class, FilterByFourTerms::class);
        app()->bind(ClientRequestInterface::class, Guzzle::class);
        app()->bind(MappingInterface::class, GoogleGson::class);
        //for this package tebru/gson-php to make annotations works
        \Doctrine\Common\Annotations\AnnotationRegistry::registerLoader('class_exists');
        app()->bind(CreateReservationThirdPartyInterface::class, CreateNetSuiteReservation::class);
        app()->bind(DeleteReservationThirdPartyInterface::class, DeleteNetSuiteReservation::class);
        app()->bind(UpdateReservationThirdPartyInterface::class, UpdateNetSuiteReservation::class);
        app()->bind(CreateCustomerThirdPartyInterface::class, CreateNetSuiteCustomer::class);
        app()->bind(DeleteCustomerThirdPartyInterface::class, DeleteNetSuiteCustomer::class);
        app()->bind(UpdateCustomerThirdPartyInterface::class, UpdateNetSuiteCustomer::class);
    }
}
