<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IntegrationPushingQueue extends Model
{
    public $table = 'integration_pushing_queues';
    protected $fillable = ['model_type', 'operation_type', 'model_id', 'third_party_object', 'integration_type',];
}
