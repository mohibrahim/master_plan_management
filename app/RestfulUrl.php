<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Weak Entity
 * @author Mohammed Ibrahim <mohibrahimqop@gmail.com>
 */
class RestfulUrl extends Model
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
    protected $table = 'restful_urls';

    protected $fillable = [
        'api_authorization',
        'project_api_index',
        'project_sync_date_time',
        'phase_api_index',
        'phase_sync_date_time',
        'zone_api_index',
        'zone_sync_date_time',
        'block_api_index',
        'block_sync_date_time',
        'unit_model_api_index',
        'unit_model_sync_date_time',
        'unit_api_index',
        'unit_sync_date_time',
        'customer_api_index',
        'customer_sync_date_time',
        'reservation_api_index',
        'reservation_sync_date_time',
        'notes',
        'net_suite_integration_id',

        'project_api_store',
        'project_api_update',
        'project_api_destroy',
        'project_api_show',
        'phase_api_store',
        'phase_api_update',
        'phase_api_destroy',
        'phase_api_show',
        'zone_api_store',
        'zone_api_update',
        'zone_api_destroy',
        'zone_api_show',
        'block_api_store',
        'block_api_update',
        'block_api_destroy',
        'block_api_show',
        'unit_model_api_store',
        'unit_model_api_update',
        'unit_model_api_destroy',
        'unit_model_api_show',
        'unit_api_store',
        'unit_api_update',
        'unit_api_destroy',
        'unit_api_show',
        'customer_api_store',
        'customer_api_update',
        'customer_api_destroy',
        'customer_api_show',
        'reservation_api_store',
        'reservation_api_update',
        'reservation_api_destroy',
        'reservation_api_show',
        'bank_api_store',
        'bank_api_update',
        'bank_api_destroy',
        'bank_api_show',
        'bank_api_index',
        'bank_sync_date_time',
    ];

    /*
    |--------------------------------------------------------------------------
    | Accessors
    |--------------------------------------------------------------------------
    */


    /*
    |--------------------------------------------------------------------------
    | Mutators
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */

    public function netSuiteIntegration(): BelongsTo
    {
        return $this->belongsTo(NetSuiteIntegration::class, 'net_suite_integration_id', 'id');
    }




    /*
    |--------------------------------------------------------------------------
    | Miscellaneous
    |--------------------------------------------------------------------------
    */
}
