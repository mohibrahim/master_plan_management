<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Role extends Model
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    */
    public $table = 'roles';
    protected $fillable = ['name', 'descriptions', 'creator_id', 'last_updater_id', 'net_suite_internal_id',];

    /*
    |--------------------------------------------------------------------------
    | Accessors
    |--------------------------------------------------------------------------
    */
    public function getPermissionListAttribute()
    {
        return $this->permissions->pluck('id');
    }
    /*
    |--------------------------------------------------------------------------
    | Mutators
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */


    /**
     * Getting the user who create this role.
     *
     * @return BelongsTo
     */
    public function creator() : BelongsTo
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    /**
     * Getting the last user who updated this role.
     *
     * @return BelongsTo
     */
    public function lastUpdater() : BelongsTo
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    /**
     * Getting the permissions that are associated with this role.
     *
     * @return BelongsToMany
     */
    public function permissions() : BelongsToMany
    {
        return $this->belongsToMany(Permission::class)->withTimestamps();
    }

    /**
     * Get users associated with this role.
     *
     * @param
     * @return \App\User
     */
    public function users()
    {
        return $this->belongsToMany('App\User')->withTimestamps();
    }

    public function usersOfMasterRole()
    {
        return $this->hasMany(User::class, 'master_role_id', 'id');
    }

    /*
    |--------------------------------------------------------------------------
    | Miscellaneous
    |--------------------------------------------------------------------------
    */

}
