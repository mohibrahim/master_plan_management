<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class NetSuiteIntegration extends Model
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
    protected $table = 'net_suite_integrations';

    protected $fillable = [
        'name',
        'notes',
        'creator_id',
        'last_updater_id',
    ];

    protected $appends = [
        'net_suite_integration_show_link_id',
        'net_suite_integration_show_link_name',
    ];

    /*
    |--------------------------------------------------------------------------
    | Accessors
    |--------------------------------------------------------------------------
    */

    public function getNetSuiteIntegrationShowLinkIdAttribute()
    {
        return '<a href="' . action('NetSuiteIntegrationController@show', ['id' => $this->id]) . '" target="">' . $this->id . '</a>';
    }

    public function getNetSuiteIntegrationShowLinkNameAttribute()
    {
        return '<a href="' . action('NetSuiteIntegrationController@show', ['id' => $this->id]) . '" target="">' . $this->name . '</a>';
    }

    public function getApiAuthorizationAttribute()
    {
        return optional($this->restfulUrl)->api_authorization;
    }

    public function getProjectApiIndexAttribute()
    {
        return optional($this->restfulUrl)->project_api_index;
    }

    public function getProjectSyncDateTimeAttribute()
    {
        return optional($this->restfulUrl)->project_sync_date_time;
    }

    public function getPhaseApiIndexAttribute()
    {
        return optional($this->restfulUrl)->phase_api_index;
    }

    public function getPhaseSyncDateTimeAttribute()
    {
        return optional($this->restfulUrl)->phase_sync_date_time;
    }

    public function getZoneApiIndexAttribute()
    {
        return optional($this->restfulUrl)->zone_api_index;
    }

    public function getZoneSyncDateTimeAttribute()
    {
        return optional($this->restfulUrl)->zone_sync_date_time;
    }

    public function getBlockApiIndexAttribute()
    {
        return optional($this->restfulUrl)->block_api_index;
    }

    public function getBlockSyncDateTimeAttribute()
    {
        return optional($this->restfulUrl)->block_sync_date_time;
    }

    public function getUnitModelApiIndexAttribute()
    {
        return optional($this->restfulUrl)->unit_model_api_index;
    }

    public function getUnitModelSyncDateTimeAttribute()
    {
        return optional($this->restfulUrl)->unit_model_sync_date_time;
    }

    public function getUnitApiIndexAttribute()
    {
        return optional($this->restfulUrl)->unit_api_index;
    }

    public function getUnitSyncDateTimeAttribute()
    {
        return optional($this->restfulUrl)->unit_sync_date_time;
    }

    public function getCustomerApiIndexAttribute()
    {
        return optional($this->restfulUrl)->customer_api_index;
    }

    public function getCustomerApiStoreAttribute()
    {
        return optional($this->restfulUrl)->customer_api_store;
    }

    public function getCustomerApiUpdateAttribute()
    {
        return optional($this->restfulUrl)->customer_api_update;
    }

    public function getCustomerApiDestroyAttribute()
    {
        return optional($this->restfulUrl)->customer_api_destroy;
    }

    public function getCustomerSyncDateTimeAttribute()
    {
        return optional($this->restfulUrl)->customer_sync_date_time;
    }

    public function getBankApiIndexAttribute()
    {
        return optional($this->restfulUrl)->bank_api_index;
    }

    public function getBankSyncDateTimeAttribute()
    {
        return optional($this->restfulUrl)->bank_sync_date_time;
    }

    public function getReservationApiIndexAttribute()
    {
        return optional($this->restfulUrl)->reservation_api_index;
    }

    public function getReservationApiStoreAttribute()
    {
        return optional($this->restfulUrl)->reservation_api_store;
    }

    public function getReservationApiUpdateAttribute()
    {
        return optional($this->restfulUrl)->reservation_api_update;
    }

    public function getReservationApiDestroyAttribute()
    {
        return optional($this->restfulUrl)->reservation_api_destroy;
    }

    public function getReservationSyncDateTimeAttribute()
    {
        return optional($this->restfulUrl)->reservation_sync_date_time;
    }

    /*
    |--------------------------------------------------------------------------
    | Mutators
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */

    /**
     * Getting the creator user who create this Net suite integration.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    /**
     * Getting the last updater user who update this Net suite integration.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function lastUpdater(): BelongsTo
    {
        return $this->belongsTo(User::class, 'last_updater_id', 'id');
    }

    /**
     * Getting restful url that are associated with this net suite integration.
     * Cardinality Constraint: 1:1.
     * Degree of Relationship: Binary
     * @return HasOne
     */
    public function restfulUrl(): HasOne
    {
        return $this->hasOne(RestfulUrl::class, 'net_suite_integration_id', 'id');
    }

    /*
    |--------------------------------------------------------------------------
    | Miscellaneous
    |--------------------------------------------------------------------------
    */

    public function search(Builder $builder, string $keyword)
    {
        return $builder->with('creator')
            ->where('name', 'like', "%$keyword%")
            ->orWhereHas('creator', function ($query) use ($keyword) {
                $query->where('name', 'like', "%$keyword%");
            });
    }
}
