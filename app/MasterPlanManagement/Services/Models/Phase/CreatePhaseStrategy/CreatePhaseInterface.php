<?php

namespace App\MasterPlanManagement\Services\Models\Phase\CreatePhaseStrategy;

use Illuminate\Http\Request;
use App\Phase;

interface CreatePhaseInterface
{
    public function create(Request $request): Phase;
}