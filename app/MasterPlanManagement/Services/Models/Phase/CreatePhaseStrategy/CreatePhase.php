<?php

namespace App\MasterPlanManagement\Services\Models\Phase\CreatePhaseStrategy;

use App\Phase;
use Illuminate\Http\Request;

class CreatePhase implements CreatePhaseInterface
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
  
    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function create(Request $request): Phase
    {
        $attributes = $request->all();
        $attributes['creator_id'] = auth()->id();
        $phase = Phase::create($attributes);
        return $phase;
    }

}