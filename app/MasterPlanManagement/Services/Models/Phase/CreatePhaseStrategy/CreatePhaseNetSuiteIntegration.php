<?php

namespace App\MasterPlanManagement\Services\Models\Phase\CreatePhaseStrategy;

use App\Phase;
use App\Project;
use Illuminate\Http\Request;

class CreatePhaseNetSuiteIntegration implements CreatePhaseInterface
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function create(Request $request): Phase
    {
        $attributes = $request->all();
        $project = Project::where('net_suite_internal_id', $attributes['project_net_suite_internal_id'])->firstOrFail();
        $attributes['project_id'] = $project->id;
        $attributes['creator_id'] = auth()->id();
        $phase = Phase::create($attributes);
        return $phase;
    }

}
