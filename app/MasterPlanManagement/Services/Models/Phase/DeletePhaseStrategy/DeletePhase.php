<?php

namespace App\MasterPlanManagement\Services\Models\Phase\DeletePhaseStrategy;

use App\Phase;
use App\MasterPlanManagement\Services\Models\Unit\DeleteUnitStrategy\DeleteUnitInterface;
use App\MasterPlanManagement\Services\Models\Zone\DeleteZoneStrategy\DeleteZoneInterface;
use App\MasterPlanManagement\Services\Models\Catalog\DeleteCatalogStrategy\DeleteCatalogInterface;

class DeletePhase implements DeletePhaseInterface
{
    public function delete(Phase $phase): bool
    {
        $catalogs = $phase->catalogs;
        if ($catalogs->isNotEmpty()) {
            $deleteCatalog = resolve(DeleteCatalogInterface::class);
            foreach ($catalogs as $catalog) {
                $deleteCatalog->delete($catalog);
            }
        }

        $zones = $phase->zones;
        if ($zones->isNotEmpty()) {
            $deleteZone = resolve(DeleteZoneInterface::class);
            foreach ($zones as $zone) {
                $deleteZone->delete($zone);
            }
        }

        $units = $phase->units;
        if ($units->isNotEmpty()) {
            $deleteUnit = resolve(DeleteUnitInterface::class);
            foreach ($units as $unit) {
                $deleteUnit->delete($unit);
            }
        }

        return $phase->delete();
    }
}
