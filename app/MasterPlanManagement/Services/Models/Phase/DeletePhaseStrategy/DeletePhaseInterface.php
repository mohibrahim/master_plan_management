<?php

namespace App\MasterPlanManagement\Services\Models\Phase\DeletePhaseStrategy;

use App\Phase;

interface DeletePhaseInterface
{
    public function delete(Phase $phase): bool;
}
