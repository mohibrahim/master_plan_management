<?php

namespace App\MasterPlanManagement\Services\Models\Phase\UpdatePhaseStrategy;

use App\Phase;
use App\Project;
use Illuminate\Http\Request;

class UpdatePhaseNetSuiteIntegration implements UpdatePhaseInterface
{

    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */
    public function update($phaseId, Request $request): Phase
    {
        $phase = Phase::findOrFail($phaseId);
        $attributes = $request->all();
        $project = Project::where('net_suite_internal_id', $attributes['project_net_suite_internal_id'])->firstOrFail();
        $attributes['project_id'] = $project->id;
        $attributes['last_updater_id'] = auth()->id();
        $phase->update($attributes);
        return $phase->fresh();
    }

}
