<?php

namespace App\MasterPlanManagement\Services\Models\Phase\UpdatePhaseStrategy;

use Illuminate\Http\Request;
use App\Phase;

interface UpdatePhaseInterface
{
    public function update($phaseId, Request $request): Phase;
}