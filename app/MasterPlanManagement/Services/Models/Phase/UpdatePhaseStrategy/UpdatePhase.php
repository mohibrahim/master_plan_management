<?php

namespace App\MasterPlanManagement\Services\Models\Phase\UpdatePhaseStrategy;

use App\Phase;
use Illuminate\Http\Request;

class UpdatePhase implements UpdatePhaseInterface
{

    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */
    public function update($phaseId, Request $request): Phase
    {
        $phase = Phase::findOrFail($phaseId);
        $attributes = $request->all();
        $attributes['last_updater_id'] = auth()->id();
        $phase->update($attributes);
        return $phase->fresh();
    }

}
