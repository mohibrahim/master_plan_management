<?php

namespace App\MasterPlanManagement\Services\Models\Reservation\CreateReservationStrategy;

use Illuminate\Http\Request;
use App\Reservation;

interface CreateReservationInterface
{
    public function create(Request $request): Reservation;
}