<?php

namespace App\MasterPlanManagement\Services\Models\Reservation\CreateReservationStrategy;

use App\Bank;
use App\Unit;
use App\Customer;
use App\Reservation;
use Illuminate\Http\Request;
use App\MasterPlanManagement\Services\Models\Unit\UnitStatusStrategy\UnitStatusAbstract;
use Exception;

class CreateReservationNetSuiteIntegration implements CreateReservationInterface
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function create(Request $request): Reservation
    {
        //FIXME: if integrated with netsuite please update keys like customer_id and unit_id;
        $attributes = $request->all();
        // dd($attributes);

        $units = Unit::where('net_suite_internal_id', $attributes['unit_net_suite_internal_id'])
        ->get();
        if ($units->count() === 1) {
            $unit = $units->first();
            $attributes['unit_id'] = $unit->id;
        }else{
            return resolve(Reservation::class);
        }

        $customers = Customer::where('net_suite_internal_id', $attributes['customer_net_suite_internal_id'])
            ->get();
        if ($customers->count() === 1) {
            $customer = $customers->first();
            $attributes['customer_id'] = $customer->id;
        }

        $banks = Bank::where('net_suite_internal_id', $attributes['bank_net_suite_internal_id'])
            ->get();
        if ($banks->count() === 1) {
            $bank = $banks->first();
            $attributes['bank_id'] = $bank->id;
        }

        $attributes['creator_id'] = auth()->id();
        $reservation = Reservation::create($attributes);


        //Update unit status
        $booking = $attributes["booking"];
        if (!empty($booking)) {
            $unit->status = UnitStatusAbstract::BOOKED;
        } else {
            $unit->status = UnitStatusAbstract::RESERVED;
        }
        $unit->save();

        return $reservation->fresh();
    }

}
