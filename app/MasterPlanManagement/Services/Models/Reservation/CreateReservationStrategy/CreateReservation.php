<?php

namespace App\MasterPlanManagement\Services\Models\Reservation\CreateReservationStrategy;

use App\Unit;
use App\Reservation;
use Illuminate\Http\Request;
use App\MasterPlanManagement\Services\Models\Unit\UnitStatusStrategy\UnitStatusAbstract;


class CreateReservation implements CreateReservationInterface
{

    public function create(Request $request): Reservation
    {
        $attributes = $request->all();
        $attributes['creator_id'] = auth()->id();
        $attributes['sales_representative_id'] = auth()->id();

        //Create Reservation
        $reservation = Reservation::create($attributes);

        //Update unit status
        $unitId = $request->unit_id;
        $unit = Unit::findOrFail($unitId);
        $booking = $request->input("booking");
        if (!empty($booking)) {
            $unit->status = UnitStatusAbstract::BOOKED;
        } else {
            $unit->status = UnitStatusAbstract::RESERVED;
        }
        $unit->save();

        return $reservation;
    }
}
