<?php

namespace App\MasterPlanManagement\Services\Models\Reservation\UpdateReservationStrategy;

use App\Unit;
use App\Reservation;
use Illuminate\Http\Request;
use App\MasterPlanManagement\Services\Models\Unit\UnitStatusStrategy\UnitStatusAbstract;

class UpdateReservation implements UpdateReservationInterface
{

    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */
    public function update($reservationId, Request $request): Reservation
    {
        $reservation = Reservation::findOrFail($reservationId);
        $attributes = $request->all();
        $attributes['last_updater_id'] = auth()->id();
        $reservation->update($attributes);

        //update unit status
        $unitId = $request->unit_id;
        $unit = Unit::findOrFail($unitId);
        $booking = $request->input("booking");
        if (!empty($booking)) {
            $unit->status = UnitStatusAbstract::BOOKED;
        } else {
            $unit->status = UnitStatusAbstract::RESERVED;
        }
        $unit->save();

        return $reservation->fresh();
    }
}
