<?php

namespace App\MasterPlanManagement\Services\Models\Reservation\UpdateReservationStrategy;

use Illuminate\Http\Request;
use App\Reservation;

interface UpdateReservationInterface
{
    public function update($reservationId, Request $request): Reservation;
}