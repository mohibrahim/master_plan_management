<?php

namespace App\MasterPlanManagement\Services\Models\Reservation\UpdateReservationStrategy;

use App\Bank;
use App\Customer;
use App\Unit;
use App\Reservation;
use Illuminate\Http\Request;

class UpdateReservationNetSuiteIntegration implements UpdateReservationInterface
{

    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */
    public function update($reservationId, Request $request): Reservation
    {
        $reservation = Reservation::findOrFail($reservationId);
        $attributes = $request->all();

        $units = Unit::where('net_suite_internal_id', $attributes['unit_net_suite_internal_id'])
            ->get();
        if ($units->count() === 1) {
            $unit = $units->first();
            $attributes['unit_id'] = $unit->id;
        }else{
            return resolve(Reservation::class);
        }

        $customers = Customer::where('net_suite_internal_id', $attributes['customer_net_suite_internal_id'])
            ->get();
        if ($customers->count() === 1) {
            $customer = $customers->first();
            $attributes['customer_id'] = $customer->id;
        }

        $banks = Bank::where('net_suite_internal_id', $attributes['bank_net_suite_internal_id'])
            ->get();
        if ($banks->count() === 1) {
            $bank = $banks->first();
            $attributes['bank_id'] = $bank->id;
        }

        $attributes['last_updater_id'] = auth()->id();
        $attributes['net_suite_updated_at'] = now();
        $reservation->update($attributes);
        return $reservation->fresh();
    }

}
