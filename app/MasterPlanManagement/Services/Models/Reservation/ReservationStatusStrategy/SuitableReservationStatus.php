<?php

namespace App\MasterPlanManagement\Services\Models\Reservation\ReservationStatusStrategy;

use App\MasterPlanManagement\Services\LanguageLocator\LanguageLocatorTrait;


class SuitableReservationStatus
{
    use LanguageLocatorTrait;

    private $reservationStatusArabic;
    private $reservationStatusEnglish;


    public function __construct(ReservationStatusArabic $reservationStatusArabic, ReservationStatusEnglish $reservationStatusEnglish)
    {
        $this->reservationStatusArabic = $reservationStatusArabic;
        $this->reservationStatusEnglish = $reservationStatusEnglish;
    }

    public  function getList(): array
    {
        $list = [];
        if ($this->isArabic()) {
            $list = $this->reservationStatusArabic->list();
        } elseif ($this->isEnglish()) {
            $list = $this->reservationStatusEnglish->list();
        }
        return $list;
    }

    public function selectedValue(?string $key): string
    {
        $value = '';
        if ($this->isArabic()) {
            $list = $this->reservationStatusArabic->list();
            if (empty($list[$key])) {
                return $value;
            }
            $value = $list[$key];
        } elseif ($this->isEnglish()) {
            $list = $this->reservationStatusEnglish->list();
            if (empty($list[$key])) {
                return $value;
            }
            $value = $list[$key];
        }
        return $value;
    }
}
