<?php

namespace App\MasterPlanManagement\Services\Models\Reservation\ReservationStatusStrategy;


class ReservationStatusArabic extends ReservationStatusAbstract
{
    public static function list(): array
    {
        return [
            self::NEW_ => 'جديد',
            self::CLOSED => 'أغلاق',
            self::CANCELED => 'ألغي',
        ];
    }
}
