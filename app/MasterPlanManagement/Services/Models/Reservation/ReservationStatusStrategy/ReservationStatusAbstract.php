<?php

namespace App\MasterPlanManagement\Services\Models\Reservation\ReservationStatusStrategy;


abstract class ReservationStatusAbstract
{
    const NEW_ = 'new_';
    const CLOSED = 'closed';
    const CANCELED = 'canceled';

    abstract static function list(): array;
}
