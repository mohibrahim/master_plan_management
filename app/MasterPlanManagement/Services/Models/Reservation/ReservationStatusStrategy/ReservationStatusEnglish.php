<?php

namespace App\MasterPlanManagement\Services\Models\Reservation\ReservationStatusStrategy;


class ReservationStatusEnglish extends ReservationStatusAbstract
{

    public static function list(): array
    {
        return [
            self::NEW_ => 'New',
            self::CLOSED => 'Closed',
            self::CANCELED => 'Canceled',
        ];
    }
}
