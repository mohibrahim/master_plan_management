<?php

namespace App\MasterPlanManagement\Services\Models\Reservation\DeleteReservationStrategy;

use App\Reservation;

interface DeleteReservationInterface
{
    public function delete(Reservation $reservation): bool;
}