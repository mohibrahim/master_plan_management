<?php

namespace App\MasterPlanManagement\Services\Models\Reservation\DeleteReservationStrategy;

use App\Reservation;
use App\MasterPlanManagement\Services\Models\Unit\UnitStatusStrategy\UnitStatusAbstract;
use App\MasterPlanManagement\Services\Models\Catalog\DeleteCatalogStrategy\DeleteCatalogInterface;

class DeleteReservationNetSuiteIntegration implements DeleteReservationInterface
{
    public function delete(Reservation $reservation): bool
    {
        //delete Catalogs
        $catalogs = $reservation->catalogs;
        if ($catalogs->isNotEmpty()) {
            $deleteCatalog = resolve(DeleteCatalogInterface::class);
            foreach ($catalogs as $catalog) {
                $deleteCatalog->delete($catalog);
            }
        }

        $unit = $reservation->unit;
        if (!empty($unit)) {
            $unit->status = UnitStatusAbstract::AVAILABLE;
            $unit->save();
        }
        return $reservation->delete();
    }
}
