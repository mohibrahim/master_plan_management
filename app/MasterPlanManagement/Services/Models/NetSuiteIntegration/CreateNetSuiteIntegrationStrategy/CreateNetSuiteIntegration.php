<?php

namespace App\MasterPlanManagement\Services\Models\NetSuiteIntegration\CreateNetSuiteIntegrationStrategy;

use App\NetSuiteIntegration;
use Illuminate\Http\Request;

class CreateNetSuiteIntegration implements CreateNetSuiteIntegrationInterface
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function create(Request $request): NetSuiteIntegration
    {
        $attributes = $request->all();
        $attributes['creator_id'] = auth()->id();
        $netSuiteIntegration = NetSuiteIntegration::create($attributes);
        $netSuiteIntegration->restfulUrl()->create($attributes);
        return $netSuiteIntegration;
    }
}
