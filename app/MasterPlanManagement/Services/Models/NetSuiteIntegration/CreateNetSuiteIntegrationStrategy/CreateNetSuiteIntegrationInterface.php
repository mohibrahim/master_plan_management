<?php

namespace App\MasterPlanManagement\Services\Models\NetSuiteIntegration\CreateNetSuiteIntegrationStrategy;

use App\NetSuiteIntegration;
use Illuminate\Http\Request;

interface CreateNetSuiteIntegrationInterface
{
    public function create(Request $request): NetSuiteIntegration;
}
