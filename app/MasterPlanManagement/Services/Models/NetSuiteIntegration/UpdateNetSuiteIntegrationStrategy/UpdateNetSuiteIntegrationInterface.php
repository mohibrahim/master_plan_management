<?php

namespace App\MasterPlanManagement\Services\Models\NetSuiteIntegration\UpdateNetSuiteIntegrationStrategy;

use App\NetSuiteIntegration;
use Illuminate\Http\Request;

interface UpdateNetSuiteIntegrationInterface
{
    public function update(NetSuiteIntegration $netSuiteIntegration, Request $request): NetSuiteIntegration;
}
