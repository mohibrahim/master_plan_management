<?php

namespace App\MasterPlanManagement\Services\Models\NetSuiteIntegration\UpdateNetSuiteIntegrationStrategy;

use App\NetSuiteIntegration;
use Illuminate\Http\Request;

class UpdateNetSuiteIntegration implements UpdateNetSuiteIntegrationInterface
{

    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */
    public function update(NetSuiteIntegration $netSuiteIntegration, Request $request): NetSuiteIntegration
    {
        $attributes = $request->all();
        $attributes['last_updater_id'] = auth()->id();
        $netSuiteIntegration->update($attributes);
        if (!empty($netSuiteIntegration->restfulUrl)) {
            $netSuiteIntegration->restfulUrl->update($attributes);
        }
        return $netSuiteIntegration->fresh();
    }

}
