<?php

namespace App\MasterPlanManagement\Services\Models\RestfulUrl\RestfulUrlTypeStrategy;


class RestfulUrlTypes
{
    const PROJECT = 'project';
    const PHASE = 'phase';
    const ZONE = 'zone';
    const BLOCK = 'block';
    const UNIT_MODEL = 'unit_model';
    const UNIT = 'unit';
    const CUSTOMER = 'customer';
    const RESERVATION = 'reservation';

}
