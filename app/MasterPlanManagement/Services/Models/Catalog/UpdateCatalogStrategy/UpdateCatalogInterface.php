<?php

namespace App\MasterPlanManagement\Services\Models\Catalog\UpdateCatalogStrategy;

use App\Catalog;
use Illuminate\Http\Request;

interface UpdateCatalogInterface
{
    public function update($catalogId, Request $request): Catalog;
}