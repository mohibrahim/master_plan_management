<?php

namespace App\MasterPlanManagement\Services\Models\Catalog\UpdateCatalogStrategy;

use App\Catalog;
use Illuminate\Http\Request;
use App\MasterPlanManagement\Services\Models\ProjectFile\Type\ProjectFileTypes;
use App\MasterPlanManagement\Services\Models\ProjectFile\UploadFile\UploadCatalogPdfs;
use App\MasterPlanManagement\Services\Models\ProjectFile\UploadFile\UploadCatalogImages;
use App\MasterPlanManagement\Services\Models\ProjectFile\UploadFile\UploadCatalogCoverImage;

class UpdateCatalog implements UpdateCatalogInterface
{

    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */
    public function update($catalogId, Request $request): Catalog
    {
        $authenticatedUserId = auth()->id();
        $catalog = Catalog::findOrFail($catalogId);
        $attributes = $request->all();
        $attributes['last_updater_id'] = auth()->id();
        
        //upload cover
        $uploadCatalogCoverImage = new UploadCatalogCoverImage('cover_image', $catalog, 'coverImage', 'project_files', 'Catalog cover', ProjectFileTypes::IMAGE);
        $uploadCatalogCoverImage->upload($request);

        //upload images
        $uploadCatalogImages = new UploadCatalogImages('catalog_images', $catalog, 'files', 'project_files', ProjectFileTypes::IMAGE, 'catalog_images_titles', 'catalog_images_statuses', $authenticatedUserId, null);
        $uploadCatalogImages->upload($request);
        //upload pdfs
        $uploadCatalogImages = new UploadCatalogPdfs('catalog_pdfs', $catalog, 'files', 'project_files', ProjectFileTypes::PDF, 'catalog_pdfs_titles', 'catalog_pdfs_statuses', $authenticatedUserId, null);
        $uploadCatalogImages->upload($request);
        
        $isUpdated = $catalog->update($attributes);
        return $catalog->fresh();
    }

}