<?php

namespace App\MasterPlanManagement\Services\Models\Catalog\DeleteCatalogStrategy;

use App\Catalog;
use App\MasterPlanManagement\Services\Models\ProjectFile\UploadFile\DestroyUploadedFile\Destroy;

class DeleteCatalog implements DeleteCatalogInterface
{
    public function delete(Catalog $catalog): bool
    {
        $projectFile = $catalog->coverImage;
        if (!empty($projectFile)) {
            $destroyProjectFileCover = new Destroy('project_files', $projectFile->id);
            $destroyProjectFileCover->destroyFile();
        }
        $projectFiles = $catalog->files;
        if ($projectFiles->isNotEmpty()) {
            foreach ($projectFiles as $file) {
                $destroyProjectFileImage = new Destroy('project_files', $file->id);
                $destroyProjectFileImage->destroyFile();
            }
        }
        return $catalog->delete();
    }
}