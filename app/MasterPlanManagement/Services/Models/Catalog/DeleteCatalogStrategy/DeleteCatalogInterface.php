<?php

namespace App\MasterPlanManagement\Services\Models\Catalog\DeleteCatalogStrategy;

use App\Catalog;

interface DeleteCatalogInterface
{
    public function delete(Catalog $catalog): bool;
}