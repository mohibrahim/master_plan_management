<?php

namespace App\MasterPlanManagement\Services\Models\Catalog\CreateCatalogStrategy;

use App\Catalog;
use Illuminate\Http\Request;

interface CreateCatalogInterface
{
    public function create(Request $request): Catalog;
}