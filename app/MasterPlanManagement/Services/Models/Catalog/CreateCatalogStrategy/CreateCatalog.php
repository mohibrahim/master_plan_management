<?php

namespace App\MasterPlanManagement\Services\Models\Catalog\CreateCatalogStrategy;

use App\Catalog;
use Illuminate\Http\Request;
use App\MasterPlanManagement\Services\Models\ProjectFile\Type\ProjectFileTypes;
use App\MasterPlanManagement\Services\Models\ProjectFile\UploadFile\UploadCatalogPdfs;
use App\MasterPlanManagement\Services\Models\ProjectFile\UploadFile\UploadCatalogImages;
use App\MasterPlanManagement\Services\Models\ProjectFile\UploadFile\UploadCatalogCoverImage;

class CreateCatalog implements CreateCatalogInterface
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
  
    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function create(Request $request): Catalog
    {
        $authenticatedUserId = auth()->id();
        $attributes = $request->all();
        $attributes['creator_id'] = auth()->id();
        $catalog = Catalog::create($attributes);

        //upload image cover
        $uploadCatalogCoverImage = new UploadCatalogCoverImage('cover_image', $catalog, 'coverImage', 'project_files', 'Catalog cover', ProjectFileTypes::IMAGE);
        $uploadCatalogCoverImage->upload($request);
        
        //upload images
        $uploadCatalogImages = new UploadCatalogImages('catalog_images', $catalog, 'files', 'project_files', ProjectFileTypes::IMAGE, 'catalog_images_titles', 'catalog_images_statuses', $authenticatedUserId, null);
        $uploadCatalogImages->upload($request);
        //upload pdfs
        $uploadCatalogImages = new UploadCatalogPdfs('catalog_pdfs', $catalog, 'files', 'project_files', ProjectFileTypes::PDF, 'catalog_pdfs_titles', 'catalog_pdfs_statuses', $authenticatedUserId, null);
        $uploadCatalogImages->upload($request);


        return $catalog;
    }

}