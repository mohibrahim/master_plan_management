<?php

namespace App\MasterPlanManagement\Services\Models\Catalog\HiddenInputCreator;

use Illuminate\Http\Request;

interface HiddenInputCreatorInterface
{
    public function createHiddenInput(Request $request): string;
}