<?php

namespace App\MasterPlanManagement\Services\Models\Catalog\HiddenInputCreator;

use Illuminate\Http\Request;

class HiddenInputCreator implements HiddenInputCreatorInterface
{
    public function createHiddenInput(Request $request): string
    {
        $modelAssociatedType = request()->input('model_associated_type');
        $modelAssociatedTypeId = request()->input('model_associated_type_id');
        $hiddenInput = '';
        
        if (!empty($modelAssociatedType) && !empty($modelAssociatedTypeId)) {
            $hiddenInput = '<input type="hidden" name="'.$modelAssociatedType.'" value="'.$modelAssociatedTypeId.'">';
        }

        return $hiddenInput;
    }
}