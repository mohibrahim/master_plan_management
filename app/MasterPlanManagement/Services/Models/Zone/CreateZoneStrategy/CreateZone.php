<?php

namespace App\MasterPlanManagement\Services\Models\Zone\CreateZoneStrategy;

use App\Zone;
use Illuminate\Http\Request;

class CreateZone implements CreateZoneInterface
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function create(Request $request): Zone
    {
        $attributes = $request->all();
        $attributes['creator_id'] = auth()->id();
        $zone = Zone::create($attributes);
        return $zone;
    }

}
