<?php

namespace App\MasterPlanManagement\Services\Models\Zone\CreateZoneStrategy;

use Illuminate\Http\Request;
use App\Zone;

interface CreateZoneInterface
{
    public function create(Request $request): Zone;
}