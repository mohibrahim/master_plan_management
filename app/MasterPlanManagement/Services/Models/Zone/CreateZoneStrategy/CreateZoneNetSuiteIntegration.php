<?php

namespace App\MasterPlanManagement\Services\Models\Zone\CreateZoneStrategy;

use App\Zone;
use App\Project;
use Illuminate\Http\Request;

class CreateZoneNetSuiteIntegration implements CreateZoneInterface
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function create(Request $request): Zone
    {
        $attributes = $request->all();
        $project = Project::where('net_suite_internal_id', $attributes['project_net_suite_internal_id'])->firstOrFail();
        $attributes['project_id'] = $project->id;
        $attributes['creator_id'] = auth()->id();
        $zone = Zone::create($attributes);
        return $zone;
    }

}
