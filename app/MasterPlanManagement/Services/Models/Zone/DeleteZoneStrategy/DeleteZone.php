<?php

namespace App\MasterPlanManagement\Services\Models\Zone\DeleteZoneStrategy;

use App\MasterPlanManagement\Services\Models\Block\DeleteBlockStrategy\DeleteBlockInterface;
use App\Zone;
use App\MasterPlanManagement\Services\Models\Catalog\DeleteCatalogStrategy\DeleteCatalogInterface;
use App\MasterPlanManagement\Services\Models\Unit\DeleteUnitStrategy\DeleteUnitInterface;

class DeleteZone implements DeleteZoneInterface
{
    public function delete(Zone $zone): bool
    {
        $catalogs = $zone->catalogs;
        if ($catalogs->isNotEmpty()) {
            $deleteCatalog = resolve(DeleteCatalogInterface::class);
            foreach ($catalogs as $catalog) {
                $deleteCatalog->delete($catalog);
            }
        }

        $blocks = $zone->blocks;
        if ($blocks->isNotEmpty()) {
            $deleteBlock = resolve(DeleteBlockInterface::class);
            foreach ($blocks as $block) {
                $deleteBlock->delete($block);
            }
        }

        $units = $zone->units;
        if ($units->isNotEmpty()) {
            $deleteUnit = resolve(DeleteUnitInterface::class);
            foreach ($units as $unit) {
                $deleteUnit->delete($unit);
            }
        }

        return $zone->delete();
    }
}
