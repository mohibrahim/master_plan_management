<?php

namespace App\MasterPlanManagement\Services\Models\Zone\DeleteZoneStrategy;

use App\Zone;

interface DeleteZoneInterface
{
    public function delete(Zone $zone): bool;
}
