<?php

namespace App\MasterPlanManagement\Services\Models\Zone\UpdateZoneStrategy;

use App\Zone;
use Illuminate\Http\Request;

class UpdateZone implements UpdateZoneInterface
{

    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */
    public function update($zoneId, Request $request): Zone
    {
        $zone = Zone::findOrFail($zoneId);
        $attributes = $request->all();
        $attributes['last_updater_id'] = auth()->id();
        $zone->update($attributes);
        return $zone->fresh();
    }

}
