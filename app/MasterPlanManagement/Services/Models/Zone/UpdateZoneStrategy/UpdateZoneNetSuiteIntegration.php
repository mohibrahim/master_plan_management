<?php

namespace App\MasterPlanManagement\Services\Models\Zone\UpdateZoneStrategy;

use App\Zone;
use App\Project;
use Illuminate\Http\Request;

class UpdateZoneNetSuiteIntegration implements UpdateZoneInterface
{

    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */
    public function update($zoneId, Request $request): Zone
    {
        $zone = Zone::findOrFail($zoneId);
        $attributes = $request->all();
        $project = Project::where('net_suite_internal_id', $attributes['project_net_suite_internal_id'])->firstOrFail();
        $attributes['project_id'] = $project->id;
        $attributes['last_updater_id'] = auth()->id();
        $zone->update($attributes);
        return $zone->fresh();
    }

}
