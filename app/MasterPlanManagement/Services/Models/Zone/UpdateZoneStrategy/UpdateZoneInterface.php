<?php

namespace App\MasterPlanManagement\Services\Models\Zone\UpdateZoneStrategy;

use Illuminate\Http\Request;
use App\Zone;

interface UpdateZoneInterface
{
    public function update($zoneId, Request $request): Zone;
}