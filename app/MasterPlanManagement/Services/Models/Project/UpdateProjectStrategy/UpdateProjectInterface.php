<?php

namespace App\MasterPlanManagement\Services\Models\Project\UpdateProjectStrategy;

use Illuminate\Http\Request;
use App\Project;

interface UpdateProjectInterface
{
    public function update($phaseId, Request $request): Project;
}
