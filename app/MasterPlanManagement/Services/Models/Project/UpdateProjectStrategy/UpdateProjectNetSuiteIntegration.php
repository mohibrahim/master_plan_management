<?php

namespace App\MasterPlanManagement\Services\Models\Project\UpdateProjectStrategy;

use App\Project;
use Illuminate\Http\Request;

class UpdateProjectNetSuiteIntegration implements UpdateProjectInterface
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */
    public function update($projectId, Request $request): Project
    {
        $project = Project::findOrFail($projectId);
        $attributes = $request->all();
        $attributes['last_updater_id'] = auth()->id();
        $project->update($attributes);
        return $project->fresh();
    }
}
