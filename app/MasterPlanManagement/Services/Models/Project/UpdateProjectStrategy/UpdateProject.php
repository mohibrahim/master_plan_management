<?php

namespace App\MasterPlanManagement\Services\Models\Project\UpdateProjectStrategy;

use App\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\MasterPlanManagement\Services\FileUploader\FileUploaderInterface;
use App\MasterPlanManagement\Services\Models\Unit\UnitsBulkCreation\FromJson;
use App\MasterPlanManagement\Services\CloudStorage\S3\ProcessingFileTemporaryLocally;

class UpdateProject implements UpdateProjectInterface
{

    use ProcessingFileTemporaryLocally;

    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */
    public function update($projectId, Request $request): Project
    {
        $project = Project::findOrFail($projectId);
        //update project image
        $fileUploader = resolve(FileUploaderInterface::class);
        $fileUploader->setFileNameToSkip('images/master_plan_images/no_image.png');
        $attributes = $fileUploader
            ->update(
                $request,
                'master_plan_image',
                $project->master_plan_image,
                'images/master_plan_images'
            );
        //update logo
        $fileUploader->setFileNameToSkip('images/projects_logos/no_image.png');
        $attributes2 = $fileUploader
            ->update(
                $request,
                'logo',
                $project->logo,
                'images/projects_logos'
            );
        if (!empty($attributes2['logo'])) {
            $attributes['logo'] = $attributes2['logo'];
            unset($attributes2);
        }


        $attributes['last_updater_id'] = auth()->id();
        $project->update($attributes);

        $masterPlanJson = $project->master_plan_json;
        $imagePath = $this->setFileInLocalDisk($project->master_plan_image);
        $newImageWidth = (int) $request->input('new_image_width');

        $fromJson = new FromJson(
            $request,
            $project,
            $masterPlanJson,
            $imagePath,
            $newImageWidth
        );
        $units = $fromJson->create();
        $this->deleteFileFromLocalDiskAndUpdateCloud($project->master_plan_image);

        return $project->fresh();
    }

}
