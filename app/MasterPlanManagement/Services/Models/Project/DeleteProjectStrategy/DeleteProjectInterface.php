<?php

namespace App\MasterPlanManagement\Services\Models\Project\DeleteProjectStrategy;

use App\Project;


interface DeleteProjectInterface
{
    public function delete(Project $project): bool;
}
