<?php

namespace App\MasterPlanManagement\Services\Models\Project\DeleteProjectStrategy;

use App\Project;
use App\MasterPlanManagement\Services\FileUploader\FileUploaderInterface;
use App\MasterPlanManagement\Services\Models\Block\DeleteBlockStrategy\DeleteBlockInterface;
use App\MasterPlanManagement\Services\Models\Catalog\DeleteCatalogStrategy\DeleteCatalogInterface;
use App\MasterPlanManagement\Services\Models\Phase\DeletePhaseStrategy\DeletePhaseInterface;
use App\MasterPlanManagement\Services\Models\Unit\DeleteUnitStrategy\DeleteUnitInterface;
use App\MasterPlanManagement\Services\Models\UnitModel\DeleteUnitModelStrategy\DeleteUnitModelInterface;
use App\MasterPlanManagement\Services\Models\Zone\DeleteZoneStrategy\DeleteZoneInterface;

class DeleteProject implements DeleteProjectInterface
{
    public function delete(Project $project): bool
    {
        //delete Catalogs
        $catalogs = $project->catalogs;
        if ($catalogs->isNotEmpty()) {
            $deleteCatalog = resolve(DeleteCatalogInterface::class);
            foreach ($catalogs as $catalog) {
                $deleteCatalog->delete($catalog);
            }
        }

        $fileUploader = resolve(FileUploaderInterface::class);
        $fileUploader->setFileNameToSkip('images/master_plan_images/no_image.png');
        $isMasterPlanImageDeleted = $fileUploader->delete($project->master_plan_image, 'images/master_plan_images');
        $fileUploader->setFileNameToSkip('images/projects_logos/no_image.png');
        $isProjectLogoDeleted = $fileUploader->delete($project->logo, 'images/projects_logos');

        $phases = $project->phases;
        if ($phases->isNotEmpty()) {
            $deletePhase = resolve(DeletePhaseInterface::class);
            foreach ($phases as $phase) {
                $deletePhase->delete($phase);
            }
        }

        $zones = $project->zones;
        if ($zones->isNotEmpty()) {
            $deleteZone = resolve(DeleteZoneInterface::class);
            foreach ($zones as $zone) {
                $deleteZone->delete($zone);
            }
        }

        $blocks = $project->blocks;
        if ($blocks->isNotEmpty()) {
            $deleteBlock = resolve(DeleteBlockInterface::class);
            foreach ($blocks as $block) {
                $deleteBlock->delete($block);
            }
        }

        $unitModels = $project->unitModels;
        if ($unitModels->isNotEmpty()) {
            $deleteUnitModel = resolve(DeleteUnitModelInterface::class);
            foreach ($unitModels as $unitModel) {
                // The relation between the project and unit model is m:m so if
                // that the unit model to delete has more than 1 project we can't
                // delete else if delete the unit model.
                if ($unitModel->projects->count() > 1) {
                    continue;
                }
                $deleteUnitModel->delete($unitModel);
            }
        }

        $units = $project->units;
        if ($units->isNotEmpty()) {
            $deleteUnit = resolve(DeleteUnitInterface::class);
            foreach ($units as $unit) {
                $deleteUnit->delete($unit);
            }
        }

        return $project->delete();
    }
}
