<?php

namespace App\MasterPlanManagement\Services\Models\Project\CreateProjectStrategy;

use Illuminate\Http\Request;
use App\Project;
use App\MasterPlanManagement\Services\FileUploader\FileUploaderInterface;

class CreateProject implements CreateProjectInterface
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function create(Request $request): Project
    {
        $attributes = resolve(FileUploaderInterface::class)->add($request, 'master_plan_image', 'images/master_plan_images');

        $attributesTwo = resolve(FileUploaderInterface::class)->add($request, 'logo', 'images/projects_logs');
        if (!empty($attributesTwo['logo'])) {
            $attributes['logo'] = $attributesTwo['logo'];
        }

        $attributes['creator_id'] = auth()->id();
        $project = Project::create($attributes);
        return $project;
    }

}
