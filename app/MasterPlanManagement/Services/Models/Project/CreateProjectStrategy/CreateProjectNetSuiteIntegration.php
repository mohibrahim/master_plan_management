<?php

namespace App\MasterPlanManagement\Services\Models\Project\CreateProjectStrategy;

use Illuminate\Http\Request;
use App\Project;

class CreateProjectNetSuiteIntegration implements CreateProjectInterface
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function create(Request $request): Project
    {
        $attributes = $request->all();
        $attributes['creator_id'] = auth()->id();
        $project = Project::create($attributes);
        return $project;
    }

}
