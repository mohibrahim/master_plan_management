<?php

namespace App\MasterPlanManagement\Services\Models\Project\CreateProjectStrategy;

use App\MasterPlanManagement\Services\CloudStorage\S3\ProcessingFileTemporaryLocally;
use App\Project;
use Illuminate\Http\Request;
use App\MasterPlanManagement\Services\FileUploader\FileUploaderInterface;
use App\MasterPlanManagement\Services\Models\Project\CreateProjectStrategy\CreateProjectInterface;
use App\MasterPlanManagement\Services\Models\Unit\UnitsBulkCreation\FromJson;

class CreateProjectWithUnitsJson implements CreateProjectInterface
{

    use ProcessingFileTemporaryLocally;


    public function create(Request $request): Project
    {
        //Upload master plan image
        $fileUploader = resolve(FileUploaderInterface::class);
        $attributes = $fileUploader->add(
            $request,
            'master_plan_image',
            'images/master_plan_images'
        );

        $attributesWithLogo = $fileUploader->add(
            $request,
            'logo',
            'images/projects_logos'
        );

        if (!empty($attributesWithLogo['logo'])) {
            $attributes['logo'] = $attributesWithLogo['logo'];
            unset($attributesWithLogo);
        }



        $attributes['creator_id'] = auth()->id();
        $project = Project::create($attributes);

        $masterPlanJson = $project->master_plan_json;
        $imagePath = $this->setFileInLocalDisk($project->master_plan_image);
        $newImageWidth = (int) $request->input('new_image_width');

        $fromJson = new FromJson(
            $request,
            $project,
            $masterPlanJson,
            $imagePath,
            $newImageWidth
        );
        $units = $fromJson->create();
        $this->deleteFileFromLocalDiskAndUpdateCloud($project->master_plan_image);
        return $project;
    }
}
