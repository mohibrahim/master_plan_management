<?php

namespace App\MasterPlanManagement\Services\Models\Project\CreateProjectStrategy;

use Illuminate\Http\Request;
use App\Project;

interface CreateProjectInterface
{
    public function create(Request $request): Project;
}
