<?php
namespace App\MasterPlanManagement\Services\Models\Project\ProjectAsJson;

class Geometry 
{
    private $type;
    private $coordinates ;


    public function getType():string
    {
        return $this->type;
    }

    public function setType(string $type)
    {
        $this->type = $type;
        return $this;
    }

    public function getCoordinates():array 
    {
        return $this->coordinates;
    }

    public function setCoordinates(array $coordinates)
    {
        $this->coordinates = $coordinates;
        return $this;
    }
}
