<?php

namespace App\MasterPlanManagement\Services\Models\Project\ProjectAsJson;

class Properties
{
    private $name;
    private $layer;
    private $subClasses;
    private $entityHandle;



    public function getName(): string
    {
        return $this->name;
    }


    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }


    public function getLayer(): ?string
    {
        return $this->layer;
    }


    public function setLayer(string $layer)
    {
        $this->layer = $layer;
        return $this;
    }


    public function getSubClasses(): string
    {
        return $this->subClasses;
    }


    public function setSubClasses(string $subClasses)
    {
        $this->subClasses = $subClasses;
        return $this;
    }

    public function getEntityHandle(): string
    {
        return $this->entityHandle;
    }


    public function setEntityHandle(string $entityHandle)
    {
        $this->entityHandle = $entityHandle;
        return $this;
    }
}
