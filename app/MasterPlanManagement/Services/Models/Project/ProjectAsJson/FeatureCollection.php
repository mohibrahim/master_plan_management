<?php

namespace App\MasterPlanManagement\Services\Models\Project\ProjectAsJson;


class FeatureCollection
{

    /**
     * Undocumented variable
     *
     * @var string
     */
    private $type;
    /**
     * Undocumented variable
     *
     * @var string
     */
    private $name;
    /**
     * Undocumented variable
     *
     * @var Crs
     */
    private $crs;
    /**
     * Undocumented variable
     *
     * @var Feature[]
     */
    private $features;

    
    
    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type)
    {
        $this->type = $type;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getCrs(): Crs
    {
        return $this->crs;
    }

    public function setCrs(Crs $crs)
    {
        $this->crs = $crs;
        return $this;
    }

    public function getFeatures(): array
    {
        return $this->features;
    }

    
    public function setFeatures(array $features)
    {
        $this->features = $features;
        return $this;
    }
}
