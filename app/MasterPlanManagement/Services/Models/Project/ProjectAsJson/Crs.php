<?php

namespace App\MasterPlanManagement\Services\Models\Project\ProjectAsJson;

class Crs
{
    private $type;
    private $properties;


    public function getType():string
    {
        return $this->type;
    }


    public function setType(string $type)
    {
        $this->type = $type;
        return $this;
    }


    public function getProperties():Properties
    {
        return $this->properties;
    }


    public function setProperties(Properties $properties)
    {
        $this->properties = $properties;
        return $this;
    }
}
