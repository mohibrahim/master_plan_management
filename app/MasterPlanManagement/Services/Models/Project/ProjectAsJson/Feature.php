<?php

namespace App\MasterPlanManagement\Services\Models\Project\ProjectAsJson;

class Feature
{
    private $type;
    private $properties;
    private $geometry;


    public function getType(): string
    {
        return $this->type;
    }


    public function setType(string $type)
    {
        $this->type = $type;
        return $this;
    }


    public function getProperties(): Properties
    {
        return $this->properties;
    }


    public function setProperties(Properties $properties)
    {
        $this->properties = $properties;
        return $this;
    }


    public function getGeometry(): Geometry
    {
        return $this->geometry;
    }


    public function setGeometry(Geometry $geometry)
    {
        $this->geometry = $geometry;
        return $this;
    }
}
