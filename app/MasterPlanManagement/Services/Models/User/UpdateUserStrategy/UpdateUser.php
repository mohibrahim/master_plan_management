<?php

namespace App\MasterPlanManagement\Services\Models\User\UpdateUserStrategy;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\MasterPlanManagement\Services\Models\ProjectFile\Type\ProjectFileTypes;
use App\MasterPlanManagement\Services\Models\ProjectFile\UploadFile\UploadUserPersonalImage;

class UpdateUser implements UpdateUserInterface
{

    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */
    public function update($userId, Request $request): User
    {
        $user = User::findOrFail($userId);
        $attributes = $request->all();
        if (!empty($attributes['password'])) {
            $attributes['password'] = Hash::make($attributes['password']);
        } else {
            unset($attributes['password']);
        }
        
        $uploadUserPersonalImage = new UploadUserPersonalImage('personal_image', $user, 'personalImage', 'project_files', 'Personal image', ProjectFileTypes::IMAGE);
        $uploadUserPersonalImage->upload($request);
        
        $isUpdated = $user->update($attributes);
        return $user;
    }

}