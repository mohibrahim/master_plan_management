<?php

namespace App\MasterPlanManagement\Services\Models\User\UpdateUserStrategy;

use App\User;
use Illuminate\Http\Request;


interface UpdateUserInterface
{
    public function update($userId, Request $request): User;
}