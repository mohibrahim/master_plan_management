<?php

namespace App\MasterPlanManagement\Services\Models\User\Dashboard;

use Illuminate\Http\Response;



interface DashboardControllerInterface
{
    /**
     * Undocumented function
     *
     * @return Illuminate\Http\Response
     */
    public function home();
}
