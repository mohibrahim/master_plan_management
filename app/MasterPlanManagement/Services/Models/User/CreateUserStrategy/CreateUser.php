<?php

namespace App\MasterPlanManagement\Services\Models\User\CreateUserStrategy;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\MasterPlanManagement\Services\Models\ProjectFile\Type\ProjectFileTypes;
use App\MasterPlanManagement\Services\Models\ProjectFile\UploadFile\UploadUserPersonalImage;

class CreateUser implements CreateUserInterface
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
  
    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function create(Request $request): User
    {
        $attributes = $request->all();
        $attributes['password'] = Hash::make($attributes['password']);
        $attributes['creator_id'] = auth()->id();
        $user = User::create($attributes);
        
        $uploadUserPersonalImage = new UploadUserPersonalImage('personal_image', $user, 'personalImage', 'project_files', 'Personal image', ProjectFileTypes::IMAGE);
        $uploadUserPersonalImage->upload($request);

        return $user;
    }

}