<?php

namespace App\MasterPlanManagement\Services\Models\User\CreateUserStrategy;

use App\User;
use Illuminate\Http\Request;


interface CreateUserInterface
{
    public function create(Request $request): User;
}