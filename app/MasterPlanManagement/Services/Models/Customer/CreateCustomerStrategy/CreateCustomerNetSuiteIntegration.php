<?php

namespace App\MasterPlanManagement\Services\Models\Customer\CreateCustomerStrategy;

use App\Customer;
use Illuminate\Http\Request;

class CreateCustomerNetSuiteIntegration implements CreateCustomerInterface
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function create(Request $request): Customer
    {
        $attributes = $request->all();
        $attributes['creator_id'] = auth()->id();
        $customer = Customer::create($attributes);
        return $customer;
    }

}
