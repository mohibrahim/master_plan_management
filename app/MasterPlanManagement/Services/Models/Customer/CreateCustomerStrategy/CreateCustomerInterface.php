<?php

namespace App\MasterPlanManagement\Services\Models\Customer\CreateCustomerStrategy;

use Illuminate\Http\Request;
use App\Customer;

interface CreateCustomerInterface
{
    public function create(Request $request): Customer;
}
