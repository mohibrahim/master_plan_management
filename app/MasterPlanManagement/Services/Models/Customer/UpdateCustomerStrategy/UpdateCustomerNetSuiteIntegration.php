<?php

namespace App\MasterPlanManagement\Services\Models\Customer\UpdateCustomerStrategy;

use App\Customer;
use Illuminate\Http\Request;

class UpdateCustomerNetSuiteIntegration implements UpdateCustomerInterface
{

    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */
    public function update($customerId, Request $request): Customer
    {
        $customer = Customer::findOrFail($customerId);
        $attributes = $request->all();
        $attributes['last_updater_id'] = auth()->id();
        $customer->update($attributes);
        return $customer->fresh();
    }

}
