<?php

namespace App\MasterPlanManagement\Services\Models\Customer\UpdateCustomerStrategy;

use Illuminate\Http\Request;
use App\Customer;

interface UpdateCustomerInterface
{
    public function update($customerId, Request $request): Customer;
}
