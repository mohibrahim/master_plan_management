<?php

namespace App\MasterPlanManagement\Services\Models\Customer\DeleteCustomerStrategy;

use App\Customer;

interface DeleteCustomerInterface
{
    public function delete(Customer $customer): bool;
}
