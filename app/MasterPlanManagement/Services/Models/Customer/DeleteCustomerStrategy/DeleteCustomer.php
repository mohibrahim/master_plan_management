<?php

namespace App\MasterPlanManagement\Services\Models\Customer\DeleteCustomerStrategy;

use App\Customer;
use App\MasterPlanManagement\Services\Models\Reservation\DeleteReservationStrategy\DeleteReservationInterface;

class DeleteCustomer implements DeleteCustomerInterface
{
    public function delete(Customer $customer): bool
    {
        $reservations = $customer->reservations;
        if ($reservations->isNotEmpty()) {
            $deleteReservation = resolve(DeleteReservationInterface::class);
            foreach ($reservations as $reservation) {
                $deleteReservation->delete($reservation);
            }
        }
        return $customer->delete();
    }
}
