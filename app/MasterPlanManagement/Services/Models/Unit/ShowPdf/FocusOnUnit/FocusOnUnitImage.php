<?php

namespace App\MasterPlanManagement\Services\Models\Unit\ShowPdf\FocusOnUnit;

use App\Unit;
use Illuminate\Support\Facades\DB;
use stdClass;

class  FocusOnUnitImage
{

    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */

    private $unit;
    private $canvasWidth;
    private $canvasHeight;
    private $canvasImage;
    private $unitHtmlAreaTagCoords;
    private $unitPoints;
    private $focusOnUnitCss;
    private $mapPinCss;
    private const MAP_POINT_HEIGHT = 45;
    private const MAP_POINT_WIDTH = 30;
    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */

    public function __construct(Unit $unit, int $canvasWidth, int $canvasHeight)
    {
        $this->setUnit($unit);
        $this->setCanvasWidth($canvasWidth);
        $this->setCanvasHeight($canvasHeight);
        $this->setCanvasImage('');
        $this->setUnitHtmlAreaTagCoords($this->getUnit()->html_area_tag_coords);
        $this->setUnitPoints([]);
        $this->setFocusOnUnitCss('');
        $this->setMapPinCss('');
    }

    /*
    |--------------------------------------------------------------------------
    | Getters & Setters
    |--------------------------------------------------------------------------
    |
    */

    /**
     * Get the value of unit
     */
    public function getUnit(): Unit
    {
        return $this->unit;
    }

    /**
     * Set the value of unit
     *
     * @return  void
     */
    public function setUnit(Unit $unit)
    {
        $this->unit = $unit;
    }

    /**
     * Get the value of canvasWidth
     */
    public function getCanvasWidth(): int
    {
        return $this->canvasWidth;
    }

    /**
     * Set the value of canvasWidth
     *
     * @return  void
     */
    public function setCanvasWidth(int $canvasWidth)
    {
        $this->canvasWidth = $canvasWidth;
    }

    /**
     * Get the value of canvasHeight
     */
    public function getCanvasHeight(): int
    {
        return $this->canvasHeight;
    }

    /**
     * Set the value of canvasHeight
     *
     * @return  void
     */
    public function setCanvasHeight(int $canvasHeight)
    {
        $this->canvasHeight = $canvasHeight;
    }

    /**
     * Get the value of canvasImage
     */
    public function getCanvasImage(): string
    {
        return $this->canvasImage;
    }

    /**
     * Set the value of canvasImage
     *
     * @return  void
     */
    public function setCanvasImage(string $canvasImage)
    {
        $master_plan_images = DB::table('projects')
            ->select('master_plan_image')
            ->where('id', $this->getUnit()->project_id)
            ->get();
        if ($master_plan_images->isEmpty()) {
            $this->canvasImage = '';
        }
        $url = $this->canvasImage = $master_plan_images
            ->first()
            ->master_plan_image;
        $this->canvasImage = asset($url);
    }

    /**
     * Get the value of unitHtmlAreaTagCoords
     */
    public function getUnitHtmlAreaTagCoords(): string
    {
        return $this->unitHtmlAreaTagCoords;
    }

    /**
     * Set the value of unitHtmlAreaTagCoords
     *
     * @return  void
     */
    public function setUnitHtmlAreaTagCoords(string $unitHtmlAreaTagCoords)
    {
        $this->unitHtmlAreaTagCoords = $unitHtmlAreaTagCoords;
    }

    /**
     * Get the value of points
     */
    public function getUnitPoints(): array
    {
        return $this->unitPoints;
    }

    /**
     * Set the value of points
     *
     * @return  void
     */
    public function setUnitPoints(array $unitPoints)
    {
        $htmlAreaTagCoords = $this->getUnitHtmlAreaTagCoords();
        $coords = explode(',', $htmlAreaTagCoords);
        $coordsCount = count($coords);
        $flag = 0;
        if ($coordsCount % 2 == 0) {
            for ($i = 0; $i < $coordsCount / 2; $i++) {
                $unitPoints[] = [
                    'x' => $coords[$flag],
                    'y' => $coords[$flag + 1]
                ];
                $flag = $flag + 2;
            }
        }
        $this->unitPoints = $unitPoints;
    }

    /**
     * Get the value of focusOnUnitCss
     */
    public function getFocusOnUnitCss(): string
    {
        return $this->focusOnUnitCss;
    }

    /**
     * Set the value of focusOnUnitCss
     *
     * @return  void
     */
    public function setFocusOnUnitCss(string $focusOnUnitCss)
    {
        $pointX = - ($this->getUnitPoints()[0]['x'] - ($this->getCanvasWidth() / 2));
        $pointY = - ($this->getUnitPoints()[0]['y'] - ($this->getCanvasHeight() / 2));
        $regulationValue = $this->regulateX(
            $this->getUnitPoints()[0]['x'],
            $this->getUnitPoints()[1]['x'],
            $this->getUnitPoints()[2]['x']
        );
        $focusOnUnitCss = (object) [];
        $focusOnUnitCss->width = $this->getCanvasWidth() . 'px';
        $focusOnUnitCss->height = $this->getCanvasHeight() . 'px';
        $focusOnUnitCss->backgroundImage = 'url(' . $this->getCanvasImage() . ')';
        $focusOnUnitCss->backgroundPosition = $pointX + ($regulationValue) . 'px ' . ($pointY - 6). 'px';
        $focusOnUnitCss->backgroundSize = 'unset';
        $focusOnUnitCss->backgroundRepeat = 'no-repeat';

        $this->focusOnUnitCss = json_encode($focusOnUnitCss);
    }

    /**
     * Get the value of mapPinCss
     */
    public function getMapPinCss(): string
    {
        return $this->mapPinCss;
    }

    /**
     * Set the value of mapPinCss
     *
     * @return  void
     */
    public function setMapPinCss(string $mapPinCss)
    {
        $mapPinCss = (object) [];
        $mapPinCss->position = 'relative';
        $mapPinCss->width = $this::MAP_POINT_WIDTH . 'px';
        $mapPinCss->height = $this::MAP_POINT_HEIGHT . 'px';
        $mapPinCss->top = ($this->getCanvasHeight() / 2) - $this::MAP_POINT_HEIGHT;
        $mapPinCss->left = ($this->getCanvasWidth() / 2) - 15;
        if (app()->getLocale() == 'ar') {
            $mapPinCss->right = ($this->getCanvasWidth() / 2) - 18;
        }

        $this->mapPinCss = json_encode($mapPinCss);
    }

    /*
    |--------------------------------------------------------------------------
    | Interfaces implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    private function regulateX(int $firstPointX, int $secondPointX, int $thirdPoint)
    {
        $distance = 3;

        if ($distance <= 0) {
            return 0;
        }
        if ($firstPointX > $secondPointX) {
            return $distance;
        }
        if ($firstPointX < $secondPointX) {
            return -$distance;
        }
        if ($firstPointX == $secondPointX) {
            if ($firstPointX > $thirdPoint) {
                return $distance;
            }
            return -$distance;
        }
    }
}
