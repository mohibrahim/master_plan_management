<?php

namespace App\MasterPlanManagement\Services\Models\Unit\UnitsBulkCreation;

use App\Unit;
use App\Project;
use Tebru\Gson\Gson;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Intervention\Image\ImageManager;
use App\MasterPlanManagement\Services\Models\Project\ProjectAsJson\FeatureCollection;
use App\MasterPlanManagement\Services\Models\Unit\UnitStatusStrategy\UnitStatusAbstract;
use Illuminate\Contracts\Filesystem\FileNotFoundException;

class FromJson implements UnitsBulkCreationInterface
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */

    private $request;
    private $project;
    private $masterPlanJsonData;
    private $masterPlanImagePath;
    private $masterPlanScaleDownImageWidth;
    private $newImageHight;

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */

    public function __construct(
        Request $request,
        Project $project,
        ?string $masterPlanJsonData,
        string $masterPlanImagePath,
        int $masterPlanScaleDownImageWidth
    ) {
        $this->setRequest($request);
        $this->setProject($project);
        $this->setMasterPlanJsonData($masterPlanJsonData);
        $this->setMasterPlanImagePath($masterPlanImagePath);
        $this->setMasterPlanScaleDownImageWidth($masterPlanScaleDownImageWidth);
    }

    /*
    |--------------------------------------------------------------------------
    | Getters & Setters
    |--------------------------------------------------------------------------
    |
    */


    /**
     * Get the value of request
     */
    public function getRequest(): Request
    {
        return $this->request;
    }

    /**
     * Set the value of request
     *
     * @return  self
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;
        return $this;
    }

    /**
     * Get the value of project
     */
    public function getProject(): Project
    {
        return $this->project;
    }

    /**
     * Set the value of project
     *
     * @return  self
     */
    public function setProject(Project $project)
    {
        $this->project = $project;
        return $this;
    }

    /**
     * Get the value of masterPlanJsonData
     */
    public function getMasterPlanJsonData(): ?string
    {
        return $this->masterPlanJsonData;
    }

    /**
     * Set the value of masterPlanJsonData
     *
     * @return  self
     */
    public function setMasterPlanJsonData(?string $masterPlanJsonData)
    {
        $this->masterPlanJsonData = $masterPlanJsonData;
        return $this;
    }

    /**
     * Get the value of masterPlanImagePath
     */
    public function getMasterPlanImagePath(): string
    {
        return $this->masterPlanImagePath;
    }

    /**
     * Set the value of masterPlanImagePath
     *
     * @return  self
     */
    public function setMasterPlanImagePath(string $masterPlanImagePath)
    {
        if (file_exists($masterPlanImagePath)) {
            $this->masterPlanImagePath = $masterPlanImagePath;
            return $this;
        }
        throw new FileNotFoundException("No Master plan image for this project");
    }

    /**
     * Get the value of masterPlanScaleDownImageWidth
     */
    public function getMasterPlanScaleDownImageWidth(): int
    {
        return $this->masterPlanScaleDownImageWidth;
    }

    /**
     * Set the value of masterPlanScaleDownImageWidth
     *
     * @return  self
     */
    public function setMasterPlanScaleDownImageWidth(int $masterPlanScaleDownImageWidth)
    {
        $this->masterPlanScaleDownImageWidth = $masterPlanScaleDownImageWidth;
        return $this;
    }


    /*
    |--------------------------------------------------------------------------
    | Interfaces implementations
    |--------------------------------------------------------------------------
    |
    */
    public function create(): Collection
    {
        $units = collect();
        $projectHasNoUnits = $this->getProject()->units->isEmpty();
        if (!empty($this->getMasterPlanJsonData()) && $projectHasNoUnits) {
            // Deserialize Json
            $featureCollection = $this->deserializeJson(
                $this->getMasterPlanJsonData(),
                FeatureCollection::class
            );
            $features = $featureCollection->getFeatures();

            $scaleDownRatio = $this->decreaseImageSizeAndGetRatio(
                $this->getMasterPlanImagePath(),
                $this->getMasterPlanScaleDownImageWidth()
            );

            foreach ($features as $feature) {
                $htmlAreaTagCoords = '';
                $coordinates = $feature->getGeometry()->getCoordinates();
                $layer = optional($feature->getProperties())->getLayer();
                foreach ($coordinates as $coordinate) {
                    if ($layer == "0") {
                        continue;
                    }

                    $x = $coordinate[0];
                    $y = $coordinate[1];
                    $smallX = $this->decreaseImagePointValue($x, $scaleDownRatio);
                    $smallY = $this->decreaseImagePointValue($y, $scaleDownRatio);
                    //we subtracting new image hight from small y for flipping vertically
                    $htmlAreaTagCoords .= $smallX . ',' . ($this->newImageHight - $smallY) . ',';
                }
                //remove "," from the end
                $htmlAreaTagCoords = substr($htmlAreaTagCoords, 0, -1);
                //Creating units
                $unit = Unit::create([
                    'code' => $layer,
                    'status' => UnitStatusAbstract::AVAILABLE,
                    'html_area_tag_coords' => $htmlAreaTagCoords,
                    'creator_id' => auth()->id(),
                    'project_id' => $this->getProject()->id,
                ]);
                $units->add($unit);
            }
        }
        return $units;
    }
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    private function deserializeJson($json, $to): FeatureCollection
    {
        $json = str_replace("Layer", "layer", $json);

        $gson = Gson::builder()->build();
        $output = $gson->fromJson($json, $to);
        return $output;
    }

    private function decreaseImageSizeAndGetRatio($imagePath, int $newImageWidth): int
    {
        if ($newImageWidth == 0) {
            /**
             * Returning 100 meaning no image decreasing in case the user didn't
             * add any value to new_image_width
             */
            return 100;
        }
        $newImageHeight = 0;
        $imageMetaData = getimagesize($imagePath);
        $uploadedImageWidth = $imageMetaData[0];
        $uploadedImageHeight = $imageMetaData[1];

        while (($newImageWidth * 100) % $uploadedImageWidth != 0) {
            $newImageWidth++;
        }

        // New height
        if ($uploadedImageWidth != 0) {
            $newImageHeight = ($newImageWidth * $uploadedImageHeight) / $uploadedImageWidth;
        }
        $this->newImageHight = $newImageHeight;

        //Resize down the image
        $manager = new ImageManager(array('gd' => 'imagick'));
        $image = $manager->make($imagePath)->resize($newImageWidth, $newImageHeight);
        $image->save($imagePath);


        $scaleDownRatio = ($newImageWidth * 100) / $uploadedImageWidth;
        return $scaleDownRatio;
    }

    private function decreaseImagePointValue(float $bigNumber, float $ratio): int
    {
        $smallNumber = ($ratio * $bigNumber) / 100;
        $smallNumber = (int) round($smallNumber);
        return $smallNumber;
    }
}
