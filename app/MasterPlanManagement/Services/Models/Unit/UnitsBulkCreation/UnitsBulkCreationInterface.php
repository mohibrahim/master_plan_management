<?php

namespace App\MasterPlanManagement\Services\Models\Unit\UnitsBulkCreation;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;

interface UnitsBulkCreationInterface
{
    public function create(): Collection;
}
