<?php

namespace App\MasterPlanManagement\Services\Models\Unit\DeleteUnitStrategy;

use App\Unit;
use App\MasterPlanManagement\Services\Models\Catalog\DeleteCatalogStrategy\DeleteCatalogInterface;
use App\MasterPlanManagement\Services\Models\Reservation\DeleteReservationStrategy\DeleteReservationInterface;

class DeleteUnit implements DeleteUnitInterface
{
    public function delete(Unit $unit): bool
    {
        $catalogs = $unit->catalogs;
        if ($catalogs->isNotEmpty()) {
            $deleteCatalog = resolve(DeleteCatalogInterface::class);
            foreach ($catalogs as $catalog) {
                $deleteCatalog->delete($catalog);
            }
        }

        $reservation = $unit->reservation;
        if (!empty($reservation)) {
            $deleteReservation = resolve(DeleteReservationInterface::class);
            $deleteReservation->delete($reservation);
        }
        return $unit->delete();
    }
}
