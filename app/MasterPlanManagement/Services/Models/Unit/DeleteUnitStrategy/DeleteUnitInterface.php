<?php

namespace App\MasterPlanManagement\Services\Models\Unit\DeleteUnitStrategy;

use App\Unit;

interface DeleteUnitInterface
{
    public function delete(Unit $unit): bool;
}
