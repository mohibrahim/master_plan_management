<?php
namespace App\MasterPlanManagement\Services\Models\Unit\UnitCurrenciesStrategy;

use App\MasterPlanManagement\Services\LanguageLocator\LanguageLocatorTrait;


class SuitableUnitCurrencies
{
    use LanguageLocatorTrait;

    private $unitCurrencyArabic;
    private $unitCurrencyEnglish;


    public function __construct(UnitCurrencyArabic $unitCurrencyArabic, UnitCurrencyEnglish $unitCurrencyEnglish)
    {
        $this->unitCurrencyArabic = $unitCurrencyArabic;
        $this->unitCurrencyEnglish = $unitCurrencyEnglish;
    }

    public  function getList(): array
    {
        $list = [];
        if ($this->isArabic()) {
            $list = $this->unitCurrencyArabic->list();
        } elseif ($this->isEnglish()) {
            $list = $this->unitCurrencyEnglish->list();
        }
        return $list;
    }

    public function selectedValue(?string $key): string
    {
        $value ='';
        if ($this->isArabic()) {
            $list = $this->unitCurrencyArabic->list();
            if (empty($list[$key])){
                return $value;
            }
            $value = $list[$key];
        } elseif ($this->isEnglish()) {
            $list = $this->unitCurrencyEnglish->list();
            if (empty($list[$key])){
                return $value;
            }
            $value = $list[$key];
        }
        return $value;
    }
}
