<?php
namespace App\MasterPlanManagement\Services\Models\Unit\UnitHasGardenStrategy;

use App\MasterPlanManagement\Services\LanguageLocator\LanguageLocatorTrait;


class SuitableUnitHasGarden
{
    use LanguageLocatorTrait;

    private $unitHasGardenArabic;
    private $unitHasGardenEnglish;


    public function __construct(UnitHasGardenArabic $unitHasGardenArabic, UnitHasGardenEnglish $unitHasGardenEnglish)
    {
        $this->unitHasGardenArabic = $unitHasGardenArabic;
        $this->unitHasGardenEnglish = $unitHasGardenEnglish;
    }

    public  function getList(): array
    {
        $list = [];
        if ($this->isArabic()) {
            $list = $this->unitHasGardenArabic->list();
        } elseif ($this->isEnglish()) {
            $list = $this->unitHasGardenEnglish->list();
        }
        return $list;
    }

    public function selectedValue(?string $key): string
    {
        $value ='';
        if ($this->isArabic()) {
            $list = $this->unitHasGardenArabic->list();
            if (empty($list[$key])){
                return $value;
            }
            $value = $list[$key];
        } elseif ($this->isEnglish()) {
            $list = $this->unitHasGardenEnglish->list();
            if (empty($list[$key])){
                return $value;
            }
            $value = $list[$key];
        }
        return $value;
    }
}
