<?php

namespace App\MasterPlanManagement\Services\Models\Unit\UnitHasGardenStrategy;


class UnitHasGardenEnglish extends UnitHasGardenAbstract
{
    public static function list(): array
    {
        return [
                self::YES => 'Yes',
                self::NO => 'No',
        ];
    }
}
