<?php

namespace App\MasterPlanManagement\Services\Models\Unit\UnitHasGardenStrategy;


abstract class UnitHasGardenAbstract
{
    const YES = true;
    const NO = false;

    abstract static function list(): array;
}
