<?php

namespace App\MasterPlanManagement\Services\Models\Unit\UnitHasGardenStrategy;


class UnitHasGardenArabic extends UnitHasGardenAbstract
{
    public static function list(): array
    {
        return [
                self::YES => 'نعم',
                self::NO => 'لا',
        ];
    }
}
