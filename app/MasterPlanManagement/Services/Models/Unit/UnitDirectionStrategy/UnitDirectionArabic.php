<?php

namespace App\MasterPlanManagement\Services\Models\Unit\UnitDirectionStrategy;


class UnitDirectionArabic implements UnitDirectionInterface
{
    const SOUTH_DIRECTION = 'south_direction';
    const NORTH_DIRECTION = 'north_direction';

    
    public static function list(): array
    {
        return [
            self::SOUTH_DIRECTION => 'قبلي',
            self::NORTH_DIRECTION => 'بحري',
        ];
    }
}
