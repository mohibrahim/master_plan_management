<?php

namespace App\MasterPlanManagement\Services\Models\Unit\UnitDirectionStrategy;


interface UnitDirectionInterface
{
    public static function list(): array;
}