<?php
namespace App\MasterPlanManagement\Services\Models\Unit\UnitDirectionStrategy;

use App\MasterPlanManagement\Services\LanguageLocator\LanguageLocatorTrait;


class SuitableUnitDirection
{
    use LanguageLocatorTrait;

    private $unitDirectionArabic;
    private $unitDirectionEnglish;


    public function __construct(UnitDirectionArabic $unitDirectionArabic, UnitDirectionEnglish $unitDirectionEnglish)
    {
        $this->unitDirectionArabic = $unitDirectionArabic;
        $this->unitDirectionEnglish = $unitDirectionEnglish;
    }

    public  function getList(): array
    {
        $list = [];
        if ($this->isArabic()) {
            $list = $this->unitDirectionArabic->list();
        } elseif ($this->isEnglish()) {
            $list = $this->unitDirectionEnglish->list();
        }
        return $list;
    }

    public function selectedValue(?string $key): string
    {
        $value ='';
        if ($this->isArabic()) {
            $list = $this->unitDirectionArabic->list();
            if (empty($list[$key])){
                return $value;
            }
            $value = $list[$key];
        } elseif ($this->isEnglish()) {
            $list = $this->unitDirectionEnglish->list();
            if (empty($list[$key])){
                return $value;
            }
            $value = $list[$key];
        } 
        return $value;
    }
}