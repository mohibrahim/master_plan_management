<?php

namespace App\MasterPlanManagement\Services\Models\Unit\UnitStatusStrategy;


abstract class UnitStatusAbstract
{
    const AVAILABLE = 'available';
    const HOLD = 'hold';
    const BOOKED = 'booked';
    const RESERVED = 'reserved';
    const SOLD = 'sold';
    const HANDED_OVER = 'handed_over';

    abstract static function list(): array;
}
