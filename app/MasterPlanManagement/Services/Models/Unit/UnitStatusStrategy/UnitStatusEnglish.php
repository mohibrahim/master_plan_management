<?php
namespace App\MasterPlanManagement\Services\Models\Unit\UnitStatusStrategy;


class UnitStatusEnglish extends UnitStatusAbstract
{

    public static function list(): array
    {
        return [
            self::AVAILABLE => 'Available',
            self::HOLD => 'Hold',
            self::BOOKED => 'Booked',
            self::RESERVED => 'Reserved',
            self::SOLD => 'Sold',
            self::HANDED_OVER => 'Handed over',
        ];
    }
}
