<?php

namespace App\MasterPlanManagement\Services\Models\Unit\UnitStatusStrategy;


class UnitStatusArabic extends UnitStatusAbstract
{
    public static function list(): array
    {
        return [
            self::AVAILABLE => 'متاحة',
            self::HOLD => 'معلقة',
            self::BOOKED => 'محجوزة',
            self::RESERVED => 'محفوظة',
            self::SOLD => 'تم البيع',
            self::HANDED_OVER => 'سلمت',
        ];
    }
}
