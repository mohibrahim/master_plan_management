<?php
namespace App\MasterPlanManagement\Services\Models\Unit\UnitStatusStrategy;

use App\MasterPlanManagement\Services\LanguageLocator\LanguageLocatorTrait;


class SuitableUnitStatus
{
    use LanguageLocatorTrait;

    private $unitStatusArabic;
    private $unitStatusEnglish;


    public function __construct(UnitStatusArabic $unitStatusArabic, UnitStatusEnglish $unitStatusEnglish)
    {
        $this->unitStatusArabic = $unitStatusArabic;
        $this->unitStatusEnglish = $unitStatusEnglish;
    }

    public  function getList(): array
    {
        $list = [];
        if ($this->isArabic()) {
            $list = $this->unitStatusArabic->list();
        } elseif ($this->isEnglish()) {
            $list = $this->unitStatusEnglish->list();
        }
        return $list;
    }

    public function selectedValue(?string $key): string
    {
        $value ='';
        if ($this->isArabic()) {
            $list = $this->unitStatusArabic->list();
            if (empty($list[$key])){
                return $value;
            }
            $value = $list[$key];
        } elseif ($this->isEnglish()) {
            $list = $this->unitStatusEnglish->list();
            if (empty($list[$key])){
                return $value;
            }
            $value = $list[$key];
        } 
        return $value;
    }
}