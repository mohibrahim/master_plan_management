<?php
namespace App\MasterPlanManagement\Services\Models\Unit\FilterUnits;

use App\Unit;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;


class FilterByThreeTerms implements FilterUnitInterface
{
    public function filter(Request $request): Collection
    {
        $unitStatus = $request->input('unit_status');
        $zoneId = $request->input('zone_id');
        $inBlock = $request->input('in_block');
        $projectId = $request->input('project_id');

        $units = collect();


        //1
        if (!empty($unitStatus) && !empty($zoneId) && $inBlock == "true") {
            $units = Unit::where('project_id', $projectId)
                ->where('status', $unitStatus)
                ->whereHas('zone', function($query) use ($zoneId){
                    $query->where('id', $zoneId);
                })
                ->whereHas('block')
                ->get();
        }
        //2
        if (!empty($unitStatus) && empty($zoneId) && $inBlock == "false") {
            $units = Unit::where('project_id', $projectId)
                ->where('status', $unitStatus)
                ->get();
        }
        //3
        if (!empty($unitStatus) && !empty($zoneId) && $inBlock == "false") {
            $units = Unit::where('project_id', $projectId)
                ->where('status', $unitStatus)
                ->whereHas('zone', function($query) use ($zoneId){
                    $query->where('id', $zoneId);
                })
                ->get();
        }
        //4
        if (!empty($unitStatus) && empty($zoneId) && $inBlock == "true") {
            $units = Unit::where('project_id', $projectId)
                ->where('status', $unitStatus)
                ->whereHas('block')
                ->get();
        }
        //5
        if (empty($unitStatus) && !empty($zoneId) && $inBlock == "true") {
            $units = Unit::where('project_id', $projectId)
                ->whereHas('zone', function($query) use ($zoneId){
                    $query->where('id', $zoneId);
                })
                ->whereHas('block')
                ->get();
        }
        //6
        if (empty($unitStatus) && empty($zoneId) && $inBlock == "false") {
            //
        }
        //7
        if (empty($unitStatus) && !empty($zoneId) && $inBlock == "false") {
            $units = Unit::where('project_id', $projectId)
                ->whereHas('zone', function($query) use ($zoneId){
                    $query->where('id', $zoneId);
                })
                ->get();
        }
        //8
        if (empty($unitStatus) && empty($zoneId) && $inBlock == "true") {
            $units = Unit::where('project_id', $projectId)
                ->whereHas('block')
                ->get();
        }

        return $units;
    }
}
