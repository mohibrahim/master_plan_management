<?php
namespace App\MasterPlanManagement\Services\Models\Unit\FilterUnits;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;

interface FilterUnitInterface
{
    public function filter(Request $request): Collection;
}
