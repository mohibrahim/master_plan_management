<?php

namespace App\MasterPlanManagement\Services\Models\Unit\FilterUnits;

use App\Unit;
use App\Zone;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;


class FilterByFourTerms implements FilterUnitInterface
{
    public function filter(Request $request): Collection
    {

        // status
        // ->where('status', $unitStatus)
        $unitStatus = $request->input('unit_status');
        //zone
        // ->whereHas('zone', function($query) use ($zoneId){
        //     $query->where('id', $zoneId);
        // })
        $zoneId = $request->input('zone_id');
        //unit model
        // ->whereHas('unitModel', function($query) use ($unitModelId){
        //     $query->where('id', $unitModelId);
        // })
        $unitModelId = $request->input('unit_model_id');
        //block
        //->whereHas('block')
        $inBlock = $request->input('in_block');

        $projectId = $request->input('project_id');
        $builder = Unit::where('project_id', $projectId);

        //1
        if (empty($unitStatus) && empty($zoneId) && empty($unitModelId) && $inBlock == 'false') {
            //
        }
        //2
        if (empty($unitStatus) && empty($zoneId) && empty($unitModelId) && $inBlock == 'true') {
            $builder =
                $builder->whereHas('block');
        }
        //3
        if (empty($unitStatus) && empty($zoneId) && !empty($unitModelId) && $inBlock == 'false') {
            $builder =
                $builder->whereHas('unitModel', function ($query) use ($unitModelId) {
                    $query->where('id', $unitModelId);
                });
        }
        //4
        if (empty($unitStatus) && empty($zoneId) && !empty($unitModelId) && $inBlock == 'true') {
            $builder =
                $builder->whereHas('unitModel', function ($query) use ($unitModelId) {
                    $query->where('id', $unitModelId);
                })
                ->whereHas('block');
        }
        //5
        if (empty($unitStatus) && !empty($zoneId) && empty($unitModelId) && $inBlock == 'false') {
            $builder =
                $builder->whereHas('zone', function ($query) use ($zoneId) {
                    $query->where('id', $zoneId);
                });
        }
        //6
        if (empty($unitStatus) && !empty($zoneId) && empty($unitModelId) && $inBlock == 'true') {
            $builder =
                $builder->whereHas('zone', function ($query) use ($zoneId) {
                    $query->where('id', $zoneId);
                })
                ->whereHas('block');
        }
        //7
        if (empty($unitStatus) && !empty($zoneId) && !empty($unitModelId) && $inBlock == 'false') {
            $builder =
                $builder->whereHas('zone', function ($query) use ($zoneId) {
                    $query->where('id', $zoneId);
                })
                ->whereHas('unitModel', function ($query) use ($unitModelId) {
                    $query->where('id', $unitModelId);
                });
        }
        //8
        if (empty($unitStatus) && !empty($zoneId) && !empty($unitModelId) && $inBlock == 'true') {
            $builder =
                $builder->whereHas('zone', function ($query) use ($zoneId) {
                    $query->where('id', $zoneId);
                })
                ->whereHas('unitModel', function ($query) use ($unitModelId) {
                    $query->where('id', $unitModelId);
                })
                ->whereHas('block');
        }
        //9
        if (!empty($unitStatus) && empty($zoneId) && empty($unitModelId) && $inBlock == 'false') {

            $builder =
                $builder->where('status', $unitStatus);
        }
        //10
        if (!empty($unitStatus) && empty($zoneId) && empty($unitModelId) && $inBlock == 'true') {
            $builder =
                $builder->where('status', $unitStatus)
                ->whereHas('block');
        }
        //11
        if (!empty($unitStatus) && empty($zoneId) && !empty($unitModelId) && $inBlock == 'false') {
            $builder =
                $builder->where('status', $unitStatus)
                ->whereHas('unitModel', function ($query) use ($unitModelId) {
                    $query->where('id', $unitModelId);
                });
        }
        //12
        if (!empty($unitStatus) && empty($zoneId) && !empty($unitModelId) && $inBlock == 'true') {
            $builder =
                $builder->where('status', $unitStatus)
                ->whereHas('unitModel', function ($query) use ($unitModelId) {
                    $query->where('id', $unitModelId);
                })
                ->whereHas('block');
        }
        //13
        if (!empty($unitStatus) && !empty($zoneId) && empty($unitModelId) && $inBlock == 'false') {
            $builder =
                $builder->where('status', $unitStatus)
                ->whereHas('zone', function ($query) use ($zoneId) {
                    $query->where('id', $zoneId);
                });
        }
        //14
        if (!empty($unitStatus) && !empty($zoneId) && empty($unitModelId) && $inBlock == 'true') {
            $builder =
                $builder->where('status', $unitStatus)
                ->whereHas('zone', function ($query) use ($zoneId) {
                    $query->where('id', $zoneId);
                })
                ->whereHas('block');
        }
        //15
        if (!empty($unitStatus) && !empty($zoneId) && !empty($unitModelId) && $inBlock == 'false') {
            $builder =
                $builder->where('status', $unitStatus)
                ->whereHas('zone', function ($query) use ($zoneId) {
                    $query->where('id', $zoneId);
                })
                ->whereHas('unitModel', function ($query) use ($unitModelId) {
                    $query->where('id', $unitModelId);
                });
        }
        //16
        if (!empty($unitStatus) && !empty($zoneId) && !empty($unitModelId) && $inBlock == 'true') {
            $builder =
                $builder->where('status', $unitStatus)
                ->whereHas('zone', function ($query) use ($zoneId) {
                    $query->where('id', $zoneId);
                })
                ->whereHas('unitModel', function ($query) use ($unitModelId) {
                    $query->where('id', $unitModelId);
                })
                ->whereHas('block');
        }

        if (!empty($zoneId)) {
            $zone = Zone::find($zoneId);
            $htmlAreaTagCoords = $zone->html_area_tag_coords;
            $coordsArray = explode(',', $htmlAreaTagCoords);
            $lowestX = 10000000000000;
            for ($i = 0; $i < count($coordsArray); $i += 2) {
                if ($coordsArray[$i] < $lowestX) {
                    $lowestX = $coordsArray[$i];
                }
            }
            return collect(['zone_lowest_x' => $lowestX, 'units' => $builder->get()]);
        }
        return collect(['zone_lowest_x' => 0, 'units' => $builder->get()]);
    }
}
