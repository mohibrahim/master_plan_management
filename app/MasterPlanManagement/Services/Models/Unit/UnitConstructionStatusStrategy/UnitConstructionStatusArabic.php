<?php

namespace App\MasterPlanManagement\Services\Models\Unit\UnitConstructionStatusStrategy;


class UnitConstructionStatusArabic implements UnitConstructionStatusInterface
{
    const NOT_STARTED = 'not_started';
    const UNDER_CONSTRUCTION = 'under_construction';
    const HOLD = 'hold';
    const COMPLETED = 'completed';

    public static function list(): array
    {
        return [
            self::NOT_STARTED => 'لم يبدأ',
            self::UNDER_CONSTRUCTION => 'تحت التشيد',
            self::HOLD => 'معلق',
            self::COMPLETED => 'منجز',
        ];
    }
}
