<?php
namespace App\MasterPlanManagement\Services\Models\Unit\UnitConstructionStatusStrategy;

use App\MasterPlanManagement\Services\LanguageLocator\LanguageLocatorTrait;


class SuitableUnitConstructionStatus
{
    use LanguageLocatorTrait;

    private $unitConstructionStatusArabic;
    private $unitConstructionStatusEnglish;


    public function __construct(UnitConstructionStatusArabic $unitConstructionStatusArabic, UnitConstructionStatusEnglish $unitConstructionStatusEnglish)
    {
        $this->unitConstructionStatusArabic = $unitConstructionStatusArabic;
        $this->unitConstructionStatusEnglish = $unitConstructionStatusEnglish;
    }

    public  function getList(): array
    {
        $list = [];
        if ($this->isArabic()) {
            $list = $this->unitConstructionStatusArabic->list();
        } elseif ($this->isEnglish()) {
            $list = $this->unitConstructionStatusEnglish->list();
        }
        return $list;
    }

    public function selectedValue(?string $key): string
    {
        $value ='';
        if ($this->isArabic()) {
            $list = $this->unitConstructionStatusArabic->list();
            if (empty($list[$key])){
                return $value;
            }
            $value = $list[$key];
        } elseif ($this->isEnglish()) {
            $list = $this->unitConstructionStatusEnglish->list();
            if (empty($list[$key])){
                return $value;
            }
            $value = $list[$key];
        } 
        return $value;
    }
}