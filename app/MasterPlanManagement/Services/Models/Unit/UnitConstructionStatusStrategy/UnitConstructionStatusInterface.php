<?php

namespace App\MasterPlanManagement\Services\Models\Unit\UnitConstructionStatusStrategy;


interface UnitConstructionStatusInterface
{
    public static function list(): array;
}