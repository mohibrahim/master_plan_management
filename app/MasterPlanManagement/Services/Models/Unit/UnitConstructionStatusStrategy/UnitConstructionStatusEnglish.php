<?php
namespace App\MasterPlanManagement\Services\Models\Unit\UnitConstructionStatusStrategy;


class UnitConstructionStatusEnglish implements UnitConstructionStatusInterface
{
    const NOT_STARTED = 'not_started';
    const UNDER_CONSTRUCTION = 'under_construction';
    const HOLD = 'hold';
    const COMPLETED = 'completed';

    public static function list(): array
    {
        return [
            self::NOT_STARTED => 'Not started',
            self::UNDER_CONSTRUCTION => 'Under construction',
            self::HOLD => 'Hold',
            self::COMPLETED => 'Completed',
        ];
    }
}