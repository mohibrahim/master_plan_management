<?php

namespace App\MasterPlanManagement\Services\Models\Unit\CreateUnitStrategy;

use App\Unit;
use Exception;
use App\Project;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;

class CreateUnit implements CreateUnitInterface
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function create(Request $request): Unit
    {
        $attributes = $request->all();
        $attributes['creator_id'] = auth()->id();
        $unit = Unit::create($attributes);

        try {
            $project = Project::find($request->input('project_id'));
            $project->unitModels()->attach($request->input('unit_model_id'));
        } catch (QueryException $exception) {
            if (!$exception->errorInfo[1] == '1062') {
                throw new Exception($exception->getMessage());
            }
            // if catch it don't do any thing coz project_id
            //and unit_model_id is primary key
        }
        return $unit;
    }

}
