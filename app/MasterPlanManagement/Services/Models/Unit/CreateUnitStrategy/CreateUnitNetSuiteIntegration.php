<?php

namespace App\MasterPlanManagement\Services\Models\Unit\CreateUnitStrategy;

use App\Unit;
use App\Zone;
use App\Block;
use App\Phase;
use Exception;
use App\Project;
use App\UnitModel;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;

class CreateUnitNetSuiteIntegration implements CreateUnitInterface
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function create(Request $request): Unit
    {
        $attributes = $request->all();
        $attributes['creator_id'] = auth()->id();

        $project = Project::where('net_suite_internal_id', $attributes['project_net_suite_internal_id'])->firstOrFail();
        $attributes['project_id'] = $project->id;
        $phase = Phase::where('net_suite_internal_id', $attributes['phase_net_suite_internal_id'])->firstOrFail();
        $attributes['phase_id'] = $phase->id;
        $zone = Zone::where('net_suite_internal_id', $attributes['zone_net_suite_internal_id'])->firstOrFail();
        $attributes['zone_id'] = $zone->id;
        $block = Block::where('net_suite_internal_id', $attributes['block_net_suite_internal_id'])->firstOrFail();
        $attributes['block_id'] = $block->id;
        $unitModel = UnitModel::where('net_suite_internal_id', $attributes['unit_model_net_suite_internal_id'])->firstOrFail();
        $attributes['unit_model_id'] = $unitModel->id;

        $unitModel->save();

        $unit = Unit::create($attributes);

        try {
            $project->unitModels()->attach($attributes['unit_model_id']);
        } catch (QueryException $exception) {
            if (!$exception->errorInfo[1] == '1062') {
                throw new Exception($exception->getMessage());
            }
            // if catch it don't do any thing coz project_id
            //and unit_model_id is primary key
        }
        return $unit;
    }
}
