<?php

namespace App\MasterPlanManagement\Services\Models\Unit\CreateUnitStrategy;

use Illuminate\Http\Request;
use App\Unit;

interface CreateUnitInterface
{
    public function create(Request $request): Unit;
}