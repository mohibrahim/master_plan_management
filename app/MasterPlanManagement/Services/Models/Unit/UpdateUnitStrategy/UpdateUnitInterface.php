<?php

namespace App\MasterPlanManagement\Services\Models\Unit\UpdateUnitStrategy;

use Illuminate\Http\Request;
use App\Unit;

interface UpdateUnitInterface
{
    public function update($unitId, Request $request): Unit;
}