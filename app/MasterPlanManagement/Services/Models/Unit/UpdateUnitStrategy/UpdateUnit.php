<?php

namespace App\MasterPlanManagement\Services\Models\Unit\UpdateUnitStrategy;

use App\Unit;
use App\Project;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class UpdateUnit implements UpdateUnitInterface
{

    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */
    public function update($unitId, Request $request): Unit
    {
        $unit = Unit::findOrFail($unitId);
        $attributes = $request->all();
        $attributes['last_updater_id'] = auth()->id();
        $unit->update($attributes);

        try {
            $project = Project::find($request->input('project_id'));
            $project->unitModels()->attach($request->input('unit_model_id'));
        } catch (QueryException $exception) {
            if (!$exception->errorInfo[1] == '1062') {
                throw new Exception($exception->getMessage());
            }
            // if catch it don't do any thing coz project_id
            //and unit_model_id is primary key
        }
        return $unit->fresh();
    }

}
