<?php

namespace App\MasterPlanManagement\Services\Models\Unit\UnitHasPoolStrategy;


abstract class UnitHasPoolAbstract
{
    const YES = true;
    const NO = false;

    abstract static function list(): array;
}
