<?php

namespace App\MasterPlanManagement\Services\Models\Unit\UnitHasPoolStrategy;


class UnitHasPoolArabic extends UnitHasPoolAbstract
{
    public static function list(): array
    {
        return [
                self::YES => 'نعم',
                self::NO => 'لا',
        ];
    }
}
