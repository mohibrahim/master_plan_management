<?php

namespace App\MasterPlanManagement\Services\Models\Unit\UnitHasPoolStrategy;


class UnitHasPoolEnglish extends UnitHasPoolAbstract
{
    public static function list(): array
    {
        return [
                self::YES => 'Yes',
                self::NO => 'No',
        ];
    }
}
