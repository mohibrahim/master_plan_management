<?php
namespace App\MasterPlanManagement\Services\Models\Unit\UnitHasPoolStrategy;

use App\MasterPlanManagement\Services\LanguageLocator\LanguageLocatorTrait;


class SuitableUnitHasPool
{
    use LanguageLocatorTrait;

    private $unitHasPoolArabic;
    private $unitHasPoolEnglish;


    public function __construct(UnitHasPoolArabic $unitHasPoolArabic, UnitHasPoolEnglish $unitHasPoolEnglish)
    {
        $this->unitHasPoolArabic = $unitHasPoolArabic;
        $this->unitHasPoolEnglish = $unitHasPoolEnglish;
    }

    public  function getList(): array
    {
        $list = [];
        if ($this->isArabic()) {
            $list = $this->unitHasPoolArabic->list();
        } elseif ($this->isEnglish()) {
            $list = $this->unitHasPoolEnglish->list();
        }
        return $list;
    }

    public function selectedValue(?string $key): string
    {
        $value ='';
        if ($this->isArabic()) {
            $list = $this->unitHasPoolArabic->list();
            if (empty($list[$key])){
                return $value;
            }
            $value = $list[$key];
        } elseif ($this->isEnglish()) {
            $list = $this->unitHasPoolEnglish->list();
            if (empty($list[$key])){
                return $value;
            }
            $value = $list[$key];
        }
        return $value;
    }
}
