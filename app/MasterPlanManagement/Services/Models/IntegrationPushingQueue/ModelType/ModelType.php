<?php

namespace App\MasterPlanManagement\Services\Models\IntegrationPushingQueue\ModelType;


class ModelType
{
    const RESERVATION = 'reservation';
    const CUSTOMER = 'customer';

    public function getList(): array
    {
        return [
            self::RESERVATION => self::RESERVATION,
            self::CUSTOMER => self::CUSTOMER,
        ];
    }
}
