<?php

namespace App\MasterPlanManagement\Services\Models\IntegrationPushingQueue\OperationType;


class OperationType
{
    const CREATE = 'create';
    const UPDATE = 'update';
    const DELETE = 'delete';

    public function getList(): array
    {
        return [
            self::CREATE => self::CREATE,
            self::UPDATE => self::UPDATE,
            self::DELETE => self::DELETE,
        ];
    }
}
