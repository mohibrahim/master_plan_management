<?php

namespace App\MasterPlanManagement\Services\Models\IntegrationPushingQueue\CreateIntegrationPushingQueue;

use App\IntegrationPushingQueue;


interface CreateIntegrationPushingQueueInterface
{
    public function create(): IntegrationPushingQueue;
}
