<?php

namespace App\MasterPlanManagement\Services\Models\IntegrationPushingQueue\CreateIntegrationPushingQueue;

use Exception;
use App\Customer;
use App\IntegrationPushingQueue;
use App\MasterPlanManagement\Services\Models\IntegrationPushingQueue\ModelType\ModelType;
use App\MasterPlanManagement\Services\Models\IntegrationPushingQueue\OperationType\OperationType;
use App\MasterPlanManagement\Services\Models\IntegrationPushingQueue\IntegrationType\IntegrationType;
use App\MasterPlanManagement\Services\Integrations\Push\ThirdPartyObjects\Customer\MapEloquentCustomerToThirdPartyInterface;

class CreateIntegrationPushingQueueCustomer implements CreateIntegrationPushingQueueInterface
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */

    private $customer;
    private $integrationType;
    private $operationType;
    private $mapEloquentCustomerToThirdParty;

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Getters & Setters
    |--------------------------------------------------------------------------
    |
    */

    private function getCustomer(): Customer
    {
        return $this->customer;
    }

    public function setCustomer(Customer $customer): self
    {
        $this->customer = $customer;
        return $this;
    }

    private function getOperationType(): string
    {
        return $this->operationType;
    }

    public function setOperationType(string $operationType): self
    {
        $this->operationType = $operationType;
        return $this;
    }

    private function getIntegrationType(): string
    {
        return $this->integrationType;
    }

    public function setIntegrationType(string $integrationType): self
    {
        $this->integrationType = $integrationType;
        return $this;
    }

    private function getMapEloquentCustomerToThirdParty(): MapEloquentCustomerToThirdPartyInterface
    {
        return $this->mapEloquentCustomerToThirdParty;
    }

    public function setMapEloquentCustomerToThirdParty(
        MapEloquentCustomerToThirdPartyInterface $mapEloquentCustomerToThirdParty
    ): self {
        $this->mapEloquentCustomerToThirdParty = $mapEloquentCustomerToThirdParty;
        return $this;
    }

    /*
    |--------------------------------------------------------------------------
    | Interfaces implementations
    |--------------------------------------------------------------------------
    |
    */

    public function create(): IntegrationPushingQueue
    {
        $this->validate();
        return $this->factoryCreate();
    }
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    private function validate()
    {
        if (empty($this->getCustomer()->id)) {
            throw new Exception('Eloquent customer id not set.');
        }

        $operationTypeString = $this->getOperationType();
        if (empty($operationTypeString)) {
            throw new Exception('Operation type not set.');
        }
        $operationType = new OperationType();
        if (!in_array($operationTypeString, $operationType->getList())) {
            throw new Exception('Operation type not compatible.');
        }


        $integrationTypeString =  $this->getIntegrationType();
        if (empty($integrationTypeString)) {
            throw new Exception('Integration type not set.');
        }
        $integrationType = new IntegrationType();
        if (!in_array($integrationTypeString, $integrationType->getList())) {
            throw new Exception('Integration type not compatible.');
        }

        if (empty($this->getMapEloquentCustomerToThirdParty())) {
            throw new Exception('Map eloquent customer to third party not set.');
        }
    }

    private function factoryCreate(): IntegrationPushingQueue
    {
        if ($this->getOperationType() == OperationType::CREATE) {
            return $this->createForeCreate();
        }
        if ($this->getOperationType() == OperationType::UPDATE) {
            return $this->createForeUpdate();
        }
        if ($this->getOperationType() == OperationType::DELETE) {
            return $this->createForeDelete();
        }
    }

    private function createForeCreate(): IntegrationPushingQueue
    {
        $pushingQueue = IntegrationPushingQueue::create([
            'model_type' => ModelType::CUSTOMER,
            'operation_type' => $this->getOperationType(),
            'model_id' => $this->getCustomer()->id,
            'integration_type' => $this->getIntegrationType(),
        ]);
        return $pushingQueue;
    }
    private function createForeUpdate(): IntegrationPushingQueue
    {
        IntegrationPushingQueue::where('model_type', ModelType::CUSTOMER)
            ->where('model_id', $this->getCustomer()->id)
            ->where('operation_type', OperationType::UPDATE)
            ->delete();

        $pushingQueue = IntegrationPushingQueue::create([
            'model_type' => ModelType::CUSTOMER,
            'operation_type' => $this->getOperationType(),
            'model_id' => $this->getCustomer()->id,
            'integration_type' => $this->getIntegrationType(),
        ]);
        return $pushingQueue;
    }
    private function createForeDelete(): IntegrationPushingQueue
    {
        $mapper = $this->getMapEloquentCustomerToThirdParty();
        $mapper->map($this->getCustomer());
        $pushingQueue = IntegrationPushingQueue::create([
            'model_type' => ModelType::CUSTOMER,
            'operation_type' => $this->getOperationType(),
            'model_id' => $this->getCustomer()->id,
            'third_party_object' => json_encode($mapper->asArray()),
            'integration_type' => $this->getIntegrationType(),
        ]);
        return $pushingQueue;
    }



}
