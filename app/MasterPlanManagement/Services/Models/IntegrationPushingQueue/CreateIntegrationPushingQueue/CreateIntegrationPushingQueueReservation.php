<?php

namespace App\MasterPlanManagement\Services\Models\IntegrationPushingQueue\CreateIntegrationPushingQueue;

use Exception;
use App\Reservation;
use App\IntegrationPushingQueue;
use App\MasterPlanManagement\Services\Models\IntegrationPushingQueue\ModelType\ModelType;
use App\MasterPlanManagement\Services\Models\IntegrationPushingQueue\OperationType\OperationType;
use App\MasterPlanManagement\Services\Models\IntegrationPushingQueue\IntegrationType\IntegrationType;
use App\MasterPlanManagement\Services\Integrations\Push\ThirdPartyObjects\Reservation\MapEloquentReservationToThirdPartyInterface;

class CreateIntegrationPushingQueueReservation implements CreateIntegrationPushingQueueInterface
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */

    private $reservation;
    private $integrationType;
    private $operationType;
    private $mapEloquentReservationToThirdParty;

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Getters & Setters
    |--------------------------------------------------------------------------
    |
    */

    private function getReservation(): Reservation
    {
        return $this->reservation;
    }

    public function setReservation(Reservation $reservation): self
    {
        $this->reservation = $reservation;
        return $this;
    }

    private function getOperationType(): string
    {
        return $this->operationType;
    }

    public function setOperationType(string $operationType): self
    {
        $this->operationType = $operationType;
        return $this;
    }

    private function getIntegrationType(): string
    {
        return $this->integrationType;
    }

    public function setIntegrationType(string $integrationType): self
    {
        $this->integrationType = $integrationType;
        return $this;
    }

    private function getMapEloquentReservationToThirdParty(): MapEloquentReservationToThirdPartyInterface
    {
        return $this->mapEloquentReservationToThirdParty;
    }

    public function setMapEloquentReservationToThirdParty(
        MapEloquentReservationToThirdPartyInterface $mapEloquentReservationToThirdParty
    ): self {
        $this->mapEloquentReservationToThirdParty = $mapEloquentReservationToThirdParty;
        return $this;
    }

    /*
    |--------------------------------------------------------------------------
    | Interfaces implementations
    |--------------------------------------------------------------------------
    |
    */

    public function create(): IntegrationPushingQueue
    {
        $this->validate();
        return $this->factoryCreate();
    }
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    private function validate()
    {
        if (empty($this->getReservation()->id)) {
            throw new Exception('Eloquent reservation id not set.');
        }

        $operationTypeString = $this->getOperationType();
        if (empty($operationTypeString)) {
            throw new Exception('Operation type not set.');
        }
        $operationType = new OperationType();
        if (!in_array($operationTypeString, $operationType->getList())) {
            throw new Exception('Operation type not compatible.');
        }


        $integrationTypeString =  $this->getIntegrationType();
        if (empty($integrationTypeString)) {
            throw new Exception('Integration type not set.');
        }
        $integrationType = new IntegrationType();
        if (!in_array($integrationTypeString, $integrationType->getList())) {
            throw new Exception('Integration type not compatible.');
        }

        if (empty($this->getMapEloquentReservationToThirdParty())) {
            throw new Exception('Map eloquent reservation to third party not set.');
        }
    }

    private function factoryCreate(): IntegrationPushingQueue
    {
        if ($this->getOperationType() == OperationType::CREATE) {
            return $this->createForeCreate();
        }
        if ($this->getOperationType() == OperationType::UPDATE) {
            return $this->createForeUpdate();
        }
        if ($this->getOperationType() == OperationType::DELETE) {
            return $this->createForeDelete();
        }
    }

    private function createForeCreate(): IntegrationPushingQueue
    {
        $pushingQueue = IntegrationPushingQueue::create([
            'model_type' => ModelType::RESERVATION,
            'operation_type' => $this->getOperationType(),
            'model_id' => $this->getReservation()->id,
            'integration_type' => $this->getIntegrationType(),
        ]);
        return $pushingQueue;
    }
    private function createForeUpdate(): IntegrationPushingQueue
    {
        IntegrationPushingQueue::where('model_type', ModelType::RESERVATION)
            ->where('model_id', $this->getReservation()->id)
            ->where('operation_type', OperationType::UPDATE)
            ->delete();

        $pushingQueue = IntegrationPushingQueue::create([
            'model_type' => ModelType::RESERVATION,
            'operation_type' => $this->getOperationType(),
            'model_id' => $this->getReservation()->id,
            'integration_type' => $this->getIntegrationType(),
        ]);
        return $pushingQueue;
    }
    private function createForeDelete(): IntegrationPushingQueue
    {
        $mapper = $this->getMapEloquentReservationToThirdParty();
        $mapper->map($this->getReservation());
        $pushingQueue = IntegrationPushingQueue::create([
            'model_type' => ModelType::RESERVATION,
            'operation_type' => $this->getOperationType(),
            'model_id' => $this->getReservation()->id,
            'third_party_object' => json_encode($mapper->asArray()),
            'integration_type' => $this->getIntegrationType(),
        ]);
        return $pushingQueue;
    }



}
