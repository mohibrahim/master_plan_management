<?php

namespace App\MasterPlanManagement\Services\Models\IntegrationPushingQueue\IntegrationType;


class IntegrationType
{
    const NETSUITE = 'net_suite';

    public function getList(): array
    {
        return [
            self::NETSUITE => self::NETSUITE,
        ];
    }
}
