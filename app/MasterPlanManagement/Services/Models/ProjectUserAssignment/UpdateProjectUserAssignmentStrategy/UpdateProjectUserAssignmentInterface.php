<?php

namespace App\MasterPlanManagement\Services\Models\ProjectUserAssignment\UpdateProjectUserAssignmentStrategy;

use App\ProjectUserAssignment;
use Illuminate\Http\Request;

interface UpdateProjectUserAssignmentInterface
{
    public function update($projectUserAssignmentId, Request $request): ProjectUserAssignment;
}