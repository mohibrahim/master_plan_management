<?php

namespace App\MasterPlanManagement\Services\Models\ProjectUserAssignment\UpdateProjectUserAssignmentStrategy;

use App\User;
use Illuminate\Http\Request;
use App\ProjectUserAssignment;

class UpdateProjectUserAssignment implements UpdateProjectUserAssignmentInterface
{

    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */
    public function update($projectUserAssignmentId, Request $request): ProjectUserAssignment
    {
        $projectUserAssignment = ProjectUserAssignment::findOrFail($projectUserAssignmentId);

        $attributes = $request->all();
        $attributes['last_updater_id'] = auth()->id();
        
        $projectsIds = $attributes['project_id'];
        $employeeId = $attributes['employee_id'];
        if (!empty($projectsIds)) {
            //delete old project user assignments
            $employee = User::find($employeeId);
            if (isset($employee)) {
                $employee->projectUserAssignments()->delete();
            }
            
            foreach ($projectsIds as $projectId) {
                $attributes['project_id'] = $projectId;
                $projectUserAssignment = ProjectUserAssignment::create($attributes);
            }
            
        }
        return $projectUserAssignment;
    }

}