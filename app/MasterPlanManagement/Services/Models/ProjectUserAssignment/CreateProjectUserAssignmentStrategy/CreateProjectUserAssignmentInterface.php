<?php

namespace App\MasterPlanManagement\Services\Models\ProjectUserAssignment\CreateProjectUserAssignmentStrategy;

use App\ProjectUserAssignment;
use Illuminate\Http\Request;

interface CreateProjectUserAssignmentInterface
{
    public function create(Request $request): ProjectUserAssignment;
}