<?php

namespace App\MasterPlanManagement\Services\Models\ProjectUserAssignment\CreateProjectUserAssignmentStrategy;

use App\ProjectUserAssignment;
use App\Zone;
use Illuminate\Http\Request;

class CreateProjectUserAssignment implements CreateProjectUserAssignmentInterface
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
  
    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function create(Request $request): ProjectUserAssignment
    {
        
        $attributes = $request->all();
        $attributes['creator_id'] = auth()->id();
        
        $projectsIds = $attributes['project_id'];
        
        foreach ($projectsIds as $projectId) {
            $attributes['project_id'] = $projectId;
            $projectUserAssignment = ProjectUserAssignment::create($attributes);
        }

        return $projectUserAssignment;
    }

}