<?php

namespace App\MasterPlanManagement\Services\Models\UnitModel\UpdateUnitModelStrategy;

use App\Project;
use App\UnitModel;
use Illuminate\Http\Request;
use App\MasterPlanManagement\Services\Models\UnitModel\UnitModelTypeStrategy\UnitModelTypeAbstract;

class UpdateUnitModelNetSuiteIntegration implements UpdateUnitModelInterface
{

    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */
    public function update($unitModelId, Request $request): UnitModel
    {
        $unitModel = UnitModel::findOrFail($unitModelId);
        $attributes = $request->all();
        $attributes['last_updater_id'] = auth()->id();
        $attributes['type'] = UnitModelTypeAbstract::FLAT;
        $unitModel->update($attributes);
        $project = Project::where('net_suite_internal_id', $attributes['project_net_suite_internal_id'])->firstOrFail();
        $unitModel->projects()->sync($project->id);
        return $unitModel->fresh();
    }

}
