<?php

namespace App\MasterPlanManagement\Services\Models\UnitModel\UpdateUnitModelStrategy;

use Illuminate\Http\Request;
use App\UnitModel;

interface UpdateUnitModelInterface
{
    public function update($unitModelId, Request $request): UnitModel;
}