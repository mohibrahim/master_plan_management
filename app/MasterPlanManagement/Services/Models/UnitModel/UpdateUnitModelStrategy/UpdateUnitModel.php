<?php

namespace App\MasterPlanManagement\Services\Models\UnitModel\UpdateUnitModelStrategy;

use App\UnitModel;
use Illuminate\Http\Request;

class UpdateUnitModel implements UpdateUnitModelInterface
{

    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */
    public function update($unitModelId, Request $request): UnitModel
    {
        $unitModel = UnitModel::findOrFail($unitModelId);
        $attributes = $request->all();
        $attributes['last_updater_id'] = auth()->id();
        $unitModel->update($attributes);
        return $unitModel->fresh();
    }

}