<?php
namespace App\MasterPlanManagement\Services\Models\UnitModel\UnitModelBusinessTypeStrategy;


class UnitModelBusinessTypeEnglish extends UnitModelBusinessTypeAbstract
{

    public static function list(): array
    {
        return [
            self::PRIVATE_SECTOR => 'Private sector',
            self::GOVERNMENT_SECTOR => 'Government sector',
            self::BANKING_SECTOR => 'Banking sector',
        ];
    }
}
