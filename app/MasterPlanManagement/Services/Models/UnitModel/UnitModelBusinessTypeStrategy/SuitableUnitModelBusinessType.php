<?php
namespace App\MasterPlanManagement\Services\Models\UnitModel\UnitModelBusinessTypeStrategy;

use App\MasterPlanManagement\Services\LanguageLocator\LanguageLocatorTrait;


class SuitableUnitModelBusinessType
{
    use LanguageLocatorTrait;

    private $unitModelBusinessTypeArabic;
    private $unitModelBusinessTypeEnglish;


    public function __construct(UnitModelBusinessTypeArabic $unitModelBusinessTypeArabic, UnitModelBusinessTypeEnglish $unitModelBusinessTypeEnglish)
    {
        $this->unitModelBusinessTypeArabic = $unitModelBusinessTypeArabic;
        $this->unitModelBusinessTypeEnglish = $unitModelBusinessTypeEnglish;
    }

    public  function getList(): array
    {
        $list = [];
        if ($this->isArabic()) {
            $list = $this->unitModelBusinessTypeArabic->list();
        } elseif ($this->isEnglish()) {
            $list = $this->unitModelBusinessTypeEnglish->list();
        }
        return $list;
    }

    public function selectedValue(?string $key): string
    {
        $value ='';
        if ($this->isArabic()) {
            $list = $this->unitModelBusinessTypeArabic->list();
            if (empty($list[$key])){
                return $value;
            }
            $value = $list[$key];
        } elseif ($this->isEnglish()) {
            $list = $this->unitModelBusinessTypeEnglish->list();
            if (empty($list[$key])){
                return $value;
            }
            $value = $list[$key];
        } 
        return $value;
    }
}