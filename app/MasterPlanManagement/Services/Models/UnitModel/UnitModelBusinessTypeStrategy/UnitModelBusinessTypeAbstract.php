<?php

namespace App\MasterPlanManagement\Services\Models\UnitModel\UnitModelBusinessTypeStrategy;


abstract class UnitModelBusinessTypeAbstract
{
    const PRIVATE_SECTOR = 'private_sector';
    const GOVERNMENT_SECTOR = 'government_sector';
    const BANKING_SECTOR = 'banking_sector';

    abstract static function list(): array;
}
