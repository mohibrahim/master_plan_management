<?php

namespace App\MasterPlanManagement\Services\Models\UnitModel\UnitModelBusinessTypeStrategy;


class UnitModelBusinessTypeArabic extends UnitModelBusinessTypeAbstract
{

    public static function list(): array
    {
        return [
            self::PRIVATE_SECTOR => 'القطاع الخاص',
            self::GOVERNMENT_SECTOR => 'القطاع الحكومي',
            self::BANKING_SECTOR => 'القطاع مصرفي',
        ];
    }
}
