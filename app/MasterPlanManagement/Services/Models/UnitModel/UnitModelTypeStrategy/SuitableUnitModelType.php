<?php
namespace App\MasterPlanManagement\Services\Models\UnitModel\UnitModelTypeStrategy;

use App\MasterPlanManagement\Services\LanguageLocator\LanguageLocatorTrait;


class SuitableUnitModelType
{
    use LanguageLocatorTrait;

    private $unitModelTypeArabic;
    private $unitModelTypeEnglish;


    public function __construct(UnitModelTypeArabic $unitModelTypeArabic, UnitModelTypeEnglish $unitModelTypeEnglish)
    {
        $this->unitModelTypeArabic = $unitModelTypeArabic;
        $this->unitModelTypeEnglish = $unitModelTypeEnglish;
    }

    public  function getList(): array
    {
        $list = [];
        if ($this->isArabic()) {
            $list = $this->unitModelTypeArabic->list();
        } elseif ($this->isEnglish()) {
            $list = $this->unitModelTypeEnglish->list();
        }
        return $list;
    }

    public function selectedValue(?string $key): string
    {
        $value ='';
        if ($this->isArabic()) {
            $list = $this->unitModelTypeArabic->list();
            if (empty($list[$key])){
                return $value;
            }
            $value = $list[$key];
        } elseif ($this->isEnglish()) {
            $list = $this->unitModelTypeEnglish->list();
            if (empty($list[$key])){
                return $value;
            }
            $value = $list[$key];
        } 
        return $value;
    }
}