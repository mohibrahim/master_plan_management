<?php

namespace App\MasterPlanManagement\Services\Models\UnitModel\UnitModelTypeStrategy;


class UnitModelTypeArabic extends UnitModelTypeAbstract
{

    public static function list(): array
    {
        return [
            self::RESIDENTIAL_BUILDING => 'مبنى سكني',
            self::BLOCK => 'بلوك',
            self::FLAT => 'شقة',
            self::APARTMENT_SINGLE => 'شقة سنجل',
            self::APARTMENT_DUPLEX => 'شقة دوبلكس',
            self::ROOF => 'رووف',
            self::VILLA => 'فيلا',
            self::PALACE => 'قصر',
            self::MALL => 'مجمع تجاري',
            self::SHOP => 'متجر',
            self::CLUB => 'النادي',
            self::GARDEN => 'حديقة',
            self::SCHOOL => 'مدرسة',
            self::BANK => 'بنك',
            self::MOSQUE => 'مسجد',
            self::CHURCH => 'كنيسة',
            self::PARKING => 'موقف سيارات',
            self::AMUSEMENT_PARK => 'متنزه',
            self::MUSEUM => 'متحف',
            self::COURT => 'صالة',
            self::HOSPITAL => 'مستشفى',
            self::POLICE_STATION => 'قسم شرطة',
            self::FIREFIGHTER_STATION => 'محطة إطفاء الحرائق',
            self::BUILDING => 'بناء',
            self::AIR_PORT => 'مطار',
        ];
    }
}
