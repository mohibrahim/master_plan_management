<?php

namespace App\MasterPlanManagement\Services\Models\UnitModel\UnitModelTypeStrategy;


abstract class UnitModelTypeAbstract
{
    const RESIDENTIAL_BUILDING = 'residential_building';
    const BLOCK = 'block';
    const FLAT = 'flat';
    const APARTMENT_SINGLE = 'apartment_single';
    const APARTMENT_DUPLEX = 'apartment_duplex';
    const ROOF = 'roof';
    const VILLA = 'villa';
    const PALACE = 'palace';
    const MALL = 'mall';
    const SHOP = 'shop';
    const CLUB = 'club';
    const GARDEN = 'garden';
    const SCHOOL = 'school';
    const BANK = 'bank';
    const MOSQUE = 'mosque';
    const CHURCH = 'church';
    const PARKING = 'parking';
    const AMUSEMENT_PARK = 'amusement_park';
    const MUSEUM = 'museum';
    const COURT = 'court"food courts"';
    const HOSPITAL = 'hospital';
    const POLICE_STATION = 'police_station';
    const FIREFIGHTER_STATION = 'firefighter_station';
    const BUILDING = 'building';
    const AIR_PORT = 'air_port';

    abstract static function list(): array;
}
