<?php
namespace App\MasterPlanManagement\Services\Models\UnitModel\UnitModelTypeStrategy;


class UnitModelTypeEnglish extends UnitModelTypeAbstract
{

    public static function list(): array
    {
        return [
            self::RESIDENTIAL_BUILDING => 'Residential building',
            self::BLOCK => 'Block',
            self::FLAT => 'Flat',
            self::APARTMENT_SINGLE => 'Apartment single',
            self::APARTMENT_DUPLEX => 'Apartment duplex',
            self::ROOF => 'Roof',
            self::VILLA => 'Villa',
            self::PALACE => 'Palace',
            self::MALL => 'Mall',
            self::SHOP => 'Shop',
            self::CLUB => 'Club',
            self::GARDEN => 'Garden',
            self::SCHOOL => 'School',
            self::BANK => 'Bank',
            self::MOSQUE => 'Mosque',
            self::CHURCH => 'Church',
            self::PARKING => 'Parking',
            self::AMUSEMENT_PARK => 'Amusement park',
            self::MUSEUM => 'Museum',
            self::COURT => 'Court',
            self::HOSPITAL => 'Hospital',
            self::POLICE_STATION => 'Police station',
            self::FIREFIGHTER_STATION => 'Fire fighter station',
            self::BUILDING => 'Building',
            self::AIR_PORT => 'Air port',
        ];
    }
}
