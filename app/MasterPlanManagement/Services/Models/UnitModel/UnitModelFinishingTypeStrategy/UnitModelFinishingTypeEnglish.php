<?php
namespace App\MasterPlanManagement\Services\Models\UnitModel\UnitModelFinishingTypeStrategy;


class UnitModelFinishingTypeEnglish extends UnitModelFinishingTypeAbstract
{

    public static function list(): array
    {
        return [
            self::WITHOUT_FINISHING => 'Without finishing',
            self::HALF_FINISHING => 'Half finishing',
            self::FULL_FINISHING => 'Full finishing',
            self::LUXE => 'Luxe',
            self::SUPER_LUXE => 'Super luxe',
            self::DELUXE => 'Deluxe',
        ];
    }
}
