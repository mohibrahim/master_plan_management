<?php

namespace App\MasterPlanManagement\Services\Models\UnitModel\UnitModelFinishingTypeStrategy;


abstract class UnitModelFinishingTypeAbstract
{

    const WITHOUT_FINISHING = 'without_finishing';
    const HALF_FINISHING = 'half_finishing';
    const FULL_FINISHING = 'full_finishing';
    const LUXE = 'luxe';
    const SUPER_LUXE = 'super_luxe';
    const DELUXE = 'deluxe';

    abstract static function list(): array;
}
