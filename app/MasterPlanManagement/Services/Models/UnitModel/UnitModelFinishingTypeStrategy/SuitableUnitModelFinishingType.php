<?php
namespace App\MasterPlanManagement\Services\Models\UnitModel\UnitModelFinishingTypeStrategy;

use App\MasterPlanManagement\Services\LanguageLocator\LanguageLocatorTrait;


class SuitableUnitModelFinishingType
{
    use LanguageLocatorTrait;

    private $unitModelFinishingTypeArabic;
    private $unitModelFinishingTypeEnglish;


    public function __construct(UnitModelFinishingTypeArabic $unitModelFinishingTypeArabic, UnitModelFinishingTypeEnglish $unitModelFinishingTypeEnglish)
    {
        $this->unitModelFinishingTypeArabic = $unitModelFinishingTypeArabic;
        $this->unitModelFinishingTypeEnglish = $unitModelFinishingTypeEnglish;
    }

    public  function getList(): array
    {
        $list = [];
        if ($this->isArabic()) {
            $list = $this->unitModelFinishingTypeArabic->list();
        } elseif ($this->isEnglish()) {
            $list = $this->unitModelFinishingTypeEnglish->list();
        }
        return $list;
    }

    public function selectedValue(?string $key): string
    {
        $value ='';
        if ($this->isArabic()) {
            $list = $this->unitModelFinishingTypeArabic->list();
            if (empty($list[$key])){
                return $value;
            }
            $value = $list[$key];
        } elseif ($this->isEnglish()) {
            $list = $this->unitModelFinishingTypeEnglish->list();
            if (empty($list[$key])){
                return $value;
            }
            $value = $list[$key];
        } 
        return $value;
    }
}