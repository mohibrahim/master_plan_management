<?php

namespace App\MasterPlanManagement\Services\Models\UnitModel\UnitModelFinishingTypeStrategy;


class UnitModelFinishingTypeArabic extends UnitModelFinishingTypeAbstract
{

    public static function list(): array
    {
        return [
            self::WITHOUT_FINISHING => 'بدون تشطيب',
            self::HALF_FINISHING => 'نصف تشطيب',
            self::FULL_FINISHING => 'تشطيب كامل',
            self::LUXE => 'لوكس',
            self::SUPER_LUXE => 'سوبر لوكس',
            self::DELUXE => 'دي-لوكس',
        ];
    }
}
