<?php

namespace App\MasterPlanManagement\Services\Models\UnitModel\DeleteUnitModelStrategy;

use App\UnitModel;
use App\MasterPlanManagement\Services\Models\Unit\DeleteUnitStrategy\DeleteUnitInterface;
use App\MasterPlanManagement\Services\Models\Catalog\DeleteCatalogStrategy\DeleteCatalogInterface;

class DeleteUnitModel implements DeleteUnitModelInterface
{
    public function delete(UnitModel $unitModel): bool
    {
        $catalogs = $unitModel->catalogs;
        if ($catalogs->isNotEmpty()) {
            $deleteCatalog = resolve(DeleteCatalogInterface::class);
            foreach ($catalogs as $catalog) {
                $deleteCatalog->delete($catalog);
            }
        }

        $units = $unitModel->units;
        if ($units->isNotEmpty()) {
            $deleteUnit = resolve(DeleteUnitInterface::class);
            foreach ($units as $unit) {
                $deleteUnit->delete($unit);
            }
        }
        return $unitModel->delete();
    }
}
