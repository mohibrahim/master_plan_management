<?php

namespace App\MasterPlanManagement\Services\Models\UnitModel\DeleteUnitModelStrategy;

use App\UnitModel;

interface DeleteUnitModelInterface
{
    public function delete(UnitModel $unitModel): bool;
}
