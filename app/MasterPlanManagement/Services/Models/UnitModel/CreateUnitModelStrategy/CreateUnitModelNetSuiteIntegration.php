<?php

namespace App\MasterPlanManagement\Services\Models\UnitModel\CreateUnitModelStrategy;

use App\Project;
use App\UnitModel;
use Illuminate\Http\Request;
use App\MasterPlanManagement\Services\Models\UnitModel\UnitModelTypeStrategy\UnitModelTypeAbstract;

class CreateUnitModelNetSuiteIntegration implements CreateUnitModelInterface
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function create(Request $request): UnitModel
    {
        $attributes = $request->all();
        $attributes['creator_id'] = auth()->id();
        $attributes['type'] = UnitModelTypeAbstract::FLAT;
        $unitModel = UnitModel::create($attributes);
        $project = Project::where('net_suite_internal_id', $attributes['project_net_suite_internal_id'])->firstOrFail();
        $unitModel->projects()->attach($project->id);
        return $unitModel;
    }

}
