<?php

namespace App\MasterPlanManagement\Services\Models\UnitModel\CreateUnitModelStrategy;

use Illuminate\Http\Request;
use App\UnitModel;

interface CreateUnitModelInterface
{
    public function create(Request $request): UnitModel;
}