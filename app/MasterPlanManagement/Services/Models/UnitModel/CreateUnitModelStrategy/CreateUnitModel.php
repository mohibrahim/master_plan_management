<?php

namespace App\MasterPlanManagement\Services\Models\UnitModel\CreateUnitModelStrategy;

use App\UnitModel;
use Illuminate\Http\Request;

class CreateUnitModel implements CreateUnitModelInterface
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
  
    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function create(Request $request): UnitModel
    {
        $attributes = $request->all();
        $attributes['creator_id'] = auth()->id();
        $unitModel = UnitModel::create($attributes);
        return $unitModel;
    }

}