<?php

namespace App\MasterPlanManagement\Services\Models\ProjectFile\CreateProjectFileStrategy;

use Illuminate\Http\Request;
use App\ProjectFile;

class CreateProjectFile implements CreateProjectFileInterface
{

    public function create(Request $request): ProjectFile
    {
        $attributes = $request->all();
        $projectFile = ProjectFile::create($attributes);
        return $projectFile;
    }

}