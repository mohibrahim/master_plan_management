<?php

namespace App\MasterPlanManagement\Services\Models\ProjectFile\CreateProjectFileStrategy;

use Illuminate\Http\Request;
use App\ProjectFile;

interface CreateProjectFileInterface
{
    public function create(Request $request): ProjectFile;
}