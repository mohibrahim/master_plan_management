<?php

namespace App\MasterPlanManagement\Services\Models\User\UpdateUserStrategy;

use Illuminate\Http\Request;
use App\ProjectFile;

class UpdateProjectFile implements UpdateProjectFileInterface
{

    public function update($projectFileId, Request $request): ProjectFile
    {
        $projectFile = ProjectFile::findOrFail($projectFileId);
        $attributes = $request->all();
        $isUpdated = $projectFile->update($attributes);
        return $projectFile;
    }

}