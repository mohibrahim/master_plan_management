<?php

namespace App\MasterPlanManagement\Services\Models\User\UpdateUserStrategy;

use App\ProjectFile;
use Illuminate\Http\Request;


interface UpdateProjectFileInterface
{
    public function update($projectFileId, Request $request): ProjectFile;
}