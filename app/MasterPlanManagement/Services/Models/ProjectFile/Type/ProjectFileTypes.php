<?php

namespace App\MasterPlanManagement\Services\Models\ProjectFile\Type;


class ProjectFileTypes
{
    const IMAGE = 'image';
    const PDF = 'pdf';
}