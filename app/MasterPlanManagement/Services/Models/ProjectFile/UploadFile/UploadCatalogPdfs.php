<?php

namespace App\MasterPlanManagement\Services\Models\ProjectFile\UploadFile;

use App\ProjectFile;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use App\MasterPlanManagement\Services\FileUploader\FileUploaderInterface;

class UploadCatalogPdfs implements UploadInterface
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
    private $fileUploader;
    private $formFileName;
    private $model;
    private $modelRelationshipName;
    private $storageDirectoryName;
    private $projectFileTyp;
    private $formFileTitle;
    private $formFileStatus;
    private $creatorId;
    private $lastUpdaterId;
    private $results;
    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    public function __construct(
        string $formFileName,
        Model $model,
        string $modelRelationshipName,
        string $storageDirectoryName,
        string $projectFileType,
        string $formFileTitle,
        string $fromFileStatus,
        ?int $creatorId,
        ?int $lastUpdaterId
    ) {
        $this->setFileUploader(resolve(FileUploaderInterface::class));
        $this->setFormFileName($formFileName);
        $this->setModel($model);
        $this->setModelRelationshipName($modelRelationshipName);
        $this->setStorageDirectoryName($storageDirectoryName);
        $this->setProjectFileTyp($projectFileType);
        $this->setFormFileTitle($formFileTitle);
        $this->setFormFileStatus($fromFileStatus);
        $this->setCreatorId($creatorId);
        $this->setLastUpdaterId($lastUpdaterId);
    }
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */



    public function getFileUploader(): FileUploaderInterface
    {
        return $this->fileUploader;
    }


    public function setFileUploader(FileUploaderInterface $fileUploader)
    {
        $this->fileUploader = $fileUploader;
        return $this;
    }
    
    

    public function getFormFileName(): string
    {
        return $this->formFileName;
    }


    public function setFormFileName(string $formFileName)
    {
        $this->formFileName = $formFileName;
        return $this;
    }


    public function getModel(): Model
    {
        return $this->model;
    }


    public function setModel(Model $model)
    {
        $this->model = $model;
        return $this;
    }


    public function getModelRelationshipName(): string
    {
        return $this->modelRelationshipName;
    }


    public function setModelRelationshipName(string $modelRelationshipName)
    {
        $this->modelRelationshipName = $modelRelationshipName;
        return $this;
    }


    public function getStorageDirectoryName(): string
    {
        return $this->storageDirectoryName;
    }


    public function setStorageDirectoryName(string $storageDirectoryName)
    {
        $this->storageDirectoryName = $storageDirectoryName;
        return $this;
    }


    public function getProjectFileTyp(): string
    {
        return $this->projectFileTyp;
    }


    public function setProjectFileTyp(string $projectFileTyp)
    {
        $this->projectFileTyp = $projectFileTyp;
        return $this;
    }


    public function getFormFileTitle(): string
    {
        return $this->formFileTitle;
    }


    public function setFormFileTitle(string $formFileTitle)
    {
        $this->formFileTitle = $formFileTitle;
        return $this;
    }


    public function getResults(): array
    {
        return $this->results;
    }


    private function setResults(array $results)
    {
        $this->results = $results;
        return $this;
    }

    public function getFormFileStatus(): string
    {
        return $this->formFileStatus;
    }

    public function setFormFileStatus(string $formFileStatus)
    {
        $this->formFileStatus = $formFileStatus;
        return $this;
    }

    public function getCreatorId(): ?int
    {
        return $this->creatorId;
    }

    public function setCreatorId(?int $creatorId)
    {
        $this->creatorId = $creatorId;
        return $this;
    }

    public function getLastUpdaterId(): ?int
    {
        return $this->lastUpdaterId;
    }

    public function setLastUpdaterId(?int $lastUpdaterId)
    {
        $this->lastUpdaterId = $lastUpdaterId;
        return $this;
    }

    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */
    public function upload(Request $request): void
    {
        $requestAsArray = [];
        //has uploaded file
        if ($request->hasFile($this->getFormFileName()) && is_array($request->file($this->getFormFileName()))) {
            
           
            $requestAsArray = $this->getFileUploader()->add($request, $this->getFormFileName(), $this->getStorageDirectoryName());
            foreach ($requestAsArray[$this->getFormFileName()] as $projectFilesNameKey => $projectFilesName) {
                ProjectFile::create([
                    'name' => $projectFilesName,
                    'type' => $this->getProjectFileTyp(),
                    'title' => $requestAsArray[$this->getFormFileTitle()][$projectFilesNameKey],
                    'status' => $requestAsArray[$this->getFormFileStatus()][$projectFilesNameKey],
                    'catalog_id' => $this->getModel()->id,
                    'creator_id' => $this->getCreatorId(),
                    'last_updater_id' => $this->getLastUpdaterId(),
                ]);
            }
        }

        $this->setResults($requestAsArray);
    }
    
}
