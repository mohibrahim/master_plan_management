<?php

namespace App\MasterPlanManagement\Services\Models\ProjectFile\UploadFile;

use Illuminate\Http\Request;

interface UploadInterface
{
    public function upload(Request $request);
}
