<?php

namespace App\MasterPlanManagement\Services\Models\ProjectFile\UploadFile;

use App\ProjectFile;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use App\MasterPlanManagement\Services\FileUploader\FileUploaderInterface;

class UploadCatalogCoverImage implements UploadInterface
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
    private $fileUploader;
    private $formFileName;
    private $model;
    private $modelRelationshipName;
    private $storageDirectoryName;
    private $projectFileTitle;
    private $projectFileTyp;
    private $results;
    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    public function __construct(
        string $formFileName,
        Model $model,
        string $modelRelationshipName,
        string $storageDirectoryName,
        string $projectFileTitle,
        string $projectFileType
    ) {
        $this->setFileUploader(resolve(FileUploaderInterface::class));
        $this->setFormFileName($formFileName);
        $this->setModel($model);
        $this->setModelRelationshipName($modelRelationshipName);
        $this->setStorageDirectoryName($storageDirectoryName);
        $this->setProjectFileTitle($projectFileTitle);
        $this->setProjectFileTyp($projectFileType);
    }
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */



    public function getFileUploader(): FileUploaderInterface
    {
        return $this->fileUploader;
    }


    public function setFileUploader(FileUploaderInterface $fileUploader)
    {
        $this->fileUploader = $fileUploader;
        return $this;
    }
    
    

    public function getFormFileName(): string
    {
        return $this->formFileName;
    }


    public function setFormFileName(string $formFileName)
    {
        $this->formFileName = $formFileName;
        return $this;
    }


    public function getModel(): Model
    {
        return $this->model;
    }


    public function setModel(Model $model)
    {
        $this->model = $model;
        return $this;
    }


    public function getModelRelationshipName(): string
    {
        return $this->modelRelationshipName;
    }


    public function setModelRelationshipName(string $modelRelationshipName)
    {
        $this->modelRelationshipName = $modelRelationshipName;
        return $this;
    }


    public function getStorageDirectoryName(): string
    {
        return $this->storageDirectoryName;
    }


    public function setStorageDirectoryName(string $storageDirectoryName)
    {
        $this->storageDirectoryName = $storageDirectoryName;
        return $this;
    }


    public function getProjectFileTitle(): string
    {
        return $this->projectFileTitle;
    }


    public function setProjectFileTitle(string $projectFileTitle)
    {
        $this->projectFileTitle = $projectFileTitle;
        return $this;
    }


    public function getProjectFileTyp(): string
    {
        return $this->projectFileTyp;
    }


    public function setProjectFileTyp(string $projectFileTyp)
    {
        $this->projectFileTyp = $projectFileTyp;
        return $this;
    }


    public function getResults(): array
    {
        return $this->results;
    }


    private function setResults(array $results)
    {
        $this->results = $results;
        return $this;
    }

    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */
    public function upload(Request $request): void
    {
        $requestAsArray = [];
        //has uploaded file
        if ($request->hasFile($this->getFormFileName())) {
            $modelRelationshipName = $this->getModelRelationshipName();
            $projectFile = $this->getModel()->$modelRelationshipName;
            //has old file
            if (!empty($projectFile)) {
                $fileName = $projectFile->name;
                $requestAsArray = $this->getFileUploader()->update($request, $this->getFormFileName(), $fileName, $this->getStorageDirectoryName());
                $projectFile->update([
                    'name' => $requestAsArray[$this->getFormFileName()],
                ]);
            }
            //has no old file
            if (empty($projectFile)) {
                $requestAsArray = $this->getFileUploader()->add($request, $this->getFormFileName(), $this->getStorageDirectoryName());
                ProjectFile::create([
                    'name' => $requestAsArray[$this->getFormFileName()],
                    'type' => $this->getProjectFileTyp(),
                    'title' => $this->getProjectFileTitle(),
                    'catalog_cover_image_id' => $this->getModel()->id,
                ]);
            }
        }

        $this->setResults($requestAsArray);
    }

    

    
}
