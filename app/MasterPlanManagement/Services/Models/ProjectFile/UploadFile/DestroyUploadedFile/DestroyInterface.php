<?php

namespace App\MasterPlanManagement\Services\Models\ProjectFile\UploadFile\DestroyUploadedFile;


interface DestroyInterface
{
    public function destroyFile(): void;
}