<?php

namespace App\MasterPlanManagement\Services\Models\ProjectFile\UploadFile\DestroyUploadedFile;

use App\MasterPlanManagement\Services\FileUploader\FileUploaderInterface;
use Illuminate\Database\Eloquent\Model;
use App\ProjectFile;

class Destroy implements DestroyInterface
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
    private $fileUploader;
    private $model;
    private $modelRelationshipName;
    private $storageDirectoryName;
    private $projectFileId;

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */

    public function __construct(
        string $storageDirectoryName,
        int $projectFileId
    ) {
        $this->setFileUploader(resolve(FileUploaderInterface::class));
        $this->setStorageDirectoryName($storageDirectoryName);
        $this->setProjectFileId($projectFileId);
    }


    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */


    public function getFileUploader(): FileUploaderInterface
    {
        return $this->fileUploader;
    }

    public function setFileUploader(FileUploaderInterface $fileUploader)
    {
        $this->fileUploader = $fileUploader;
        return $this;
    }

    public function getModel(): Model
    {
        return $this->model;
    }

    public function setModel(Model $model)
    {
        $this->model = $model;
        return $this;
    }

    public function getModelRelationshipName(): string
    {
        return $this->modelRelationshipName;
    }

    public function setModelRelationshipName(string $modelRelationshipName)
    {
        $this->modelRelationshipName = $modelRelationshipName;
        return $this;
    }

    public function getStorageDirectoryName(): string
    {
        return $this->storageDirectoryName;
    }

    public function setStorageDirectoryName(string $storageDirectoryName)
    {
        $this->storageDirectoryName = $storageDirectoryName;
        return $this;
    }

    public function getProjectFileId(): int
    {
        return $this->projectFileId;
    }

    public function setProjectFileId(int $projectFileId)
    {
        $this->projectFileId = $projectFileId;
        return $this;
    }


    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */
    public function destroyFile(): void
    {
        $projectFile = ProjectFile::findOrFail($this->getProjectFileId());
        $this->getFileUploader()->delete($projectFile->name, $this->getStorageDirectoryName());
        $projectFile->delete();
    }
}
