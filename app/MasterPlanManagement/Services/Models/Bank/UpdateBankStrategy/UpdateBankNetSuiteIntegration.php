<?php

namespace App\MasterPlanManagement\Services\Models\Bank\UpdateBankStrategy;

use App\Bank;
use Illuminate\Http\Request;

class UpdateBankNetSuiteIntegration implements UpdateBankInterface
{

    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */
    public function update($bankId, Request $request): Bank
    {
        $bank = Bank::findOrFail($bankId);
        $attributes = $request->all();
        $bank->update($attributes);
        return $bank->fresh();
    }

}
