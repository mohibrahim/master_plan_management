<?php

namespace App\MasterPlanManagement\Services\Models\Bank\UpdateBankStrategy;

use App\Bank;
use Illuminate\Http\Request;

class UpdateBank implements UpdateBankInterface
{

    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */
    public function update($zoneId, Request $request): Bank
    {
        $zone = Bank::findOrFail($zoneId);
        $attributes = $request->all();
        $zone->update($attributes);
        return $zone->fresh();
    }

}
