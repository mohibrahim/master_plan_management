<?php

namespace App\MasterPlanManagement\Services\Models\Bank\UpdateBankStrategy;

use Illuminate\Http\Request;
use App\Bank;

interface UpdateBankInterface
{
    public function update($bankId, Request $request): Bank;
}
