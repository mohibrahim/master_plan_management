<?php

namespace App\MasterPlanManagement\Services\Models\Bank\DeleteBankStrategy;

use App\Bank;

class DeleteBank implements DeleteBankInterface
{
    public function delete(Bank $bank): bool
    {
        return $bank->delete();
    }
}
