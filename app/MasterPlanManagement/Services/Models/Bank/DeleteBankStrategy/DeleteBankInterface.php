<?php

namespace App\MasterPlanManagement\Services\Models\Bank\DeleteBankStrategy;

use App\Bank;

interface DeleteBankInterface
{
    public function delete(Bank $zone): bool;
}
