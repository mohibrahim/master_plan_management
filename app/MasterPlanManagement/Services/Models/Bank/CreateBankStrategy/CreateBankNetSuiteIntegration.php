<?php

namespace App\MasterPlanManagement\Services\Models\Bank\CreateBankStrategy;

use App\Bank;
use Illuminate\Http\Request;

class CreateBankNetSuiteIntegration implements CreateBankInterface
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function create(Request $request): Bank
    {
        $attributes = $request->all();
        $zone = Bank::create($attributes);
        return $zone;
    }

}
