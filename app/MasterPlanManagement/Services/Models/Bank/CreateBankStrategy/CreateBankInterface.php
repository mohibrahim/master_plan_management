<?php

namespace App\MasterPlanManagement\Services\Models\Bank\CreateBankStrategy;

use Illuminate\Http\Request;
use App\Bank;

interface CreateBankInterface
{
    public function create(Request $request): Bank;
}
