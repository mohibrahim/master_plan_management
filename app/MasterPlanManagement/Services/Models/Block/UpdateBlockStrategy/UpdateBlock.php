<?php

namespace App\MasterPlanManagement\Services\Models\Block\UpdateBlockStrategy;

use App\Block;
use Illuminate\Http\Request;

class UpdateBlock implements UpdateBlockInterface
{

    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*s
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */
    public function update($blockId, Request $request): Block
    {
        $block = Block::findOrFail($blockId);
        $attributes = $request->all();
        $attributes['last_updater_id'] = auth()->id();
        $block->update($attributes);
        return $block->fresh();
    }

}