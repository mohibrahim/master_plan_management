<?php

namespace App\MasterPlanManagement\Services\Models\Block\UpdateBlockStrategy;

use Illuminate\Http\Request;
use App\Block;

interface UpdateBlockInterface
{
    public function update($zoneId, Request $request): Block;
}