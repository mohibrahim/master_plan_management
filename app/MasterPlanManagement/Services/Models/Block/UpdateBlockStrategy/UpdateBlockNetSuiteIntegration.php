<?php

namespace App\MasterPlanManagement\Services\Models\Block\UpdateBlockStrategy;

use App\Block;
use App\Project;
use Illuminate\Http\Request;

class UpdateBlockNetSuiteIntegration implements UpdateBlockInterface
{

    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*s
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */
    public function update($blockId, Request $request): Block
    {
        $block = Block::findOrFail($blockId);
        $attributes = $request->all();
        $project = Project::where('net_suite_internal_id', $attributes['project_net_suite_internal_id'])->firstOrFail();
        $attributes['project_id'] = $project->id;
        $attributes['last_updater_id'] = auth()->id();
        $block->update($attributes);
        return $block->fresh();
    }

}
