<?php

namespace App\MasterPlanManagement\Services\Models\Block\DeleteBlockStrategy;

use App\Block;

interface DeleteBlockInterface
{
    public function delete(Block $block): bool;
}
