<?php

namespace App\MasterPlanManagement\Services\Models\Block\DeleteBlockStrategy;

use App\Block;
use App\MasterPlanManagement\Services\Models\Catalog\DeleteCatalogStrategy\DeleteCatalogInterface;
use App\MasterPlanManagement\Services\Models\Unit\DeleteUnitStrategy\DeleteUnitInterface;

class DeleteBlock implements DeleteBlockInterface
{
    public function delete(Block $block): bool
    {
        $catalogs = $block->catalogs;
        if ($catalogs->isNotEmpty()) {
            $deleteCatalog = resolve(DeleteCatalogInterface::class);
            foreach ($catalogs as $catalog) {
                $deleteCatalog->delete($catalog);
            }
        }

        $units = $block->units;
        if ($units->isNotEmpty()) {
            $deleteUnit = resolve(DeleteUnitInterface::class);
            foreach ($units as $unit) {
                $deleteUnit->delete($unit);
            }
        }

        return $block->delete();
    }
}
