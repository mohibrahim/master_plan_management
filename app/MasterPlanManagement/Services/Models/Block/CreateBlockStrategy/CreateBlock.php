<?php

namespace App\MasterPlanManagement\Services\Models\Block\CreateBlockStrategy;

use App\Block;
use Illuminate\Http\Request;

class CreateBlock implements CreateBlockInterface
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function create(Request $request): Block
    {
        $attributes = $request->all();
        $attributes['creator_id'] = auth()->id();
        $block = Block::create($attributes);
        return $block;
    }

}
