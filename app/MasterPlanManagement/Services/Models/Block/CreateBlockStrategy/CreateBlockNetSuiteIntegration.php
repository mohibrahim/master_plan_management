<?php

namespace App\MasterPlanManagement\Services\Models\Block\CreateBlockStrategy;

use App\Block;
use App\Project;
use Illuminate\Http\Request;

class CreateBlockNetSuiteIntegration implements CreateBlockInterface
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function create(Request $request): Block
    {
        $attributes = $request->all();
        $project = Project::where('net_suite_internal_id', $attributes['project_net_suite_internal_id'])->firstOrFail();
        $attributes['project_id'] = $project->id;
        $attributes['creator_id'] = auth()->id();
        $block = Block::create($attributes);
        return $block;
    }

}
