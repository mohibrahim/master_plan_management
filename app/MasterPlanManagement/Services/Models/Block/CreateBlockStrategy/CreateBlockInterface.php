<?php

namespace App\MasterPlanManagement\Services\Models\Block\CreateBlockStrategy;

use App\Block;
use Illuminate\Http\Request;

interface CreateBlockInterface
{
    public function create(Request $request): Block;
}