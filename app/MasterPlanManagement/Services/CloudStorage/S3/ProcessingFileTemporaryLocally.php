<?php

namespace App\MasterPlanManagement\Services\CloudStorage\S3;

use Illuminate\Support\Facades\Storage;

/**
 *
 */
trait ProcessingFileTemporaryLocally
{
    public function setFileInLocalDisk(string $filePath): string
    {
        Storage::disk('public')->put($filePath, Storage::get($filePath));
        $imagePath = (storage_path('app/public/' . $filePath));
        return $imagePath;
    }


    public function deleteFileFromLocalDiskAndUpdateCloud(string $filePath): void
    {
        Storage::disk('s3')->delete($filePath);
        Storage::disk('s3')->put($filePath, Storage::disk('public')->get($filePath));
        Storage::disk('public')->delete($filePath);
    }
}
