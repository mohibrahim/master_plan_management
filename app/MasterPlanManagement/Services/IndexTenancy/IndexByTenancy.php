<?php

namespace App\MasterPlanManagement\Services\IndexTenancy;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\MasterPlanManagement\Services\IndexTenancy\IndexStrategyInterface;


class IndexByTenancy implements IndexStrategyInterface
{
    public function index(Model $model, User $authenticatedUser, string $permissionName) : Builder
    {
        if (auth()->check()) {
            $permissionsNames = $authenticatedUser->getPermissionsNamesAsArray();
            if (in_array($permissionName, $permissionsNames)) {
                return $model::query();
            }
            return $model->where('creator_id', $authenticatedUser->id);
        }
    }
}