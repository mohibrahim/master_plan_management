<?php 

namespace App\MasterPlanManagement\Services\IndexTenancy;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use App\User;


interface IndexStrategyInterface
{
    public function index(Model $model, User $authenticatedUser, string $permissionName) : Builder;
}