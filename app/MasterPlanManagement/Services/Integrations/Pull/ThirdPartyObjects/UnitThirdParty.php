<?php

namespace App\MasterPlanManagement\Services\Integrations\Pull\ThirdPartyObjects;

use Tebru\Gson\Annotation\SerializedName;

class UnitThirdParty implements ThirdPartyObjectsInterface
{

    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */

    /**
     * @SerializedName("itemid")
     */
    private $code;

    /**
     * @SerializedName("custitem_az_re_unitstatus")
     */
    private $status;

    /**
     * @SerializedName("custitem_az_re_landnumber")
     */
    private $landNumber;

    /**
     * @SerializedName("custitem_az_re_landarea")
     */
    private $landArea;

    /**
     * @SerializedName("custitem_az_re_constructionstatus")
     */
    private $constructionStatus;

    /**
     * @SerializedName("custitem_az_re_bookingamount")
     */
    private $bookingAmount;

    /**
     * @SerializedName("custitem_az_re_minimumbookingamount")
     */
    private $minimumBookingAmount;

    /**
     * @SerializedName("currencyname")
     */
    private $currency;

    /**
     * @SerializedName("custitem_az_re_unitpoc")
     */
    private $percentageOfCompletion;

    /**
     * @SerializedName("custitem_az_re_developercode")
     */
    private $developerName;

    /**
     * @SerializedName("price")
     */
    private $salePrice;

    /**
     * @SerializedName("custitem_az_re_unitnotes")
     */
    private $notes;

    /**
     * @SerializedName("custitem_az_re_project")
     */
    private $projectNetSuiteInternalId;

    /**
     * @SerializedName("custitem_az_re_itemphase")
     */
    private $phaseNetSuiteInternalId;

    /**
     * @SerializedName("custitem_az_re_zone")
     */
    private $zoneNetSuiteInternalId;

    /**
     * @SerializedName("custitem_az_re_unitmodel")
     */
    private $unitModelNetSuiteInternalId;


    /**
     * @SerializedName("custitem_az_re_housearea")
     */
    private $builtUpArea;

    /**
     * @SerializedName("custitem_az_re_numberofbedrooms")
     */
    private $numberOfBedrooms;

    /**
     * @SerializedName("custitem_az_re_noofbathrooms")
     */
    private $numberOfBathrooms;

    /**
     * @SerializedName("custitem_az_re_noofkitchens")
     */
    private $numberOfKitchens;

    /**
     * @SerializedName("custitem_az_re_nooffloors")
     */
    private $numberOfFloors;

    /**
     * @SerializedName("custitem_az_re_numberofbalconies")
     */
    private $numberOfBalconies;

    /**
     * @SerializedName("custitem_az_re_numberofelevators")
     */
    private $numberOfElevators;

    /**
     * @SerializedName("custitem_az_re_swimmingpool")
     */
    private $hasPool;

    /**
     * @SerializedName("custitem_az_re_swimmingpoolarea")
     */
    private $poolArea;

    /**
     * @SerializedName("custitem_az_re_garden")
     */
    private $hasGarden;

    /**
     * @SerializedName("custitem_az_re_gardenarea")
     */
    private $gardenArea;

    /**
     * @SerializedName("custitem_az_re_blocknumber")
     */
    private $blockNetSuiteInternalId;

    /**
     * @SerializedName("internalid")
     */
    private $netSuiteInternalId;

    /**
     * @SerializedName("custitem_az_re_productnumber")
     */
    private $productNumber;

    /**
     * @SerializedName("custitem_az_re_landprice")
     */
    private $landPrice;


    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */

    /**
     * Get the value of code
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set the value of code
     *
     * @return  self
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * Get the value of status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set the value of status
     *
     * @return  self
     */
    public function setStatus($status)
    {
        $this->status = snake_case($status);
        return $this;
    }

    /**
     * Get the value of landNumber
     */
    public function getLandNumber()
    {
        return $this->landNumber;
    }

    /**
     * Set the value of landNumber
     *
     * @return  self
     */
    public function setLandNumber($landNumber)
    {
        $this->landNumber = $landNumber;
        return $this;
    }

    /**
     * Get the value of landArea
     */
    public function getLandArea()
    {
        return $this->landArea;
    }

    /**
     * Set the value of landArea
     *
     * @return  self
     */
    public function setLandArea($landArea)
    {
        $this->landArea = $landArea;
        return $this;
    }



    /**
     * Get the value of numberOfFloors
     */
    public function getNumberOfFloors()
    {
        return $this->numberOfFloors;
    }

    /**
     * Set the value of numberOfFloors
     *
     * @return  self
     */
    public function setNumberOfFloors($numberOfFloors)
    {
        $this->numberOfFloors = (int)$numberOfFloors;
        return $this;
    }

    /**
     * Get the value of numberOfBalconies
     */
    public function getNumberOfBalconies()
    {
        return $this->numberOfBalconies;
    }

    /**
     * Set the value of numberOfBalconies
     *
     * @return  self
     */
    public function setNumberOfBalconies($numberOfBalconies)
    {
        $this->numberOfBalconies = (int)$numberOfBalconies;
        return $this;
    }

    /**
     * Get the value of numberOfBedrooms
     */
    public function getNumberOfBedrooms()
    {
        return $this->numberOfBedrooms;
    }

    /**
     * Set the value of numberOfBedrooms
     *
     * @return  self
     */
    public function setNumberOfBedrooms($numberOfBedrooms)
    {
        $this->numberOfBedrooms = (int)$numberOfBedrooms;
        return $this;
    }

    /**
     * Get the value of numberOfBathrooms
     */
    public function getNumberOfBathrooms()
    {
        return $this->numberOfBathrooms;
    }

    /**
     * Set the value of numberOfBathrooms
     *
     * @return  self
     */
    public function setNumberOfBathrooms($numberOfBathrooms)
    {
        $this->numberOfBathrooms = (int)$numberOfBathrooms;
        return $this;
    }

    /**
     * Get the value of numberOfKitchens
     */
    public function getNumberOfKitchens()
    {
        return $this->numberOfKitchens;
    }

    /**
     * Set the value of numberOfKitchens
     *
     * @return  self
     */
    public function setNumberOfKitchens($numberOfKitchens)
    {
        $this->numberOfKitchens = (int)$numberOfKitchens;
        return $this;
    }

    /**
     * Get the value of numberOfElevators
     */
    public function getNumberOfElevators()
    {
        return $this->numberOfElevators;
    }

    /**
     * Set the value of numberOfElevators
     *
     * @return  self
     */
    public function setNumberOfElevators($numberOfElevators)
    {
        $this->numberOfElevators = (int)$numberOfElevators;
        return $this;
    }

    /**
     * Get the value of hasGarden
     */
    public function getHasGarden()
    {
        return $this->hasGarden;
    }

    /**
     * Set the value of hasGarden
     *
     * @return  self
     */
    public function setHasGarden($hasGarden)
    {
        $this->hasGarden = $hasGarden;
        return $this;
    }

    /**
     * Get the value of gardenArea
     */
    public function getGardenArea()
    {
        return $this->gardenArea;
    }

    /**
     * Set the value of gardenArea
     *
     * @return  self
     */
    public function setGardenArea($gardenArea)
    {
        $this->gardenArea = (int)$gardenArea;
        return $this;
    }

    /**
     * Get the value of hasPool
     */
    public function getHasPool()
    {
        return $this->hasPool;
    }

    /**
     * Set the value of hasPool
     *
     * @return  self
     */
    public function setHasPool($hasPool)
    {
        $this->hasPool = $hasPool;
        return $this;
    }

    /**
     * Get the value of poolArea
     */
    public function getPoolArea()
    {
        return $this->poolArea;
    }

    /**
     * Set the value of poolArea
     *
     * @return  self
     */
    public function setPoolArea($poolArea)
    {
        $this->poolArea = (int)$poolArea;
        return $this;
    }

    /**
     * Get the value of constructionStatus
     */
    public function getConstructionStatus()
    {
        return $this->constructionStatus;
    }

    /**
     * Set the value of constructionStatus
     *
     * @return  self
     */
    public function setConstructionStatus($constructionStatus)
    {
        $this->constructionStatus = $constructionStatus;
        return $this;
    }

    /**
     * Get the value of bookingAmount
     */
    public function getBookingAmount()
    {
        return $this->bookingAmount;
    }

    /**
     * Set the value of bookingAmount
     *
     * @return  self
     */
    public function setBookingAmount($bookingAmount)
    {
        if (empty($bookingAmount)) {
            return null;
        }
        $this->bookingAmount = (float) $bookingAmount;
        return $this;
    }

    /**
     * Get the value of minimumBookingAmount
     */
    public function getMinimumBookingAmount()
    {
        return $this->minimumBookingAmount;
    }

    /**
     * Set the value of minimumBookingAmount
     *
     * @return  self
     */
    public function setMinimumBookingAmount($minimumBookingAmount)
    {
        if (empty($minimumBookingAmount)) {
            return null;
        }
        $this->minimumBookingAmount = (float) $minimumBookingAmount;
        return $this;
    }

    /**
     * Get the value of currency
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set the value of currency
     *
     * @return  self
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * Get the value of percentageOfCompletion
     */
    public function getPercentageOfCompletion()
    {
        return $this->percentageOfCompletion;
    }

    /**
     * Set the value of percentageOfCompletion
     *
     * @return  self
     */
    public function setPercentageOfCompletion($percentageOfCompletion)
    {
        if (empty($percentageOfCompletion)) {
            return null;
        }
        $this->percentageOfCompletion = (int) $percentageOfCompletion;
        return $this;
    }

    /**
     * Get the value of developerName
     */
    public function getDeveloperName()
    {
        return $this->developerName;
    }

    /**
     * Set the value of developerName
     *
     * @return  self
     */
    public function setDeveloperName($developerName)
    {
        $this->developerName = $developerName;
        return $this;
    }

    /**
     * Get the value of salePrice
     */
    public function getSalePrice()
    {
        return $this->salePrice;
    }

    /**
     * Set the value of salePrice
     *
     * @return  self
     */
    public function setSalePrice($salePrice)
    {
        $this->salePrice = (double)$salePrice;
        return $this;
    }

    /**
     * Get the value of notes
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set the value of notes
     *
     * @return  self
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
        return $this;
    }

    /**
     * Get the value of projectNetSuiteInternalId
     */
    public function getProjectNetSuiteInternalId()
    {
        return $this->projectNetSuiteInternalId;
    }

    /**
     * Set the value of projectNetSuiteInternalId
     *
     * @return  self
     */
    public function setProjectNetSuiteInternalId($projectNetSuiteInternalId)
    {
        $this->projectNetSuiteInternalId = $projectNetSuiteInternalId;
        return $this;
    }

    /**
     * Get the value of phaseNetSuiteInternalId
     */
    public function getPhaseNetSuiteInternalId()
    {
        return $this->phaseNetSuiteInternalId;
    }

    /**
     * Set the value of phaseNetSuiteInternalId
     *
     * @return  self
     */
    public function setPhaseNetSuiteInternalId($phaseNetSuiteInternalId)
    {
        $this->phaseNetSuiteInternalId = $phaseNetSuiteInternalId;
        return $this;
    }

    /**
     * Get the value of zoneNetSuiteInternalId
     */
    public function getZoneNetSuiteInternalId()
    {
        return $this->zoneNetSuiteInternalId;
    }

    /**
     * Set the value of zoneNetSuiteInternalId
     *
     * @return  self
     */
    public function setZoneNetSuiteInternalId($zoneNetSuiteInternalId)
    {
        $this->zoneNetSuiteInternalId = $zoneNetSuiteInternalId;
        return $this;
    }

    /**
     * Get the value of unitModelNetSuiteInternalId
     */
    public function getUnitModelNetSuiteInternalId()
    {
        return $this->unitModelNetSuiteInternalId;
    }

    /**
     * Set the value of unitModelNetSuiteInternalId
     *
     * @return  self
     */
    public function setUnitModelNetSuiteInternalId($unitModelNetSuiteInternalId)
    {
        $this->unitModelNetSuiteInternalId = $unitModelNetSuiteInternalId;
        return $this;
    }

    /**
     * Get the value of builtUpArea
     */
    public function getBuiltUpArea()
    {
        return $this->builtUpArea;
    }

    /**
     * Set the value of builtUpArea
     *
     * @return  self
     */
    public function setBuiltUpArea($builtUpArea)
    {
        if (empty($builtUpArea)) {
            return null;
        }
        $this->builtUpArea = (int) $builtUpArea;
        return $this;
    }

    /**
     * Get the value of blockNetSuiteInternalId
     */
    public function getBlockNetSuiteInternalId()
    {
        return $this->blockNetSuiteInternalId;
    }

    /**
     * Set the value of blockNetSuiteInternalId
     *
     * @return  self
     */
    public function setBlockNetSuiteInternalId($blockNetSuiteInternalId)
    {
        $this->blockNetSuiteInternalId = $blockNetSuiteInternalId;
        return $this;
    }

    /**
     * Get the value of netSuiteInternalId
     */
    public function getNetSuiteInternalId()
    {
        return $this->netSuiteInternalId;
    }

    /**
     * Set the value of netSuiteInternalId
     *
     * @return  self
     */
    public function setNetSuiteInternalId($netSuiteInternalId)
    {
        $this->netSuiteInternalId = $netSuiteInternalId;
        return $this;
    }

    /**
     * Get the value of productNumber
     */
    public function getProductNumber()
    {
        return $this->productNumber;
    }

    /**
     * Set the value of productNumber
     *
     * @return  self
     */
    public function setProductNumber($productNumber)
    {
        $this->productNumber = $productNumber;
        return $this;
    }

    /**
     * Get the value of landPrice
     */
    public function getLandPrice()
    {
        return $this->landPrice;
    }

    /**
     * Set the value of landPrice
     *
     * @return  self
     */
    public function setLandPrice($landPrice)
    {
        $this->landPrice = $landPrice;
        return $this;
    }


    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function attributesValuesList(): array
    {
        return [
            'code' => $this->getCode(),
            'status' => $this->getStatus(),
            'land_number' => $this->getLandNumber(),
            'land_area' => $this->getLandArea(),
            'built_up_area' => $this->getBuiltUpArea(),
            'number_of_bedrooms' => $this->getNumberOfBedrooms(),
            'number_of_bathrooms' => $this->getNumberOfBathrooms(),
            'number_of_kitchens' => $this->getNumberOfKitchens(),
            'number_of_floors' => $this->getNumberOfFloors(),
            'number_of_balconies' => $this->getNumberOfBalconies(),
            'number_of_elevators' => $this->getNumberOfElevators(),
            'has_pool' => $this->getHasPool(),
            'pool_area' => $this->getPoolArea(),
            'has_garden' => $this->getHasGarden(),
            'garden_area' => $this->getGardenArea(),
            'construction_status' => $this->getConstructionStatus(),
            'booking_amount' => $this->getBookingAmount(),
            'minimum_booking_amount' => $this->getMinimumBookingAmount(),
            'currency' => $this->getCurrency(),
            'percentage_of_completion' => $this->getPercentageOfCompletion(),
            'developer_name' => $this->getDeveloperName(),
            'sale_price' => $this->getSalePrice(),
            'notes' => $this->getNotes(),
            'project_net_suite_internal_id' => $this->getProjectNetSuiteInternalId(),
            'phase_net_suite_internal_id' => $this->getPhaseNetSuiteInternalId(),
            'zone_net_suite_internal_id' => $this->getZoneNetSuiteInternalId(),
            'net_suite_internal_id' => $this->getUnitModelNetSuiteInternalId(),
            'block_net_suite_internal_id' => $this->getBlockNetSuiteInternalId(),
            'net_suite_internal_id' => $this->getNetSuiteInternalId(),
            'product_number' => $this->getProductNumber(),
            'land_price' => $this->getLandPrice(),
            'unit_model_net_suite_internal_id' => $this->getUnitModelNetSuiteInternalId(),
        ];
    }
}
