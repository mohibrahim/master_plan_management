<?php

namespace App\MasterPlanManagement\Services\Integrations\Pull\ThirdPartyObjects;

use Tebru\Gson\Annotation\SerializedName;

class ProjectThirdParty implements ThirdPartyObjectsInterface
{
    use HelpersTraitsForMapping;

    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */

    /**
     * @SerializedName("companyname")
     */
    private $name;

    /**
     * @SerializedName("custentity_az_re_developername")
     */
    private $ownerName;

    /**
     * @SerializedName("startdate")
     */
    private $actuallyStartDate;

    /**
     * @SerializedName("projectedenddate")
     */
    private $supposedEndDate;

    /**
     * @SerializedName("enddate")
     */
    private $actuallyEndDate;

    /**
     * @SerializedName("custentity_az_re_percentofcompletion")
     */
    private $percentageOfCompletion;

    /**
     * @SerializedName("comments")
     */
    private $notes;

    /**
     * @SerializedName("internalid")
     */
    public $netSuiteInternalId;

    /**
     * @SerializedName("entityid")
     */
    private $projectCode;

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */

    /**
     * Get the value of name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get the value of ownerName
     */
    public function getOwnerName()
    {
        return $this->ownerName;
    }

    /**
     * Set the value of ownerName
     *
     * @return  self
     */
    public function setOwnerName($ownerName)
    {
        $this->ownerName = $ownerName;
        return $this;
    }

    /**
     * Get the value of actuallyStartDate
     */
    public function getActuallyStartDate()
    {
        return $this->actuallyStartDate;
    }

    /**
     * Set the value of actuallyStartDate
     *
     * @return  self
     */
    public function setActuallyStartDate($actuallyStartDate)
    {
        if (!empty($actuallyStartDate)) {
            // $yearMonthDay = $this->monthDayYearToYearMonthDate($actuallyStartDate, '-');
            $this->actuallyStartDate = now()->parse($actuallyStartDate);
        }
        return $this;
    }

    /**
     * Get the value of supposedEndDate
     */
    public function getSupposedEndDate()
    {
        return $this->supposedEndDate;
    }

    /**
     * Set the value of supposedEndDate
     *
     * @return  self
     */
    public function setSupposedEndDate($supposedEndDate)
    {
        if (!empty($supposedEndDate)) {
            // $yearMonthDay = $this->monthDayYearToYearMonthDate($supposedEndDate, '/');
            $this->supposedEndDate = now()->parse($supposedEndDate);
        }
        return $this;
    }

    /**
     * Get the value of actuallyEndDate
     */
    public function getActuallyEndDate()
    {
        return $this->actuallyEndDate;
    }

    /**
     * Set the value of actuallyEndDate
     *
     * @return  self
     */
    public function setActuallyEndDate($actuallyEndDate)
    {
        if (!empty($actuallyEndDate)) {
            // $yearMonthDay = $this->monthDayYearToYearMonthDate($actuallyEndDate, '/');
            return $this->actuallyEndDate = now()->parse($actuallyEndDate);
        }
        return $this;
    }

    /**
     * Get the value of percentageOfCompletion
     */
    public function getPercentageOfCompletion()
    {
        return $this->percentageOfCompletion;
    }

    /**
     * Set the value of percentageOfCompletion
     *
     * @return  self
     */
    public function setPercentageOfCompletion($percentageOfCompletion)
    {
        $this->percentageOfCompletion = (float) $percentageOfCompletion;
        return $this;
    }

    /**
     * Get the value of notes
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set the value of notes
     *
     * @return  self
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
        return $this;
    }

    /**
     * Get the value of netSuiteInternalId
     */
    public function getNetSuiteInternalId()
    {
        return $this->netSuiteInternalId;
    }

    /**
     * Set the value of netSuiteInternalId
     *
     * @return  self
     */
    public function setNetSuiteInternalId($netSuiteInternalId)
    {
        $this->netSuiteInternalId = $netSuiteInternalId;
        return $this;
    }

    /**
     * Get the value of projectCode
     */
    public function getProjectCode()
    {
        return $this->projectCode;
    }

    /**
     * Set the value of projectCode
     *
     * @return  self
     */
    public function setProjectCode($projectCode)
    {
        $this->projectCode = $projectCode;
        return $this;
    }

    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */


    public function attributesValuesList(): array
    {
        return [
            'name' => $this->getName(),
            'owner_name' => $this->getOwnerName(),
            'actually_start_date' => $this->getActuallyStartDate(),
            'supposed_end_date' => $this->getSupposedEndDate(),
            'actually_end_date' => $this->getActuallyEndDate(),
            'percentage_of_completion' => $this->getPercentageOfCompletion(),
            'notes' => $this->getNotes(),
            'net_suite_internal_id' => $this->getNetSuiteInternalId(),
            'project_code' => $this->getProjectCode(),
        ];
    }
}
