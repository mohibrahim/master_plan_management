<?php

namespace App\MasterPlanManagement\Services\Integrations\Pull\ThirdPartyObjects;

use Tebru\Gson\Annotation\SerializedName;


class ReservationThirdParty implements ThirdPartyObjectsInterface
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */

    /**
     * @SerializedName("id")
     */
    private $netSuiteInternalId;

    /**
     * @SerializedName("endcustomer")
     */
    private $customerNetSuiteInternalId;

    /**
     * @SerializedName("bank")
     */
    private $bankNetSuiteInternalId;

    /**
     * @SerializedName("expectedclosedate")
     */
    private $expiryDate;

    /**
     * @SerializedName("trandate")
     */
    private $bookingDate;

    /**
     * @SerializedName("endcustomerunit")
     */
    private $unitNetSuiteInternalId;

    /**
     * @SerializedName("reservationstatus")
     */
    private $status;

    /**
     * @SerializedName("winlossreason")
     */
    private $reason;

    /**
     * @SerializedName("booked")
     */
    private $booking;

    /**
     * @SerializedName("bookingamount")
     */
    private $bookingAmount;

    /**
     * @SerializedName("minimumbookingamount")
     */
    private $minimumBookingAmount;

    /**
     * @SerializedName("paidamount")
     */
    private $paidAmount;

    /**
     * @SerializedName("remainingamount")
     */
    private $remainingAmount;

    /**
     * @SerializedName("documentnumber")
     */
    private $documentNumber;

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Getters & Setters
    |--------------------------------------------------------------------------
    |
    */

    /**
     * Get the value of netSuiteInternalId
     */
    public function getNetSuiteInternalId()
    {
        return $this->netSuiteInternalId;
    }

    /**
     * Set the value of netSuiteInternalId
     *
     * @return  self
     */
    public function setNetSuiteInternalId($netSuiteInternalId)
    {
        $this->netSuiteInternalId = $netSuiteInternalId;
        return $this;
    }

    /**
     * Get the value of customerNetSuiteInternalId
     */
    public function getCustomerNetSuiteInternalId()
    {
        return $this->customerNetSuiteInternalId;
    }

    /**
     * Set the value of customerNetSuiteInternalId
     *
     * @return  self
     */
    public function setCustomerNetSuiteInternalId($customerNetSuiteInternalId)
    {
        $this->customerNetSuiteInternalId = $customerNetSuiteInternalId;
        return $this;
    }

    /**
     * Get the value of bankNetSuiteInternalId
     */
    public function getBankNetSuiteInternalId()
    {
        return $this->bankNetSuiteInternalId;
    }

    /**
     * Set the value of bankNetSuiteInternalId
     *
     * @return  self
     */
    public function setBankNetSuiteInternalId($bankNetSuiteInternalId)
    {
        $this->bankNetSuiteInternalId = $bankNetSuiteInternalId;
        return $this;
    }

    /**
     * Get the value of expiryDate
     */
    public function getExpiryDate()
    {
        return $this->expiryDate;
    }

    /**
     * Set the value of expiryDate
     *
     * @return  self
     */
    public function setExpiryDate($expiryDate)
    {
        if (!empty($expiryDate)) {
            $this->expiryDate = now()->parse($expiryDate);
        }
        return $this;
    }

    /**
     * Get the value of bookingDate
     */
    public function getBookingDate()
    {
        return $this->bookingDate;
    }

    /**
     * Set the value of bookingDate
     *
     * @return  self
     */
    public function setBookingDate($bookingDate)
    {
        if (!empty($bookingDate)) {
            $this->bookingDate = now()->parse($bookingDate);
        }
        return $this;
    }

    /**
     * Get the value of unitNetSuiteInternalId
     */
    public function getUnitNetSuiteInternalId()
    {
        return $this->unitNetSuiteInternalId;
    }

    /**
     * Set the value of unitNetSuiteInternalId
     *
     * @return  self
     */
    public function setUnitNetSuiteInternalId($unitNetSuiteInternalId)
    {
        $this->unitNetSuiteInternalId = $unitNetSuiteInternalId;
        return $this;
    }

    /**
     * Get the value of status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set the value of status
     *
     * @return  self
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Get the value of reason
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * Set the value of reason
     *
     * @return  self
     */
    public function setReason($reason)
    {
        $this->reason = $reason;
        return $this;
    }

    /**
     * Get the value of booking
     */
    public function getBooking()
    {
        return $this->booking;
    }

    /**
     * Set the value of booking
     *
     * @return  self
     */
    public function setBooking($booking)
    {
        $this->booking = $booking;
        return $this;
    }

    /**
     * Get the value of bookingAmount
     */
    public function getBookingAmount()
    {
        return $this->bookingAmount;
    }

    /**
     * Set the value of bookingAmount
     *
     * @return  self
     */
    public function setBookingAmount($bookingAmount)
    {
        $this->bookingAmount = $bookingAmount;
        return $this;
    }

    /**
     * Get the value of minimumBookingAmount
     */
    public function getMinimumBookingAmount()
    {
        return $this->minimumBookingAmount;
    }

    /**
     * Set the value of minimumBookingAmount
     *
     * @return  self
     */
    public function setMinimumBookingAmount($minimumBookingAmount)
    {
        $this->minimumBookingAmount = $minimumBookingAmount;
        return $this;
    }

    /**
     * Get the value of paidAmount
     */
    public function getPaidAmount()
    {
        return $this->paidAmount;
    }

    /**
     * Set the value of paidAmount
     *
     * @return  self
     */
    public function setPaidAmount($paidAmount)
    {
        $this->paidAmount = $paidAmount;
        return $this;
    }

    /**
     * Get the value of remainingAmount
     */
    public function getRemainingAmount()
    {
        return $this->remainingAmount;
    }

    /**
     * Set the value of remainingAmount
     *
     * @return  self
     */
    public function setRemainingAmount($remainingAmount)
    {
        $this->remainingAmount = $remainingAmount;
        return $this;
    }


    /**
     * Get the value of documentNumber
     */
    public function getDocumentNumber()
    {
        return $this->documentNumber;
    }

    /**
     * Set the value of documentNumber
     *
     * @return  self
     */
    public function setDocumentNumber($documentNumber)
    {
        $this->documentNumber = $documentNumber;

        return $this;
    }



    /*
    |--------------------------------------------------------------------------
    | Interfaces implementations
    |--------------------------------------------------------------------------
    |
    */

    public function attributesValuesList(): array
    {
        return [
            'net_suite_internal_id' => $this->getNetSuiteInternalId(),
            'customer_net_suite_internal_id' => $this->getCustomerNetSuiteInternalId(),
            'bank_net_suite_internal_id' => $this->getBankNetSuiteInternalId(),
            'expiry_date' => $this->getExpiryDate(),
            'booking_date' => $this->getBookingDate(),
            'unit_net_suite_internal_id' => $this->getUnitNetSuiteInternalId(),
            'status' => $this->getStatus(),
            'reason' => $this->getReason(),
            'booking' => $this->getBooking(),
            'booking_amount' => $this->getBookingAmount(),
            'minimum_booking_amount' => $this->getMinimumBookingAmount(),
            'paid_amount' => $this->getPaidAmount(),
            'remaining_amount' => $this->getRemainingAmount(),
            'document_number' => $this->getDocumentNumber(),
        ];
    }

    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */
}
