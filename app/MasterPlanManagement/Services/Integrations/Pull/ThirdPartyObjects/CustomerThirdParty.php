<?php

namespace App\MasterPlanManagement\Services\Integrations\Pull\ThirdPartyObjects;

use Tebru\Gson\Annotation\SerializedName;


class CustomerThirdParty implements ThirdPartyObjectsInterface
{
    use HelpersTraitsForMapping;

    /**
     * @SerializedName("companyname")
     */
    private $name;

    /**
     * @SerializedName("phone")
     */
    private $phone;

    /**
     * @SerializedName("email")
     */
    private $email;

    /**
     * @SerializedName("defaultaddress")
     */
    private $address;

    /**
     * @SerializedName("custentity_az_customeridno")
     */
    private $nationalId;

    /**
     * @SerializedName("comments")
     */
    private $notes;

    /**
     * @SerializedName("internalid")
     */
    private $netSuiteInternalId;

    /**
     * @SerializedName("entityid")
     */
    private $customerCode;

    /**
     * @SerializedName("custentity_az_re_vatnumber")
     */
    private $vatRegistrationNumber;

    /**
     * @SerializedName("custentity_az_re_vatnumberexpiry")
     */
    private $vatRegistrationExpiry;

    /**
     * @SerializedName("custentity_az_re_endcustsecphone")
     */
    private $secondaryPhone;

    /**
     * @SerializedName("altphone")
     */
    private $altPhone;



    /**
     * Get the value of name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get the value of phone
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set the value of phone
     *
     * @return  self
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * Get the value of email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Get the value of address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set the value of address
     *
     * @return  self
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * Get the value of nationalId
     */
    public function getNationalId()
    {
        return $this->nationalId;
    }

    /**
     * Set the value of nationalId
     *
     * @return  self
     */
    public function setNationalId($nationalId)
    {
        $this->nationalId = $nationalId;
        return $this;
    }

    /**
     * Get the value of notes
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set the value of notes
     *
     * @return  self
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
        return $this;
    }

    /**
     * Get the value of netSuiteInternalId
     */
    public function getNetSuiteInternalId()
    {
        return $this->netSuiteInternalId;
    }

    /**
     * Set the value of netSuiteInternalId
     *
     * @return  self
     */
    public function setNetSuiteInternalId($netSuiteInternalId)
    {
        $this->netSuiteInternalId = $netSuiteInternalId;
        return $this;
    }

    /**
     * Get the value of customerCode
     */
    public function getCustomerCode()
    {
        return $this->customerCode;
    }

    /**
     * Set the value of customerCode
     *
     * @return  self
     */
    public function setCustomerCode($customerCode)
    {
        $this->customerCode = $customerCode;
        return $this;
    }

    /**
     * Get the value of vatRegistrationNumber
     */
    public function getVatRegistrationNumber()
    {
        return $this->vatRegistrationNumber;
    }

    /**
     * Set the value of vatRegistrationNumber
     *
     * @return  self
     */
    public function setVatRegistrationNumber($vatRegistrationNumber)
    {
        $this->vatRegistrationNumber = $vatRegistrationNumber;
        return $this;
    }

    /**
     * Get the value of vatRegistrationExpiry
     */
    public function getVatRegistrationExpiry()
    {
        return $this->vatRegistrationExpiry;
    }

    /**
     * Set the value of vatRegistrationExpiry
     *
     * @return  self
     */
    public function setVatRegistrationExpiry($vatRegistrationExpiry)
    {
        if (!empty($vatRegistrationExpiry)) {
            $yearMonthDay = $this->monthDayYearToYearMonthDate($vatRegistrationExpiry, '/');
            $this->vatRegistrationExpiry = now()->parse($yearMonthDay);

        }
        return $this;
    }

    /**
     * Get the value of secondaryPhone
     */
    public function getSecondaryPhone()
    {
        return $this->secondaryPhone;
    }

    /**
     * Set the value of secondaryPhone
     *
     * @return  self
     */
    public function setSecondaryPhone($secondaryPhone)
    {
        $this->secondaryPhone = $secondaryPhone;
        return $this;
    }

    /**
     * Get the value of altPhone
     */
    public function getAltPhone()
    {
        return $this->altPhone;
    }

    /**
     * Set the value of altPhone
     *
     * @return  self
     */
    public function setAltPhone($altPhone)
    {
        $this->altPhone = $altPhone;
        return $this;
    }

    public function attributesValuesList(): array
    {
        return [
            'name' => $this->getName(),
            'phone' => $this->getPhone(),
            'email' => $this->getEmail(),
            'address' => $this->getAddress(),
            'national_id' => $this->getNationalId(),
            'notes' => $this->getNotes(),
            'net_suite_internal_id' => $this->getNetSuiteInternalId(),
            'customer_code' => $this->getCustomerCode(),
            'vat_registration_number' => $this->getVatRegistrationNumber(),
            'vat_registration_expiry' => $this->getVatRegistrationExpiry(),
            'secondary_phone' => $this->getSecondaryPhone(),
            'alt_phone  ' => $this->getAltPhone(),
        ];
    }
}
