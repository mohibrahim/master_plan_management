<?php

namespace App\MasterPlanManagement\Services\Integrations\Pull\ThirdPartyObjects;

use Tebru\Gson\Annotation\SerializedName;

class BankThirdParty implements ThirdPartyObjectsInterface
{

    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */

    /**
     * @SerializedName("companyname")
     */
    private $name;

    /**
     * @SerializedName("phone")
     */
    private $phone;

    /**
     * @SerializedName("email")
     */
    private $email;

    /**
     * @SerializedName("internalid")
     */
    private $netSuiteInternalId;

    private $netSuiteUpdatedAt;

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */

    /**
     * Get the value of name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }


    /**
     * Get the value of netSuiteInternalId
     */
    public function getNetSuiteInternalId()
    {
        return $this->netSuiteInternalId;
    }

    /**
     * Set the value of netSuiteInternalId
     *
     * @return  self
     */
    public function setNetSuiteInternalId($netSuiteInternalId)
    {
        $this->netSuiteInternalId = $netSuiteInternalId;
        return $this;
    }

     /**
     * Get the value of phone
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set the value of phone
     *
     * @return  self
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * Get the value of email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }


    /**
     * Get the value of netSuiteUpdatedAt
     */
    public function getNetSuiteUpdatedAt()
    {
        return now();
    }

    /**
     * Set the value of netSuiteUpdatedAt
     *
     * @return  self
     */
    public function setNetSuiteUpdatedAt($netSuiteUpdatedAt)
    {
        $this->netSuiteUpdatedAt = $netSuiteUpdatedAt;
    }

    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function attributesValuesList(): array
    {
        return [
            'name' => $this->getName(),
            'phone' => $this->getPhone(),
            'email' => $this->getEmail(),
            'net_suite_updated_at' => $this->getNetSuiteUpdatedAt(),
            'net_suite_internal_id' => $this->getNetSuiteInternalId(),
        ];
    }



}
