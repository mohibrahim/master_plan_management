<?php

namespace App\MasterPlanManagement\Services\Integrations\Pull\ThirdPartyObjects;


trait HelpersTraitsForMapping
{
    public function monthDayYearToYearMonthDate(string $stringDate, string $splitter): string
    {
        $dateAsArray = explode($splitter, $stringDate);
        $day = $dateAsArray['0'];
        $month = $dateAsArray['1'];
        $year = $dateAsArray['2'];
        $result = $year . $month . $day;
        return $result;
    }
}
