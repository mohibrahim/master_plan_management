<?php
namespace App\MasterPlanManagement\Services\Integrations\Pull\ThirdPartyObjects\MappingStrategy\Listing;


use Illuminate\Support\Collection;

interface MappingInterface
{
    public function mapList(string $jsonAsString, string $thirdPartyObject): Collection;
}
