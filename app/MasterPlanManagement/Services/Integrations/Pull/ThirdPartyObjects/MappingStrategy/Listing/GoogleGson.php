<?php

namespace App\MasterPlanManagement\Services\Integrations\Pull\ThirdPartyObjects\MappingStrategy\Listing;


use Tebru\Gson\Gson;
use Illuminate\Support\Collection;

class GoogleGson implements MappingInterface
{
    public function mapList(string $jsonAsString, string $thirdPartyObject): Collection
    {
        $collection = collect();
        $jsonArray = json_decode($jsonAsString);
        $gson = Gson::builder()->build();
        foreach ($jsonArray as $json) {
            $asString = json_encode($json);
            $object = $gson->fromJson($asString, $thirdPartyObject);
            $collection->add($object);
        }
        return $collection;
    }
}
