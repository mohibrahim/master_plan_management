<?php

namespace App\MasterPlanManagement\Services\Integrations\Pull\ThirdPartyObjects;


interface ThirdPartyObjectsInterface
{
    public function attributesValuesList(): array;
}
