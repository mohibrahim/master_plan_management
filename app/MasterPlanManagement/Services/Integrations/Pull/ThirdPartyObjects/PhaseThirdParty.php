<?php

namespace App\MasterPlanManagement\Services\Integrations\Pull\ThirdPartyObjects;

use Tebru\Gson\Annotation\SerializedName;

class PhaseThirdParty implements ThirdPartyObjectsInterface
{

    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */

    /**
     * @SerializedName("name")
     */
    private $name;

    /**
     * @SerializedName("project")
     */
    private $projectNetSuiteInternalId;

    /**
     * @SerializedName("internalid")
     */
    private $netSuiteInternalId;

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */

    /**
     * Get the value of name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get the value of projectNetSuiteInternalId
     */
    public function getProjectNetSuiteInternalId()
    {
        return $this->projectNetSuiteInternalId;
    }

    /**
     * Set the value of projectNetSuiteInternalId
     *
     * @return  self
     */
    public function setProjectNetSuiteInternalId($projectNetSuiteInternalId)
    {
        $this->projectNetSuiteInternalId = $projectNetSuiteInternalId;
        return $this;
    }

    /**
     * Get the value of netSuiteInternalId
     */
    public function getNetSuiteInternalId()
    {
        return $this->netSuiteInternalId;
    }

    /**
     * Set the value of netSuiteInternalId
     *
     * @return  self
     */
    public function setNetSuiteInternalId($netSuiteInternalId)
    {
        $this->netSuiteInternalId = $netSuiteInternalId;
        return $this;
    }

    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function attributesValuesList(): array
    {
        return [
            'name' => $this->getName(),
            'project_net_suite_internal_id' => $this->getProjectNetSuiteInternalId(),
            'net_suite_internal_id' => $this->getNetSuiteInternalId(),
        ];
    }
}
