<?php

namespace App\MasterPlanManagement\Services\Integrations\Pull\CommunicationStrategy;

use GuzzleHttp\Client;



class Guzzle implements ClientRequestInterface
{
    public function requestClient(string $url, string $authorization): string
    {
        $client = new Client();
        $response = $client->get(
            $url,
            [
                'headers' => [
                    'authorization' => $authorization,
                ]
            ]
        );
        $stringResponseAsJson = $response->getBody()->getContents();
        return $stringResponseAsJson;
    }
}
