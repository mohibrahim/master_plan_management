<?php

namespace App\MasterPlanManagement\Services\Integrations\Pull\CommunicationStrategy;


interface ClientRequestInterface
{
    public function requestClient(string $url, string $authorization): string;
}
