<?php

namespace App\MasterPlanManagement\Services\Integrations\Pull\DataSync;

use App\Reservation;
use Illuminate\Support\Collection;
use App\MasterPlanManagement\Services\Integrations\Pull\ThirdPartyObjects\ReservationThirdParty;
use App\MasterPlanManagement\Services\Models\Reservation\CreateReservationStrategy\CreateReservationNetSuiteIntegration;
use App\MasterPlanManagement\Services\Models\Reservation\DeleteReservationStrategy\DeleteReservationNetSuiteIntegration;
use App\MasterPlanManagement\Services\Models\Reservation\UpdateReservationStrategy\UpdateReservationNetSuiteIntegration;

class ReservationsSyncing implements DataSyncInterface
{
    private $creatingCount = 0;
    private $updatingCount = 0;
    private $deletingCount = 0;

    public function sync(Collection $reservationsThirdPartyObjects): array {
        //check if existing
        if ($reservationsThirdPartyObjects->isEmpty()) {
            return $this->getResponseMessage();
        }

        foreach ($reservationsThirdPartyObjects as $key => $reservationThirdPartyObject) {
            $netSuiteInternalId = $reservationThirdPartyObject->getNetSuiteInternalId();
            $reservation = Reservation::where('net_suite_internal_id', $netSuiteInternalId)
                ->first();
            $this->decideCreateOrUpdate($reservationThirdPartyObject, $reservation);
        }

        //delete other reservations that are not in the $reservationsThirdPartyObjects collection
        $netSuiteInternalIds = $reservationsThirdPartyObjects->pluck('netSuiteInternalId')->toArray();
        $reservationsToDelete = Reservation::whereNotIn('net_suite_internal_id', $netSuiteInternalIds)
            ->orWhereNull('net_suite_internal_id')
            ->get();

        $reservationEraser = new DeleteReservationNetSuiteIntegration();
        $reservationsToDelete->map(function ($reservation, $key) use ($reservationEraser) {
            $reservationEraser->delete($reservation);
            $this->deletingCount += 1;
        });

        return $this->getResponseMessage();
    }

    private function decideCreateOrUpdate(ReservationThirdParty $reservationThirdParty, ?Reservation $reservation)
    {
        if (empty($reservation)) {
            $this->createReservation($reservationThirdParty);
            return;
        }
        if (!empty($reservation)) {
            $this->updateReservation($reservationThirdParty, $reservation);
        }
        return;
    }

    private function createReservation(ReservationThirdParty $reservationThirdParty): Reservation
    {
        $request = request()->replace($reservationThirdParty->attributesValuesList());
        $reservationThirdPartyCreator = new CreateReservationNetSuiteIntegration();
        $reservation = $reservationThirdPartyCreator->create($request);
        if (!empty($reservation->id)) {
            $this->creatingCount += 1;
        }
        return $reservation;
    }

    private function updateReservation(ReservationThirdParty $reservationThirdParty, Reservation $reservation): Reservation
    {
        $request = request()->replace($reservationThirdParty->attributesValuesList());
        $reservationThirdPartyUpdater = new UpdateReservationNetSuiteIntegration();
        $reservation = $reservationThirdPartyUpdater->update($reservation->id, $request);
        $this->updatingCount += 1;
        return $reservation;
    }

    private function getResponseMessage(): array
    {
        return [
            __('reservation.reservation_sync_create_message').$this->creatingCount,
            __('reservation.reservation_sync_update_message').$this->updatingCount,
            __('reservation.reservation_sync_delete_message').$this->deletingCount,
        ];
    }
}
