<?php

namespace App\MasterPlanManagement\Services\Integrations\Pull\DataSync;

use App\Customer;
use Illuminate\Support\Collection;
use App\MasterPlanManagement\Services\Integrations\Pull\ThirdPartyObjects\CustomerThirdParty;
use App\MasterPlanManagement\Services\Models\Customer\CreateCustomerStrategy\CreateCustomerNetSuiteIntegration;
use App\MasterPlanManagement\Services\Models\Customer\DeleteCustomerStrategy\DeleteCustomer;
use App\MasterPlanManagement\Services\Models\Customer\UpdateCustomerStrategy\UpdateCustomerNetSuiteIntegration;

class CustomersSyncing implements DataSyncInterface
{
    private $creatingCount = 0;
    private $updatingCount = 0;
    private $deletingCount = 0;

    public function sync(Collection $customersThirdPartyObjects): array {
        //check if existing
        if ($customersThirdPartyObjects->isEmpty()) {
            return $this->getResponseMessage();
        }

        foreach ($customersThirdPartyObjects as $customerThirdPartyObject) {
            $netSuiteInternalId = $customerThirdPartyObject->getNetSuiteInternalId();
            $customer = Customer::where('net_suite_internal_id', $netSuiteInternalId)
                ->first();
            $this->decideCreateOrUpdate($customerThirdPartyObject, $customer);
        }

        //delete other customer that are not in the $customersThirdPartyObjects collection
        // $netSuiteInternalIds = $customersThirdPartyObjects->pluck('netSuiteInternalId')->toArray();
        // $customersToDelete = Customer::whereNotIn('net_suite_internal_id', $netSuiteInternalIds)
        //     ->orWhereNull('net_suite_internal_id')
        //     ->get();

        // $customerEraser = new DeleteCustomer();
        // $customersToDelete->map(function ($customer, $key) use ($customerEraser) {
        //     $customerEraser->delete($customer);
        //     $this->deletingCount += 1;
        // });
        return $this->getResponseMessage();
    }

    private function decideCreateOrUpdate(CustomerThirdParty $customerThirdParty, ?Customer $customer)
    {
        if (empty($customer)) {
            $this->createCustomer($customerThirdParty);
            return;
        }
        $this->updateCustomer($customerThirdParty, $customer);
        return;
    }

    private function createCustomer(CustomerThirdParty $customerThirdParty): Customer
    {
        $request = request()->replace($customerThirdParty->attributesValuesList());
        $customerThirdPartyCreator = new CreateCustomerNetSuiteIntegration();
        $customer = $customerThirdPartyCreator->create($request);
        $this->creatingCount += 1;
        return $customer;
    }

    private function updateCustomer(CustomerThirdParty $customerThirdParty, Customer $customer): Customer
    {
        $request = request()->replace($customerThirdParty->attributesValuesList());
        $customerThirdPartyUpdater = new UpdateCustomerNetSuiteIntegration();
        $customer = $customerThirdPartyUpdater->update($customer->id, $request);
        $this->updatingCount += 1;
        return $customer;

    }

    private function getResponseMessage(): array
    {
        return [
            __('customer.customer_sync_create_message').$this->creatingCount,
            __('customer.customer_sync_update_message').$this->updatingCount,
            __('customer.customer_sync_delete_message').$this->deletingCount,
        ];
    }
}
