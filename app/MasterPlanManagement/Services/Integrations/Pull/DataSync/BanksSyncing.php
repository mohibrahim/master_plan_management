<?php

namespace App\MasterPlanManagement\Services\Integrations\Pull\DataSync;

use App\Bank;
use Illuminate\Support\Collection;
use App\MasterPlanManagement\Services\Integrations\Pull\ThirdPartyObjects\BankThirdParty;
use App\MasterPlanManagement\Services\Models\Bank\CreateBankStrategy\CreateBankNetSuiteIntegration;
use App\MasterPlanManagement\Services\Models\Bank\UpdateBankStrategy\UpdateBankNetSuiteIntegration;

class BanksSyncing implements DataSyncInterface
{
    private $creatingCount = 0;
    private $updatingCount = 0;
    private $deletingCount = 0;

    public function sync(Collection $banksThirdPartyObjects): array {
        //check if existing
        if ($banksThirdPartyObjects->isEmpty()) {
            return $this->getResponseMessage();
        }

        foreach ($banksThirdPartyObjects as $bankThirdPartyObject) {
            $netSuiteInternalId = $bankThirdPartyObject->getNetSuiteInternalId();
            $bank = Bank::where('net_suite_internal_id', $netSuiteInternalId)
                ->first();
            $this->decideCreateOrUpdate($bankThirdPartyObject, $bank);
        }
        //delete other bank that are not in the $banksThirdPartyObjects collection
        // $netSuiteInternalIds = $banksThirdPartyObjects->pluck('netSuiteInternalId')->toArray();
        // $banksToDelete = Bank::whereNotIn('net_suite_internal_id', $netSuiteInternalIds)
        //     ->orWhereNull('net_suite_internal_id')
        //     ->get();

        // $bankEraser = new DeleteBank();
        // $banksToDelete->map(function ($bank, $key) use ($bankEraser) {
        //     $bankEraser->delete($bank);
        //     $this->deletingCount += 1;
        // });
        return $this->getResponseMessage();
    }

    private function decideCreateOrUpdate(BankThirdParty $bankThirdParty, ?Bank $bank)
    {
        if (empty($bank)) {
            $this->createBank($bankThirdParty);
            return;
        }
        $this->updateBank($bankThirdParty, $bank);
        return;
    }

    private function createBank(BankThirdParty $bankThirdParty): Bank
    {
        $request = request()->replace($bankThirdParty->attributesValuesList());
        $bankThirdPartyCreator = new CreateBankNetSuiteIntegration();
        $bank = $bankThirdPartyCreator->create($request);
        $this->creatingCount += 1;
        return $bank;
    }

    private function updateBank(BankThirdParty $bankThirdParty, Bank $bank): Bank
    {
        $request = request()->replace($bankThirdParty->attributesValuesList());
        $bankThirdPartyUpdater = new UpdateBankNetSuiteIntegration();
        $bank = $bankThirdPartyUpdater->update($bank->id, $request);
        $this->updatingCount += 1;
        return $bank;
    }

    private function getResponseMessage(): array
    {
        return [
            __('bank.bank_sync_create_message').$this->creatingCount,
            __('bank.bank_sync_update_message').$this->updatingCount,
            __('bank.bank_sync_delete_message').$this->deletingCount,
        ];
    }
}
