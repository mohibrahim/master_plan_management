<?php

namespace App\MasterPlanManagement\Services\Integrations\Pull\DataSync;

use App\Unit;
use App\Project;
use Illuminate\Support\Collection;
use App\MasterPlanManagement\Services\Models\Unit\DeleteUnitStrategy\DeleteUnit;
use App\MasterPlanManagement\Services\Integrations\Pull\ThirdPartyObjects\UnitThirdParty;
use App\MasterPlanManagement\Services\Models\Unit\CreateUnitStrategy\CreateUnitNetSuiteIntegration;
use App\MasterPlanManagement\Services\Models\Unit\UpdateUnitStrategy\UpdateUnitNetSuiteIntegration;

class UnitsSyncing implements DataSyncInterface
{
    private $creatingCount = 0;
    private $updatingCount = 0;
    private $deletingCount = 0;

    public function sync(Collection $unitsThirdPartyObjects): array {
        //check if existing
        if ($unitsThirdPartyObjects->isEmpty()) {
            return $this->getResponseMessage();
        }

        foreach ($unitsThirdPartyObjects as $key => $unitThirdPartyObject) {
            $netSuiteInternalId = $unitThirdPartyObject->getNetSuiteInternalId();
            $unitCode = $unitThirdPartyObject->getCode();
            //FIXME: remove next line when unit code from Autocade like third party
            // $unitCode = substr_replace($unitCode, ' ',2,1);
            $unit = Unit::where('net_suite_internal_id', $netSuiteInternalId)
                ->orWhere('code', $unitCode)
                ->first();
            // dd($unit);
            $this->decideCreateOrUpdate($unitThirdPartyObject, $unit);
        }

        //delete other units that are not in the $unitsThirdPartyObjects collection
        // $netSuiteInternalIds = $unitsThirdPartyObjects->pluck('netSuiteInternalId')->toArray();
        // $unitsToDelete = Unit::whereNotIn('net_suite_internal_id', $netSuiteInternalIds)
        //     ->orWhereNull('net_suite_internal_id')
        //     ->get();

        // $unitEraser = new DeleteUnit();
        // $unitsToDelete->map(function ($unit, $key) use ($unitEraser) {
        //     $unitEraser->delete($unit);
        //     $this->deletingCount += 1;
        // });

        return $this->getResponseMessage();
    }

    private function decideCreateOrUpdate(UnitThirdParty $unitThirdParty, ?Unit $unit)
    {
        $projectNetSuiteInternalId = $unitThirdParty->getProjectNetSuiteInternalId();
        $project = Project::where('net_suite_internal_id', $projectNetSuiteInternalId)->first();

        if (empty($project)) {
            return;
        }
        if (empty($unit)) {
            // $this->createUnit($unitThirdParty);
            return;
        }
        $this->updateUnit($unitThirdParty, $unit);
        return;
    }

    private function createUnit(UnitThirdParty $unitThirdParty): Unit
    {
        $request = request()->replace($unitThirdParty->attributesValuesList());
        $unitThirdPartyCreator = new CreateUnitNetSuiteIntegration();
        $unit = $unitThirdPartyCreator->create($request);
        $this->creatingCount += 1;
        return $unit;
    }

    private function updateUnit(UnitThirdParty $unitThirdParty, Unit $unit): Unit
    {
        $request = request()->replace($unitThirdParty->attributesValuesList());
        $unitThirdPartyUpdater = new UpdateUnitNetSuiteIntegration();
        $unit = $unitThirdPartyUpdater->update($unit->id, $request);
        $this->updatingCount += 1;
        return $unit;
    }

    private function getResponseMessage(): array
    {
        return [
            __('unit.unit_sync_create_message').': '.$this->creatingCount,
            __('unit.unit_sync_update_message').': '.$this->updatingCount,
            __('unit.unit_sync_delete_message').': '.$this->deletingCount,
        ];
    }
}
