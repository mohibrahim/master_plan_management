<?php

namespace App\MasterPlanManagement\Services\Integrations\Pull\DataSync;

use App\Phase;
use App\Project;
use Illuminate\Support\Collection;
use App\MasterPlanManagement\Services\Models\Phase\DeletePhaseStrategy\DeletePhase;
use App\MasterPlanManagement\Services\Integrations\Pull\ThirdPartyObjects\PhaseThirdParty;
use App\MasterPlanManagement\Services\Models\Phase\CreatePhaseStrategy\CreatePhaseNetSuiteIntegration;
use App\MasterPlanManagement\Services\Models\Phase\UpdatePhaseStrategy\UpdatePhaseNetSuiteIntegration;

class PhasesSyncing implements DataSyncInterface
{
    private $creatingCount = 0;
    private $updatingCount = 0;
    private $deletingCount = 0;

    public function sync(Collection $phasesThirdPartyObjects): array {
        //check if existing
        if ($phasesThirdPartyObjects->isEmpty()) {
            return $this->getResponseMessage();
        }

        foreach ($phasesThirdPartyObjects as $phaseThirdPartyObject) {
            $netSuiteInternalId = $phaseThirdPartyObject->getNetSuiteInternalId();
            $phase = Phase::where('net_suite_internal_id', $netSuiteInternalId)
                ->first();
            $this->decideCreateOrUpdate($phaseThirdPartyObject, $phase);
        }
        //delete other phase that are not in the $phasesThirdPartyObjects collection
        // $netSuiteInternalIds = $phasesThirdPartyObjects->pluck('netSuiteInternalId')->toArray();
        // $phasesToDelete = Phase::whereNotIn('net_suite_internal_id', $netSuiteInternalIds)
        //     ->orWhereNull('net_suite_internal_id')
        //     ->get();

        // $phaseEraser = new DeletePhase();
        // $phasesToDelete->map(function ($phase, $key) use ($phaseEraser) {
        //     $phaseEraser->delete($phase);
        //     $this->deletingCount += 1;
        // });
        return $this->getResponseMessage();
    }

    private function decideCreateOrUpdate(PhaseThirdParty $phaseThirdParty, ?Phase $phase)
    {
        $projectNetSuiteInternalId = $phaseThirdParty->getProjectNetSuiteInternalId();
        $project = Project::where('net_suite_internal_id', $projectNetSuiteInternalId)->first();
        if (empty($project)) {
            return;
        }
        if (empty($phase)) {
            $this->createPhase($phaseThirdParty);
            return;
        }
        $this->updatePhase($phaseThirdParty, $phase);
        return;
    }

    private function createPhase(PhaseThirdParty $phaseThirdParty): Phase
    {
        $request = request()->replace($phaseThirdParty->attributesValuesList());
        $phaseThirdPartyCreator = new CreatePhaseNetSuiteIntegration();
        $phase = $phaseThirdPartyCreator->create($request);
        $this->creatingCount += 1;
        return $phase;
    }

    private function updatePhase(PhaseThirdParty $phaseThirdParty, Phase $phase): Phase
    {
        $request = request()->replace($phaseThirdParty->attributesValuesList());
        $phaseThirdPartyUpdater = new UpdatePhaseNetSuiteIntegration();
        $phase = $phaseThirdPartyUpdater->update($phase->id, $request);
        $this->updatingCount += 1;
        return $phase;
    }

    private function getResponseMessage(): array
    {
        return [
            __('phase.phase_sync_create_message').$this->creatingCount,
            __('phase.phase_sync_update_message').$this->updatingCount,
            __('phase.phase_sync_delete_message').$this->deletingCount,
        ];
    }
}
