<?php

namespace App\MasterPlanManagement\Services\Integrations\Pull\DataSync;

use App\Project;
use Illuminate\Support\Collection;
use App\MasterPlanManagement\Services\Integrations\Pull\ThirdPartyObjects\ProjectThirdParty;
use App\MasterPlanManagement\Services\Models\Project\CreateProjectStrategy\CreateProjectNetSuiteIntegration;
use App\MasterPlanManagement\Services\Models\Project\DeleteProjectStrategy\DeleteProject;
use App\MasterPlanManagement\Services\Models\Project\UpdateProjectStrategy\UpdateProjectNetSuiteIntegration;

class ProjectsSyncing implements DataSyncInterface
{
    private $creatingCount = 0;
    private $updatingCount = 0;
    private $deletingCount = 0;

    public function sync(Collection $projectsThirdPartyObjects): array {
        //check if existing
        if ($projectsThirdPartyObjects->isEmpty()) {
            return $this->getResponseMessage();
        }

        foreach ($projectsThirdPartyObjects as $projectThirdPartyObject) {
            $netSuiteInternalId = $projectThirdPartyObject->getNetSuiteInternalId();
            $project = Project::where('net_suite_internal_id', $netSuiteInternalId)
                ->first();
            $this->decideCreateOrUpdate($projectThirdPartyObject, $project);
        }

        //delete other project that are not in the $projectsThirdPartyObjects collection
        // $netSuiteInternalIds = $projectsThirdPartyObjects->pluck('netSuiteInternalId')->toArray();
        // $projectsToDelete = Project::whereNotIn('net_suite_internal_id', $netSuiteInternalIds)
        //     ->orWhereNull('net_suite_internal_id')
        //     ->get();

        // $projectEraser = new DeleteProject();
        // $projectsToDelete->map(function ($project, $key) use ($projectEraser) {
        //     $projectEraser->delete($project);
        //     $this->deletingCount += 1;
        // });
        return $this->getResponseMessage();
    }

    private function decideCreateOrUpdate(ProjectThirdParty $projectThirdParty, ?Project $project)
    {
        if (empty($project)) {
            $this->createProject($projectThirdParty);
            return;
        }
        $this->updateProject($projectThirdParty, $project);
        return;
    }

    private function createProject(ProjectThirdParty $projectThirdParty): Project
    {
        $request = request()->replace($projectThirdParty->attributesValuesList());
        $projectThirdPartyCreator = new CreateProjectNetSuiteIntegration();
        $project = $projectThirdPartyCreator->create($request);
        $this->creatingCount += 1;
        return $project;

    }

    private function updateProject(ProjectThirdParty $projectThirdParty, Project $project): Project
    {
        $request = request()->replace($projectThirdParty->attributesValuesList());
        $projectThirdPartyUpdater = new UpdateProjectNetSuiteIntegration();
        $project = $projectThirdPartyUpdater->update($project->id, $request);
        $this->updatingCount += 1;
        return $project;
    }

    private function getResponseMessage(): array
    {
        return [
            __('project.project_sync_create_message').': '.$this->creatingCount,
            __('project.project_sync_update_message').': '.$this->updatingCount,
            __('project.project_sync_delete_message').': '.$this->deletingCount,
        ];
    }
}
