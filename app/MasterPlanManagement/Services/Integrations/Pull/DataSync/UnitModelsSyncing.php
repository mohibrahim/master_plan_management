<?php

namespace App\MasterPlanManagement\Services\Integrations\Pull\DataSync;

use App\UnitModel;
use App\Project;
use Illuminate\Support\Collection;
use App\MasterPlanManagement\Services\Models\UnitModel\DeleteUnitModelStrategy\DeleteUnitModel;
use App\MasterPlanManagement\Services\Integrations\Pull\ThirdPartyObjects\UnitModelThirdParty;
use App\MasterPlanManagement\Services\Models\UnitModel\CreateUnitModelStrategy\CreateUnitModelNetSuiteIntegration;
use App\MasterPlanManagement\Services\Models\UnitModel\UpdateUnitModelStrategy\UpdateUnitModelNetSuiteIntegration;

class UnitModelsSyncing implements DataSyncInterface
{
    private $creatingCount = 0;
    private $updatingCount = 0;
    private $deletingCount = 0;

    public function sync(Collection $unitModelsThirdPartyObjects): array {
        //check if existing
        if ($unitModelsThirdPartyObjects->isEmpty()) {
            return $this->getResponseMessage();
        }

        foreach ($unitModelsThirdPartyObjects as $unitModelThirdPartyObject) {
            $netSuiteInternalId = $unitModelThirdPartyObject->getNetSuiteInternalId();
            $unitModel = UnitModel::where('net_suite_internal_id', $netSuiteInternalId)
                ->first();
            $this->decideCreateOrUpdate($unitModelThirdPartyObject, $unitModel);
        }
        //delete other unit_model that are not in the $unitModelsThirdPartyObjects collection
        // $netSuiteInternalIds = $unitModelsThirdPartyObjects->pluck('netSuiteInternalId')->toArray();
        // $unitModelsToDelete = UnitModel::whereNotIn('net_suite_internal_id', $netSuiteInternalIds)
        //     ->orWhereNull('net_suite_internal_id')
        //     ->get();

        // $unitModelEraser = new DeleteUnitModel();
        // $unitModelsToDelete->map(function ($unitModel, $key) use ($unitModelEraser) {
        //     $unitModelEraser->delete($unitModel);
        //     $this->deletingCount += 1;
        // });
        return $this->getResponseMessage();
    }

    private function decideCreateOrUpdate(UnitModelThirdParty $unitModelThirdParty, ?UnitModel $unitModel)
    {
        $projectNetSuiteInternalId = $unitModelThirdParty->getProjectNetSuiteInternalId();
        $project = Project::where('net_suite_internal_id', $projectNetSuiteInternalId)->first();
        if (empty($project)) {
            return;
        }
        if (empty($unitModel)) {
            $this->createUnitModel($unitModelThirdParty);
            return;
        }
        $this->updateUnitModel($unitModelThirdParty, $unitModel);
        return;
    }

    private function createUnitModel(UnitModelThirdParty $unitModelThirdParty): UnitModel
    {
        $request = request()->replace($unitModelThirdParty->attributesValuesList());
        $unitModelThirdPartyCreator = new CreateUnitModelNetSuiteIntegration();
        $unitModel = $unitModelThirdPartyCreator->create($request);
        $this->creatingCount += 1;
        return $unitModel;
    }

    private function updateUnitModel(UnitModelThirdParty $unitModelThirdParty, UnitModel $unitModel): UnitModel
    {
        $request = request()->replace($unitModelThirdParty->attributesValuesList());
        $unitModelThirdPartyUpdater = new UpdateUnitModelNetSuiteIntegration();
        $unitModel = $unitModelThirdPartyUpdater->update($unitModel->id, $request);
        $this->updatingCount += 1;
        return $unitModel;
    }

    private function getResponseMessage(): array
    {
        return [
            __('unit_model.unit_model_sync_create_message').$this->creatingCount,
            __('unit_model.unit_model_sync_update_message').$this->updatingCount,
            __('unit_model.unit_model_sync_delete_message').$this->deletingCount,
        ];
    }
}
