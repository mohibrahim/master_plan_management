<?php

namespace App\MasterPlanManagement\Services\Integrations\Pull\DataSync;

use Illuminate\Support\Collection;

interface DataSyncInterface
{
    public function sync(Collection $thirdPartyObjects);
}
