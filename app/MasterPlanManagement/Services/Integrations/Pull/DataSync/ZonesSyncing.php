<?php

namespace App\MasterPlanManagement\Services\Integrations\Pull\DataSync;

use App\Zone;
use App\Project;
use Illuminate\Support\Collection;
use App\MasterPlanManagement\Services\Models\Zone\DeleteZoneStrategy\DeleteZone;
use App\MasterPlanManagement\Services\Integrations\Pull\ThirdPartyObjects\ZoneThirdParty;
use App\MasterPlanManagement\Services\Models\Zone\CreateZoneStrategy\CreateZoneNetSuiteIntegration;
use App\MasterPlanManagement\Services\Models\Zone\UpdateZoneStrategy\UpdateZoneNetSuiteIntegration;

class ZonesSyncing implements DataSyncInterface
{
    private $creatingCount = 0;
    private $updatingCount = 0;
    private $deletingCount = 0;

    public function sync(Collection $zonesThirdPartyObjects): array {
        //check if existing
        if ($zonesThirdPartyObjects->isEmpty()) {
            return $this->getResponseMessage();
        }

        foreach ($zonesThirdPartyObjects as $zoneThirdPartyObject) {
            $netSuiteInternalId = $zoneThirdPartyObject->getNetSuiteInternalId();
            $zone = Zone::where('net_suite_internal_id', $netSuiteInternalId)
                ->first();
            $this->decideCreateOrUpdate($zoneThirdPartyObject, $zone);
        }
        //delete other zone that are not in the $zonesThirdPartyObjects collection
        // $netSuiteInternalIds = $zonesThirdPartyObjects->pluck('netSuiteInternalId')->toArray();
        // $zonesToDelete = Zone::whereNotIn('net_suite_internal_id', $netSuiteInternalIds)
        //     ->orWhereNull('net_suite_internal_id')
        //     ->get();

        // $zoneEraser = new DeleteZone();
        // $zonesToDelete->map(function ($zone, $key) use ($zoneEraser) {
        //     $zoneEraser->delete($zone);
        //     $this->deletingCount += 1;
        // });
        return $this->getResponseMessage();
    }

    private function decideCreateOrUpdate(ZoneThirdParty $zoneThirdParty, ?Zone $zone)
    {
        $projectNetSuiteInternalId = $zoneThirdParty->getProjectNetSuiteInternalId();
        $project = Project::where('net_suite_internal_id', $projectNetSuiteInternalId)->first();
        if (empty($project)) {
            return;
        }
        if (empty($zone)) {
            $this->createZone($zoneThirdParty);
            return;
        }
        $this->updateZone($zoneThirdParty, $zone);
        return;
    }

    private function createZone(ZoneThirdParty $zoneThirdParty): Zone
    {
        $request = request()->replace($zoneThirdParty->attributesValuesList());
        $zoneThirdPartyCreator = new CreateZoneNetSuiteIntegration();
        $zone = $zoneThirdPartyCreator->create($request);
        $this->creatingCount += 1;
        return $zone;
    }

    private function updateZone(ZoneThirdParty $zoneThirdParty, Zone $zone): Zone
    {
        $request = request()->replace($zoneThirdParty->attributesValuesList());
        $zoneThirdPartyUpdater = new UpdateZoneNetSuiteIntegration();
        $zone = $zoneThirdPartyUpdater->update($zone->id, $request);
        $this->updatingCount += 1;
        return $zone;
    }

    private function getResponseMessage(): array
    {
        return [
            __('zone.zone_sync_create_message').$this->creatingCount,
            __('zone.zone_sync_update_message').$this->updatingCount,
            __('zone.zone_sync_delete_message').$this->deletingCount,
        ];
    }
}
