<?php

namespace App\MasterPlanManagement\Services\Integrations\Pull\DataSync;

use App\Block;
use App\Project;
use Illuminate\Support\Collection;
use App\MasterPlanManagement\Services\Models\Block\DeleteBlockStrategy\DeleteBlock;
use App\MasterPlanManagement\Services\Integrations\Pull\ThirdPartyObjects\BlockThirdParty;
use App\MasterPlanManagement\Services\Models\Block\CreateBlockStrategy\CreateBlockNetSuiteIntegration;
use App\MasterPlanManagement\Services\Models\Block\UpdateBlockStrategy\UpdateBlockNetSuiteIntegration;

class BlocksSyncing implements DataSyncInterface
{
    private $creatingCount = 0;
    private $updatingCount = 0;
    private $deletingCount = 0;

    public function sync(Collection $blocksThirdPartyObjects): array {
        //check if existing
        if ($blocksThirdPartyObjects->isEmpty()) {
            return $this->getResponseMessage();
        }

        foreach ($blocksThirdPartyObjects as $blockThirdPartyObject) {
            $netSuiteInternalId = $blockThirdPartyObject->getNetSuiteInternalId();
            $block = Block::where('net_suite_internal_id', $netSuiteInternalId)
                ->first();
            $this->decideCreateOrUpdate($blockThirdPartyObject, $block);
        }
        //delete other block that are not in the $blocksThirdPartyObjects collection
        // $netSuiteInternalIds = $blocksThirdPartyObjects->pluck('netSuiteInternalId')->toArray();
        // $blocksToDelete = Block::whereNotIn('net_suite_internal_id', $netSuiteInternalIds)
        //     ->orWhereNull('net_suite_internal_id')
        //     ->get();

        // $blockEraser = new DeleteBlock();
        // $blocksToDelete->map(function ($block, $key) use ($blockEraser) {
        //     $blockEraser->delete($block);
        //     $this->deletingCount += 1;
        // });
        return $this->getResponseMessage();
    }

    private function decideCreateOrUpdate(BlockThirdParty $blockThirdParty, ?Block $block)
    {
        $projectNetSuiteInternalId = $blockThirdParty->getProjectNetSuiteInternalId();
        $project = Project::where('net_suite_internal_id', $projectNetSuiteInternalId)->first();
        if (empty($project)) {
            return;
        }
        if (empty($block)) {
            $this->createBlock($blockThirdParty);
            return;
        }
        $this->updateBlock($blockThirdParty, $block);
        return;
    }

    private function createBlock(BlockThirdParty $blockThirdParty): Block
    {
        $request = request()->replace($blockThirdParty->attributesValuesList());
        $blockThirdPartyCreator = new CreateBlockNetSuiteIntegration();
        $block = $blockThirdPartyCreator->create($request);
        $this->creatingCount += 1;
        return $block;
    }

    private function updateBlock(BlockThirdParty $blockThirdParty, Block $block): Block
    {
        $request = request()->replace($blockThirdParty->attributesValuesList());
        $blockThirdPartyUpdater = new UpdateBlockNetSuiteIntegration();
        $block = $blockThirdPartyUpdater->update($block->id, $request);
        $this->updatingCount += 1;
        return $block;
    }

    private function getResponseMessage(): array
    {
        return [
            __('block.block_sync_create_message').$this->creatingCount,
            __('block.block_sync_update_message').$this->updatingCount,
            __('block.block_sync_delete_message').$this->deletingCount,
        ];
    }
}
