<?php

namespace App\MasterPlanManagement\Services\Integrations\Push\CommunicationStrategy;

use GuzzleHttp\Client;


class GuzzleDelete implements PostToClientInterface
{
    public function send(string $url, string $authorization, array $data): string
    {
        $url = $this->updateUrlQueryStrings($url, $data);
        $client = new Client();
        $response = $client->delete(
            $url,
            [
                'headers' => [
                    'authorization' => $authorization,
                    'Content-type' => 'application/json'
                ],
            ]
        );
        $stringResponseAsJson = $response->getBody()->getContents();
        return $stringResponseAsJson;
    }

    private function updateUrlQueryStrings(string $url, array $data)
    {
        $parsed = parse_url($url);
        $queryStringToArray = explode('&', $parsed['query']);
        $parameters = [];
        foreach ($queryStringToArray as $element) {
            $elementToArray = explode('=', $element);
            $parameters[$elementToArray[0]] = $elementToArray[1];
        }
        foreach ($data as $key => $val) {
            if (array_key_exists($key, $parameters)) {
                $parameters[$key] = $val;
            } else {
                $parameters[$key] = $val;
            }
        }
        $parametersWithEqualSign = [];
        foreach ($parameters as $key => $parameter) {
            $parametersWithEqualSign[] = $key . '=' . $parameter;
        }

        return $parsed['scheme'] . '://' . $parsed['host'] . $parsed['path'] . '?' . implode('&', $parametersWithEqualSign);
    }
}
