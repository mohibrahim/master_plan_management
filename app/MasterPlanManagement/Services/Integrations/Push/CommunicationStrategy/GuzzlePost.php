<?php

namespace App\MasterPlanManagement\Services\Integrations\Push\CommunicationStrategy;

use GuzzleHttp\Client;


class GuzzlePost implements PostToClientInterface
{
    public function send(string $url, string $authorization, array $data): string
    {
        // dd($data);
        $client = new Client();
        $response = $client->post(
            $url,
            [
                'headers' => [
                    'authorization' => $authorization,
                    'Content-type' => 'application/json'
                ],
                'json' =>  $data,
            ]
        );
        $stringResponseAsJson = $response->getBody()->getContents();
        return $stringResponseAsJson;
    }
}
