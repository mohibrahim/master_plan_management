<?php

namespace App\MasterPlanManagement\Services\Integrations\Push\CommunicationStrategy;


interface PostToClientInterface
{
    public function send(string $url, string $authorization, array $data): string;
}
