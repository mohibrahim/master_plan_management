<?php

namespace App\MasterPlanManagement\Services\Integrations\Push\ThirdPartyObjects\Customer;

use App\Customer;

interface MapEloquentCustomerToThirdPartyInterface
{
    public function map(Customer $customer): void;
    public function asArray(): array;
}
