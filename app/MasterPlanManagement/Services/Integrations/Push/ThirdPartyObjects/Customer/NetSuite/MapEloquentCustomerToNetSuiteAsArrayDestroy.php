<?php

namespace App\MasterPlanManagement\Services\Integrations\Push\ThirdPartyObjects\Customer\NetSuite;

use App\Customer;
use App\MasterPlanManagement\Services\Integrations\Push\ThirdPartyObjects\Customer\MapEloquentCustomerToThirdPartyInterface;

class MapEloquentCustomerToNetSuiteAsArrayDestroy implements MapEloquentCustomerToThirdPartyInterface
{
    private $netSuiteCustomerAsArray;

    public function getNetSuiteCustomerAsArray(): array
    {
        return $this->netSuiteCustomerAsArray;
    }

    public function setNetSuiteCustomerAsArray(array $netSuiteCustomerAsArray)
    {
        $this->netSuiteCustomerAsArray = $netSuiteCustomerAsArray;
    }

    public function map(Customer $customer): void
    {
        $data = [];
        $customerNetSuiteInternalId = $customer->net_suite_internal_id;

        $data = [
            "recordtype" => "prospect",
            "id" => $customerNetSuiteInternalId,
        ];

        $this->setNetSuiteCustomerAsArray($data);
    }

    public function asArray(): array
    {
        return $this->getNetSuiteCustomerAsArray();
    }
}
