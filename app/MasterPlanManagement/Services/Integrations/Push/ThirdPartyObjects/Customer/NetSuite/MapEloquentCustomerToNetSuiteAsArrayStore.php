<?php

namespace App\MasterPlanManagement\Services\Integrations\Push\ThirdPartyObjects\Customer\NetSuite;

use App\Customer;
use App\MasterPlanManagement\Services\Integrations\Push\ThirdPartyObjects\Customer\MapEloquentCustomerToThirdPartyInterface;

class MapEloquentCustomerToNetSuiteAsArrayStore implements MapEloquentCustomerToThirdPartyInterface
{
    private $netSuiteCustomerAsArray;

    public function getNetSuiteCustomerAsArray(): array
    {
        return $this->netSuiteCustomerAsArray;
    }

    public function setNetSuiteCustomerAsArray(array $netSuiteCustomerAsArray)
    {
        $this->netSuiteCustomerAsArray = $netSuiteCustomerAsArray;
    }

    public function map(Customer $customer): void
    {
        $data = [];
        // $customerNetSuiteInternalId = $customer->net_suite_internal_id;
        $customerName = $customer->name;
        $phone = $customer->phone;
        $secondaryPhone = $customer->secondary_phone;
        $altPhone = $customer->alt_phone;
        $email = $customer->email;
        $nationalId = $customer->national_id;
        $vatRegistrationNumber = $customer->vat_registration_number;
        $notes = $customer->notes;

        $data = [
            "recordtype" => "prospect",
            "companyname" => $customerName,
            "phone" => $phone,
            "custentity_az_re_endcustsecphone" => $secondaryPhone,
            "custentity_az_re_altphone" => $altPhone,
            "email" => $email,
            "custentity_az_customeridno" => $nationalId,
            "custentity_az_re_vatnumber" => $vatRegistrationNumber,
            "comments" => $notes,
            "subsidiary" => "9",
            "currency" => "5",
        ];

        $this->setNetSuiteCustomerAsArray($data);
    }

    public function asArray(): array
    {
        return $this->getNetSuiteCustomerAsArray();
    }
}
