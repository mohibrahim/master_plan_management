<?php

namespace App\MasterPlanManagement\Services\Integrations\Push\ThirdPartyObjects\Reservation\NetSuite;

use App\Reservation;
use App\MasterPlanManagement\Services\Integrations\Push\ThirdPartyObjects\Reservation\MapEloquentReservationToThirdPartyInterface;

class MapEloquentReservationToNetSuiteAsArrayDestroy implements MapEloquentReservationToThirdPartyInterface
{
    private $netSuiteReservationAsArray;

    public function getNetSuiteReservationAsArray(): array
    {
        return $this->netSuiteReservationAsArray;
    }

    public function setNetSuiteReservationAsArray(array $netSuiteReservationAsArray)
    {
        $this->netSuiteReservationAsArray = $netSuiteReservationAsArray;
    }

    public function map(Reservation $reservation): void
    {
        $data = [];
        $reservationNetSuiteInternalId = $reservation->net_suite_internal_id;
        // $customerNetSuiteInternalId = $reservation->customer->net_suite_internal_id;
        // $unitNetSuiteInternalId = $reservation->unit->net_suite_internal_id;
        // $booking = $reservation->booking;
        // $bookingStatus = $reservation->booking_status;
        // $status = $reservation->status;
        // $expiryDate = $reservation->expiry_date;
        // $reason = $reservation->reason;
        // $salesRepresentative = $reservation->salesRepresentativeName;
        // $remainingAmount = $reservation->remaining_amount;

        if (!empty($reservation->expiry_date)) {
            $expiryDate = now()->parse($reservation->expiry_date)
                ->format('d/m/Y');
        }

        $data = [
            "recordtype" => "opportunity",
            "id" => $reservationNetSuiteInternalId,
            // "entity" => $customerNetSuiteInternalId,
            // "custbody_az_re_unitquot" => $unitNetSuiteInternalId,
            // "expectedclosedate" => $expiryDate,
            // 'custbody_az_re_booked' => $booking,
            // 'custbody_az_re_bookingstatus' => $bookingStatus,
            // 'entitystatus' => $status,
            // 'winlossreason' => $reason,
            // 'salesrep' => $salesRepresentative,
            // 'custbody_az_re_remainingamount' => $remainingAmount,
        ];

        $this->setNetSuiteReservationAsArray($data);
    }

    public function asArray(): array
    {
        return $this->getNetSuiteReservationAsArray();
    }
}
