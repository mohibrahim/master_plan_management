<?php

namespace App\MasterPlanManagement\Services\Integrations\Push\ThirdPartyObjects\Reservation;

use App\Reservation;

interface MapEloquentReservationToThirdPartyInterface
{
    public function map(Reservation $reservation): void;
    public function asArray(): array;
}
