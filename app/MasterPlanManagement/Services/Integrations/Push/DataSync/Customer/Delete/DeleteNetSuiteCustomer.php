<?php

namespace App\MasterPlanManagement\Services\Integrations\Push\DataSync\Customer\Delete;

use Exception;
use App\Customer;
use App\NetSuiteIntegration;
use App\MasterPlanManagement\Services\Integrations\Push\CommunicationStrategy\PostToClientInterface;
use App\MasterPlanManagement\Services\Integrations\Push\ThirdPartyObjects\Customer\NetSuite\MapEloquentCustomerToNetSuiteAsArrayDestroy;

class DeleteNetSuiteCustomer implements DeleteCustomerThirdPartyInterface
{

        /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */

    private $netSuiteIntegration;
    private $postToClientInterface;

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */



    /**
     * Get the value of netSuiteIntegration
     */
    public function getNetSuiteIntegration(): NetSuiteIntegration
    {
        return $this->netSuiteIntegration;
    }

    /**
     * Set the value of netSuiteIntegration
     *
     * @return  self
     */
    public function setNetSuiteIntegration(NetSuiteIntegration $netSuiteIntegration)
    {
        $this->netSuiteIntegration = $netSuiteIntegration;
        return $this;
    }

    /**
     * Get the value of postToClientInterface
     */
    public function getPostToClientInterface(): PostToClientInterface
    {
        return $this->postToClientInterface;
    }

    /**
     * Set the value of postToClientInterface
     *
     * @return  self
     */
    public function setPostToClientInterface(PostToClientInterface $postToClientInterface)
    {
        $this->postToClientInterface = $postToClientInterface;
        return $this;
    }


    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function delete(Customer $customer): void
    {
        //Posting data to netSuite third party API
        $customerNetSuiteInternalId = $this->deleteToNetSuite(
            $this->getNetSuiteIntegration(),
            $this->getPostToClientInterface(),
            $this->mapEloquentCustomerToNetSuiteAsArray($customer)
        );
    }

    private function mapEloquentCustomerToNetSuiteAsArray(
        Customer $customer
    ): array {
        $mapEloquentCustomerToNetSuiteAsArray = new MapEloquentCustomerToNetSuiteAsArrayDestroy();
        $mapEloquentCustomerToNetSuiteAsArray->map($customer);
        return $mapEloquentCustomerToNetSuiteAsArray->getNetSuiteCustomerAsArray();
    }

    private function deleteToNetSuite(
        NetSuiteIntegration $netSuite,
        PostToClientInterface $postToClient,
        array $data
    ): string {
        $response = '';
        $apiAuthorization = $netSuite->api_authorization;
        if (empty($apiAuthorization)) {
            throw new Exception('NetSuite Authorization key not set!');
        }
        $customerApiDelete = $netSuite->customer_api_destroy;
        if (empty($customerApiDelete)) {
            throw new Exception('NetSuite Customer("Opportunity") deleting URL not set!');
        }
        try {
            $response = $postToClient->send($customerApiDelete, $apiAuthorization, $data);
        } catch (Exception $e) {
            flash()->error(
                'Sorry, a customer could not be delete on NetSuite third party server::--> ' . $e->getMessage()
            );
        }
        return str_replace('"', '', $response);
    }
}
