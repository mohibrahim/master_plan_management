<?php

namespace App\MasterPlanManagement\Services\Integrations\Push\DataSync\Customer\Delete;

use App\Customer;

interface DeleteCustomerThirdPartyInterface
{
    public function delete(Customer $customer): void;
}
