<?php

namespace App\MasterPlanManagement\Services\Integrations\Push\DataSync\Customer\Create;

use Exception;
use App\Customer;
use App\NetSuiteIntegration;
use App\MasterPlanManagement\Services\Integrations\Push\CommunicationStrategy\PostToClientInterface;
use App\MasterPlanManagement\Services\Integrations\Push\ThirdPartyObjects\Customer\NetSuite\MapEloquentCustomerToNetSuiteAsArrayStore;

class CreateNetSuiteCustomer implements CreateCustomerThirdPartyInterface
{

    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */

    private $netSuiteIntegration;
    private $postToClientInterface;

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */



    /**
     * Get the value of netSuiteIntegration
     */
    public function getNetSuiteIntegration(): NetSuiteIntegration
    {
        return $this->netSuiteIntegration;
    }

    /**
     * Set the value of netSuiteIntegration
     *
     * @return  self
     */
    public function setNetSuiteIntegration(NetSuiteIntegration $netSuiteIntegration)
    {
        $this->netSuiteIntegration = $netSuiteIntegration;
        return $this;
    }

    /**
     * Get the value of postToClientInterface
     */
    public function getPostToClientInterface(): PostToClientInterface
    {
        return $this->postToClientInterface;
    }

    /**
     * Set the value of postToClientInterface
     *
     * @return  self
     */
    public function setPostToClientInterface(PostToClientInterface $postToClientInterface)
    {
        $this->postToClientInterface = $postToClientInterface;
        return $this;
    }


    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function store(Customer $customer): void
    {

        //Posting data to netSuite third party API
        $customerNetSuiteInternalId = $this->storeToNetSuite(
            $this->getNetSuiteIntegration(),
            $this->getPostToClientInterface(),
            $this->mapEloquentCustomerToNetSuiteAsArray($customer)
        );

        //Update Customer net_suite_internal_id and net_suite_created_at.
        if (!empty($customerNetSuiteInternalId)) {
            $customer->update([
                'net_suite_internal_id' => $customerNetSuiteInternalId,
                'net_suite_created_at' => now(),
            ]);
        }
    }

    private function mapEloquentCustomerToNetSuiteAsArray(
        Customer $customer
    ): array {
        $mapEloquentCustomerToNetSuiteAsArray = new MapEloquentCustomerToNetSuiteAsArrayStore();
        $mapEloquentCustomerToNetSuiteAsArray->map($customer);
        return $mapEloquentCustomerToNetSuiteAsArray->getNetSuiteCustomerAsArray();
    }

    private function storeToNetSuite(
        NetSuiteIntegration $netSuite,
        PostToClientInterface $postToClient,
        array $data
    ): string {
        $response = '';
        $apiAuthorization = $netSuite->api_authorization;
        if (empty($apiAuthorization)) {
            throw new Exception('NetSuite Authorization key not set!');
        }
        $customerApiStore = $netSuite->customer_api_store;
        if (empty($customerApiStore)) {
            throw new Exception('NetSuite Customer("Opportunity") storing URL not set!');
        }
        try {
            $response = $postToClient->send($customerApiStore, $apiAuthorization, $data);
        } catch (Exception $e) {
            flash()->error(
                'Sorry, a customer could not be created on NetSuite third party server::--> ' . $e->getMessage()
            );
        }

        return str_replace('"', '', $response);
    }
}
