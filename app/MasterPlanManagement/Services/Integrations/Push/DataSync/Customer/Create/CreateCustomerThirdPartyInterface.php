<?php

namespace App\MasterPlanManagement\Services\Integrations\Push\DataSync\Customer\Create;

use App\Customer;

interface CreateCustomerThirdPartyInterface
{
    public function store(Customer $customer): void;
}
