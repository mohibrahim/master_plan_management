<?php

namespace App\MasterPlanManagement\Services\Integrations\Push\DataSync\Customer\Update;

use Exception;
use App\Customer;
use App\NetSuiteIntegration;
use App\MasterPlanManagement\Services\Integrations\Push\CommunicationStrategy\PostToClientInterface;
use App\MasterPlanManagement\Services\Integrations\Push\ThirdPartyObjects\Customer\NetSuite\MapEloquentCustomerToNetSuiteAsArrayUpdate;

class UpdateNetSuiteCustomer implements UpdateCustomerThirdPartyInterface
{

        /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */

    private $netSuiteIntegration;
    private $postToClientInterface;

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */



    /**
     * Get the value of netSuiteIntegration
     */
    public function getNetSuiteIntegration(): NetSuiteIntegration
    {
        return $this->netSuiteIntegration;
    }

    /**
     * Set the value of netSuiteIntegration
     *
     * @return  self
     */
    public function setNetSuiteIntegration(NetSuiteIntegration $netSuiteIntegration)
    {
        $this->netSuiteIntegration = $netSuiteIntegration;
        return $this;
    }

    /**
     * Get the value of postToClientInterface
     */
    public function getPostToClientInterface(): PostToClientInterface
    {
        return $this->postToClientInterface;
    }

    /**
     * Set the value of postToClientInterface
     *
     * @return  self
     */
    public function setPostToClientInterface(PostToClientInterface $postToClientInterface)
    {
        $this->postToClientInterface = $postToClientInterface;
        return $this;
    }


    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function update(Customer $customer): void
    {
        //Posting data to netSuite third party API
        $response = $this->updateToNetSuite(
            $this->getNetSuiteIntegration(),
            $this->getPostToClientInterface(),
            $this->mapEloquentCustomerToNetSuiteAsArray($customer)
        );

        //Update Customer net_suite_update_at.
        if (!empty($response)) {
            $customer->update([
                'net_suite_updated_at' => now(),
            ]);
        }
    }

    private function mapEloquentCustomerToNetSuiteAsArray(
        Customer $customer
    ): array {
        $mapEloquentCustomerToNetSuiteAsArray = new MapEloquentCustomerToNetSuiteAsArrayUpdate();
        $mapEloquentCustomerToNetSuiteAsArray->map($customer);
        return $mapEloquentCustomerToNetSuiteAsArray->getNetSuiteCustomerAsArray();
    }

    private function updateToNetSuite(
        NetSuiteIntegration $netSuite,
        PostToClientInterface $postToClient,
        array $data
    ): string {
        $response = '';
        $apiAuthorization = $netSuite->api_authorization;
        if (empty($apiAuthorization)) {
            throw new Exception('NetSuite Authorization key not set!');
        }
        $customerApiUpdate = $netSuite->customer_api_update;
        if (empty($customerApiUpdate)) {
            throw new Exception('NetSuite Customer("Opportunity") updating URL not set!');
        }
        try {
            $response = $postToClient->send($customerApiUpdate, $apiAuthorization, $data);
        } catch (Exception $e) {
            flash()->error(
                'Sorry, a customer could not be updated on NetSuite third party server::--> ' . $e->getMessage()
            );
        }
        return $response;
    }
}
