<?php

namespace App\MasterPlanManagement\Services\Integrations\Push\DataSync\Customer\Update;

use App\Customer;

interface UpdateCustomerThirdPartyInterface
{
    public function update(Customer $customer): void;
}
