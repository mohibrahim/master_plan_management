<?php

namespace App\MasterPlanManagement\Services\Integrations\Push\DataSync\Reservation\Create;

use Exception;
use App\Reservation;
use App\NetSuiteIntegration;
use App\MasterPlanManagement\Services\Integrations\Push\CommunicationStrategy\PostToClientInterface;
use App\MasterPlanManagement\Services\Integrations\Push\ThirdPartyObjects\Reservation\NetSuite\MapEloquentReservationToNetSuiteAsArrayStore;

class CreateNetSuiteReservation implements CreateReservationThirdPartyInterface
{

    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */

    private $netSuiteIntegration;
    private $postToClientInterface;

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */



    /**
     * Get the value of netSuiteIntegration
     */
    public function getNetSuiteIntegration(): NetSuiteIntegration
    {
        return $this->netSuiteIntegration;
    }

    /**
     * Set the value of netSuiteIntegration
     *
     * @return  self
     */
    public function setNetSuiteIntegration(NetSuiteIntegration $netSuiteIntegration)
    {
        $this->netSuiteIntegration = $netSuiteIntegration;
        return $this;
    }

    /**
     * Get the value of postToClientInterface
     */
    public function getPostToClientInterface(): PostToClientInterface
    {
        return $this->postToClientInterface;
    }

    /**
     * Set the value of postToClientInterface
     *
     * @return  self
     */
    public function setPostToClientInterface(PostToClientInterface $postToClientInterface)
    {
        $this->postToClientInterface = $postToClientInterface;
        return $this;
    }


    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function store(Reservation $reservation): void
    {

        //Posting data to netSuite third party API
        $reservationNetSuiteInternalId = $this->storeToNetSuite(
            $this->getNetSuiteIntegration(),
            $this->getPostToClientInterface(),
            $this->mapEloquentReservationToNetSuiteAsArray($reservation)
        );

        //Update Reservation net_suite_internal_id and net_suite_created_at.
        if (!empty($reservationNetSuiteInternalId)) {
            $reservation->update([
                'net_suite_internal_id' => $reservationNetSuiteInternalId,
                'net_suite_created_at' => now(),
            ]);
        }
    }

    private function mapEloquentReservationToNetSuiteAsArray(
        Reservation $reservation
    ): array {
        $mapEloquentReservationToNetSuiteAsArray = new MapEloquentReservationToNetSuiteAsArrayStore();
        $mapEloquentReservationToNetSuiteAsArray->map($reservation);
        return $mapEloquentReservationToNetSuiteAsArray->getNetSuiteReservationAsArray();
    }

    private function storeToNetSuite(
        NetSuiteIntegration $netSuite,
        PostToClientInterface $postToClient,
        array $data
    ): string {
        $response = '';
        $apiAuthorization = $netSuite->api_authorization;
        if (empty($apiAuthorization)) {
            throw new Exception('NetSuite Authorization key not set!');
        }
        $reservationApiStore = $netSuite->reservation_api_store;
        if (empty($reservationApiStore)) {
            throw new Exception('NetSuite Reservation("Opportunity") storing URL not set!');
        }
        try {

            $response = $postToClient->send($reservationApiStore, $apiAuthorization, $data);
            // dd($response);
        } catch (Exception $e) {
            flash()->error(
                'Sorry, a reservation could not be created on NetSuite third party server::--> ' . $e->getMessage()
            );
        }

        return str_replace('"', '', $response);
    }
}
