<?php

namespace App\MasterPlanManagement\Services\Integrations\Push\DataSync\Reservation\Create;

use App\Reservation;

interface CreateReservationThirdPartyInterface
{
    public function store(Reservation $reservation): void;
}
