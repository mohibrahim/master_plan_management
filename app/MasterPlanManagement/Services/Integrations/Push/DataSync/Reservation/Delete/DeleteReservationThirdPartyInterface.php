<?php

namespace App\MasterPlanManagement\Services\Integrations\Push\DataSync\Reservation\Delete;

use App\Reservation;

interface DeleteReservationThirdPartyInterface
{
    public function delete(Reservation $reservation): void;
}
