<?php

namespace App\MasterPlanManagement\Services\Integrations\Push\DataSync\Reservation\Delete;

use Exception;
use App\Reservation;
use App\NetSuiteIntegration;
use App\MasterPlanManagement\Services\Integrations\Push\CommunicationStrategy\PostToClientInterface;
use App\MasterPlanManagement\Services\Integrations\Push\ThirdPartyObjects\Reservation\NetSuite\MapEloquentReservationToNetSuiteAsArrayDestroy;

class DeleteNetSuiteReservation implements DeleteReservationThirdPartyInterface
{

        /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */

    private $netSuiteIntegration;
    private $postToClientInterface;

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */



    /**
     * Get the value of netSuiteIntegration
     */
    public function getNetSuiteIntegration(): NetSuiteIntegration
    {
        return $this->netSuiteIntegration;
    }

    /**
     * Set the value of netSuiteIntegration
     *
     * @return  self
     */
    public function setNetSuiteIntegration(NetSuiteIntegration $netSuiteIntegration)
    {
        $this->netSuiteIntegration = $netSuiteIntegration;
        return $this;
    }

    /**
     * Get the value of postToClientInterface
     */
    public function getPostToClientInterface(): PostToClientInterface
    {
        return $this->postToClientInterface;
    }

    /**
     * Set the value of postToClientInterface
     *
     * @return  self
     */
    public function setPostToClientInterface(PostToClientInterface $postToClientInterface)
    {
        $this->postToClientInterface = $postToClientInterface;
        return $this;
    }


    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function delete(Reservation $reservation): void
    {
        //Posting data to netSuite third party API
        $reservationNetSuiteInternalId = $this->deleteToNetSuite(
            $this->getNetSuiteIntegration(),
            $this->getPostToClientInterface(),
            $this->mapEloquentReservationToNetSuiteAsArray($reservation)
        );
    }

    private function mapEloquentReservationToNetSuiteAsArray(
        Reservation $reservation
    ): array {
        $mapEloquentReservationToNetSuiteAsArray = new MapEloquentReservationToNetSuiteAsArrayDestroy();
        $mapEloquentReservationToNetSuiteAsArray->map($reservation);
        return $mapEloquentReservationToNetSuiteAsArray->getNetSuiteReservationAsArray();
    }

    private function deleteToNetSuite(
        NetSuiteIntegration $netSuite,
        PostToClientInterface $postToClient,
        array $data
    ): string {
        $response = '';
        $apiAuthorization = $netSuite->api_authorization;
        if (empty($apiAuthorization)) {
            throw new Exception('NetSuite Authorization key not set!');
        }
        $reservationApiDelete = $netSuite->reservation_api_destroy;
        if (empty($reservationApiDelete)) {
            throw new Exception('NetSuite Reservation("Opportunity") deleting URL not set!');
        }
        try {
            $response = $postToClient->send($reservationApiDelete, $apiAuthorization, $data);
        } catch (Exception $e) {
            flash()->error(
                'Sorry, a reservation could not be delete on NetSuite third party server::--> ' . $e->getMessage()
            );
        }
        return str_replace('"', '', $response);
    }
}
