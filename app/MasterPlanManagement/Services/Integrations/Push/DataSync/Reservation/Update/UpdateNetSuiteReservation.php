<?php

namespace App\MasterPlanManagement\Services\Integrations\Push\DataSync\Reservation\Update;

use Exception;
use App\Reservation;
use App\NetSuiteIntegration;
use App\MasterPlanManagement\Services\Integrations\Push\CommunicationStrategy\PostToClientInterface;
use App\MasterPlanManagement\Services\Integrations\Push\ThirdPartyObjects\Reservation\NetSuite\MapEloquentReservationToNetSuiteAsArrayUpdate;

class UpdateNetSuiteReservation implements UpdateReservationThirdPartyInterface
{

        /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */

    private $netSuiteIntegration;
    private $postToClientInterface;

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */



    /**
     * Get the value of netSuiteIntegration
     */
    public function getNetSuiteIntegration(): NetSuiteIntegration
    {
        return $this->netSuiteIntegration;
    }

    /**
     * Set the value of netSuiteIntegration
     *
     * @return  self
     */
    public function setNetSuiteIntegration(NetSuiteIntegration $netSuiteIntegration)
    {
        $this->netSuiteIntegration = $netSuiteIntegration;
        return $this;
    }

    /**
     * Get the value of postToClientInterface
     */
    public function getPostToClientInterface(): PostToClientInterface
    {
        return $this->postToClientInterface;
    }

    /**
     * Set the value of postToClientInterface
     *
     * @return  self
     */
    public function setPostToClientInterface(PostToClientInterface $postToClientInterface)
    {
        $this->postToClientInterface = $postToClientInterface;
        return $this;
    }


    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function update(Reservation $reservation): void
    {
        //Posting data to netSuite third party API
        $response = $this->updateToNetSuite(
            $this->getNetSuiteIntegration(),
            $this->getPostToClientInterface(),
            $this->mapEloquentReservationToNetSuiteAsArray($reservation)
        );

        //Update Reservation net_suite_update_at.
        if (!empty($response)) {
            $reservation->update([
                'net_suite_updated_at' => now(),
            ]);
        }
    }

    private function mapEloquentReservationToNetSuiteAsArray(
        Reservation $reservation
    ): array {
        $mapEloquentReservationToNetSuiteAsArray = new MapEloquentReservationToNetSuiteAsArrayUpdate();
        $mapEloquentReservationToNetSuiteAsArray->map($reservation);
        return $mapEloquentReservationToNetSuiteAsArray->getNetSuiteReservationAsArray();
    }

    private function updateToNetSuite(
        NetSuiteIntegration $netSuite,
        PostToClientInterface $postToClient,
        array $data
    ): string {
        $response = '';
        $apiAuthorization = $netSuite->api_authorization;
        if (empty($apiAuthorization)) {
            throw new Exception('NetSuite Authorization key not set!');
        }
        $reservationApiUpdate = $netSuite->reservation_api_update;
        if (empty($reservationApiUpdate)) {
            throw new Exception('NetSuite Reservation("Opportunity") updating URL not set!');
        }
        try {
            $response = $postToClient->send($reservationApiUpdate, $apiAuthorization, $data);
        } catch (Exception $e) {
            flash()->error(
                'Sorry, a reservation could not be updated on NetSuite third party server::--> ' . $e->getMessage()
            );
        }
        return $response;
    }
}
