<?php

namespace App\MasterPlanManagement\Services\Integrations\Push\DataSync\Reservation\Update;

use App\Reservation;

interface UpdateReservationThirdPartyInterface
{
    public function update(Reservation $reservation): void;
}
