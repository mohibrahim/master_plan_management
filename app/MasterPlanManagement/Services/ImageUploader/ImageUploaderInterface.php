<?php

namespace App\GhazalaBayCommunity\Services\ImageUploader;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;


interface ImageUploaderInterface
{
    /**
     * Undocumented function
     *
     * @param Request $request
     * @param string $formImageName
     * @param string $savingPath
     * @return array
     */
    public function add(
        Request $request,
        string $formImageName,
        string $savingPath
    ) : array;

    /**
     * Undocumented function
     *
     * @param Request $request
     * @param string $formImageName
     * @param Model $model
     * @param string $modelImageName
     * @param string $savingPath
     * @return array
     */
    public function update(
        Request $request,
        string $formImageName,
        Model $model,
        string $modelImageName,
        string $savingPath
    ) : array;

    /**
     * Undocumented function
     *
     * @param Model $model
     * @param string $modelImageName
     * @param string $savingPath
     * @return boolean
     */
    public function delete(
        Model $model,
        string $modelImageName,
        string $savingPath
    ) : bool;

}