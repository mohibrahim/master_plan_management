<?php

namespace App\GhazalaBayCommunity\Services\ImageUploader;

use ErrorException;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use App\GhazalaBayCommunity\Services\ImageUploader\ImageUploaderInterface;


class ImageUploader implements ImageUploaderInterface
{
    public function add(
        Request $request,
        string $formImageName,
        string $savingPath
    ): array {
        // 'images/users_personal_images'
        $requestArray = $request->all();
        if ($request->hasFile($formImageName)) {
            if ($request->file($formImageName)->isValid()) {
                $imageFile = $request->file($formImageName);
                $randomName = bin2hex((openssl_random_pseudo_bytes(20)));
                $extension = $imageFile->extension();
                $imageFile = $imageFile->move($savingPath, $randomName . '.' . $extension);

                $path = $randomName . '.' . $extension;

                $requestArray[$formImageName] = $path;
            }
        }
        return $requestArray;
    }

    public function update(
        Request $request,
        string $formImageName,
        Model $model,
        string $modelImageName,
        string $savingPath
    ): array {
        $requestArray = $request->all();
        if ($request->hasFile($formImageName)) {
            if ($request->file($formImageName)->isValid()) {
                $isDeleted = $this->delete($model, $modelImageName, $savingPath);
                $requestArray = $this->add($request, $formImageName, $savingPath);
            }
        }
        return $requestArray;
    }

    public function delete(
        Model $model,
        string $modelImageName,
        string $savingPath
    ): bool {
        $isRemoved = false;
        if (!empty($model->$modelImageName) && $model->$modelImageName != 'no_image.png') {
            try {
                $imageFile = public_path($savingPath .'/'. $model->$modelImageName);
                if (file_exists($imageFile)) {
                    $isRemoved = unlink($imageFile);
                }
            } catch (ErrorException  $th) {
                //throw $th;
            }
        }
        return $isRemoved;
    }
}

