<?php

namespace App\MasterPlanManagement\Services\FlashMessageLanguage;

use App\MasterPlanManagement\Services\LanguageLocator\LanguageLocatorTrait;

class FlashMessageArabicEnglish implements FlashMessageArabicEnglishInterface
{
    use LanguageLocatorTrait;

    public function getSuitableFlashMessage(string $arabic, string $english): string
    {
        if ($this->isArabic()) {
            return $arabic;
        } elseif ($this->isEnglish()) {
            return $english;
        }
        return '**Undefined Language**';
    }
}