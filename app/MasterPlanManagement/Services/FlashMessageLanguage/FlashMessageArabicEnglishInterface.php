<?php

namespace App\MasterPlanManagement\Services\FlashMessageLanguage;


interface FlashMessageArabicEnglishInterface
{
    public function getSuitableFlashMessage(string $arabic, string $english): string;
}