<?php

namespace App\MasterPlanManagement\Services\WorldCountries\Countries;

interface CountriesInterface
{
    public function getCountries(): array;
}