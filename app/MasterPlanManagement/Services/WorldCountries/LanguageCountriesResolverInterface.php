<?php

namespace App\MasterPlanManagement\Services\WorldCountries;


interface LanguageCountriesResolverInterface
{
    public function getSuitableCountriesListByCurrentLanguage(): array;
    public function getSuitableCountryByCurrentLanguage(string $countryKey): string;
}