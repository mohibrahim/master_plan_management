<?php

namespace App\MasterPlanManagement\Services\WorldCountries;

use App\MasterPlanManagement\Services\LanguageLocator\LanguageLocatorTrait;
use App\MasterPlanManagement\Services\WorldCountries\Countries\ArabicCountries;
use App\MasterPlanManagement\Services\WorldCountries\Countries\EnglishCountries;
use App\MasterPlanManagement\Services\WorldCountries\LanguageCountriesResolverInterface;

class ArabicEnglishCountriesResolver implements LanguageCountriesResolverInterface
{
    use LanguageLocatorTrait;

    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
    private $arabicCountries;
    private $englishCountries;
    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    public function __construct(ArabicCountries $arabicCountries, EnglishCountries $englishCountries) {
        $this->arabicCountries = $arabicCountries;
        $this->englishCountries = $englishCountries;
    }
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */
  
    public function getSuitableCountriesListByCurrentLanguage(): array
    {
        $countries = [];
        if ($this->isArabic()) {
            $countries = $this->arabicCountries->getCountries();
        } elseif ($this->isEnglish()) {
            $countries = $this->englishCountries->getCountries();
        }
        return $countries;
    }

    public function getSuitableCountryByCurrentLanguage(string $countryKey): string
    {
        $country = '';
        if ($this->isArabic()) {
            if(array_key_exists($countryKey, $this->arabicCountries->getCountries())) {
                $country = $this->arabicCountries->getCountries()[$countryKey];
            }
        } elseif ($this->isEnglish()) {
            if(array_key_exists($countryKey, $this->englishCountries->getCountries())) {
                $country = $this->englishCountries->getCountries()[$countryKey];
            }
        }
        return $country;
    }
}