<?php

namespace App\MasterPlanManagement\Services\FileUploader;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;


interface FileUploaderInterface
{
    /**
     * Taking the file from the request and save it to the directory you select
     * it, and return the request as array
     *
     * @param Request $request
     * @param string $formFileName
     * @param string $savingPath
     * @return array
     */
    public function add(
        Request $request,
        string $formFileName,
        string $savingPath
    ) : array;

    /**
     * Undocumented function
     *
     * @param Request $request
     * @param string $formFileName
     * @param Model $model
     * @param string $fileName
     * @param string $savingPath
     * @return array
     */
    public function update(
        Request $request,
        string $formFileName,
        string $fileName,
        string $savingPath
    ) : array;

    /**
     * Undocumented function
     *
     * @param Model $model
     * @param string $fileName
     * @param string $savingPath
     * @return boolean
     */
    public function delete(
        string $fileName,
        string $savingPath
    ) : bool;

}