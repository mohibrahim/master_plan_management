<?php

namespace App\MasterPlanManagement\Services\FileUploader;

use ErrorException;
use Illuminate\Http\Request;


class FileUploader implements FileUploaderInterface
{
    private $fileNameToSkip;

     /**
     * Get the value of fileNameToSkip
     */
    public function getFileNameToSkip(): string
    {
        if (empty($this->fileNameToSkip)) {
            return '';
        }
        return $this->fileNameToSkip;
    }

    /**
     * Set the value of fileNameToSkip
     *
     * @return  void
     */
    public function setFileNameToSkip(string $fileNameToSkip)
    {
        $this->fileNameToSkip = $fileNameToSkip;
    }


    public function add(
        Request $request,
        string $formFileName,
        string $savingPath
    ): array {
        $requestArray = $request->all();
        if ($request->hasFile($formFileName)) {
            if ($request->file($formFileName)) {
                $files = $request->file($formFileName);
                if (!is_array($files)) {
                    $file = $files;
                    if ($file->isValid()) {
                        $randomName = bin2hex((openssl_random_pseudo_bytes(20)));
                        $extension = $file->extension();
                        $file = $file->move($savingPath, $randomName . '.' . $extension);
                        $path = $randomName . '.' . $extension;
                        $requestArray[$formFileName] = $path;
                    }
                } elseif (is_array($files)) {
                    foreach ($files as $fileKey => $oneFile) {
                        if($oneFile->isValid()) {
                            $randomName = bin2hex((openssl_random_pseudo_bytes(20)));
                            $extension = $oneFile->extension();
                            $oneFile = $oneFile->move($savingPath, $randomName . '.' . $extension);
                            $path = $randomName . '.' . $extension;
                            $requestArray[$formFileName][$fileKey] = $path;
                        }
                    }
                }

            }
        }
        return $requestArray;
    }

    public function update(
        Request $request,
        string $formFileName,
        string $fileName,
        string $savingPath
    ): array {
        $requestArray = $request->all();
        if ($request->hasFile($formFileName)) {
            if ($request->file($formFileName)->isValid()) {
                $isDeleted = $this->delete($fileName, $savingPath);
                $requestArray = $this->add($request, $formFileName, $savingPath);
            }
        }
        return $requestArray;
    }

    public function delete(
        string $fileName,
        string $savingPath
    ): bool {
        $isRemoved = false;
        if (!empty($fileName)) {
            if ($fileName == $this->getFileNameToSkip()) {
                return true;
            }
            try {
                $file = public_path($savingPath . '/' . $fileName);
                if (file_exists($file)) {
                    $isRemoved = unlink($file);
                }
            } catch (ErrorException  $th) {
                //throw $th;
            }
        }
        return $isRemoved;
    }
}
