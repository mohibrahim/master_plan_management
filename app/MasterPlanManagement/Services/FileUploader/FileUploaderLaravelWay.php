<?php

namespace App\MasterPlanManagement\Services\FileUploader;

use ErrorException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FileUploaderLaravelWay implements FileUploaderInterface
{
    private $fileNameToSkip;

    /**
     * Get the value of fileNameToSkip
     */
    public function getFileNameToSkip(): string
    {
        if (empty($this->fileNameToSkip)) {
            return '';
        }
        return $this->fileNameToSkip;
    }

    /**
     * Set the value of fileNameToSkip
     *
     * @return  void
     */
    public function setFileNameToSkip(string $fileNameToSkip)
    {
        $this->fileNameToSkip = $fileNameToSkip;
    }


    public function add(
        Request $request,
        string $formFileName,
        string $savingPath
    ): array {
        $requestArray = $request->all();
        if ($request->hasFile($formFileName)) {
            if ($request->file($formFileName)) {
                $files = $request->file($formFileName);
                if (!is_array($files)) {
                    $file = $files;
                    if ($file->isValid()) {
                        $fileName = $file->store($savingPath, 's3');
                        $requestArray[$formFileName] = $fileName;
                    }
                } elseif (is_array($files)) {
                    foreach ($files as $fileKey => $oneFile) {
                        if ($oneFile->isValid()) {
                            $fileName = $oneFile->store($savingPath, 's3');
                            $requestArray[$formFileName][$fileKey] = $fileName;
                        }
                    }
                }
            }
        }
        return $requestArray;
    }

    public function update(
        Request $request,
        string $formFileName,
        string $fileName,
        string $savingPath
    ): array {
        $requestArray = $request->all();
        if ($request->hasFile($formFileName)) {
            if ($request->file($formFileName)->isValid()) {
                $isDeleted = $this->delete($fileName, $savingPath);
                $requestArray = $this->add($request, $formFileName, $savingPath);
            }
        }
        return $requestArray;
    }

    public function delete(
        string $fileName,
        string $savingPath
    ): bool {
        if (!empty($fileName)) {

            if ($fileName == $this->getFileNameToSkip()) {
                return true;
            }
            try {
                $isRemoved = Storage::delete($fileName);
                $isRemoved = Storage::disk('public')->delete($fileName);
            } catch (ErrorException  $e) {
                return $isRemoved;
            }
        }
        return $isRemoved;
    }
}
