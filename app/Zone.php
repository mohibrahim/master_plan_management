<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Builder;

class Zone extends Model
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
    protected $table = 'zones';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'notes',
        'creator_id',
        'last_updater_id',
        'project_id',
        'phase_id',
        'html_area_tag_coords',
        'net_suite_internal_id',
    ];

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Accessors
    |--------------------------------------------------------------------------
    */

    public function getSelectedProjectNameIdAttribute()
    {
        $project = $this->project;
        if (!empty($project)) {
            $id = $project->id;
            $name = $project->name;
            if (!empty($id) && !empty($name)) {
                return ['id'=>$id, 'name'=>$name];
            }
        }
        return [];
    }

    public function getSelectedPhaseNameIdAttribute()
    {
        $phase = $this->phase;
        if (!empty($phase)) {
            $id = $phase->id;
            $name = $phase->name;
            if (!empty($id) && !empty($name)) {
                return ['id'=>$id, 'name'=>$name];
            }
        }
        return [];
    }

    public function getSelectedProjectPhasesNamesIdsAttribute()
    {
        $project = $this->project;
        if (request()->old('old_selected_phase_name') && request()->old('phase_id')) {
            return [request()->old('phase_id') => request()->old('old_selected_phase_name')];
        } elseif (!empty($project)) {
            $phases = $project->phases;
            if ($phases->isNotEmpty()) {
                return $phases->pluck('name', 'id');
            }
        }
        return [];
    }

    public function getSelectedProjectMasterPlanImageAttribute()
    {
        $project = $this->project;
        if (!empty($project)) {
            return $project->master_plan_image;
        }
        return '';
    }

    public function getZoneShowLinkAttribute()
    {
        if (!empty($this->id)) {
            return '<a href="'.action('ZoneController@show', ['id'=>$this->id]).'">'.$this->name.'</a>';
        }
        return '';
    }

    public function getZoneCatalogsShowLinksAttribute()
    {
        $zoneCatalogs = $this->catalogs;
        if ($zoneCatalogs->isNotEmpty()) {
            return $zoneCatalogs->pluck('catalog_show_link')->toArray();
        }
        return [];
    }

    /*
    |--------------------------------------------------------------------------
    | Mutators
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */

    /**
     * Getting the creator user who create this catalog.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    /**
     * Getting the last updater user who update this catalog.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function lastUpdater(): BelongsTo
    {
        return $this->belongsTo(User::class, 'last_updater_id', 'id');
    }

    /**
     * Getting the project that are associated with this zone.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id', 'id');
    }

    /**
     * Getting the phase that are associated with this zone.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function phase()
    {
        return $this->belongsTo(Phase::class, 'phase_id', 'id');
    }

    /**
     * Getting the units that are associated with this zone.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return void
     */
    public function units()
    {
        return $this->hasMany(Unit::class, 'zone_id', 'id');
    }

    /**
     * Getting the catalogs that are associated with this zone.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return void
     */
    public function catalogs()
    {
        return $this->hasMany(Catalog::class, 'zone_id', 'id');
    }

    /**
     * Getting the blocks that are associated with this zone.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return void
     */
    public function blocks()
    {
        return $this->hasMany(Block::class, 'zone_id', 'id');
    }



    /*
    |--------------------------------------------------------------------------
    | Miscellaneous
    |--------------------------------------------------------------------------
    */

    public function search(Builder $builder, string $keyword)
    {
        return $builder->with('creator', 'project')
            ->where('name', 'like', "%$keyword%")
            ->orWhereHas('project', function ($query) use ($keyword) {
                $query->where('name', 'like', "%$keyword%");
            });
    }
}
