<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use App\Console\Commands\PullUnitsFromNetSuiteThirdParty;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Console\Commands\PullCustomersFromNetSuiteThirdParty;
use App\Console\Commands\PushReservationsToNetSuiteThirdParty;
use App\Console\Commands\PullReservationsFromNetSuiteThirdParty;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        PushReservationsToNetSuiteThirdParty::class,
        PullUnitsFromNetSuiteThirdParty::class,
        PullReservationsFromNetSuiteThirdParty::class,
        PullCustomersFromNetSuiteThirdParty::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        $schedule->command('netsuite:reservations_push')
            ->everyMinute();
        $schedule->command('netsuite:units_pull')
            ->daily();
        $schedule->command('netsuite:reservations_pull')
            ->daily();
        $schedule->command('netsuite:customers_pull')
            ->hourly();
        $schedule->command('netsuite:customers_push')
            ->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
