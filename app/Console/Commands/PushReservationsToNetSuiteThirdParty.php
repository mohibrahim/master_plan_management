<?php

namespace App\Console\Commands;

use App\Reservation;
use App\NetSuiteIntegration;
use Illuminate\Console\Command;
use App\IntegrationPushingQueue;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\MasterPlanManagement\Services\Integrations\Push\CommunicationStrategy\GuzzlePost;
use App\MasterPlanManagement\Services\Models\IntegrationPushingQueue\ModelType\ModelType;
use App\MasterPlanManagement\Services\Integrations\Push\CommunicationStrategy\GuzzleDelete;
use App\MasterPlanManagement\Services\Integrations\Push\CommunicationStrategy\GuzzlePut;
use App\MasterPlanManagement\Services\Models\IntegrationPushingQueue\OperationType\OperationType;
use App\MasterPlanManagement\Services\Models\IntegrationPushingQueue\IntegrationType\IntegrationType;
use App\MasterPlanManagement\Services\Integrations\Push\DataSync\Reservation\Create\CreateReservationThirdPartyInterface;
use App\MasterPlanManagement\Services\Integrations\Push\DataSync\Reservation\Delete\DeleteReservationThirdPartyInterface;
use App\MasterPlanManagement\Services\Integrations\Push\DataSync\Reservation\Update\UpdateReservationThirdPartyInterface;

class PushReservationsToNetSuiteThirdParty extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'netsuite:reservations_push';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(
        GuzzlePost $guzzlePost,
        GuzzlePut $guzzlePut,
        GuzzleDelete $guzzleDelete,
        NetSuiteIntegration $netSuiteIntegration
    ) {
        $queuedReservations = IntegrationPushingQueue::where('model_type', ModelType::RESERVATION)
            ->oldest()
            ->limit(3)
            ->get();
        foreach ($queuedReservations as $queuedReservation) {

            $operationType = $queuedReservation->operation_type;
            $modelId = $queuedReservation->model_id;
            $thirdPartyJson = $queuedReservation->third_party_object;
            $integrationType = $queuedReservation->integration_type;
            if ($integrationType != IntegrationType::NETSUITE) {
                continue;
            }
            try {
                if ($operationType == OperationType::CREATE) {
                    $reservation = Reservation::findOrFail($modelId);
                    $createNetSuiteReservation = resolve(CreateReservationThirdPartyInterface::class);
                    $createNetSuiteReservation->setNetSuiteIntegration($netSuiteIntegration::first());
                    $createNetSuiteReservation->setPostToClientInterface($guzzlePost);
                    $createNetSuiteReservation->store($reservation);
                }
                if ($operationType == OperationType::UPDATE) {
                    $reservation = Reservation::findOrFail($modelId);
                    $updateNetSuiteReservation = resolve(UpdateReservationThirdPartyInterface::class);
                    $updateNetSuiteReservation->setNetSuiteIntegration($netSuiteIntegration::first());
                    $updateNetSuiteReservation->setPostToClientInterface($guzzlePut);
                    $updateNetSuiteReservation->update($reservation);
                }
            } catch (ModelNotFoundException $exception) {
                $queuedReservation->delete();
            }
            if ($operationType == OperationType::DELETE) {
                $thirdPartyObject = json_decode($thirdPartyJson);
                $reservation = new Reservation();
                $reservation->net_suite_internal_id = $thirdPartyObject->id;
                $deleteNetSuiteReservation = resolve(DeleteReservationThirdPartyInterface::class);
                $deleteNetSuiteReservation->setNetSuiteIntegration($netSuiteIntegration::first());
                $deleteNetSuiteReservation->setPostToClientInterface($guzzleDelete);
                $deleteNetSuiteReservation->delete($reservation);
            }
            $queuedReservation->delete();

        }
    }
}
