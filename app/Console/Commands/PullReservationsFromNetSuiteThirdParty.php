<?php

namespace App\Console\Commands;

use App\NetSuiteIntegration;
use Illuminate\Console\Command;
use App\MasterPlanManagement\Services\Integrations\Pull\DataSync\ReservationsSyncing;
use App\MasterPlanManagement\Services\Integrations\Pull\ThirdPartyObjects\ReservationThirdParty;
use App\MasterPlanManagement\Services\Integrations\Pull\CommunicationStrategy\ClientRequestInterface;
use App\MasterPlanManagement\Services\Integrations\Pull\ThirdPartyObjects\MappingStrategy\Listing\MappingInterface;

class PullReservationsFromNetSuiteThirdParty extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'netsuite:reservations_pull';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $netSuiteIntegration = NetSuiteIntegration::first();
        $clientRequest = resolve(ClientRequestInterface::class);
        $mapping = resolve(MappingInterface::class);
        $reservationsSyncing = resolve(ReservationsSyncing::class);

        $reservationUrl = $netSuiteIntegration->reservation_api_index;
        $authorization = $netSuiteIntegration->api_authorization;
        //communicate and getting string json
        $response = $clientRequest->requestClient($reservationUrl, $authorization);
        //mapping objects
        $collectionOfThirdPartyReservations = $mapping->mapList($response, ReservationThirdParty::class);
        //Syncing
        $reservationsSyncing->sync($collectionOfThirdPartyReservations);
        $netSuiteIntegration->restfulUrl
            ->update(['reservation_sync_date_time' => now()]);
    }
}
