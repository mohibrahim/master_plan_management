<?php

namespace App\Console\Commands;

use App\NetSuiteIntegration;
use Illuminate\Console\Command;
use App\MasterPlanManagement\Services\Integrations\Pull\DataSync\CustomersSyncing;
use App\MasterPlanManagement\Services\Integrations\Pull\ThirdPartyObjects\CustomerThirdParty;
use App\MasterPlanManagement\Services\Integrations\Pull\CommunicationStrategy\ClientRequestInterface;
use App\MasterPlanManagement\Services\Integrations\Pull\ThirdPartyObjects\MappingStrategy\Listing\MappingInterface;

class PullCustomersFromNetSuiteThirdParty extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'netsuite:customers_pull';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $netSuiteIntegration = NetSuiteIntegration::first();
        $clientRequest =  resolve(ClientRequestInterface::class);
        $mapping =  resolve(MappingInterface::class);
        $customersSyncing =  resolve(CustomersSyncing::class);

        $customerUrl = $netSuiteIntegration->customer_api_index;
        $authorization = $netSuiteIntegration->api_authorization;
        //communicate and getting string json
        $response = $clientRequest->requestClient($customerUrl, $authorization);
        //mapping objects
        $collectionOfThirdPartyCustomers = $mapping->mapList($response, CustomerThirdParty::class);
        //syncing
        $customersSyncing->sync($collectionOfThirdPartyCustomers);
        $netSuiteIntegration->restfulUrl->update(['customer_sync_date_time' => now()]);
    }
}
