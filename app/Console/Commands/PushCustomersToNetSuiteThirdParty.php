<?php

namespace App\Console\Commands;

use App\Customer;
use App\NetSuiteIntegration;
use Illuminate\Console\Command;
use App\IntegrationPushingQueue;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\MasterPlanManagement\Services\Integrations\Push\CommunicationStrategy\GuzzlePost;
use App\MasterPlanManagement\Services\Models\IntegrationPushingQueue\ModelType\ModelType;
use App\MasterPlanManagement\Services\Integrations\Push\CommunicationStrategy\GuzzleDelete;
use App\MasterPlanManagement\Services\Integrations\Push\CommunicationStrategy\GuzzlePut;
use App\MasterPlanManagement\Services\Models\IntegrationPushingQueue\OperationType\OperationType;
use App\MasterPlanManagement\Services\Models\IntegrationPushingQueue\IntegrationType\IntegrationType;
use App\MasterPlanManagement\Services\Integrations\Push\DataSync\Customer\Create\CreateCustomerThirdPartyInterface;
use App\MasterPlanManagement\Services\Integrations\Push\DataSync\Customer\Delete\DeleteCustomerThirdPartyInterface;
use App\MasterPlanManagement\Services\Integrations\Push\DataSync\Customer\Update\UpdateCustomerThirdPartyInterface;

class PushCustomersToNetSuiteThirdParty extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'netsuite:customers_push';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(
        GuzzlePost $guzzlePost,
        GuzzlePut $guzzlePut,
        GuzzleDelete $guzzleDelete,
        NetSuiteIntegration $netSuiteIntegration
    ) {
        $queuedCustomers = IntegrationPushingQueue::where('model_type', ModelType::CUSTOMER)
            ->oldest()
            ->limit(3)
            ->get();
        foreach ($queuedCustomers as $queuedCustomer) {

            $operationType = $queuedCustomer->operation_type;
            $modelId = $queuedCustomer->model_id;
            $thirdPartyJson = $queuedCustomer->third_party_object;
            $integrationType = $queuedCustomer->integration_type;
            if ($integrationType != IntegrationType::NETSUITE) {
                continue;
            }
            try {
                if ($operationType == OperationType::CREATE) {
                    $customer = Customer::findOrFail($modelId);
                    $createNetSuiteCustomer = resolve(CreateCustomerThirdPartyInterface::class);
                    $createNetSuiteCustomer->setNetSuiteIntegration($netSuiteIntegration::first());
                    $createNetSuiteCustomer->setPostToClientInterface($guzzlePost);
                    $createNetSuiteCustomer->store($customer);
                }
                if ($operationType == OperationType::UPDATE) {
                    $customer = Customer::findOrFail($modelId);
                    $updateNetSuiteCustomer = resolve(UpdateCustomerThirdPartyInterface::class);
                    $updateNetSuiteCustomer->setNetSuiteIntegration($netSuiteIntegration::first());
                    $updateNetSuiteCustomer->setPostToClientInterface($guzzlePut);
                    $updateNetSuiteCustomer->update($customer);
                }
            } catch (ModelNotFoundException $exception) {
                $queuedCustomer->delete();
            }
            if ($operationType == OperationType::DELETE) {
                $thirdPartyObject = json_decode($thirdPartyJson);
                $customer = new Customer();
                $customer->net_suite_internal_id = $thirdPartyObject->id;
                $deleteNetSuiteCustomer = resolve(DeleteCustomerThirdPartyInterface::class);
                $deleteNetSuiteCustomer->setNetSuiteIntegration($netSuiteIntegration::first());
                $deleteNetSuiteCustomer->setPostToClientInterface($guzzleDelete);
                $deleteNetSuiteCustomer->delete($customer);
            }
            $queuedCustomer->delete();

        }
    }
}
