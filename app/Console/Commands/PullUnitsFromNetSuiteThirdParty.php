<?php

namespace App\Console\Commands;

use App\NetSuiteIntegration;
use Illuminate\Console\Command;
use App\MasterPlanManagement\Services\Integrations\Pull\DataSync\UnitsSyncing;
use App\MasterPlanManagement\Services\Integrations\Pull\ThirdPartyObjects\UnitThirdParty;
use App\MasterPlanManagement\Services\Integrations\Pull\CommunicationStrategy\ClientRequestInterface;
use App\MasterPlanManagement\Services\Integrations\Pull\ThirdPartyObjects\MappingStrategy\Listing\MappingInterface;

class PullUnitsFromNetSuiteThirdParty extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'netsuite:units_pull';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $netSuiteIntegration = NetSuiteIntegration::first();
        $clientRequest = resolve(ClientRequestInterface::class);
        $mapping = resolve(MappingInterface::class);
        $unitsSyncing = resolve(UnitsSyncing::class);

        $unitUrl = $netSuiteIntegration->unit_api_index;
        $authorization = $netSuiteIntegration->api_authorization;
        //communicate and getting string json
        $response = $clientRequest->requestClient($unitUrl, $authorization);
        //mapping objects
        $collectionOfThirdPartyUnits = $mapping->mapList($response, UnitThirdParty::class);
        //Syncing
        $unitsSyncing->sync($collectionOfThirdPartyUnits);
        $netSuiteIntegration->restfulUrl
            ->update(['unit_sync_date_time' => now()]);
    }
}
