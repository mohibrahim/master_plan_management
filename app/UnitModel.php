<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use App\MasterPlanManagement\Services\Models\UnitModel\UnitModelTypeStrategy\SuitableUnitModelType;
use App\MasterPlanManagement\Services\Models\UnitModel\UnitModelBusinessTypeStrategy\SuitableUnitModelBusinessType;
use App\MasterPlanManagement\Services\Models\UnitModel\UnitModelFinishingTypeStrategy\SuitableUnitModelFinishingType;

class UnitModel extends Model
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
    protected $table = 'unit_models';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'code',
        'type',
        // 'business_type',
        'built_up_area',
        'land_area',
        'number_of_floors',
        'number_of_balconies',
        'number_of_bedrooms',
        'number_of_bathrooms',
        'number_of_kitchens',
        'number_of_elevators',
        // 'number_of_entries',
        // 'number_of_exits',
        // 'number_of_shops',
        // 'number_of_courts',
        'finishing_type',
        'has_garden',
        'garden_area',
        'has_pool',
        'pool_area',
        'notes',
        'creator_id',
        'last_updater_id',
        'net_suite_internal_id',
    ];

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Accessors
    |--------------------------------------------------------------------------
    */


    /*
    |--------------------------------------------------------------------------
    | Mutators
    |--------------------------------------------------------------------------
    */
    public function getTypeAttribute($data)
    {
        if (request()->route()->getName() != 'unit_models.edit') {
            return resolve(SuitableUnitModelType::class)->selectedValue($data);
        }
        return $data;
    }
    public function getBusinessTypeAttribute($data)
    {
        if (request()->route()->getName() != 'unit_models.edit') {
            return resolve(SuitableUnitModelBusinessType::class)->selectedValue($data);
        }
        return $data;
    }
    public function getFinishingTypeAttribute($data)
    {
        if (request()->route()->getName() != 'unit_models.edit') {
            return resolve(SuitableUnitModelFinishingType::class)->selectedValue($data);
        }
        return $data;
    }

    public function getUnitModelCatalogsShowLinksAttribute()
    {
        $unitModelCatalogs = $this->catalogs;
        if ($unitModelCatalogs->isNotEmpty()) {
            return $unitModelCatalogs->pluck('catalog_show_link')->toArray();
        }
        return [];
    }

    public function getUnitModelShowLinkAttribute()
    {
        if (!empty($this->id)) {
            return '<a href="'.action('UnitModelController@show', ['id'=>$this->id]).'" target="_blank">'.$this->name.'</a>';
        }
        return '';
    }
    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */

    /**
     * Getting the creator user who create this catalog.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    /**
     * Getting the last updater user who update this catalog.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function lastUpdater(): BelongsTo
    {
        return $this->belongsTo(User::class, 'last_updater_id', 'id');
    }

    /**
     * Getting the units that are associated with this unit model.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return void
     */
    public function units()
    {
        return $this->hasMany(Unit::class, 'unit_model_id', 'id');
    }

    /**
     * Getting the catalogs that are associated with this unit model.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return void
     */
    public function catalogs()
    {
        return $this->hasMany(Catalog::class, 'unit_model_id', 'id');
    }


    /**
     * Getting the projects that are associated with this unit model.
     * Cardinality Constraint: M:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsToMany
     */
    public function projects()
    {
        return $this->belongsToMany(Project::class, 'project_unit_model', 'unit_model_id', 'project_id');
    }

    /*
    |--------------------------------------------------------------------------
    | Miscellaneous
    |--------------------------------------------------------------------------
    */

    public function search(Builder $builder, string $keyword)
    {
        return $builder->with('creator')->where('name', 'like', "%$keyword%");
    }
}
