<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Catalog extends Model
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
    protected $table = 'catalogs';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'notes',
        'creator_id',
        'last_updater_id',
        'project_id',
        'phase_id',
        'zone_id',
        'unit_model_id',
        'unit_id',
        'block_id',
        'reservation_id',
        'net_suite_internal_id',
    ];
    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Accessors
    |--------------------------------------------------------------------------
    */

    public function getCoverImagePathAttribute()
    {
        $projectFile = $this->coverImage;
        if (!empty($projectFile)) {
            return asset($projectFile->name);
        }
        return ('/images/helper_images/no_image.png');
    }

    public function getAssociatedModelTypeDataAttribute()
    {
        $type = '';
        $id = '';
        $name = '';
        $project = $this->project;
        if (!empty($project)) {
            $url = action('ProjectController@show', ['id' => $project->id]);
            return ['catalog_type_project', $url, $project->name];
        }
        $phase = $this->phase;
        if (!empty($phase)) {
            $url = action('PhaseController@show', ['id' => $phase->id]);
            return ['catalog_type_phase', $url, $phase->name];
        }
        $zone = $this->zone;
        if (!empty($zone)) {
            $url = action('ZoneController@show', ['id' => $zone->id]);
            return ['catalog_type_zone', $url, $zone->name];
        }
        $block = $this->block;
        if (!empty($block)) {
            $url = action('BlockController@show', ['id' => $block->id]);
            return ['catalog_type_block', $url, $block->name];
        }
        $unitModel = $this->unitModel;
        if (!empty($unitModel)) {
            $url = action('UnitModelController@show', ['id' => $unitModel->id]);
            return ['catalog_type_unit_model', $url, $unitModel->name];
        }
        $unit = $this->unit;
        if (!empty($unit)) {
            $url = action('UnitController@show', ['id' => $unit->id]);
            return ['catalog_type_unit', $url, $unit->code];
        }
        $reservation = $this->reservation;
        if (!empty($reservation)) {
            $url = action('ReservationController@show', ['id' => $reservation->id]);
            return [
                'catalog_type_reservation',
                $url,
                $reservation->id . ' | ' .
                    optional($reservation->customer)->name . ' | ' .
                    optional($reservation->unit)->code . ' | ' .
                    optional(optional($reservation->unit)->project)->name
            ];
        }
        return [];
    }

    public function getCatalogShowLinkAttribute()
    {
        return '<a href="' . action('CatalogController@show', ['id' => $this->id]) . '" target="_blank">' . $this->name . '</a>';
    }
    /*
    |--------------------------------------------------------------------------
    | Mutators
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */

    /**
     * Getting the creator user who create this catalog.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    /**
     * Getting the last updater user who update this catalog.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function lastUpdater(): BelongsTo
    {
        return $this->belongsTo(User::class, 'last_updater_id', 'id');
    }

    /**
     * Getting the cover image for this associated catalog.
     * Cardinality Constraint: 1:1.
     * Degree of Relationship: Binary
     *
     * @return HasOne
     */
    public function coverImage(): HasOne
    {
        return $this->hasOne(ProjectFile::class, 'catalog_cover_image_id', 'id');
    }

    /**
     * Getting the files that are associated with this catalog.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return HasMany
     */
    public function files(): HasMany
    {
        return $this->hasMany(ProjectFile::class, 'catalog_id', 'id');
    }

    /**
     * Getting the project that are associated with this catalog.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function project(): BelongsTo
    {
        return $this->belongsTo(Project::class, 'project_id', 'id');
    }

    /**
     * Getting the phase that are associated with this catalog.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function phase(): BelongsTo
    {
        return $this->belongsTo(Phase::class, 'phase_id', 'id');
    }

    /**
     * Getting the zone that are associated with this catalog.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function zone(): BelongsTo
    {
        return $this->belongsTo(Zone::class, 'zone_id', 'id');
    }

    /**
     * Getting the block that are associated with this catalog.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function block(): BelongsTo
    {
        return $this->belongsTo(Block::class, 'block_id', 'id');
    }

    /**
     * Getting the unit model that are associated with this catalog.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function unitModel(): BelongsTo
    {
        return $this->belongsTo(UnitModel::class, 'unit_model_id', 'id');
    }

    /**
     * Getting the unit that are associated with this catalog.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function unit(): BelongsTo
    {
        return $this->belongsTo(Unit::class, 'unit_id', 'id');
    }

    /**
     * Getting the reservation that are associated with this catalog.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function reservation(): BelongsTo
    {
        return $this->belongsTo(Reservation::class, 'reservation_id', 'id');
    }

    /*
    |--------------------------------------------------------------------------
    | Miscellaneous
    |--------------------------------------------------------------------------
    */

    public function search(Builder $builder, string $keyword)
    {
        return $builder->with('creator')->where('name', 'like', "%$keyword%");
    }
}
