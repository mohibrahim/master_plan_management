<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Builder;

class Block extends Model
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
    protected $table = 'blocks';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'notes',
        'creator_id',
        'last_updater_id',
        'project_id',
        'zone_id',
        'html_area_tag_coords',
        'net_suite_internal_id',
    ];

    protected $appends = [
        'block_show_link',
    ];
    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Accessors
    |--------------------------------------------------------------------------
    */

    public function getSelectedProjectNameIdAttribute()
    {
        $project = $this->project;
        if (!empty($project)) {
            $id = $project->id;
            $name = $project->name;
            if (!empty($id) && !empty($name)) {
                return ['id'=>$id, 'name'=>$name];
            }
        }
        return [];
    }

    public function getSelectedZoneNameIdAttribute()
    {
        $zone = $this->zone;
        if (!empty($zone)) {
            $id = $zone->id;
            $name = $zone->name;
            if (!empty($id) && !empty($name)) {
                return ['id'=>$id, 'name'=>$name];
            }
        }
        return [];
    }

    public function getSelectedProjectZonesNamesIdsAttribute()
    {
        $project = $this->project;
        if (request()->old('old_selected_zone_name') && request()->old('zone_id')) {
            return [request()->old('zone_id') => request()->old('old_selected_zone_name')];
        } elseif (!empty($project)) {
            $zones = $project->zones;
            if ($zones->isNotEmpty()) {
                return $zones->pluck('name', 'id');
            }
        }
        return [];
    }

    public function getSelectedProjectMasterPlanImageAttribute()
    {
        $project = $this->project;
        if (!empty($project)) {
            return $project->master_plan_image;
        }
        return '';
    }

    public function getBlockShowLinkAttribute()
    {
        return '<a href="'.action('BlockController@show', ['id'=>$this->id]).'">'.$this->name.'</a>';
    }

    public function getBlockCatalogsShowLinksAttribute()
    {
        $blockCatalogs = $this->catalogs;
        if ($blockCatalogs->isNotEmpty()) {
            return $blockCatalogs->pluck('catalog_show_link')->toArray();
        }
        return [];
    }

    /*
    |--------------------------------------------------------------------------
    | Mutators
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */

    /**
     * Getting the creator user who create this catalog.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    /**
     * Getting the last updater user who update this catalog.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function lastUpdater(): BelongsTo
    {
        return $this->belongsTo(User::class, 'last_updater_id', 'id');
    }

    /**
     * Getting the project that are associated with this block.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id', 'id');
    }

    /**
     * Getting the zone that are associated with this block.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function zone()
    {
        return $this->belongsTo(Zone::class, 'zone_id', 'id');
    }

    /**
     * Getting the units that are associated with this block.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return void
     */
    public function units()
    {
        return $this->hasMany(Unit::class, 'block_id', 'id');
    }

    /**
     * Getting the catalogs that are associated with this zone.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return void
     */
    public function catalogs()
    {
        return $this->hasMany(Catalog::class, 'block_id', 'id');
    }



    /*
    |--------------------------------------------------------------------------
    | Miscellaneous
    |--------------------------------------------------------------------------
    */

    public function search(Builder $builder, string $keyword)
    {
        return $builder->with('creator', 'project')
            ->where('name', 'like', "%$keyword%")
            ->orWhereHas('project', function ($query) use ($keyword) {
                $query->where('name', 'like', "%$keyword%");
            });
    }
}
