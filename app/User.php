<?php

namespace App;

use App\Role;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\MasterPlanManagement\Services\WorldCountries\LanguageCountriesResolverInterface;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Builder;

class User extends Authenticatable //implements MustVerifyEmail
{
    use Notifiable;
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'address',
        'country',
        'date_of_birth',
        'mobile_number',
        'id_card_number',
        'password',
        'notes',
        'creator_id',
        'last_updater_id',
        'email_verified_at',
        'master_role_id',
        'net_suite_internal_id',
    ];

    protected $dates = ['date_of_birth'];

    protected $appends = ['age'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /*
    |--------------------------------------------------------------------------
    | Getters and Setters
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | Accessors
    |--------------------------------------------------------------------------
    */
    public function getAgeAttribute()
    {
        $dateOfBirth = $this->date_of_birth;
        if (!empty($dateOfBirth)) {
            return now()->parse($dateOfBirth)->age;
        } else {
            return '';
        }
    }


    public function getDateOfBirthAttribute($date)
    {
        if (!empty($date)) {
            return now()->parse($date)->format('Y-m-d');
        }
        return '';
    }

    /*
    |--------------------------------------------------------------------------
    | Mutators
    |--------------------------------------------------------------------------
    */

    public function getPersonalImagePathAttribute()
    {
        $projectFile = $this->personalImage;
        if (!empty($projectFile)) {
            return '/project_files/' . $projectFile->name;
        }
        return '/images/helper_images/no_image.png';
    }

    public function getUserRolesNamesIdsAttribute(): array
    {
        $roles = $this->roles;
        if ($roles->isNotEmpty()) {
            return $roles->pluck('name', 'id')->toArray();
        }
        return [];
    }
    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
     */

    /**
     * Getting the creator user who create this user.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Recursive
     *
     * @return BelongsTo
     */
    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    /**
     * Getting the users that are created by creator user.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Recursive
     *
     * @return HasMany
     */
    public function createdUsers(): HasMany
    {
        return $this->hasMany(User::class, 'creator_id', 'id');
    }

    /**
     * Getting the last updater user who update this user.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Recursive
     *
     * @return BelongsTo
     */
    public function lastUpdater(): BelongsTo
    {
        return $this->belongsTo(User::class, 'last_updater_id', 'id');
    }

    /**
     * Getting the users that are updated by the last updater user.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Recursive
     *
     * @return HasMany
     */
    public function updatedUsers(): HasMany
    {
        return $this->hasMany(User::class, 'last_updater_id', 'id');
    }

    /**
     * Getting the permissions that are created by this user.
     *
     * @return HasMany
     */
    public function createdPermissions(): HasMany
    {
        return $this->hasMany(Permission::class, 'creator_id', 'id');
    }

    /**
     * Getting the roles that are created by this user.
     *
     * @return HasMany
     */
    public function createdRoles(): HasMany
    {
        return $this->hasMany(Role::class, 'creator_id', 'id');
    }

    /**
     * Getting the project files that are created by this user.
     *
     * @return HasMany
     */
    public function createdProjectFiles()
    {
        return $this->hasMany(ProjectFile::class, 'creator_id', 'id');
    }
    /**
     * Getting the catalogs that are created by this user.
     *
     * @return HasMany
     */
    public function createdCatalogs()
    {
        return $this->hasMany(Catalog::class, 'creator_id', 'id');
    }

    /**
     * Getting the projects that are created by this user.
     *
     * @return HasMany
     */
    public function createdProjects()
    {
        return $this->hasMany(Project::class, 'creator_id', 'id');
    }

    //*************************** */



    /**
     * Getting the permissions that are created by this user.
     *
     * @return HasMany
     */
    public function updatedPermissions(): HasMany
    {
        return $this->hasMany(Permission::class, 'last_updater_id', 'id');
    }

    /**
     * Getting the roles that are updated by this user.
     *
     * @return HasMany
     */
    public function updatedRoles(): HasMany
    {
        return $this->hasMany(Role::class, 'last_updater_id', 'id');
    }

    /**
     * Getting the project files that are updated by this user.
     *
     * @return HasMany
     */
    public function updatedProjectFails(): HasMany
    {
        return $this->hasMany(ProjectFile::class, 'last_updater_id', 'id');
    }

    /**
     * Getting the catalogs that are updated by this user.
     *
     * @return HasMany
     */
    public function updatedCatalogs(): HasMany
    {
        return $this->hasMany(Catalog::class, 'last_updater_id', 'id');
    }

    /**
     * Getting the projects that are updated by this user.
     *
     * @return HasMany
     */
    public function updatedProjects(): HasMany
    {
        return $this->hasMany(Project::class, 'last_updater_id', 'id');
    }

    public function masterRole(): BelongsTo
    {
        return $this->belongsTo(Role::class, 'master_role_id', 'id');
    }


    //-------------------------

    /**
     * Getting the roles that are associated with this user.
     *
     * @return BelongsToMany
     */
    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * Getting the personal image that are belongs to this user.
     *
     * @return HasOne
     */
    public function personalImage(): HasOne
    {
        return $this->hasOne(ProjectFile::class, 'personal_image_user_id', 'id');
    }

    /**
     * Getting the project user assignments that are associated with this user.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return HasMany
     */
    public function projectUserAssignments(): HasMany
    {
        return $this->hasMany(ProjectUserAssignment::class, 'employee_id', 'id');
    }

    /**
     * Getting the reservations that salesman representative created it.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return HasMany
     */
    public function reservation()
    {
        return $this->hasMany(Reservation::class, 'sales_representative_id', 'id');
    }

    /*
    |--------------------------------------------------------------------------
    | Miscellaneous
    |--------------------------------------------------------------------------
     */

    public function getPermissionsNamesAsArray(): array
    {
        $roles = $this->roles;
        $permissionsNamesAsArray = [];
        if ($roles->isNotEmpty()) {
            foreach ($roles as $roleKey => $role) {
                $permissions = $role->permissions;
                if ($permissions->isNotEmpty()) {
                    foreach ($permissions as $permissionKey => $permission) {
                        $permissionsNamesAsArray[] = $permission->name;
                    }
                }
            }
        }
        return $permissionsNamesAsArray;
    }

    public function search(Builder $builder, $keyword)
    {
        $results = $builder->with('roles', 'creator')->where('name', 'like', "%$keyword%")->get();
        return $results;
    }
}
