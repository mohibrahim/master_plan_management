<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use App\MasterPlanManagement\Services\Models\Unit\UnitStatusStrategy\SuitableUnitStatus;
use App\MasterPlanManagement\Services\Models\Unit\UnitStatusStrategy\UnitStatusAbstract;
use App\MasterPlanManagement\Services\Models\Unit\UnitDirectionStrategy\SuitableUnitDirection;
use App\MasterPlanManagement\Services\Models\Unit\UnitConstructionStatusStrategy\SuitableUnitConstructionStatus;
use App\MasterPlanManagement\Services\Models\Unit\UnitCurrenciesStrategy\SuitableUnitCurrencies;
use App\MasterPlanManagement\Services\Models\Unit\UnitHasGardenStrategy\SuitableUnitHasGarden;
use App\MasterPlanManagement\Services\Models\Unit\UnitHasPoolStrategy\SuitableUnitHasPool;

class Unit extends Model
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
    protected $table = 'units';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'status',
        'land_number',
        'land_area',
        'construction_status',

        'booking_amount',
        'minimum_booking_amount',
        'currency',

        'percentage_of_completion',
        'developer_name',
        'is_for_sale',
        'sale_details',
        'sale_price',
        'date_of_sale',
        'is_for_rent',
        'rent_from',
        'rent_to',
        'rent_price',
        'rent_details',
        'unit_account_number',
        'direction',
        'floor_number',
        'address',
        'latitude',
        'longitude',
        'electricity_meter_number',
        'notes',
        'html_area_tag_coords',
        'creator_id',
        'last_updater_id',
        'project_id',
        'phase_id',
        'zone_id',
        'unit_model_id',
        'block_id',
        'net_suite_internal_id',
        'product_number',

        'land_price',
        'number_of_floors',
        'number_of_balconies',
        'number_of_bedrooms',
        'number_of_bathrooms',
        'number_of_kitchens',
        'number_of_elevators',
        'has_garden',
        'garden_area',
        'has_pool',
        'pool_area',
        'built_up_area',
    ];

    protected $dates = [
        'date_of_sale',
        'rent_from',
        'rent_to',
    ];

    protected $appends = ['unit_show_link', 'status_color', 'suitable_status', 'reservation_details', 'reserve_the_unit_link', 'sale_price_presentation', 'show_pdf_link_as_button'];

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Accessors
    |--------------------------------------------------------------------------
    */



    public function getSuitableStatusAttribute()
    {
        return resolve(SuitableUnitStatus::class)->selectedValue($this->status);
    }

    public function getConstructionStatusAttribute($data)
    {
        if (request()->route()->getName() != 'units.edit') {
            return resolve(SuitableUnitConstructionStatus::class)->selectedValue($data);
        }
        return $data;
    }

    public function getDirectionAttribute($data)
    {
        if (request()->route()->getName() != 'units.edit') {
            return resolve(SuitableUnitDirection::class)->selectedValue($data);
        }
        return $data;
    }

    public function getCurrencyAttribute($data)
    {
        if (request()->route()->getName() != 'units.edit') {
            return resolve(SuitableUnitCurrencies::class)->selectedValue($data);
        }
        return $data;
    }

    public function getHasGardenAttribute($data)
    {
        if (request()->route()->getName() != 'units.edit') {
            return resolve(SuitableUnitHasGarden::class)->selectedValue($data);
        }
        return $data;
    }

    public function getHasPoolAttribute($data)
    {
        if (request()->route()->getName() != 'units.edit') {
            return resolve(SuitableUnitHasPool::class)->selectedValue($data);
        }
        return $data;
    }

    public function getDateOfSaleAttribute($date)
    {
        if (!empty($date)) {
            return $this->asDateTime($date)->format('Y-m-d');
        }
        return '';
    }

    public function getRentFromAttribute($date)
    {
        if (!empty($date)) {
            return $this->asDateTime($date)->format('Y-m-d');
        }
        return '';
    }

    public function getRentToAttribute($date)
    {
        if (!empty($date)) {
            return $this->asDateTime($date)->format('Y-m-d');
        }
        return '';
    }

    public function getSelectedProjectNameIdAttribute()
    {
        $project = $this->project;
        if (empty($project)) {
            return [];
        }
        return ['name'=>$project->name, 'id'=>$project->id];
    }

    public function getSelectedUnitModelNameIdAttribute()
    {
        $unitModel = $this->unitModel;
        if (!empty($unitModel)) {
            $id = $unitModel->id;
            $name = $unitModel->name;
            if (!empty($id) && !empty($name)) {
                return ['id'=>$id, 'name'=>$name];
            }
        }
        return [];
    }

    public function getSelectedProjectPhasesNamesIdsAttribute()
    {
        $project = $this->project;
        if (request()->old('old_selected_phase_name') && request()->old('phase_id')) {
            return [request()->old('phase_id') => request()->old('old_selected_phase_name')];
        } elseif (!empty($project)) {
            $phases = $project->phases;
            if ($phases->isNotEmpty()) {
                return $phases->pluck('name', 'id');
            }
        }
        return [];
    }

    public function getSelectedProjectZonesNamesIdsAttribute()
    {
        $project = $this->project;
        if (request()->old('old_selected_zone_name') && request()->old('zone_id')) {
            return [request()->old('zone_id') => request()->old('old_selected_zone_name')];
        } elseif (!empty($project)) {
            $zones = $project->zones;
            if ($zones->isNotEmpty()) {
                return $zones->pluck('name', 'id');
            }
        }
        return [];
    }

    public function getSelectedProjectBlocksNamesIdsAttribute()
    {
        $project = $this->project;
        if (request()->old('old_selected_zone_name') && request()->old('zone_id')) {
            return [request()->old('zone_id') => request()->old('old_selected_zone_name')];
        } elseif (!empty($project)) {
            $blocks = $project->blocks;
            if ($blocks->isNotEmpty()) {
                return $blocks->pluck('name', 'id');
            }
        }
        return [];
    }


    public function getSelectedProjectMasterPlanImageAttribute()
    {
        $project = $this->project;
        if (!empty($project)) {
            return $project->master_plan_image;
        }
        return '';
    }

    public function getStatusColorAttribute()
    {
        $statusColor = [
            UnitStatusAbstract::AVAILABLE => '9ECB62',
            UnitStatusAbstract::HOLD => 'F7C042',
            UnitStatusAbstract::BOOKED => 'EC3223',
            UnitStatusAbstract::RESERVED => 'FFA39C',
            UnitStatusAbstract::SOLD => 'FFFD53',
            UnitStatusAbstract::HANDED_OVER => '00FF44',
        ];
        $suitableStatusAttr = $this->suitable_status;
        if (!empty($suitableStatusAttr)) {
            $suitableUnitStatus = resolve(SuitableUnitStatus::class)->getList();
            $key = array_search($suitableStatusAttr, $suitableUnitStatus);
            if (!empty($statusColor[$key])) {
                return $statusColor[$key];
            }
        }
        return 'black';

    }

    public function getReservationDetailsAttribute()
    {
        $reservation = $this->reservation;
        if (
            !empty($reservation) &&
            ($this->status == UnitStatusAbstract::BOOKED ||
            $this->status == UnitStatusAbstract::RESERVED)
        ) {
            return $reservation->reservation_show_link;
        }
        return '';
    }

    public function getIsBookedAttribute(): bool
    {
        $reservation = $this->reservation;
        if (!empty($reservation)) {
            return true;
        }
        return false;
    }

    public function getUnitShowLinkAttribute()
    {
        if (!empty($this->id)) {
            return '<a href="'.action('UnitController@show', ['id'=>$this->id]).'" target="_blank">'.$this->code.'</a>';
        }
        return '';
    }
    public function getUnitShowLinkWithCodeAttribute()
    {
        if (!empty($this->id)) {
            return '<a href="'.action('UnitController@show', ['id'=>$this->id]).'">'.$this->code.' | '.optional($this->project)->name.'</a>';
        }
        return '';
    }

    public function getReserveTheUnitLinkAttribute()
    {
        // if (!$this->getIsBookedAttribute()) {
        if ($this->status == UnitStatusAbstract::AVAILABLE) {
            return '<a href="'. action('ReservationController@create', ['unit_id'=>$this->id]) .'" class="btn btn-sm btn-success btn-block" target="_blank"><i class="fas fa-check-square"></i> '.__('unit.reserve_unit').' </a>';
        }
        return '';
    }

    public function getShowPdfLinkAsButtonAttribute()
    {
        return '<a href="'.action('UnitController@showAsPdf', ['unit'=>$this->id]).'" class="btn btn-sm  btn-secondary btn-block" target="_blank"> <i class="far fa-file-pdf"></i>'. __('unit.show_as_pdf').' </a>';
    }

    public function getUnitCatalogsShowLinksAttribute()
    {
        $unitCatalogs = $this->catalogs;
        if ($unitCatalogs->isNotEmpty()) {
            return $unitCatalogs->pluck('catalog_show_link')->toArray();
        }
        return [];
    }


    public function getSalePricePresentationAttribute()
    {
        return number_format($this->attributes['sale_price'], 2, '.', ',');
    }

    public function getLandPricePresentationAttribute()
    {
        return number_format($this->attributes['land_price'], 2, '.', ',');
    }

    public function getBookingAmountPresentationAttribute()
    {
        return number_format($this->attributes['booking_amount'], 2, '.', ',');
    }
    public function getMinimumBookingAmountPresentationAttribute()
    {
        return number_format($this->attributes['minimum_booking_amount'], 2, '.', ',');
    }

    /*
    |--------------------------------------------------------------------------
    | Mutators
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */

    /**
     * Getting the creator user who create this catalog.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    /**
     * Getting the last updater user who update this catalog.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function lastUpdater(): BelongsTo
    {
        return $this->belongsTo(User::class, 'last_updater_id', 'id');
    }


    /**
     * Getting the project that are associated with this unit.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function project(): BelongsTo
    {
        return $this->belongsTo(Project::class, 'project_id', 'id');
    }

    /**
     * Getting the phase that are associated with this unit.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function phase(): BelongsTo
    {
        return $this->belongsTo(Phase::class, 'phase_id', 'id');
    }

    /**
     * Getting the zone that are associated with this unit.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function zone(): BelongsTo
    {
        return $this->belongsTo(Zone::class, 'zone_id', 'id');
    }

    /**
     * Getting the block that are associated with this unit.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function block(): BelongsTo
    {
        return $this->belongsTo(Block::class, 'block_id', 'id');
    }

    /**
     * Getting the unit model that are associated with this unit.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function unitModel(): BelongsTo
    {
        return $this->belongsTo(UnitModel::class, 'unit_model_id', 'id');
    }

    /**
     * Getting the catalogs that are associated with this unit.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return HasMany
     */
    public function catalogs(): HasMany
    {
        return $this->hasMany(Catalog::class, 'unit_id', 'id');
    }

    /**
     * Getting the reservation that are associated with this unit.
     * Cardinality Constraint: 1:1.
     * Degree of Relationship: Binary
     *
     * @return HasOne
     */
    public function reservation(): HasOne
    {
        return $this->hasOne(Reservation::class, 'unit_id', 'id');
    }



    /*
    |--------------------------------------------------------------------------
    | Miscellaneous
    |--------------------------------------------------------------------------
    */

    public function search(Builder $builder, string $keyword, float $salePriceRangeMin, float $salePriceRangeMax)
    {
        return $builder->with('creator', 'project', 'zone', 'unitModel')
            ->whereBetween('sale_price', [$salePriceRangeMin, $salePriceRangeMax])
            ->where(function ($query) use ($keyword) {
                $query->where('code', 'like', "%$keyword%")
                    ->orWhere('sale_price', 'like', "%$keyword%")
                    ->orWhereHas('project', function ($query) use ($keyword) {
                        $query->where('name', 'like', "%$keyword%");
                    })
                    ->orWhereHas('zone', function ($query) use ($keyword) {
                        $query->where('name', 'like', "%$keyword%");
                    })
                    ->orWhereHas('unitModel', function ($query) use ($keyword) {
                        $query->where('name', 'like', "%$keyword%");
                    });
            });
    }

    public function getRelatedCatalogsLinks()
    {
        $catalogsLinks = '';
        $projectCatalogs = optional($this->project)->project_catalogs_show_links;
        if (!empty($projectCatalogs)) {
            $catalogsLinks .= implode(', ', $projectCatalogs);
        }
        $phaseCatalogs = optional($this->phase)->phase_catalogs_show_links;
        if (!empty($phaseCatalogs)) {
            $catalogsLinks .= implode(', ', $phaseCatalogs);
        }
        $zoneCatalogs = optional($this->zone)->zone_catalogs_show_links;
        if (!empty($zoneCatalogs)) {
            $catalogsLinks .= implode(', ', $zoneCatalogs);
        }
        $blockCatalogs = optional($this->block)->block_catalogs_show_links;
        if (!empty($blockCatalogs)) {
            $catalogsLinks .= implode(', ', $blockCatalogs);
        }
        $unitModelCatalogs = optional($this->unitModel)->unit_model_catalogs_show_links;
        if (!empty($unitModelCatalogs)) {
            $catalogsLinks .= implode(', ', $unitModelCatalogs);
        }
        $unitCatalogs = $this->getUnitCatalogsShowLinksAttribute();
        if (!empty($unitCatalogs)) {
            $catalogsLinks .= implode(', ', $unitCatalogs);
        }
        return $catalogsLinks;

    }

    public function getRelatedCatalogsImagesPdfs()
    {
        $files = [];

        $projectCatalogs = optional($this->project)->catalogs;
        foreach ($projectCatalogs as $catalog) {
            $files[] = [
                'catalog_name' => $catalog->name,
                'catalog_files' => $catalog->files()->get(['type', 'name', 'title']),
            ];
        }

        $zoneCatalogs = optional($this->zone)->catalogs;
        foreach ($zoneCatalogs as $catalog) {
            $files[] = [
                'catalog_name' => $catalog->name,
                'catalog_files' => $catalog->files()->get(['type', 'name', 'title']),
            ];
        }

        $zoneCatalogs = optional($this->unitModel)->catalogs;
        foreach ($zoneCatalogs as $catalog) {
            $files[] = [
                'catalog_name' => $catalog->name,
                'catalog_files' => $catalog->files()->get(['type', 'name', 'title']),
            ];
        }

        $unitCatalogs = $this->catalogs;
        foreach ($unitCatalogs as $catalog) {
            $files[] = [
                'catalog_name' => $catalog->name,
                'catalog_files' => $catalog->files()->get(['type', 'name', 'title']),
            ];
        }

        return $files;
    }
}
