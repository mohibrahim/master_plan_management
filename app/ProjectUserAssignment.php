<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Builder;

class ProjectUserAssignment extends Model
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
    protected $table = 'project_user_assignments';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'notes',
        'creator_id',
        'last_updater_id',
        'project_id',
        'employee_id',
        'net_suite_internal_id',
    ];

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Accessors
    |--------------------------------------------------------------------------
    */

    public function getSelectedProjectsNamesIdsAttribute()
    {
        $employeeId = $this->employee_id;
        $employee = User::find($employeeId);
        if (isset($employee)) {
            $projectUserAssignments = $employee->projectUserAssignments;
            if ($projectUserAssignments->isNotEmpty()) {
                return $projectUserAssignments->pluck('project.name', 'project.id')->toArray();
            }
        }
        return [];
    }

    public function getSelectedEmployeeNameIdAttribute()
    {
        $employee = $this->employee;
        if (!empty($employee)) {
            $id = $employee->id;
            $name = $employee->name;
            if (!empty($id) && !empty($name)) {
                return ['id'=>$id, 'name'=>$name];
            }
        }
        return [];
    }

    /*
    |--------------------------------------------------------------------------
    | Mutators
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */

    /**
     * Getting the creator user who create this catalog.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    /**
     * Getting the last updater user who update this catalog.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function lastUpdater(): BelongsTo
    {
        return $this->belongsTo(User::class, 'last_updater_id', 'id');
    }

    /**
     * Getting the project that are associated with this project user assignment.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id', 'id');
    }


    /**
     * Getting the employee that are associated with this project user assignment.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function employee(): BelongsTo
    {
        return $this->belongsTo(User::class, 'employee_id', 'id');
    }



    /*
    |--------------------------------------------------------------------------
    | Miscellaneous
    |--------------------------------------------------------------------------
    */

    public function search(Builder $builder, string $keyword)
    {
        return $builder->with('creator', 'project', 'employee')
            ->whereHas('employee', function($query) use($keyword){
                $query->where('name', 'like', "%$keyword%");
            })
            ->orWhereHas('project', function ($query) use ($keyword) {
                $query->where('name', 'like', "%$keyword%");
            });
    }
}
