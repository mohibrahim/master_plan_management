<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use App\MasterPlanManagement\Services\Models\ProjectFile\Type\ProjectFileTypes;

class ProjectFile extends Model
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    */

    protected $table = 'project_files';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'title',
        'type',
        'status',
        'notes',
        'creator_id',
        'last_updater_id',
        'personal_image_user_id',
        'catalog_cover_image_id',
        'catalog_id',
        'net_suite_internal_id',
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /*
    |--------------------------------------------------------------------------
    | Getters and Setters
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | Accessors
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | Mutators
    |--------------------------------------------------------------------------
     */

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
     */

    /**
     * Getting the creator user who create this project file.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    /**
     * Getting the last updater user who update this project file.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function lastUpdater(): BelongsTo
    {
        return $this->belongsTo(User::class, 'last_updater_id', 'id');
    }

    /**
     * Getting the user that are the personal image belongs to him.
     * Cardinality Constraint: 1:1.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function personalImageUser()
    {
        return $this->belongsTo(User::class, 'personal_image_user_id', 'id');
    }

    /**
     * Getting the catalog that are associated with this cover image.
     * Cardinality Constraint: 1:1.
     * Degree of Relationship: Binary
     * @return BelongsTo
     */
    public function catalogOfTheCover(): BelongsTo
    {
        return $this->belongsTo(Catalog::class, 'catalog_cover_image_id', 'id');
    }

    /**
     * Getting the catalog that are associated with this image.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     * @return BelongsTo
     */
    public function catalog(): BelongsTo
    {
        return $this->belongsTo(Catalog::class, 'catalog_id', 'id');
    }


    /*
    |--------------------------------------------------------------------------
    | Miscellaneous
    |--------------------------------------------------------------------------
    */

    public function search($keyword)
    {
        $results = ProjectFile::where('title', 'like', "%$keyword%")->get();
        return $results;
    }

    public function isPdf()
    {
        if ($this->type == ProjectFileTypes::PDF) {
            return true;
        }
        return false;
    }

    public function isImage()
    {
        if ($this->type == ProjectFileTypes::IMAGE) {
            return true;
        }
        return false;
    }

}
