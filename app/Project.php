<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use App\MasterPlanManagement\Services\Models\Unit\UnitStatusStrategy\UnitStatusAbstract;

class Project extends Model
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
    protected $table = 'projects';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'owner_name',// saving developer name in this field
        'master_plan_image',
        'master_plan_json',
        'supposed_start_date',
        'actually_start_date',
        'supposed_end_date',
        'actually_end_date',
        'percentage_of_completion',
        'notes',
        'creator_id',
        'last_updater_id',
        'net_suite_internal_id',
        'project_code',
        'logo',
    ];

    protected $dates = [
        'supposed_start_date',
        'actually_start_date',
        'supposed_end_date',
        'actually_end_date',
    ];

    protected $hidden = ['master_plan_json'];

    protected $appends = [
        'project_show_link',
        'project_units_statistics',
        'project_catalogs_show_links',
    ];
    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Accessors
    |--------------------------------------------------------------------------
    */

    public function getMasterPlanImageAttribute($masterPlanImage)
    {
        if (empty($masterPlanImage)) {
            return 'images/master_plan_images/no_image.png';
        }
        return $masterPlanImage;
    }

    public function getMasterPlanImageSrcAttribute(): string
    {
        $masterPlanImage = $this->master_plan_image;
        if (!empty($masterPlanImage)) {
            return asset( $masterPlanImage);
        }
        return '';
    }

    public function getProjectUnitsStatisticsAttribute()
    {
        $statistics['total_count'] = DB::table('units')
            ->where('project_id', $this->id)
            ->count();
        $statistics['available_count'] = DB::table('units')
            ->where('status', UnitStatusAbstract::AVAILABLE)
            ->where('project_id', $this->id)
            ->count();
        $statistics['hold_count'] = DB::table('units')
            ->where('status', UnitStatusAbstract::HOLD)
            ->where('project_id', $this->id)
            ->count();
        $statistics['booked_count'] = DB::table('units')
            ->where('status', UnitStatusAbstract::BOOKED)
            ->where('project_id', $this->id)
            ->count();
        $statistics['reserved_count'] = DB::table('units')
            ->where('status', UnitStatusAbstract::RESERVED)
            ->where('project_id', $this->id)
            ->count();
        $statistics['sold_count'] = DB::table('units')
            ->where('status', UnitStatusAbstract::SOLD)
            ->where('project_id', $this->id)
            ->count();
        $statistics['handed_over_count'] = DB::table('units')
            ->where('status', UnitStatusAbstract::HANDED_OVER)
            ->where('project_id', $this->id)
            ->count();
        return $statistics;
    }

    public function getSupposedStartDateAttribute($date)
    {
        if (!empty($date)) {
            return $this->asDateTime($date)->format('Y-m-d');
        }
        return '';
    }

    public function getActuallyStartDateAttribute($date)
    {
        if (!empty($date)) {
            return $this->asDateTime($date)->format('Y-m-d');
        }
        return '';
    }

    public function getSupposedEndDateAttribute($date)
    {
        if (!empty($date)) {
            return $this->asDateTime($date)->format('Y-m-d');
        }
        return '';
    }

    public function getActuallyEndDateAttribute($date)
    {
        if (!empty($date)) {
            return $this->asDateTime($date)->format('Y-m-d');
        }
        return '';
    }

    public function getPercentageOfCompletionAttribute($percentage)
    {
        if (request()->route()->getName() == 'projects.edit') {
            return $percentage;
        } elseif (!empty($percentage)) {
            return $percentage . "%";
        }
        return '0 %';
    }

    public function getZonesNamesIdsAttribute()
    {
        $zones = $this->zones;
        if ($zones->isNotEmpty()) {
            return $zones->pluck('name', 'id');
        }
        return [];
    }

    public function getProjectShowLinkAttribute()
    {
        return '<a href="' . action('ProjectController@show', ['id' => $this->id]) . '">' . $this->name . '</a>';
    }

    public function getProjectCatalogsShowLinksAttribute()
    {
        $projectCatalogs = $this->catalogs;
        if ($projectCatalogs->isNotEmpty()) {
            return $projectCatalogs->pluck('catalog_show_link')->toArray();
        }
        return [];
    }

    public function getUnitsModelsNamesIdsAttribute()
    {
        $unitModels = $this->unitModels;
        if ($unitModels->isNotEmpty()) {
            return $unitModels->pluck('name', 'id');
        }
        return [];
    }


    /*
    |--------------------------------------------------------------------------
    | Mutators
    |--------------------------------------------------------------------------
    */


    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */

    /**
     * Getting the creator user who create this catalog.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    /**
     * Getting the last updater user who update this catalog.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function lastUpdater(): BelongsTo
    {
        return $this->belongsTo(User::class, 'last_updater_id', 'id');
    }

    /**
     * Getting the phases that are associated with this project.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     * @return HasMany
     */
    public function phases(): HasMany
    {
        return $this->hasMany(Phase::class, 'project_id', 'id');
    }

    /**
     * Getting the blocks that are associated with this project.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     * @return HasMany
     */
    public function blocks(): HasMany
    {
        return $this->hasMany(Block::class, 'project_id', 'id');
    }

    /**
     * Getting the zones that are associated with this project.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     * @return HasMany
     */
    public function zones(): HasMany
    {
        return $this->hasMany(Zone::class, 'project_id', 'id');
    }

    /**
     * Getting the units that are associated with this project.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     * @return HasMany
     */
    public function units(): HasMany
    {
        return $this->hasMany(Unit::class, 'project_id', 'id');
    }

    /**
     * Getting the catalogs that are associated with this project.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     * @return HasMany
     */
    public function catalogs(): HasMany
    {
        return $this->hasMany(Catalog::class, 'project_id', 'id');
    }

    /**
     * Getting the project user assignments that are associated with this project.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     * @return HasMany
     */
    public function projectUserAssignments(): HasMany
    {
        return $this->hasMany(ProjectUserAssignment::class, 'project_id', 'id');
    }

    /**
     * Getting the Units Models that are associated with this project.
     * Cardinality Constraint: M:M.
     * Degree of Relationship: Binary
     * @return BelongsToMany
     */
    public function unitModels(): BelongsToMany
    {
        return $this->belongsToMany(UnitModel::class, 'project_unit_model', 'project_id', 'unit_model_id');
    }



    /*
    |--------------------------------------------------------------------------
    | Miscellaneous
    |--------------------------------------------------------------------------
    */

    public function search(Builder $builder, string $keyword)
    {
        return $builder->with('creator')
            ->where('name', 'like', "%$keyword%")
            ->orWhere('project_code', 'like', "%$keyword%");
    }
}
