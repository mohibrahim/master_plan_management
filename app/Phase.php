<?php

namespace App;

use App\Project;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Phase extends Model
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
    protected $table = 'phases';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'supposed_start_date',
        'actually_start_date',
        'supposed_end_date',
        'actually_end_date',
        'percentage_of_completion',
        'notes',
        'creator_id',
        'last_updater_id',
        'project_id',
        'net_suite_internal_id',
    ];

    protected $dates = [
        'supposed_start_date',
        'actually_start_date',
        'supposed_end_date',
        'actually_end_date',
    ];
    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Accessors
    |--------------------------------------------------------------------------
    */


    /*
    |--------------------------------------------------------------------------
    | Mutators
    |--------------------------------------------------------------------------
    */

    public function getSupposedStartDateAttribute($date)
    {
        if (!empty($date)) {
            return $this->asDateTime($date)->format('Y-m-d');
        }
        return '';
    }

    public function getActuallyStartDateAttribute($date)
    {
        if (!empty($date)) {
            return $this->asDateTime($date)->format('Y-m-d');
        }
        return '';
    }

    public function getSupposedEndDateAttribute($date)
    {
        if (!empty($date)) {
            return $this->asDateTime($date)->format('Y-m-d');
        }
        return '';
    }

    public function getActuallyEndDateAttribute($date)
    {
        if (!empty($date)) {
            return $this->asDateTime($date)->format('Y-m-d');
        }
        return '';
    }

    public function getPercentageOfCompletionAttribute($percentage)
    {
        if (request()->route()->getName() == 'phases.edit') {
            return $percentage;
        } elseif (!empty($percentage)) {
            return $percentage. "%";
        }
        return '0 %';
    }

    public function getSelectedProjectNameIdAttribute()
    {
        $project = $this->project;
        if (!empty($project)) {
            $id = $project->id;
            $name = $project->name;
            if (!empty($id) && !empty($name)) {
                return ['id'=>$id, 'name'=>$name];
            }
        }
        return [];
    }

    public function getPhaseShowLinkAttribute()
    {
        if (!empty($this->id)) {
            return '<a href="'.action('PhaseController@show', ['id'=>$this->id]).'">'.$this->name.'</a>';
        }
        return '';
    }

    public function getPhaseCatalogsShowLinksAttribute()
    {
        $phaseCatalogs = $this->catalogs;
        if ($phaseCatalogs->isNotEmpty()) {
            return $phaseCatalogs->pluck('catalog_show_link')->toArray();
        }
        return [];
    }

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */

    /**
     * Getting the creator user who create this catalog.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    /**
     * Getting the last updater user who update this catalog.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function lastUpdater(): BelongsTo
    {
        return $this->belongsTo(User::class, 'last_updater_id', 'id');
    }

    /**
     * Getting the project that are associated with this phase.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id', 'id');
    }

    /**
     * Getting the zones that are associated with this phase.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     * @return HasMany
     */
    public function zones(): HasMany
    {
        return $this->hasMany(Zone::class, 'phase_id', 'id');
    }

    /**
     * Getting the units that are associated with this phase.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     * @return HasMany
     */
    public function units(): HasMany
    {
        return $this->hasMany(Unit::class, 'phase_id', 'id');
    }

    /**
     * Getting the catalogs that are associated with this phase.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     * @return HasMany
     */
    public function catalogs(): HasMany
    {
        return $this->hasMany(Catalog::class, 'phase_id', 'id');
    }





    /*
    |--------------------------------------------------------------------------
    | Miscellaneous
    |--------------------------------------------------------------------------
    */

    public function search(Builder $builder, string $keyword)
    {
        return $builder->with('creator', 'project')
            ->where('name', 'like', "%$keyword%")
            ->orWhereHas('project', function($query) use($keyword){
                $query->where('name', 'like', "%$keyword%");
            });
    }
}
