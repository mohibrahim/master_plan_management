<?php

namespace App;

use App\MasterPlanManagement\Services\Models\Reservation\ReservationStatusStrategy\SuitableReservationStatus;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Reservation extends Model
{
    use FormAccessible;
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
    protected $table = 'reservations';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'booking_date',
        'bank_account_details',
        'expiry_date',
        'status',
        'booking_status',
        'reason',
        'booking',
        'paid_amount',
        'remaining_amount',
        'notes',
        'creator_id',
        'last_updater_id',
        'unit_id',
        'customer_id',
        'net_suite_internal_id',
        'net_suite_created_at',
        'net_suite_updated_at',
        'sales_representative_id',
        'bank_id',
        'document_number',
    ];

    protected $dates = ['booking_date', 'expiry_date', 'net_suite_created_at', 'net_suite_updated_at'];
    protected $appends = ['reservation_show_link_id'];

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Accessors
    |--------------------------------------------------------------------------
    */

    public function getExpiryDateAttribute($date)
    {
        if (!empty($date)) {
            return $this->asDateTime($date)->format('Y-m-d');
        }
        return '';
    }

    public function getStatusAttribute($data)
    {
        return resolve(SuitableReservationStatus::class)->selectedValue($data);
    }

    public function formStatusAttribute($data)
    {
        return $data;
    }

    public function getBookingDateAttribute($date)
    {
        if (!empty($date)) {
            return $this->asDateTime($date)->format('Y-m-d');
        }
        return '';
    }

    public function getSelectedUnitCodeIdAttribute()
    {
        $unit = $this->unit;
        if (!empty($unit)) {
            $id = $unit->id;
            $code = $unit->code;
            if (!empty($id) && !empty($code)) {
                return ['id'=>$id, 'code'=>$code];
            }
        }
        return [];
    }

    public function getSelectedCustomerNameIdAttribute()
    {
        $customer = $this->customer;
        if (!empty($customer)) {
            $id = $customer->id;
            $name = $customer->name;
            if (!empty($id) && !empty($name)) {
                return ['id'=>$id, 'name'=>$name];
            }
        }
        return [];
    }

    public function getReservationShowLinkAttribute(): string
    {
        if (!empty($this->id)) {
            return '<a href="'.action('ReservationController@show', ['id'=>$this->id]).'" target="_blank">'.__('reservation.reservation_details').'</a>';
        }
        return '';
    }

    public function getReservationShowLinkIdAttribute(): string
    {
        if (!empty($this->id)) {
            return '<a href="'.action('ReservationController@show', ['id'=>$this->id]).'" target="_blank">'.$this->id.'</a>';
        }
        return '';
    }

    public function getPaidAmountPresentationAttribute()
    {
        return number_format($this->attributes['paid_amount'], 2, '.', ',');
    }

    public function getRemainingAmountPresentationAttribute()
    {
        return number_format($this->attributes['remaining_amount'], 2, '.', ',');
    }

    public function getSalesRepresentativeNameAttribute()
    {
        return optional(User::where('id', $this->attributes['sales_representative_id'])->first())->name;
    }

    public function getBankIdAttribute($bankId)
    {
        if (!empty(request()->route()) && request()->route()->named('reservations.create')) {
            return [];
        }
        if (!empty(request()->route()) && request()->route()->named('reservations.edit')) {
            $realEstateDeveloper = Bank::find($bankId);
            if (empty($realEstateDeveloper)) {
                return [$bankId => $bankId];
            }
            return [$bankId => $realEstateDeveloper->name];
        }
        return $bankId;
    }



    /*
    |--------------------------------------------------------------------------
    | Mutators
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    */

    /**
     * Getting the creator user who create this catalog.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    /**
     * Getting the last updater user who update this catalog.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function lastUpdater(): BelongsTo
    {
        return $this->belongsTo(User::class, 'last_updater_id', 'id');
    }

    /**
     * Getting the unit that are associated with this reservation.
     * Cardinality Constraint: 1:1.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function unit(): BelongsTo
    {
        return $this->belongsTo(Unit::class, 'unit_id', 'id');
    }

    /**
     * Getting the customer that are associated with this reservation.
     * Cardinality Constraint: 1:1.
     * Degree of Relationship: Binary
     *
     * @return BelongsTo
     */
    public function customer(): BelongsTo
    {
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }

    /**
     * Getting the catalogs that are associated with this reservation.
     * Cardinality Constraint: 1:M.
     * Degree of Relationship: Binary
     * @return HasMany
     */
    public function catalogs(): HasMany
    {
        return $this->hasMany(Catalog::class, 'reservation_id', 'id');
    }

    /**
     * Getting the salesman representative that make this reservation.
     *
     * @return HasOne
     */
    public function salesRepresentative(): HasOne
    {
        return $this->hasOne(User::class, 'sales_representative_id', 'id');
    }

    /**
     * Getting the bank that are associated with this reservations.
     *
     * @return BelongsTo
     */
    public function bank(): BelongsTo
    {
        return $this->belongsTo(Bank::class, 'bank_id', 'id');
    }

    /*
    |--------------------------------------------------------------------------
    | Miscellaneous
    |--------------------------------------------------------------------------
    */

    public function search(Builder $builder, string $keyword)
    {
        return $builder->with('creator', 'unit', 'customer')
            ->where('booking_date', 'like', "%$keyword%")
            ->orWhereHas('unit', function($query) use($keyword){
                $query->where('code', 'like', "%$keyword%");
            })
            ->orWhereHas('customer', function($query) use($keyword){
                $query->where('name', 'like', "%$keyword%")
                    ->orWhere('national_id', 'like', "%$keyword%")
                    ->orWhere('phone', 'like', "%$keyword%")
                    ->orWhere('secondary_phone', 'like', "%$keyword%")
                    ->orWhere('alt_phone', 'like', "%$keyword%");
            });
    }
}
