<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'name' => 'required',
            'email' => 'required|unique:users,email,' . $this->user,
            // 'country' => 'required',
            'mobile_number' => [
                'required',
                // 'regex:/^(010|011|012|015)\d{8}$/',
                'unique:users,mobile_number,' . $this->user,
            ],
            'id_card_number' => 'numeric|nullable',
            'personal_image' => 'image|mimes:jpeg,jpg,png|nullable',
            'password' => [
                'string',
                'min:6',
                'confirmed',
                ($this->route()->named('users.update'))?('nullable'):('')
            ],
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'برجاء إدخال الاسم الثلاثي.',
            'email. required' => 'برجاء إدخال البريد الإلكتروني.',
            'email.unique' => 'البريد الإلكتروني محجوز لشخص آخر, برجاء اختيار  بريد آخر.',
            'country.required' => 'برجاء اختيار الدولة.',
            'mobile_number.required' => 'برجاء إدخال رقم التليفون المحمول.',
            'mobile_number.regex' => 'برجاء إدخال رقم التليفون المحمول موافق لأرقام التليفونات المحمولة داخل جمهورية مصر العربية.',
            'mobile_number.unique' => 'رقم التليفون المحمول الذي أدخلته مسجل من قبل لشخص آخر, برجاء إدخال رقم آخر.',
            'personal_image.image' => 'برجاء إدراج الصورة الشخصية كا ملف صورة.',
            'personal_image.mimes' => 'برجاء إدراج الصورة الشخصية كا ملف بأمتداد JPEG أو JPG أو PNG',
            'password.string' => 'برجاء إدخال كلمة السر.',
            'password.min' => 'برجاء إدخال كلمة السر لا تقل عن 6 حروف.',
            'password.confirmed' => 'كلمة السر المدخله غير مطابقة  لتأكيد كلمة السر.'
        ];
    }
}
