<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CatalogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=> 'required',
            'catalog_images.*' =>'image',
            'catalog_images_titles.*' =>'required',
            
            'catalog_pdfs.*' =>'mimes:pdf',
        ];
    }

   
    public function messages()
    {
        return [
            'catalog_images.*.image' => 'برجاء إضافة ملف بامتداد jpg, jpeg, png',
            'catalog_images_titles.*.required' => 'عنوان الصورة مطلوب',
            'catalog_pdfs.*.mimes' => 'برجاء إضافة ملف بامتداد pdf',
            'catalog_pdfs_titles.*.required' => 'عنوان الملف مطلوب',
        ];
    }

    
}
