<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PhaseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required',
            'percentage_of_completion' => 'int|between:0,100|nullable',

        ];


        return $rules;
    }

    public function attributes()
    {
        return [
            'name' => __('phase.name'),
            'percentage_of_completion' => __('phase.percentage_of_completion'),
        ];
    }
}
