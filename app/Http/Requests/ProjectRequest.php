<?php

namespace App\Http\Requests;

use App\Project;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Http\FormRequest;

class ProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required',
            'master_plan_image' => ['required'],
            'logo' => ['nullable', 'image'],
            'master_plan_json' => ['json','nullable'],
            'percentage_of_completion' => 'numeric|between:0,100|nullable',
            'new_image_width' => ['int','nullable'],
        ];

        if ($this->route()->named('projects.update')) {
            $project = Project::findOrFail($this->project);
            if (Storage::exists($project->master_plan_image)) {
                $rules['master_plan_image'] = [];
            }
        }

        if ($this->hasFile('master_plan_image')) {
            $imageWidth = (getimagesize($this->file('master_plan_image')))[0];
            $rules['new_image_width'][] = 'between:600,'.$imageWidth;
            $rules['master_plan_image'][] = 'image';
        }

        return $rules;
    }

    public function attributes()
    {
        return [
            'name' => __('project.name'),
            'master_plan_image' => __('project.master_plan_image'),
            'percentage_of_completion' => __('project.percentage_of_completion'),
            'master_plan_json' => __('project.master_plan_json'),
            'new_image_width' => __('project.new_image_width'),
        ];
    }
}
