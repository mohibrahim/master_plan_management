<?php

namespace App\Http\Requests;

use App\Unit;
use Illuminate\Foundation\Http\FormRequest;

class ReservationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'unit_id' => 'required|unique:reservations,unit_id',
            'customer_id' => 'required',
            'booking_date' => 'required',
        ];

        if ($this->route()->named('reservations.update')) {
            $rules['unit_id'] = 'required|unique:reservations,unit_id,' . $this->reservation;
        }
        //1
        $rules = $this->isBookingChecked($rules);
        return $rules;
    }

    public function attributes()
    {
        return [
            'booking_date' => __('reservation.booking_date'),
            'unit_id' => __('reservation.unit_code'),
            'customer_id' => __('reservation.customer_name'),
            'paid_amount' => __('reservation.paid_amount'),
            'remaining_amount' => __('reservation.remaining_amount'),
        ];
    }

    public function messages()
    {
        return [
            'unit_id.unique' => __('reservation.reserved_before_unique_validation'),
            'minimum_booking_amount.required' => __('reservation.minimum_booking_amount'),
            'paid_amount_less_than_minimum_booking_amount.required' => __('reservation.paid_amount_less_than_minimum_booking_amount'),
        ];
    }

    private function isBookingChecked(array $rules): array
    {
        $booking = $this->input('booking');
        // dd($booking);
        if (!empty($booking)) {
            //FIXME:  uncomment this rules
            // $rules['paid_amount'] = 'required';
            // $rules['remaining_amount'] = 'required';
            // $rules = $this->isPaidAmountLessThanMinimumBookingAmount($rules);
        }
        return $rules;
    }

    private function isPaidAmountLessThanMinimumBookingAmount(array $rules): array
    {
        $unitId = $this->input('unit_id');
        if (empty($unitId)) {
            return $rules;
        }

        $unit = Unit::find($unitId);
        if (empty($unit)) {
            return $rules;
        }

        $minimumBookingAmount = $unit->minimum_booking_amount;
        if (empty($minimumBookingAmount)) {
            $rules['minimum_booking_amount'] = 'required';
        }

        $paidAmount = $this->input('paid_amount');
        if ($minimumBookingAmount > $paidAmount) {
            $rules['paid_amount_less_than_minimum_booking_amount'] = 'required';
        }
        return $rules;
    }
}
