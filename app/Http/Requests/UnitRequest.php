<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UnitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'project_id' => 'required',
            'phase_id' => 'required',
            'zone_id' => 'required',
            'unit_model_id' => 'required',
            'code' => 'required',
            'status' => 'required',
            'land_area' => 'numeric|nullable',
            'built_up_area' => 'numeric|nullable',
            'number_of_floors' => 'numeric|nullable',
            'number_of_balconies' => 'numeric|nullable',
            'number_of_bedrooms' => 'numeric|nullable',
            'number_of_bathrooms' => 'numeric|nullable',
            'number_of_kitchens' => 'numeric|nullable',
            'number_of_elevators' => 'numeric|nullable',
            'garden_area' => 'numeric|nullable',
            'pool_area' => 'numeric|nullable',

        ];
        return $rules;
    }

    public function attributes()
    {
        return [
            'project_id' => __('unit.project_name'),
            'phase_id' => __('unit.phase_name'),
            'zone_id' => __('unit.zone_name'),
            'unit_model_id' => __('unit.unit_model_name'),
            'code' => __('unit.code'),
            'status' => __('unit.status'),
        ];
    }
}
