<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ZoneRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'project_id' => 'required',
            'phase_id' => 'required',
            'name' => 'required',

        ];
        return $rules;
    }

    public function attributes()
    {
        return [
            'name' => __('zone.name'),
            'project_id' => __('zone.project_name'),
            'phase_id' => __('zone.phase_name'),
        ];
    }
}
