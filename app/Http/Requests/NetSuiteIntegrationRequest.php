<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NetSuiteIntegrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required',
            'project_api_index'=> 'url|nullable',
            'phase_api_index'=> 'url|nullable',
            'zone_api_index'=> 'url|nullable',
            'block_api_index'=> 'url|nullable',
            'unit_model_api_index'=> 'url|nullable',
            'unit_api_index'=> 'url|nullable',
            'customer_api_index'=> 'url|nullable',
            'bank_api_index'=> 'url|nullable',
            'reservation_api_index'=> 'url|nullable',
            'reservation_api_store'=> 'url|nullable',
        ];
        return $rules;
    }

    public function attributes()
    {
        return [
            'name' => __('net_suite_integration.name'),
            'project_api_index' => __('net_suite_integration.project_api_index'),
            'phase_api_index' => __('net_suite_integration.phase_api_index'),
            'zone_api_index' => __('net_suite_integration.zone_api_index'),
            'block_api_index' => __('net_suite_integration.block_api_index'),
            'unit_model_api_index' => __('net_suite_integration.unit_model_api_index'),
            'unit_api_index' => __('net_suite_integration.unit_api_index'),
            'customer_api_index' => __('net_suite_integration.customer_api_index'),
            'bank_api_index' => __('net_suite_integration.bank_api_index'),
            'reservation_api_index' => __('net_suite_integration.reservation_api_index'),
            'reservation_api_store' => __('net_suite_integration.reservation_api_store'),
        ];
    }
}
