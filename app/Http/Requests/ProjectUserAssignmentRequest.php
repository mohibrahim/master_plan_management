<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjectUserAssignmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'project_id' => 'required',
            'employee_id' => 'required',

        ];
        return $rules;
    }

    public function attributes()
    {
        return [
            'project_id' => __('project_user_assignment.project_name'),
            'employee_id' => __('project_user_assignment.employee_name'),
        ];
    }
}
