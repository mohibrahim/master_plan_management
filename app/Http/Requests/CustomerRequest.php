<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required',
            'customer_code' => 'required',
            'email' => 'email|nullable',
            'phone' => 'numeric|required',

        ];
        return $rules;
    }

    public function attributes()
    {
        return [
            'name' => __('customer.name'),
            'customer_code' => __('customer.customer_code'),
            'email' => __('customer.email'),
            'phone' => __('customer.phone'),
        ];
    }
}
