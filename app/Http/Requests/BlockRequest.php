<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BlockRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'project_id' => 'required',
            'zone_id' => 'required',
            'name' => 'required',

        ];
        return $rules;
    }

    public function attributes()
    {
        return [
            'name' => __('block.name'),
            'project_id' => __('block.project_name'),
            'zone_id' => __('block.zone_name'),
        ];
    }
}
