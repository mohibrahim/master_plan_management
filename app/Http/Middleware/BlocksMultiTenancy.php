<?php

namespace App\Http\Middleware;

use Closure;
use App\Block;

class BlocksMultiTenancy
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       $response = redirect(action('HomeController@index'));
        if (auth()->check()) {
            $model = $request->block;
            $modelCreatorId = $this->getModelCreatorId($model);
            $authenticatedUserId = $request->user()->id;
            $permissions = $request->user()->getPermissionsNamesAsArray();
            $routeName = $request->route()->getName();
            if ($modelCreatorId == $authenticatedUserId) {
                $response = $next($request);
            } elseif ($routeName == 'blocks.show' && in_array('show_blocks_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'blocks.edit' && in_array('update_blocks_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'blocks.update' && in_array('update_blocks_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'blocks.destroy' && in_array('destroy_blocks_multi_tenancy', $permissions)) {
                $response = $next($request);
            } else {
                abort(403);
            }
        }
        return $response;
    }
    
    private function getModelCreatorId($model) : int
    {
        if ($model instanceof Block) {
            $modelCreatorId = (int)$model->creator_id;
        } else {
            $block = Block::findOrFail($model);
            if (isset($block)) {
                $modelCreatorId = (int)$block->creator_id;
            }
        }
        return $modelCreatorId;
    }
}
