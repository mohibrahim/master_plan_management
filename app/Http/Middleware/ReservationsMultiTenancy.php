<?php

namespace App\Http\Middleware;

use Closure;
use App\Reservation;

class ReservationsMultiTenancy
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       $response = redirect(action('HomeController@index'));
        if (auth()->check()) {
            $model = $request->reservation;
            $modelCreatorId = $this->getModelCreatorId($model);
            $authenticatedUserId = $request->user()->id;
            $permissions = $request->user()->getPermissionsNamesAsArray();
            $routeName = $request->route()->getName();
            if ($modelCreatorId == $authenticatedUserId) {
                $response = $next($request);
            } elseif ($routeName == 'reservations.show' && in_array('show_reservations_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'reservations.edit' && in_array('update_reservations_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'reservations.update' && in_array('update_reservations_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'reservations.destroy' && in_array('destroy_reservations_multi_tenancy', $permissions)) {
                $response = $next($request);
            } else {
                abort(403);
            }
        }
        return $response;
    }
    
    private function getModelCreatorId($model) : int
    {
        if ($model instanceof Reservation) {
            $modelCreatorId = (int)$model->creator_id;
        } else {
            $reservation = Reservation::findOrFail($model);
            if (isset($reservation)) {
                $modelCreatorId = (int)$reservation->creator_id;
            }
        }
        return $modelCreatorId;
    }
}
