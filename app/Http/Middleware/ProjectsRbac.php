<?php

namespace App\Http\Middleware;

use Closure;

class ProjectsRbac
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $routeName = $request->route()->getName();
        $response = redirect(action('HomeController@index'));
        $permissions = [];


        if (auth()->check()) {
            $permissions = $request->user()->getPermissionsNamesAsArray();
        }

        if (empty($permissions)) {
            return $response;
        }
        if ($routeName == 'projects.index' && in_array('index_projects', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'projects.show' && in_array('show_projects', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'projects.create' && in_array('store_projects', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'projects.store' && in_array('store_projects', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'projects.edit' && in_array('update_projects', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'projects.update' && in_array('update_projects', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'projects.destroy' && in_array('destroy_projects', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'projects_miscellaneous.index_search' && in_array('index_projects', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'projects_miscellaneous.delete_image_ajax' && in_array('update_projects', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'projects_miscellaneous.delete_logo' && in_array('update_projects', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'projects_miscellaneous.filter_master_plan_image' && in_array('show_projects', $permissions)) {
            $response = $next($request);
        } else {
            abort(403);
        }
        return $response;
    }
}
