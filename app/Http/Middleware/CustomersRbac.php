<?php

namespace App\Http\Middleware;

use Closure;

class CustomersRbac
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $routeName = $request->route()->getName();
        $response = redirect(action('HomeController@index'));
        $permissions = [];


        if (auth()->check()) {
            $permissions = $request->user()->getPermissionsNamesAsArray();
        }
        if (empty($permissions)) {
            return $response;
        }
        if ($routeName == 'customers.index' && in_array('index_customers', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'customers.show' && in_array('show_customers', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'customers.create' && in_array('store_customers', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'customers.store' && in_array('store_customers', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'customers.edit' && in_array('update_customers', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'customers.update' && in_array('update_customers', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'customers.destroy' && in_array('destroy_customers', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'customers_miscellaneous.index_search' && in_array('index_customers', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'customers_miscellaneous.delete_image_ajax' && in_array('update_customers', $permissions)) {
            $response = $next($request);
        } else {
            abort(403);
        }
        return $response;
    }
}
