<?php

namespace App\Http\Middleware;

use Closure;

class UsersRbac
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $routeName = $request->route()->getName();
        $response = redirect(action('HomeController@index'));
        $permissions = [];

        if (auth()->check()) {
            $permissions = $request->user()->getPermissionsNamesAsArray();
        }

        if (empty($permissions)) {
            return $response;
        }

        if ($routeName == 'users.index' && in_array('index_users', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'users.show' && in_array('show_users', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'users.create' && in_array('store_users', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'users.store' && in_array('store_users', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'users.edit' && in_array('update_users', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'users.update' && in_array('update_users', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'users.destroy' && in_array('destroy_users', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'users_miscellaneous.index_search' && in_array('index_users', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'users_miscellaneous.delete_personal_image_ajax' && in_array('update_users', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'users_miscellaneous.index_users_not_verified_email' && in_array('index_users_not_verified_email', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'users_miscellaneous.verify_user_email' && in_array('verify_user_email', $permissions)) {
            $response = $next($request);
        } else {
            abort(403);
        }
        return $response;
    }
}
