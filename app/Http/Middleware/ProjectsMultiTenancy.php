<?php

namespace App\Http\Middleware;

use Closure;
use App\Catalog;
use App\Project;

class ProjectsMultiTenancy
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       $response = redirect(action('HomeController@index'));
        if (auth()->check()) {
            $model = $request->project;
            $modelCreatorId = $this->getModelCreatorId($model);
            $authenticatedUserId = $request->user()->id;
            $permissions = $request->user()->getPermissionsNamesAsArray();
            $routeName = $request->route()->getName();
            if ($modelCreatorId == $authenticatedUserId) {
                $response = $next($request);
            } elseif ($routeName == 'projects.show' && in_array('show_projects_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'projects.edit' && in_array('update_projects_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'projects.update' && in_array('update_projects_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'projects.destroy' && in_array('destroy_projects_multi_tenancy', $permissions)) {
                $response = $next($request);
            } else {
                abort(403);
            }
        }
        return $response;
    }

    private function getModelCreatorId($model) : int
    {
        if ($model instanceof Project) {
            $modelCreatorId = (int)$model->creator_id;
        } else {
            $project = Project::findOrFail($model);
            if (isset($project)) {
                $modelCreatorId = (int)$project->creator_id;//if null it will casted to zero
            }
        }
        return $modelCreatorId;
    }
}
