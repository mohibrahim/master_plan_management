<?php

namespace App\Http\Middleware;

use Closure;
use App\Customer;

class CustomersMultiTenancy
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       $response = redirect(action('HomeController@index'));
        if (auth()->check()) {
            $model = $request->customer;
            $modelCreatorId = $this->getModelCreatorId($model);
            $authenticatedUserId = $request->user()->id;
            $permissions = $request->user()->getPermissionsNamesAsArray();
            $routeName = $request->route()->getName();
            if ($modelCreatorId == $authenticatedUserId) {
                $response = $next($request);
            } elseif ($routeName == 'customers.show' && in_array('show_customers_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'customers.edit' && in_array('update_customers_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'customers.update' && in_array('update_customers_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'customers.destroy' && in_array('destroy_customers_multi_tenancy', $permissions)) {
                $response = $next($request);
            } else {
                abort(403);
            }
        }
        return $response;
    }
    
    private function getModelCreatorId($model) : int
    {
        if ($model instanceof Customer) {
            $modelCreatorId = (int)$model->creator_id;
        } else {
            $customer = Customer::findOrFail($model);
            if (isset($customer)) {
                $modelCreatorId = (int)$customer->creator_id;
            }
        }
        return $modelCreatorId;
    }
}
