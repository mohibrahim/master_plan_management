<?php

namespace App\Http\Middleware;

use Closure;
use App\Unit;

class UnitsMultiTenancy
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       $response = redirect(action('HomeController@index'));
        if (auth()->check()) {
            $model = $request->unit;
            $modelCreatorId = $this->getModelCreatorId($model);
            $authenticatedUserId = $request->user()->id;
            $permissions = $request->user()->getPermissionsNamesAsArray();
            $routeName = $request->route()->getName();
            if ($modelCreatorId == $authenticatedUserId) {
                $response = $next($request);
            } elseif ($routeName == 'units.show' && in_array('show_units_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'units.edit' && in_array('update_units_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'units.update' && in_array('update_units_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'units.destroy' && in_array('destroy_units_multi_tenancy', $permissions)) {
                $response = $next($request);
            } else {
                abort(403);
            }
        }
        return $response;
    }
    
    private function getModelCreatorId($model) : int
    {
        if ($model instanceof Unit) {
            $modelCreatorId = (int)$model->creator_id;
        } else {
            $unit = Unit::findOrFail($model);
            if (isset($unit)) {
                $modelCreatorId = (int)$unit->creator_id;
            }
        }
        return $modelCreatorId;
    }
}
