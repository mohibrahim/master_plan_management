<?php

namespace App\Http\Middleware;

use Closure;
use App\Zone;

class ZonesMultiTenancy
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       $response = redirect(action('HomeController@index'));
        if (auth()->check()) {
            $model = $request->zone;
            $modelCreatorId = $this->getModelCreatorId($model);
            $authenticatedUserId = $request->user()->id;
            $permissions = $request->user()->getPermissionsNamesAsArray();
            $routeName = $request->route()->getName();
            if ($modelCreatorId == $authenticatedUserId) {
                $response = $next($request);
            } elseif ($routeName == 'zones.show' && in_array('show_zones_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'zones.edit' && in_array('update_zones_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'zones.update' && in_array('update_zones_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'zones.destroy' && in_array('destroy_zones_multi_tenancy', $permissions)) {
                $response = $next($request);
            } else {
                abort(403);
            }
        }
        return $response;
    }
    
    private function getModelCreatorId($model) : int
    {
        if ($model instanceof Zone) {
            $modelCreatorId = (int)$model->creator_id;
        } else {
            $zone = Zone::findOrFail($model);
            if (isset($zone)) {
                $modelCreatorId = (int)$zone->creator_id;
            }
        }
        return $modelCreatorId;
    }
}
