<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use App\ProjectFile;

class ProjectFilesMultiTenancy
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = redirect(action('HomeController@index'));
        if (auth()->check()) {
            $model = $request->project_file;
            $modelCreatorId = $this->getModelCreatorId($model);
            $authenticatedUserId = $request->user()->id;
            $permissions = $request->user()->getPermissionsNamesAsArray();
            $routeName = $request->route()->getName();
            if ($modelCreatorId == $authenticatedUserId) {
                $response = $next($request);
            } elseif ($routeName == 'project_files.show' && in_array('show_project_files_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'project_files.edit' && in_array('update_project_files_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'project_files.update' && in_array('update_project_files_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'project_files.destroy' && in_array('destroy_project_files_multi_tenancy', $permissions)) {
                $response = $next($request);
            } else {
                abort(403);
            }
        }
        return $response;
    }

    private function getModelCreatorId($model) : int
    {
        if ($model instanceof ProjectFile) {
            $modelCreatorId = (int)$model->creator_id;
        } else {
            $projectFile = ProjectFile::findOrFail($model);
            if (isset($projectFile)) {
                $modelCreatorId = (int)$projectFile->creator_id;//if null it will casted to zero
            }
        }
        return $modelCreatorId;
    }
}
