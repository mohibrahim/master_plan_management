<?php

namespace App\Http\Middleware;

use Closure;
use App\Role;

class RolesMultiTenancy
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = redirect(action('HomeController@index'));
        if (auth()->check()) {
            $model = $request->role;
            $modelCreatorId = $this->getModelCreatorId($model);
            $authenticatedUserId = $request->user()->id;
            $permissions = $request->user()->getPermissionsNamesAsArray();
            $routeName = $request->route()->getName();
            if ($modelCreatorId == $authenticatedUserId) {
                $response = $next($request);
            } elseif ($routeName == 'roles.show' && in_array('show_roles_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'roles.edit' && in_array('update_roles_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'roles.update' && in_array('update_roles_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'roles.destroy' && in_array('destroy_roles_multi_tenancy', $permissions)) {
                $response = $next($request);
            } else {
                abort(403);
            }
        }
        return $response;
    }

    private function getModelCreatorId($model) : int
    {
        if ($model instanceof Role) {
            $modelCreatorId = (int)$model->creator_id;
        } else {
            $role = Role::findOrFail($model);
            if (isset($role)) {
                $modelCreatorId = (int)$role->creator_id;//if null it will casted to zero
            }
        }
        return $modelCreatorId;
    }
}
