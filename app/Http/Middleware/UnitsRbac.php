<?php

namespace App\Http\Middleware;

use Closure;

class UnitsRbac
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $routeName = $request->route()->getName();
        $response = redirect(action('HomeController@index'));
        $permissions = [];


        if (auth()->check()) {
            $permissions = $request->user()->getPermissionsNamesAsArray();
        }
        if (empty($permissions)) {
            return $response;
        }
        if ($routeName == 'units.index' && in_array('index_units', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'units.show' && in_array('show_units', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'units.create' && in_array('store_units', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'units.store' && in_array('store_units', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'units.edit' && in_array('update_units', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'units.update' && in_array('update_units', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'units.destroy' && in_array('destroy_units', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'units_miscellaneous.index_search' && in_array('index_units', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'units_miscellaneous.get_project_names_ids' && in_array('store_units', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'units_miscellaneous.get_unit_model_names_ids' && in_array('store_units', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'units_miscellaneous.delete_image_ajax' && in_array('update_units', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'units_miscellaneous.show_as_pdf' && in_array('show_units', $permissions)) {
            $response = $next($request);
        } else {
            abort(403);
        }
        return $response;
    }
}
