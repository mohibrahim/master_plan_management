<?php

namespace App\Http\Middleware;

use Closure;

class BlocksRbac
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $routeName = $request->route()->getName();
        $response = redirect(action('HomeController@index'));
        $permissions = [];
     

        if (auth()->check()) {
            $permissions = $request->user()->getPermissionsNamesAsArray();
        }
        if (empty($permissions)) {
            return $response;
        }
        if ($routeName == 'blocks.index' && in_array('index_blocks', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'blocks.show' && in_array('show_blocks', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'blocks.create' && in_array('store_blocks', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'blocks.store' && in_array('store_blocks', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'blocks.edit' && in_array('update_blocks', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'blocks.update' && in_array('update_blocks', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'blocks.destroy' && in_array('destroy_blocks', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'blocks_miscellaneous.index_search' && in_array('index_blocks', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'blocks_miscellaneous.get_project_names_ids' && in_array('store_blocks', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'blocks_miscellaneous.delete_image_ajax' && in_array('update_blocks', $permissions)) {
            $response = $next($request);
        } else {
            abort(403);
        }
        return $response;
    }
}
