<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CatalogsRbac
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $routeName = $request->route()->getName();
        $response = redirect(action('HomeController@index'));
        $permissions = [];

        if (auth()->check()) {
            $permissions = $request->user()->getPermissionsNamesAsArray();
        }

        if (empty($permissions)) {
            return $response;
        }

        if ($routeName == 'catalogs.index' && in_array('index_catalogs', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'catalogs.show' && in_array('show_catalogs', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'catalogs.create' && in_array('store_catalogs', $permissions) && $this->isAbleToCreateCatalogOfHisType($request, $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'catalogs.store' && in_array('store_catalogs', $permissions) && $this->isAbleToStoreCatalogOfHisType($request, $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'catalogs.edit' && in_array('update_catalogs', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'catalogs.update' && in_array('update_catalogs', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'catalogs.destroy' && in_array('destroy_catalogs', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'catalogs_miscellaneous.index_search' && in_array('index_catalogs', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'catalogs_miscellaneous.delete_file_ajax' && in_array('update_catalogs', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'catalogs_miscellaneous.download_file' && in_array('show_catalogs', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'catalogs_miscellaneous.display_file' && in_array('show_catalogs', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'catalogs_miscellaneous.show_reservation_catalog' && in_array('show_catalogs', $permissions)) {
            $response = $next($request);
        } else {
            abort(403);
        }
        return $response;
    }

    //TODO: link it and i has issue with store request.. create working ok
    public function isAbleToCreateCatalogOfHisType(Request $request, $permissions)
    {
        $modelAssociatedType = $request->input('model_associated_type');

        if ($modelAssociatedType == 'project_id' && in_array('store_projects', $permissions)) {
            return true;
        } elseif ($modelAssociatedType == 'phase_id' && in_array('store_phases', $permissions)) {
            return true;
        } elseif ($modelAssociatedType == 'zone_id' && in_array('store_zones', $permissions)) {
            return true;
        } elseif ($modelAssociatedType == 'unit_model_id' && in_array('store_unit_models', $permissions)) {
            return true;
        } elseif ($modelAssociatedType == 'unit_id' && in_array('store_units', $permissions)) {
            return true;
        } elseif ($modelAssociatedType == 'block_id' && in_array('store_blocks', $permissions)) {
            return true;
        } elseif ($modelAssociatedType == 'reservation_id' && in_array('store_reservations', $permissions)) {
            return true;
        }elseif ($request->route()->named('catalogs.create')) {
            return true;
        }

        return false;
    }
    public function isAbleToStoreCatalogOfHisType($request, $permissions)
    {

        if (!empty($request->input('project_id')) && in_array('store_projects', $permissions)) {
            return true;
        } elseif (!empty($request->input('phase_id')) && in_array('store_phases', $permissions)) {
            return true;
        } elseif (!empty($request->input('zone_id')) && in_array('store_zones', $permissions)) {
            return true;
        } elseif (!empty($request->input('unit_model_id')) && in_array('store_unit_models', $permissions)) {
            return true;
        } elseif (!empty($request->input('unit_id')) && in_array('store_units', $permissions)) {
            return true;
        } elseif (!empty($request->input('block_id')) && in_array('store_blocks', $permissions)) {
            return true;
        } elseif (!empty($request->input('reservation_id')) && in_array('store_reservations', $permissions)) {
            return true;
        }elseif ($request->route()->named('catalogs.store')) {
            return true;
        }
        return false;
    }
}
