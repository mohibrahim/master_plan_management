<?php

namespace App\Http\Middleware;

use Closure;

class ReservationsRbac
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $routeName = $request->route()->getName();
        $response = redirect(action('HomeController@index'));
        $permissions = [];


        if (auth()->check()) {
            $permissions = $request->user()->getPermissionsNamesAsArray();
        }
        if (empty($permissions)) {
            return $response;
        }
        if ($routeName == 'reservations.index' && in_array('index_reservations', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'reservations.show' && in_array('show_reservations', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'reservations.create' && in_array('store_reservations', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'reservations.store' && in_array('store_reservations', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'reservations.edit' && in_array('update_reservations', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'reservations.update' && in_array('update_reservations', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'reservations.destroy' && in_array('destroy_reservations', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'reservations_miscellaneous.index_search' && in_array('index_reservations', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'reservations_miscellaneous.delete_image_ajax' && in_array('update_reservations', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'reservations_miscellaneous.get_unit_codes_ids' && (in_array('store_reservations', $permissions) || in_array('update_reservations', $permissions))) {
            $response = $next($request);
        } elseif ($routeName == 'reservations_miscellaneous.get_customer_names_ids' && (in_array('store_reservations', $permissions) || in_array('update_reservations', $permissions))) {
            $response = $next($request);
        } elseif ($routeName == 'reservations_miscellaneous.get_banks_names_ids' && (in_array('store_reservations', $permissions) || in_array('update_reservations', $permissions))) {
            $response = $next($request);
        } else {
            abort(403);
        }
        return $response;
    }
}
