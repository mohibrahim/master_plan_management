<?php

namespace App\Http\Middleware;

use Closure;
use App\Catalog;

class CatalogsMultiTenancy
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       $response = redirect(action('HomeController@index'));
        if (auth()->check()) {
            $model = $request->catalog;
            $modelCreatorId = $this->getModelCreatorId($model);
            $authenticatedUserId = $request->user()->id;
            $permissions = $request->user()->getPermissionsNamesAsArray();
            $routeName = $request->route()->getName();
            if ($modelCreatorId == $authenticatedUserId) {
                $response = $next($request);
            } elseif ($routeName == 'catalogs.show' && in_array('show_catalogs_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'catalogs.edit' && in_array('update_catalogs_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'catalogs.update' && in_array('update_catalogs_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'catalogs.destroy' && in_array('destroy_catalogs_multi_tenancy', $permissions)) {
                $response = $next($request);
            } else {
                abort(403);
            }
        }
        return $response;
    }
    
    private function getModelCreatorId($model) : int
    {
        if ($model instanceof Catalog) {
            $modelCreatorId = (int)$model->creator_id;
        } else {
            $catalog = Catalog::findOrFail($model);
            if (isset($catalog)) {
                $modelCreatorId = (int)$catalog->creator_id;//if null it will casted to zero
            }
        }
        return $modelCreatorId;
    }
}
