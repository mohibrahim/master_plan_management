<?php

namespace App\Http\Middleware;

use Closure;

class NetSuiteIntegrationsRbac
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $routeName = $request->route()->getName();
        $response = redirect(action('HomeController@index'));
        $permissions = [];


        if (auth()->check()) {
            $permissions = $request->user()->getPermissionsNamesAsArray();
        }

        if (empty($permissions)) {
            return $response;
        }
        if ($routeName == 'net_suite_integrations.index' && in_array('index_net_suite_integrations', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'net_suite_integrations.show' && in_array('show_net_suite_integrations', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'net_suite_integrations.create' && in_array('store_net_suite_integrations', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'net_suite_integrations.store' && in_array('store_net_suite_integrations', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'net_suite_integrations.edit' && in_array('update_net_suite_integrations', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'net_suite_integrations.update' && in_array('update_net_suite_integrations', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'net_suite_integrations.destroy' && in_array('destroy_net_suite_integrations', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'net_suite_integrations_miscellaneous.index_search' && in_array('index_net_suite_integrations', $permissions)) {
            $response = $next($request);
        } elseif (
            $routeName == 'net_suite_integrations_miscellaneous.sync_projects_data' &&
            (in_array('store_projects', $permissions) && in_array('update_projects', $permissions) && in_array('destroy_projects', $permissions))
        ) {
            $response = $next($request);
        } elseif (
            $routeName == 'net_suite_integrations_miscellaneous.sync_customers_data' &&
            (in_array('store_customers', $permissions) && in_array('update_customers', $permissions) && in_array('destroy_customers', $permissions))
        ) {
            $response = $next($request);
        } elseif (
            $routeName == 'net_suite_integrations_miscellaneous.sync_phases_data' &&
            (in_array('store_phases', $permissions) && in_array('update_phases', $permissions) && in_array('destroy_phases', $permissions))
        ) {
            $response = $next($request);
        } elseif (
            $routeName == 'net_suite_integrations_miscellaneous.sync_zones_data' &&
            (in_array('store_zones', $permissions) && in_array('update_zones', $permissions) && in_array('destroy_zones', $permissions))
        ) {
            $response = $next($request);
        } elseif (
            $routeName == 'net_suite_integrations_miscellaneous.sync_blocks_data' &&
            (in_array('store_blocks', $permissions) && in_array('update_blocks', $permissions) && in_array('destroy_blocks', $permissions))
        ) {
            $response = $next($request);
        } elseif (
            $routeName == 'net_suite_integrations_miscellaneous.sync_unit_models_data' &&
            (in_array('store_unit_models', $permissions) && in_array('update_unit_models', $permissions) && in_array('destroy_unit_models', $permissions))
        ) {
            $response = $next($request);
        } elseif (
            $routeName == 'net_suite_integrations_miscellaneous.sync_units_data' &&
            (in_array('store_units', $permissions) && in_array('update_units', $permissions) && in_array('destroy_units', $permissions))
        ) {
            $response = $next($request);
        } elseif (
            $routeName == 'net_suite_integrations_miscellaneous.sync_reservations_data' &&
            (in_array('store_reservations', $permissions) && in_array('update_reservations', $permissions) && in_array('destroy_reservations', $permissions))
        ) {
            $response = $next($request);
        } elseif (
            $routeName == 'net_suite_integrations_miscellaneous.sync_banks_data' &&
            (in_array('store_banks', $permissions) && in_array('update_banks', $permissions) && in_array('destroy_banks', $permissions))
        ) {
            $response = $next($request);
        } else {
            abort(403);
        }
        return $response;
    }
}
