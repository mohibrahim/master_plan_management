<?php

namespace App\Http\Middleware;

use Closure;
use App\Phase;

class PhasesMultiTenancy
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       $response = redirect(action('HomeController@index'));
        if (auth()->check()) {
            $model = $request->phase;
            $modelCreatorId = $this->getModelCreatorId($model);
            $authenticatedUserId = $request->user()->id;
            $permissions = $request->user()->getPermissionsNamesAsArray();
            $routeName = $request->route()->getName();
            if ($modelCreatorId == $authenticatedUserId) {
                $response = $next($request);
            } elseif ($routeName == 'phases.show' && in_array('show_phases_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'phases.edit' && in_array('update_phases_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'phases.update' && in_array('update_phases_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'phases.destroy' && in_array('destroy_phases_multi_tenancy', $permissions)) {
                $response = $next($request);
            } else {
                abort(403);
            }
        }
        return $response;
    }
    
    private function getModelCreatorId($model) : int
    {
        if ($model instanceof Phase) {
            $modelCreatorId = (int)$model->creator_id;
        } else {
            $phase = Phase::findOrFail($model);
            if (isset($phase)) {
                $modelCreatorId = (int)$phase->creator_id;//if null it will casted to zero
            }
        }
        return $modelCreatorId;
    }
}
