<?php

namespace App\Http\Middleware;

use Closure;

class PhasesRbac
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $routeName = $request->route()->getName();
        $response = redirect(action('HomeController@index'));
        $permissions = [];
     

        if (auth()->check()) {
            $permissions = $request->user()->getPermissionsNamesAsArray();
        }

        if (empty($permissions)) {
            return $response;
        }
        if ($routeName == 'phases.index' && in_array('index_phases', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'phases.show' && in_array('show_phases', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'phases.create' && in_array('store_phases', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'phases.store' && in_array('store_phases', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'phases.edit' && in_array('update_phases', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'phases.update' && in_array('update_phases', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'phases.destroy' && in_array('destroy_phases', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'phases_miscellaneous.index_search' && in_array('index_phases', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'phases_miscellaneous.get_project_names_ids' && in_array('store_phases', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'phases_miscellaneous.delete_image_ajax' && in_array('update_phases', $permissions)) {
            $response = $next($request);
        } else {
            abort(403);
        }
        return $response;
    }
}
