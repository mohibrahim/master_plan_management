<?php

namespace App\Http\Middleware;

use Closure;

class ProjectFilesRbac
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $routeName = $request->route()->getName();
        $response = redirect(action('HomeController@index'));
        $permissions = [];

        if (auth()->check()) {
            $permissions = $request->user()->getPermissionsNamesAsArray();
        }

        if (empty($permissions)) {
            return $response;
        }

        if ($routeName == 'project_files.index' && in_array('index_project_files', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'project_files.show' && in_array('show_project_files', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'project_files.create' && in_array('store_project_files', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'project_files.store' && in_array('store_project_files', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'project_files.edit' && in_array('update_project_files', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'project_files.update' && in_array('update_project_files', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'project_files.destroy' && in_array('destroy_project_files', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'project_files_miscellaneous.index_search' && in_array('index_project_files', $permissions)) {
            $response = $next($request);
        } else {
            abort(403);
        }
        return $response;
    }
}
