<?php

namespace App\Http\Middleware;

use Closure;
use App\UnitModel;

class UnitModelsMultiTenancy
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       $response = redirect(action('HomeController@index'));
        if (auth()->check()) {
            $model = $request->unit_model;
            $modelCreatorId = $this->getModelCreatorId($model);
            $authenticatedUserId = $request->user()->id;
            $permissions = $request->user()->getPermissionsNamesAsArray();
            $routeName = $request->route()->getName();
            if ($modelCreatorId == $authenticatedUserId) {
                $response = $next($request);
            } elseif ($routeName == 'unit_models.show' && in_array('show_unit_models_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'unit_models.edit' && in_array('update_unit_models_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'unit_models.update' && in_array('update_unit_models_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'unit_models.destroy' && in_array('destroy_unit_models_multi_tenancy', $permissions)) {
                $response = $next($request);
            } else {
                abort(403);
            }
        }
        return $response;
    }
    
    private function getModelCreatorId($model) : int
    {
        if ($model instanceof UnitModel) {
            $modelCreatorId = (int)$model->creator_id;
        } else {
            $unitModel = UnitModel::findOrFail($model);
            if (isset($unitModel)) {
                $modelCreatorId = (int)$unitModel->creator_id;
            }
        }
        return $modelCreatorId;
    }
}
