<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Cookie;

class LocaleSessionCreator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $isHasLocal = Cookie::has('current_language');
        if (!$isHasLocal) {
            Cookie::queue('current_language', 'ar', 525600);
            app()->setLocale('ar');
            return $next($request);
        }
        app()->setLocale(Cookie::get('current_language'));
        return $next($request);
    }
}
