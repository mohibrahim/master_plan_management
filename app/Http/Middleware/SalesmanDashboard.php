<?php

namespace App\Http\Middleware;

use Closure;

class SalesmanDashboard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $authenticatedUser = auth()->user();
        $masterRoleName = optional($authenticatedUser->masterRole)->name;
        $salesmanRoleName = config('masterplanmanagement.roles_names_dashboard_controllers_names.' . $masterRoleName);

        if ($salesmanRoleName === 'Salesman') {
            return $next($request);
        }
        return abort(403);
    }
}
