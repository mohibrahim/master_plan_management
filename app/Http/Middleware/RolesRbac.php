<?php

namespace App\Http\Middleware;

use Closure;

class RolesRbac
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $routeName = $request->route()->getName();
        $response = redirect(action('HomeController@index'));
        $permissions = [];

        if (auth()->check()) {
            $permissions = $request->user()->getPermissionsNamesAsArray();
        }

        if (empty($permissions)) {
            return $response;
        }
        
        if ($routeName == 'roles.index' && in_array('index_roles', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'roles.show' && in_array('show_roles', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'roles.create' && in_array('store_roles', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'roles.store' && in_array('store_roles', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'roles.edit' && in_array('update_roles', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'roles.update' && in_array('update_roles', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'roles.destroy' && in_array('destroy_roles', $permissions)) {
            $response = $next($request);
        } else {
            abort(403);
        }
        return $response;
    }
}
