<?php

namespace App\Http\Middleware;

use Closure;

class ZonesRbac
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $routeName = $request->route()->getName();
        $response = redirect(action('HomeController@index'));
        $permissions = [];


        if (auth()->check()) {
            $permissions = $request->user()->getPermissionsNamesAsArray();
        }
        if (empty($permissions)) {
            return $response;
        }
        if ($routeName == 'zones.index' && in_array('index_zones', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'zones.show' && in_array('show_zones', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'zones.create' && in_array('store_zones', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'zones.store' && in_array('store_zones', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'zones.edit' && in_array('update_zones', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'zones.update' && in_array('update_zones', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'zones.destroy' && in_array('destroy_zones', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'zones_miscellaneous.index_search' && in_array('index_zones', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'zones_miscellaneous.get_project_names_ids' && in_array('store_zones', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'zones_miscellaneous.delete_image_ajax' && in_array('update_zones', $permissions)) {
            $response = $next($request);
        } else {
            abort(403);
        }
        return $response;
    }
}
