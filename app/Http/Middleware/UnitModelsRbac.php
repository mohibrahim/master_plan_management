<?php

namespace App\Http\Middleware;

use Closure;

class UnitModelsRbac
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $routeName = $request->route()->getName();
        $response = redirect(action('HomeController@index'));
        $permissions = [];
     

        if (auth()->check()) {
            $permissions = $request->user()->getPermissionsNamesAsArray();
        }
        if (empty($permissions)) {
            return $response;
        }
        if ($routeName == 'unit_models.index' && in_array('index_unit_models', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'unit_models.show' && in_array('show_unit_models', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'unit_models.create' && in_array('store_unit_models', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'unit_models.store' && in_array('store_unit_models', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'unit_models.edit' && in_array('update_unit_models', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'unit_models.update' && in_array('update_unit_models', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'unit_models.destroy' && in_array('destroy_unit_models', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'unit_models_miscellaneous.index_search' && in_array('index_unit_models', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'unit_models_miscellaneous.delete_image_ajax' && in_array('update_unit_models', $permissions)) {
            $response = $next($request);
        } else {
            abort(403);
        }
        return $response;
    }
}
