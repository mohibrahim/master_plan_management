<?php

namespace App\Http\Middleware;

use Closure;
use App\ProjectUserAssignment;

class ProjectUserAssignmentsMultiTenancy
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       $response = redirect(action('HomeController@index'));
        if (auth()->check()) {
            $model = $request->project_user_assignment;
            $modelCreatorId = $this->getModelCreatorId($model);
            $authenticatedUserId = $request->user()->id;
            $permissions = $request->user()->getPermissionsNamesAsArray();
            $routeName = $request->route()->getName();
            if ($modelCreatorId == $authenticatedUserId) {
                $response = $next($request);
            } elseif ($routeName == 'project_user_assignments.show' && in_array('show_project_user_assignments_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'project_user_assignments.edit' && in_array('update_project_user_assignments_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'project_user_assignments.update' && in_array('update_project_user_assignments_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'project_user_assignments.destroy' && in_array('destroy_project_user_assignments_multi_tenancy', $permissions)) {
                $response = $next($request);
            } else {
                abort(403);
            }
        }
        return $response;
    }
    
    private function getModelCreatorId($model) : int
    {
        if ($model instanceof ProjectUserAssignment) {
            $modelCreatorId = (int)$model->creator_id;
        } else {
            $projectUserAssignment = ProjectUserAssignment::findOrFail($model);
            if (isset($projectUserAssignment)) {
                $modelCreatorId = (int)$projectUserAssignment->creator_id;
            }
        }
        return $modelCreatorId;
    }
}
