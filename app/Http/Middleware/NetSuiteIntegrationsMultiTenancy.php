<?php

namespace App\Http\Middleware;

use Closure;
use App\Catalog;
use App\Project;
use App\NetSuiteIntegration;

class NetSuiteIntegrationsMultiTenancy
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       $response = redirect(action('HomeController@index'));
        if (auth()->check()) {
            $model = $request->net_suite_integration;
            $modelCreatorId = $this->getModelCreatorId($model);
            $authenticatedUserId = $request->user()->id;
            $permissions = $request->user()->getPermissionsNamesAsArray();
            $routeName = $request->route()->getName();
            if ($modelCreatorId == $authenticatedUserId) {
                $response = $next($request);
            } elseif ($routeName == 'net_suite_integrations.show' && in_array('show_net_suite_integrations_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'net_suite_integrations.edit' && in_array('update_net_suite_integrations_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'net_suite_integrations.update' && in_array('update_net_suite_integrations_multi_tenancy', $permissions)) {
                $response = $next($request);
            } elseif ($routeName == 'net_suite_integrations.destroy' && in_array('destroy_net_suite_integrations_multi_tenancy', $permissions)) {
                $response = $next($request);
            } else {
                abort(403);
            }
        }
        return $response;
    }

    private function getModelCreatorId($model) : int
    {
        if ($model instanceof NetSuiteIntegration) {
            $modelCreatorId = (int)$model->creator_id;
        } else {
            $netSuiteIntegration = NetSuiteIntegration::findOrFail($model);
            if (isset($netSuiteIntegration)) {
                $modelCreatorId = (int)$netSuiteIntegration->creator_id;//if null it will casted to zero
            }
        }
        return $modelCreatorId;
    }
}
