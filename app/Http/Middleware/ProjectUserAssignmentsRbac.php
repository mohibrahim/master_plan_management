<?php

namespace App\Http\Middleware;

use Closure;

class ProjectUserAssignmentsRbac
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $routeName = $request->route()->getName();
        $response = redirect(action('HomeController@index'));
        $permissions = [];
     

        if (auth()->check()) {
            $permissions = $request->user()->getPermissionsNamesAsArray();
        }
        if (empty($permissions)) {
            return $response;
        }
        if ($routeName == 'project_user_assignments.index' && in_array('index_project_user_assignments', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'project_user_assignments.show' && in_array('show_project_user_assignments', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'project_user_assignments.create' && in_array('store_project_user_assignments', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'project_user_assignments.store' && in_array('store_project_user_assignments', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'project_user_assignments.edit' && in_array('update_project_user_assignments', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'project_user_assignments.update' && in_array('update_project_user_assignments', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'project_user_assignments.destroy' && in_array('destroy_project_user_assignments', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'project_user_assignments_miscellaneous.index_search' && in_array('index_project_user_assignments', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'project_user_assignments_miscellaneous.get_project_names_ids' && in_array('store_project_user_assignments', $permissions)) {
            $response = $next($request);
        } elseif ($routeName == 'project_user_assignments_miscellaneous.get_employee_names_ids' && (in_array('store_project_user_assignments', $permissions) || in_array('update_project_user_assignments', $permissions))) {
            $response = $next($request);
        } else {
            abort(403);
        }
        return $response;
    }
}
