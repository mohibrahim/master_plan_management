<?php

namespace App\Http;

use App\Http\Middleware\ProjectsRbac;
use App\Http\Middleware\ProjectsMultiTenancy;
use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \App\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
        \App\Http\Middleware\TrustProxies::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
            \App\Http\Middleware\LocaleSessionCreator::class,
        ],

        'api' => [
            'throttle:60,1',
            'bindings',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'cache.headers' => \Illuminate\Http\Middleware\SetCacheHeaders::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'signed' => \Illuminate\Routing\Middleware\ValidateSignature::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'verified' => \Illuminate\Auth\Middleware\EnsureEmailIsVerified::class,

        'permissions_rbac' => \App\Http\Middleware\PermissionsRbac::class,
        'permissions_multi_tenancy' => \App\Http\Middleware\PermissionsMultiTenancy::class,

        'roles_rbac' => \App\Http\Middleware\RolesRbac::class,
        'roles_multi_tenancy' => \App\Http\Middleware\RolesMultiTenancy::class,

        'users_rbac' => \App\Http\Middleware\UsersRbac::class,
        'users_multi_tenancy' => \App\Http\Middleware\UsersMultiTenancy::class,

        'role_user_rbac' => \App\Http\Middleware\RoleUserRbac::class,

        'project_files_rbac' => \App\Http\Middleware\ProjectFilesRbac::class,
        'project_files_multi_tenancy' => \App\Http\Middleware\ProjectFilesMultiTenancy::class,

        'catalogs_rbac' => \App\Http\Middleware\CatalogsRbac::class,
        'catalogs_multi_tenancy' => \App\Http\Middleware\CatalogsMultiTenancy::class,

        'projects_rbac' => \App\Http\Middleware\ProjectsRbac::class,
        'projects_multi_tenancy' => \App\Http\Middleware\ProjectsMultiTenancy::class,

        'phases_rbac' => \App\Http\Middleware\PhasesRbac::class,
        'phases_multi_tenancy' => \App\Http\Middleware\PhasesMultiTenancy::class,

        'zones_rbac' => \App\Http\Middleware\ZonesRbac::class,
        'zones_multi_tenancy' => \App\Http\Middleware\ZonesMultiTenancy::class,

        'unit_models_rbac' => \App\Http\Middleware\UnitModelsRbac::class,
        'unit_models_multi_tenancy' => \App\Http\Middleware\UnitModelsMultiTenancy::class,

        'units_rbac' => \App\Http\Middleware\UnitsRbac::class,
        'units_multi_tenancy' => \App\Http\Middleware\UnitsMultiTenancy::class,

        'customers_rbac' => \App\Http\Middleware\CustomersRbac::class,
        'customers_multi_tenancy' => \App\Http\Middleware\CustomersMultiTenancy::class,

        'reservations_rbac' => \App\Http\Middleware\ReservationsRbac::class,
        'reservations_multi_tenancy' => \App\Http\Middleware\ReservationsMultiTenancy::class,

        'project_user_assignments_rbac' => \App\Http\Middleware\ProjectUserAssignmentsRbac::class,
        'project_user_assignments_multi_tenancy' => \App\Http\Middleware\ProjectUserAssignmentsMultiTenancy::class,

        'blocks_rbac' => \App\Http\Middleware\BlocksRbac::class,
        'blocks_multi_tenancy' => \App\Http\Middleware\BlocksMultiTenancy::class,

        'net_suite_integrations_rbac' => \App\Http\Middleware\NetSuiteIntegrationsRbac::class,
        'net_suite_integrations_multi_tenancy' => \App\Http\Middleware\NetSuiteIntegrationsMultiTenancy::class,

        'salesman_dashboard' => \App\Http\Middleware\SalesmanDashboard::class,
    ];

    /**
     * The priority-sorted list of middleware.
     *
     * This forces non-global middleware to always be in the given order.
     *
     * @var array
     */
    protected $middlewarePriority = [
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \App\Http\Middleware\Authenticate::class,
        \Illuminate\Session\Middleware\AuthenticateSession::class,
        \Illuminate\Routing\Middleware\SubstituteBindings::class,
        \Illuminate\Auth\Middleware\Authorize::class,
    ];
}
