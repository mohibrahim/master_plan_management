<?php

namespace App\Http\Controllers;

use Closure;
use App\Unit;
use App\Project;
use App\UnitModel;
use App\NetSuiteIntegration;
use Illuminate\Http\Request;
use App\Http\Requests\UnitRequest;
use App\MasterPlanManagement\Services\CloudStorage\S3\ProcessingFileTemporaryLocally;
use App\MasterPlanManagement\Services\IndexTenancy\IndexStrategyInterface;
use App\MasterPlanManagement\Services\Models\Unit\ShowPdf\FocusOnUnit\FocusOnUnitImage;
use App\MasterPlanManagement\Services\Models\Unit\UnitStatusStrategy\SuitableUnitStatus;
use App\MasterPlanManagement\Services\Models\Unit\CreateUnitStrategy\CreateUnitInterface;
use App\MasterPlanManagement\Services\Models\Unit\DeleteUnitStrategy\DeleteUnitInterface;
use App\MasterPlanManagement\Services\Models\Unit\UpdateUnitStrategy\UpdateUnitInterface;
use App\MasterPlanManagement\Services\Models\Unit\UnitHasPoolStrategy\SuitableUnitHasPool;
use App\MasterPlanManagement\Services\FlashMessageLanguage\FlashMessageArabicEnglishInterface;
use App\MasterPlanManagement\Services\Models\Unit\UnitHasGardenStrategy\SuitableUnitHasGarden;
use App\MasterPlanManagement\Services\Models\Unit\UnitCurrenciesStrategy\SuitableUnitCurrencies;
use App\MasterPlanManagement\Services\Models\Unit\UnitConstructionStatusStrategy\SuitableUnitConstructionStatus;

class UnitController extends Controller
{
    use ProcessingFileTemporaryLocally;
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */



    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('units_rbac');
        $this->middleware('units_multi_tenancy')->only(['show', 'edit', 'update', 'destroy']);

        $this->middleware(function ($request, Closure $next) {
            session(['highlighSelectedNav' => 'unit']);
            return $next($request);
        });
    }

    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */



    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function index(IndexStrategyInterface $indexByMultiTenancy, Unit $unit)
    {
        $authenticatedUser = auth()->user();
        $unit = $indexByMultiTenancy->index($unit, $authenticatedUser, 'index_units_multi_tenancy');
        $units = $unit->paginate(20);
        $salePriceMax = Unit::max('sale_price');
        $salePriceMin = Unit::min('sale_price');
        $netSuitNamesIds = NetSuiteIntegration::all()->pluck('name', 'id');
        return view('units.index', compact('units', 'salePriceMax', 'salePriceMin', 'netSuitNamesIds'));
    }

    public function create(Unit $unit)
    {
        $unitStatus = resolve(SuitableUnitStatus::class)->getList();
        $unitConstructionStatus = resolve(SuitableUnitConstructionStatus::class)->getList();
        $unitHasGarden = resolve(SuitableUnitHasGarden::class)->getList();
        $unitHasPool = resolve(SuitableUnitHasPool::class)->getList();
        $unitCurrencies = resolve(SuitableUnitCurrencies::class)->getList();
        return view('units.create', compact('unit', 'unitStatus', 'unitConstructionStatus', 'unitCurrencies', 'unitHasGarden', 'unitHasPool'));
    }


    public function store(UnitRequest $request, CreateUnitInterface $createUnit, FlashMessageArabicEnglishInterface $flashMessageArabicEnglish)
    {
        $unit = $createUnit->create($request);
        flash()->success(
            $flashMessageArabicEnglish->getSuitableFlashMessage('تم إضافة الوحدة بنجاح', 'Unit Added Successfully.')
        );
        return redirect()->action('UnitController@show', ['id' => $unit->id]);
    }


    public function show(Unit $unit)
    {
        return view('units.show.show', compact('unit'));
    }

    public function edit(Unit $unit)
    {
        $unitStatus = resolve(SuitableUnitStatus::class)->getList();
        $unitConstructionStatus = resolve(SuitableUnitConstructionStatus::class)->getList();
        $unitHasGarden = resolve(SuitableUnitHasGarden::class)->getList();
        $unitHasPool = resolve(SuitableUnitHasPool::class)->getList();
        $selectedProjectNameId = $unit->selected_project_name_id;
        $unitCurrencies = resolve(SuitableUnitCurrencies::class)->getList();
        return view('units.edit', compact('unit', 'unitStatus', 'unitConstructionStatus', 'selectedProjectNameId', 'unitCurrencies', 'unitHasGarden', 'unitHasPool'));
    }

    public function update(UnitRequest $request, int $id, UpdateUnitInterface $updateUnit, FlashMessageArabicEnglishInterface $flashMessageArabicEnglish)
    {
        $unit = $updateUnit->update($id, $request);
        flash()->success(
            $flashMessageArabicEnglish->getSuitableFlashMessage('تم تعديل الوحدة بنجاح', 'Unit has been updated successfully.')

        )->important();
        return redirect()->action('UnitController@show', ['id' => $unit->id]);
    }

    public function destroy(Unit $unit, FlashMessageArabicEnglishInterface $flashMessageArabicEnglish, DeleteUnitInterface $deleteUnit)
    {
        $deleteUnit->delete($unit);
        flash()->success(
            $flashMessageArabicEnglish->getSuitableFlashMessage("تم حذف الوحدة $unit->name بنجاح", 'Unit: ' . $unit->name . ', has been deleted successfully')

        )->important();
        return redirect('units');
    }

    /*
    |--------------------------------------------------------------------------
    | Miscellaneous
    |--------------------------------------------------------------------------
    */

    public function showAsPdf(Unit $unit)
    {
        $focusOnUnitImage = new FocusOnUnitImage($unit, 500, 500);
        $unitData = (object) [];
        $pathToMasterPlanImage = $this->setFileInLocalDisk(optional($unit->project)->master_plan_image);
        $masterPlanImageDetails = getimagesize($pathToMasterPlanImage);

        $pathToMapPinImage = 'images/helper_images/mapPin.png';
        $unitData->unitMapPinUrl = secure_url($pathToMapPinImage);
        $unitData->unitMasterPlanImageUrl = secure_url('storage/'. optional($unit->project)->master_plan_image);
        $unitData->unitMasterPlanImageWidth = $masterPlanImageDetails[0];
        $unitData->unitMasterPlanImageHeight = $masterPlanImageDetails[1];
        $unitData->unitCoords = $unit->html_area_tag_coords;
        $unitData->unitZoneCoords = optional($unit->zone)->html_area_tag_coords;
        $unitData->zoneName = optional($unit->zone)->name;
        $unitDataAsJson = json_encode($unitData);
        return view('units.show.show_as_pdf', compact('unit', 'focusOnUnitImage', 'unitDataAsJson'));
    }


    /*
    |--------------------------------------------------------------------------
    | Miscellaneous - Ajax
    |--------------------------------------------------------------------------
    */

    public function search(Request $request, Unit $unit, IndexStrategyInterface $indexByMultiTenancy)
    {
        $authenticatedUser = auth()->user();
        $builder = $indexByMultiTenancy->index($unit, $authenticatedUser, 'index_units_multi_tenancy');
        $results = $unit->search($builder, $request->input('keyword'), $request->input('sale_price_range_min'), $request->input('sale_price_range_max'));
        return $results->get();
    }

    public function getProjectNamesIds(Request $request)
    {
        $keyword  = $request->input('term');
        $results = [
            'results' =>
            Project::where('name', 'like', "$keyword%")
                ->limit(6)
                ->get()
                ->map(function ($project, $key) {
                    return [
                        'id' => $project->id, 'text' => $project->name, 'masterPlanImage' => $project->master_plan_image, 'phases' => optional($project->phases)->pluck('name', 'id'), 'zones' => optional($project->zones)->pluck('name', 'id'), 'blocks' => optional($project->blocks)->pluck('name', 'id')
                    ];
                })
        ];
        return $results;
    }

    public function getUnitModelNamesIds(Request $request)
    {
        $keyword  = $request->input('term');
        $results = [
            'results' =>
            UnitModel::where('name', 'like', "$keyword%")
                ->limit(6)
                ->get()
                ->map(function ($unitModel, $key) {
                    return ['id' => $unitModel->id, 'text' => $unitModel->name, 'code' => $unitModel->code, 'type' => $unitModel->type];
                })
        ];
        return $results;
    }
}
