<?php

namespace App\Http\Controllers;

use Closure;
use App\Permission;
use App\Http\Requests\PermissionRequest;
use App\MasterPlanManagement\Services\IndexTenancy\IndexStrategyInterface;

class PermissionController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permissions_rbac');
        $this->middleware('permissions_multi_tenancy')->only(['show', 'edit', 'update', 'destroy']);
        
        $this->middleware(function($request, Closure $next){
            session(['highlighSelectedNav'=>'userSystem']);
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(IndexStrategyInterface $indexByMultiTenancy, Permission $permission)
    {
        $permissions = $indexByMultiTenancy->index($permission, auth()->user(), 'index_permissions_multi_tenancy');
        $permissions = $permissions->orderBy('title')->get();
        return view('permissions.index', compact('permissions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('permissions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PermissionRequest $request)
    {
        $requestAsArray = array_add($request->all(), 'creator_id',  auth()->id());

        $permission = Permission::create($requestAsArray);
        flash()->success('Permission has been add successfully');
        return redirect()->action('PermissionController@show', ['id'=>$permission->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $permission = Permission::findOrFail($id);
        return view('permissions.show', compact('permission'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permission = Permission::findOrFail($id);
        return view('permissions.edit', compact('permission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PermissionRequest $request, $id)
    {
        $permission = Permission::findOrFail($id);
        $requestAsArray = array_add($request->all(), 'last_updater_id', auth()->id());
        $permission->update($requestAsArray);
        flash()->success('Permission has been updated successfully');
        return redirect(action('PermissionController@show', [$id]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $permission = Permission::findOrFail($id);
        $permission->delete();
        flash()->success('Permission ' . $permission->name . ' has been deleted successfully');
        return redirect(action('PermissionController@index'));
    }
}
