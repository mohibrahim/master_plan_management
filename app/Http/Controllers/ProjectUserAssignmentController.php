<?php

namespace App\Http\Controllers;

use Closure;
use App\User;
use App\Project;
use Illuminate\Http\Request;
use App\ProjectUserAssignment;
use App\Http\Requests\ProjectUserAssignmentRequest;
use App\MasterPlanManagement\Services\IndexTenancy\IndexStrategyInterface;
use App\MasterPlanManagement\Services\FlashMessageLanguage\FlashMessageArabicEnglishInterface;
use App\MasterPlanManagement\Services\Models\ProjectUserAssignment\CreateProjectUserAssignmentStrategy\CreateProjectUserAssignmentInterface;
use App\MasterPlanManagement\Services\Models\ProjectUserAssignment\UpdateProjectUserAssignmentStrategy\UpdateProjectUserAssignmentInterface;

class ProjectUserAssignmentController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */



    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('project_user_assignments_rbac');
        $this->middleware('project_user_assignments_multi_tenancy')->only(['show', 'edit', 'update', 'destroy']);

        $this->middleware(function ($request, Closure $next) {
            session(['highlighSelectedNav' => 'projectUserAssignment']);
            return $next($request);
        });
    }

    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */



    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function index(IndexStrategyInterface $indexByMultiTenancy, ProjectUserAssignment $projectUserAssignment)
    {
        $authenticatedUser = auth()->user();
        $builder = $indexByMultiTenancy->index($projectUserAssignment, $authenticatedUser, 'index_project_user_assignments_multi_tenancy');
        $projectUserAssignments = $builder->paginate(20);
        return view('project_user_assignments.index', compact('projectUserAssignments'));
    }

    public function create(ProjectUserAssignment $projectUserAssignment)
    {
        return view('project_user_assignments.create', compact('projectUserAssignment'));
    }


    public function store(ProjectUserAssignmentRequest $request, CreateProjectUserAssignmentInterface $createProjectUserAssignment, FlashMessageArabicEnglishInterface $flashMessageArabicEnglish)
    {
        $projectUserAssignment = $createProjectUserAssignment->create($request);
        flash()->success(
            $flashMessageArabicEnglish->getSuitableFlashMessage('تم إضافة تعينات المشاريع للمستخدم بنجاح', 'ProjectUserAssignment Added Successfully.')
        );
        return redirect()->action('ProjectUserAssignmentController@show', ['id' => $projectUserAssignment->id]);
    }


    public function show(ProjectUserAssignment $projectUserAssignment)
    {
        return view('project_user_assignments.show', compact('projectUserAssignment'));
    }

    public function edit(ProjectUserAssignment $projectUserAssignment)
    {
        return view('project_user_assignments.edit', compact('projectUserAssignment'));
    }

    public function update(ProjectUserAssignmentRequest $request, int $id, UpdateProjectUserAssignmentInterface $updateProjectUserAssignment, FlashMessageArabicEnglishInterface $flashMessageArabicEnglish)
    {
        $projectUserAssignment = $updateProjectUserAssignment->update($id, $request);
        flash()->success(
            $flashMessageArabicEnglish->getSuitableFlashMessage('تم تعديل تعينات المشاريع للمستخدم بنجاح', 'ProjectUserAssignment has been updated successfully.')

        )->important();
        return redirect()->action('ProjectUserAssignmentController@show', ['id' => $projectUserAssignment->id]);
    }

    public function destroy(ProjectUserAssignment $projectUserAssignment, FlashMessageArabicEnglishInterface $flashMessageArabicEnglish)
    {
        $employee = User::find($projectUserAssignment->employee_id);
        if (isset($employee)) {
            $employee->projectUserAssignments()->delete();
        }
        flash()->success(
            $flashMessageArabicEnglish->getSuitableFlashMessage("تم حذف تعينات المشاريع للموظف $projectUserAssignment->name بنجاح", 'ProjectUserAssignment: ' . $projectUserAssignment->name . ', has been deleted successfully')

        )->important();
        return redirect()->action('ProjectUserAssignmentController@index');
    }

    /*
    |--------------------------------------------------------------------------
    | Miscellaneous
    |-------------------------------------------------------------------------- 
    */


    /*
    |--------------------------------------------------------------------------
    | Miscellaneous - Ajax
    |-------------------------------------------------------------------------- 
    */

    public function search(Request $request, ProjectUserAssignment $projectUserAssignment, IndexStrategyInterface $indexByMultiTenancy)
    {
        $authenticatedUser = auth()->user();
        $builder = $indexByMultiTenancy->index($projectUserAssignment, $authenticatedUser, 'index_project_user_assignments_multi_tenancy');
        $results = $projectUserAssignment->search($builder, $request->input('keyword'));
        return $results->get();
    }


    public function getProjectNamesIds(Request $request)
    {
        $keyword  = $request->input('term');
        $results = [
            'results' =>
            Project::where('name', 'like', "$keyword%")
                ->limit(6)
                ->get()
                ->map(function ($project, $key) {
                    return ['id' => $project->id, 'text' => $project->name, 'masterPlanImage' => $project->master_plan_image, 'phases' => optional($project->phases)->pluck('name', 'id')];
                })
        ];
        return $results;
    }

    public function getEmployeeNamesIds(Request $request)
    {
        $keyword  = $request->input('term');
        $results = [
            'results' =>
            User::where('name', 'like', "$keyword%")
                ->limit(6)
                ->get()
                ->map(function ($item, $key) {
                    return ['id' => $item->id, 'text' => $item->name . ' | ' . $item->phone];
                })
        ];
        return $results;
    }
}
