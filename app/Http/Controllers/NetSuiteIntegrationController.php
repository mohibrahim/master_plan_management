<?php

namespace App\Http\Controllers;

use Closure;
use App\NetSuiteIntegration;
use Illuminate\Http\Request;
use App\Http\Requests\NetSuiteIntegrationRequest;
use App\MasterPlanManagement\Services\IndexTenancy\IndexStrategyInterface;
use App\MasterPlanManagement\Services\Integrations\Pull\DataSync\BanksSyncing;
use App\MasterPlanManagement\Services\Integrations\Pull\DataSync\UnitsSyncing;
use App\MasterPlanManagement\Services\Integrations\Pull\DataSync\ZonesSyncing;
use App\MasterPlanManagement\Services\Integrations\Pull\DataSync\BlocksSyncing;
use App\MasterPlanManagement\Services\Integrations\Pull\DataSync\PhasesSyncing;
use App\MasterPlanManagement\Services\Integrations\Pull\DataSync\ProjectsSyncing;
use App\MasterPlanManagement\Services\Integrations\Pull\DataSync\CustomersSyncing;
use App\MasterPlanManagement\Services\Integrations\Pull\DataSync\UnitModelsSyncing;
use App\MasterPlanManagement\Services\Integrations\Pull\DataSync\ReservationsSyncing;
use App\MasterPlanManagement\Services\Integrations\Pull\ThirdPartyObjects\BankThirdParty;
use App\MasterPlanManagement\Services\Integrations\Pull\ThirdPartyObjects\UnitThirdParty;
use App\MasterPlanManagement\Services\Integrations\Pull\ThirdPartyObjects\ZoneThirdParty;
use App\MasterPlanManagement\Services\Integrations\Pull\ThirdPartyObjects\BlockThirdParty;
use App\MasterPlanManagement\Services\Integrations\Pull\ThirdPartyObjects\PhaseThirdParty;
use App\MasterPlanManagement\Services\Integrations\Pull\ThirdPartyObjects\ProjectThirdParty;
use App\MasterPlanManagement\Services\Integrations\Pull\ThirdPartyObjects\CustomerThirdParty;
use App\MasterPlanManagement\Services\FlashMessageLanguage\FlashMessageArabicEnglishInterface;
use App\MasterPlanManagement\Services\Integrations\Pull\ThirdPartyObjects\UnitModelThirdParty;
use App\MasterPlanManagement\Services\Integrations\Pull\ThirdPartyObjects\ReservationThirdParty;
use App\MasterPlanManagement\Services\Integrations\Pull\CommunicationStrategy\ClientRequestInterface;
use App\MasterPlanManagement\Services\Integrations\Pull\ThirdPartyObjects\MappingStrategy\Listing\MappingInterface;
use App\MasterPlanManagement\Services\Models\NetSuiteIntegration\CreateNetSuiteIntegrationStrategy\CreateNetSuiteIntegrationInterface;
use App\MasterPlanManagement\Services\Models\NetSuiteIntegration\UpdateNetSuiteIntegrationStrategy\UpdateNetSuiteIntegrationInterface;

class NetSuiteIntegrationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('net_suite_integrations_rbac');
        $this->middleware('net_suite_integrations_multi_tenancy')->only(['show', 'edit', 'update', 'destroy']);

        $this->middleware(function ($request, Closure $next) {
            session(['highlighSelectedNav' => 'net_suite_integrations']);
            return $next($request);
        });
    }

    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function index(IndexStrategyInterface $indexByMultiTenancy, NetSuiteIntegration $netSuiteIntegration)
    {
        $authenticatedUser = auth()->user();
        $netSuiteIntegrations = $indexByMultiTenancy->index($netSuiteIntegration, $authenticatedUser, 'index_net_suite_integrations_multi_tenancy');
        $netSuiteIntegrations = $netSuiteIntegrations->paginate(20);
        return view('net_suite_integrations.index', compact('netSuiteIntegrations'));
    }

    public function create()
    {
        return view('net_suite_integrations.create');
    }


    public function store(
        NetSuiteIntegrationRequest $request,
        FlashMessageArabicEnglishInterface $flashMessageArabicEnglish,
        CreateNetSuiteIntegrationInterface $createNetSuiteIntegration
    ) {
        $netSuiteIntegration = $createNetSuiteIntegration->create($request);
        flash()->success(
            $flashMessageArabicEnglish->getSuitableFlashMessage('تم إضافة دمج نتسويت بنجاح', 'NetSuite Integration Added Successfully.')
        );
        return redirect()->action('NetSuiteIntegrationController@show', ['id' => $netSuiteIntegration->id]);
    }


    public function show(NetSuiteIntegration $netSuiteIntegration)
    {
        return view('net_suite_integrations.show', compact('netSuiteIntegration'));
    }

    public function edit(NetSuiteIntegration $netSuiteIntegration)
    {
        return view('net_suite_integrations.edit', compact('netSuiteIntegration'));
    }

    public function update(
        NetSuiteIntegrationRequest $request,
        NetSuiteIntegration $netSuiteIntegration,
        FlashMessageArabicEnglishInterface $flashMessageArabicEnglish,
        UpdateNetSuiteIntegrationInterface $updateNetSuiteIntegration
    ) {
        $netSuiteIntegration = $updateNetSuiteIntegration->update($netSuiteIntegration, $request);
        flash()->success(
            $flashMessageArabicEnglish->getSuitableFlashMessage('تم تعديل دمج نتسويت بنجاح', 'NetSuite Integration has been updated successfully.')

        )->important();
        return redirect()->action('NetSuiteIntegrationController@show', ['id' => $netSuiteIntegration->id]);
    }

    public function destroy(NetSuiteIntegration $netSuiteIntegration, FlashMessageArabicEnglishInterface $flashMessageArabicEnglish)
    {
        $netSuiteIntegration->delete();
        flash()->success(
            $flashMessageArabicEnglish->getSuitableFlashMessage("تم حذف دمج نتسويت $netSuiteIntegration->name بنجاح", 'NetSuite Integration: ' . $netSuiteIntegration->name . ', has been deleted successfully')

        )->important();
        return redirect('net_suite_integrations');
    }

    /*
    |--------------------------------------------------------------------------
    | Miscellaneous
    |--------------------------------------------------------------------------
    */

    public function syncProjectsData(
        NetSuiteIntegration $netSuiteIntegration,
        ClientRequestInterface $clientRequest,
        MappingInterface $mapping,
        ProjectsSyncing $projectsSyncing
    ) {
        $projectUrl = $netSuiteIntegration->project_api_index;
        $authorization = $netSuiteIntegration->api_authorization;
        //communicate and getting string json
        $response = $clientRequest->requestClient($projectUrl, $authorization);
        //mapping objects
        $collectionOfThirdPartyProjects = $mapping->mapList($response, ProjectThirdParty::class);
        //syncing
        $messages = $projectsSyncing->sync($collectionOfThirdPartyProjects);
        foreach ($messages as $message) {
            flash()->success($message)->important();
        }
        $netSuiteIntegration->restfulUrl->update(['project_sync_date_time'=>now()]);
        return redirect()->back();
    }

    public function syncCustomersData(
        NetSuiteIntegration $netSuiteIntegration,
        ClientRequestInterface $clientRequest,
        MappingInterface $mapping,
        CustomersSyncing $customersSyncing
    ) {
        $customerUrl = $netSuiteIntegration->customer_api_index;
        $authorization = $netSuiteIntegration->api_authorization;
        //communicate and getting string json
        $response = $clientRequest->requestClient($customerUrl, $authorization);
        //mapping objects
        $collectionOfThirdPartyCustomers = $mapping->mapList($response, CustomerThirdParty::class);
        //syncing
        $messages = $customersSyncing->sync($collectionOfThirdPartyCustomers);
        foreach ($messages as $message) {
            flash()->success($message)->important();
        }
        $netSuiteIntegration->restfulUrl->update(['customer_sync_date_time'=>now()]);
        return redirect()->back();
    }

    public function syncPhasesData(
        NetSuiteIntegration $netSuiteIntegration,
        ClientRequestInterface $clientRequest,
        MappingInterface $mapping,
        PhasesSyncing $phasesSyncing
    ) {
        $phaseUrl = $netSuiteIntegration->phase_api_index;
        $authorization = $netSuiteIntegration->api_authorization;
        //communicate and getting string json
        $response = $clientRequest->requestClient($phaseUrl, $authorization);
        //mapping objects
        $collectionOfThirdPartyPhases = $mapping->mapList($response, PhaseThirdParty::class);
        //Syncing
        $messages = $phasesSyncing->sync($collectionOfThirdPartyPhases);
        foreach ($messages as $message) {
            flash()->success($message)->important();
        }
        $netSuiteIntegration->restfulUrl->update(['phase_sync_date_time'=>now()]);
        return redirect()->back();
    }

    public function syncZonesData(
        NetSuiteIntegration $netSuiteIntegration,
        ClientRequestInterface $clientRequest,
        MappingInterface $mapping,
        ZonesSyncing $zonesSyncing
    ) {
        $zoneUrl = $netSuiteIntegration->zone_api_index;
        $authorization = $netSuiteIntegration->api_authorization;
        //communicate and getting string json
        $response = $clientRequest->requestClient($zoneUrl, $authorization);
        //mapping objects
        $collectionOfThirdPartyZones = $mapping->mapList($response, ZoneThirdParty::class);
        //Syncing
        $messages = $zonesSyncing->sync($collectionOfThirdPartyZones);
        foreach ($messages as $message) {
            flash()->success($message)->important();
        }
        $netSuiteIntegration->restfulUrl->update(['zone_sync_date_time'=>now()]);
        return redirect()->back();
    }

    public function syncBlocksData(
        NetSuiteIntegration $netSuiteIntegration,
        ClientRequestInterface $clientRequest,
        MappingInterface $mapping,
        BlocksSyncing $blocksSyncing
    ) {
        $blockUrl = $netSuiteIntegration->block_api_index;
        $authorization = $netSuiteIntegration->api_authorization;
        //communicate and getting string json
        $response = $clientRequest->requestClient($blockUrl, $authorization);
        //mapping objects
        $collectionOfThirdPartyBlocks = $mapping->mapList($response, BlockThirdParty::class);
        //Syncing
        $messages = $blocksSyncing->sync($collectionOfThirdPartyBlocks);
        foreach ($messages as $message) {
            flash()->success($message)->important();
        }
        $netSuiteIntegration->restfulUrl->update(['block_sync_date_time'=>now()]);
        return redirect()->back();
    }

    public function syncUnitModelsData(
        NetSuiteIntegration $netSuiteIntegration,
        ClientRequestInterface $clientRequest,
        MappingInterface $mapping,
        UnitModelsSyncing $unitModelsSyncing
    ) {
        $unitModelUrl = $netSuiteIntegration->unit_model_api_index;
        $authorization = $netSuiteIntegration->api_authorization;
        //communicate and getting string json
        $response = $clientRequest->requestClient($unitModelUrl, $authorization);
        //mapping objects
        $collectionOfThirdPartyUnitModels = $mapping->mapList($response, UnitModelThirdParty::class);
        //Syncing
        $messages = $unitModelsSyncing->sync($collectionOfThirdPartyUnitModels);
        foreach ($messages as $message) {
            flash()->success($message)->important();
        }
        $netSuiteIntegration->restfulUrl->update(['unit_model_sync_date_time'=>now()]);
        return redirect()->back();
    }

    public function syncUnitsData(
        NetSuiteIntegration $netSuiteIntegration,
        ClientRequestInterface $clientRequest,
        MappingInterface $mapping,
        UnitsSyncing $unitsSyncing
    ) {
        $unitUrl = $netSuiteIntegration->unit_api_index;
        $authorization = $netSuiteIntegration->api_authorization;
        //communicate and getting string json
        $response = $clientRequest->requestClient($unitUrl, $authorization);
        //mapping objects
        $collectionOfThirdPartyUnits = $mapping->mapList($response, UnitThirdParty::class);
        //Syncing
        $messages = $unitsSyncing->sync($collectionOfThirdPartyUnits);
        foreach ($messages as $message) {
            flash()->success($message)->important();
        }
        $netSuiteIntegration->restfulUrl->update(['unit_sync_date_time'=>now()]);
        return redirect()->back();
    }

    public function syncReservationsData(
        NetSuiteIntegration $netSuiteIntegration,
        ClientRequestInterface $clientRequest,
        MappingInterface $mapping,
        ReservationsSyncing $reservationsSyncing
    ) {
        $reservationUrl = $netSuiteIntegration->reservation_api_index;
        $authorization = $netSuiteIntegration->api_authorization;
        //communicate and getting string json
        $response = $clientRequest->requestClient($reservationUrl, $authorization);
        //mapping objects
        $collectionOfThirdPartyReservations = $mapping->mapList($response, ReservationThirdParty::class);
        // dd($collectionOfThirdPartyReservations);
        //Syncing
        $messages = $reservationsSyncing->sync($collectionOfThirdPartyReservations);
        foreach ($messages as $message) {
            flash()->success($message)->important();
        }
        $netSuiteIntegration->restfulUrl->update(['reservation_sync_date_time'=>now()]);
        return redirect()->back();
    }

    public function syncBanksData(
        NetSuiteIntegration $netSuiteIntegration,
        ClientRequestInterface $clientRequest,
        MappingInterface $mapping,
        BanksSyncing $banksSyncing
    ) {
        $bankUrl = $netSuiteIntegration->bank_api_index;
        $authorization = $netSuiteIntegration->api_authorization;
        //communicate and getting string json
        $response = $clientRequest->requestClient($bankUrl, $authorization);
        //mapping objects
        $collectionOfThirdPartyZones = $mapping->mapList($response, BankThirdParty::class);
        //Syncing
        $messages = $banksSyncing->sync($collectionOfThirdPartyZones);
        foreach ($messages as $message) {
            flash()->success($message)->important();
        }
        $netSuiteIntegration->restfulUrl->update(['bank_sync_date_time'=>now()]);
        return redirect()->back();
    }


    /*
    |--------------------------------------------------------------------------
    | Miscellaneous - Ajax
    |--------------------------------------------------------------------------
    */

    public function search(Request $request, NetSuiteIntegration $netSuiteIntegration, IndexStrategyInterface $indexByMultiTenancy)
    {
        $authenticatedUser = auth()->user();
        $builder = $indexByMultiTenancy->index($netSuiteIntegration, $authenticatedUser, 'index_net_suite_integrations_multi_tenancy');
        $results = $netSuiteIntegration->search($builder, $request->input('keyword'));
        return $results->get();
    }


}
