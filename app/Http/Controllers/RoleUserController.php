<?php

namespace App\Http\Controllers;

use Closure;
use App\Role;
use App\User;
use Illuminate\Http\Request;

class RoleUserController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('role_user_rbac');
        $this->middleware(function($request, Closure $next){
            session(['highlighSelectedNav'=>'userSystem']);
            return $next($request);
        });
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::all();
        $rolesArray = $roles->pluck('name', 'id');

        $selectedRoles = $user->roles->pluck('id')->toArray();
        $selectedRoles = isset($selectedRoles)? $selectedRoles : [];
        return view('role_user.edit', compact('user', 'rolesArray', 'selectedRoles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, ['roles'=>'required']);
        $user = User::findOrFail($id);

        $rolesIDs = $request->input('roles');
        $user->roles()->sync($rolesIDs);
        flash()->success('User role has been updated.');
        return redirect()->action('HomeController@index');
    }

}
