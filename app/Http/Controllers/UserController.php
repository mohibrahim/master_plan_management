<?php

namespace App\Http\Controllers;

use Closure;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\MasterPlanManagement\Services\IndexTenancy\IndexStrategyInterface;
use App\MasterPlanManagement\Services\LanguageLocator\LanguageLocatorTrait;
use App\MasterPlanManagement\Services\WorldCountries\LanguageCountriesResolverInterface;
use App\MasterPlanManagement\Services\Models\User\CreateUserStrategy\CreateUserInterface;
use App\MasterPlanManagement\Services\Models\User\UpdateUserStrategy\UpdateUserInterface;
use App\MasterPlanManagement\Services\FlashMessageLanguage\FlashMessageArabicEnglishInterface;
use App\MasterPlanManagement\Services\Models\ProjectFile\UploadFile\DestroyUploadedFile\Destroy;

class UserController extends Controller
{
    use LanguageLocatorTrait;
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('users_rbac');
        $this->middleware('users_multi_tenancy')->only(['show', 'edit', 'update', 'destroy']);

        $this->middleware(function ($request, Closure $next) {
            session(['highlighSelectedNav' => 'userSystem']);
            return $next($request);
        });
    }

    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function index(IndexStrategyInterface $indexByMultiTenancy)
    {
        $authenticatedUser = auth()->user();
        $builder = $indexByMultiTenancy->index($authenticatedUser, $authenticatedUser, 'index_users_multi_tenancy');
        $users = $builder->paginate(20);
        return view('users.index', compact('users'));
    }

    public function create(LanguageCountriesResolverInterface $languageCountriesResolver, User $user)
    {
        $countries = $languageCountriesResolver->getSuitableCountriesListByCurrentLanguage();
        return view('users.create', compact('countries', 'user'));
    }


    public function store(
        UserRequest $request,
        CreateUserInterface $createUser,
        FlashMessageArabicEnglishInterface $flashMessageArabicEnglish
    ) {
        $user = $createUser->create($request);
        flash()->success(
            $flashMessageArabicEnglish->getSuitableFlashMessage('تم إضافة المستخدم بنجاح', 'User Added Successfully.')
        );
        return redirect()->action('UserController@show', ['id' => $user->id]);
    }


    public function show($id)
    {
        $user = User::findOrFail($id);
        return view('users.show', compact('user'));
    }

    public function edit($id, LanguageCountriesResolverInterface $languageCountriesResolver)
    {
        $user = User::findOrFail($id);
        $countries = $languageCountriesResolver->getSuitableCountriesListByCurrentLanguage();
        return view('users.edit', compact('user', 'countries'));
    }

    public function update(
        UserRequest $request,
        int $id,
        UpdateUserInterface $updateUser,
        FlashMessageArabicEnglishInterface $flashMessageArabicEnglish
    ) {
        $user = $updateUser->update($id, $request);
        flash()->success(
            $flashMessageArabicEnglish->getSuitableFlashMessage('تم تعديل المستخدم بنجاح', 'User has been updated successfully.')

        )->important();
        return redirect()->action('UserController@show', ['id' => $user->id]);
    }

    public function destroy(
        User $user,
        FlashMessageArabicEnglishInterface $flashMessageArabicEnglish
    ) {
        $projectFile = $user->personalImage;
        if (!empty($projectFile)) {
            $destroyProjectFile = new Destroy('project_files', $projectFile->id);
            $destroyProjectFile->destroyFile();
        }
        $user->delete();
        flash()->success(
            $flashMessageArabicEnglish->getSuitableFlashMessage("تم حذف المستخم $user->name بنجاح", 'User: ' . $user->name . ', has been deleted successfully')

        )->important();
        return redirect('users');
    }
    
    /*
    |--------------------------------------------------------------------------
    | Miscellaneous
    |-------------------------------------------------------------------------- 
    */
    
    public function indexUsersNotVerifiedEmail()
    {
        $usersNotVerified = User::whereNull('email_verified_at')->paginate(25);
        return view('users.index_users_not_verified_email', compact('usersNotVerified'));
    }

    public function verifyUserEmail(User $user)
    {
        $user->update(['email_verified_at' => now()]);
        flash()->success('تم تفعيل اشتراك المستخدم: ' . $user->name)->important();
        return redirect()->action('UserController@indexUsersNotVerifiedEmail');
    }

    /*
    |--------------------------------------------------------------------------
    | Miscellaneous - Ajax
    |-------------------------------------------------------------------------- 
    */

    public function search(Request $request, IndexStrategyInterface $indexStrategy)
    {
        $authenticatedUser = auth()->user();
        $builder = $indexStrategy->index($authenticatedUser, $authenticatedUser, 'index_users_multi_tenancy');
        $results = $authenticatedUser->search($builder, $request->input('keyword'));
        return $results;
    }

    public function deletePersonalImage($projectFileId)
    {
        $destroyProjectFile = new Destroy('project_files', $projectFileId);
        $destroyProjectFile->destroyFile();
        return ['success'];
    }
    
}
