<?php

namespace App\Http\Controllers;

use Closure;
use App\Zone;
use App\Project;
use App\NetSuiteIntegration;
use Illuminate\Http\Request;
use App\Http\Requests\ZoneRequest;
use App\MasterPlanManagement\Services\IndexTenancy\IndexStrategyInterface;
use App\MasterPlanManagement\Services\Models\Zone\CreateZoneStrategy\CreateZoneInterface;
use App\MasterPlanManagement\Services\Models\Zone\DeleteZoneStrategy\DeleteZoneInterface;
use App\MasterPlanManagement\Services\Models\Zone\UpdateZoneStrategy\UpdateZoneInterface;
use App\MasterPlanManagement\Services\FlashMessageLanguage\FlashMessageArabicEnglishInterface;

class ZoneController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */



    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('zones_rbac');
        $this->middleware('zones_multi_tenancy')->only(['show', 'edit', 'update', 'destroy']);

        $this->middleware(function ($request, Closure $next) {
            session(['highlighSelectedNav' => 'zone']);
            return $next($request);
        });
    }

    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */



    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function index(IndexStrategyInterface $indexByMultiTenancy, Zone $zone)
    {
        $authenticatedUser = auth()->user();
        $zone = $indexByMultiTenancy->index($zone, $authenticatedUser, 'index_zones_multi_tenancy');
        $zones = $zone->paginate(20);
        $netSuitNamesIds = NetSuiteIntegration::all()->pluck('name', 'id');
        return view('zones.index', compact('zones', 'netSuitNamesIds'));
    }

    public function create(Zone $zone)
    {
        return view('zones.create', compact('zone'));
    }


    public function store(ZoneRequest $request, CreateZoneInterface $createZone, FlashMessageArabicEnglishInterface $flashMessageArabicEnglish)
    {
        $zone = $createZone->create($request);
        flash()->success(
            $flashMessageArabicEnglish->getSuitableFlashMessage('تم إضافة المنطقة بنجاح', 'Zone Added Successfully.')
        );
        return redirect()->action('ZoneController@show', ['id' => $zone->id]);
    }


    public function show(Zone $zone)
    {
        return view('zones.show', compact('zone'));
    }

    public function edit(Zone $zone)
    {
        return view('zones.edit', compact('zone'));
    }

    public function update(
        ZoneRequest $request,
        int $id,
        UpdateZoneInterface $updateZone,
        FlashMessageArabicEnglishInterface $flashMessageArabicEnglish
    ) {
        $zone = $updateZone->update($id, $request);
        flash()->success(
            $flashMessageArabicEnglish->getSuitableFlashMessage('تم تعديل المنطقة بنجاح', 'Zone has been updated successfully.')

        )->important();
        return redirect()->action('ZoneController@show', ['id' => $zone->id]);
    }

    public function destroy(Zone $zone, FlashMessageArabicEnglishInterface $flashMessageArabicEnglish, DeleteZoneInterface $deleteZone)
    {
        $deleteZone->delete($zone);
        flash()->success(
            $flashMessageArabicEnglish->getSuitableFlashMessage("تم حذف المنطقة $zone->name بنجاح", 'Zone: ' . $zone->name . ', has been deleted successfully')

        )->important();
        return redirect('zones');
    }

    /*
    |--------------------------------------------------------------------------
    | Miscellaneous
    |--------------------------------------------------------------------------
    */


    /*
    |--------------------------------------------------------------------------
    | Miscellaneous - Ajax
    |--------------------------------------------------------------------------
    */

    public function search(Request $request, Zone $zone, IndexStrategyInterface $indexByMultiTenancy)
    {
        $authenticatedUser = auth()->user();
        $builder = $indexByMultiTenancy->index($zone, $authenticatedUser, 'index_zones_multi_tenancy');
        $results = $zone->search($builder, $request->input('keyword'));
        return $results->get();
    }


    public function getProjectNamesIds(Request $request)
    {
        $keyword  = $request->input('term');
        $results = [
            'results' =>
            Project::where('name', 'like', "$keyword%")
                ->limit(6)
                ->get()
                ->map(function ($project, $key) {
                    return ['id' => $project->id, 'text' => $project->name, 'masterPlanImage' => $project->master_plan_image, 'phases'=>optional($project->phases)->pluck('name', 'id')];
                })
        ];
        return $results;
    }
}
