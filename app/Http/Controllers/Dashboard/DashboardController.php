<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;

class DashboardController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function specifyUserDashboard()
    {
        $masterRoleName = $this->getAuthenticatedUserMasterRoleName();
        return $this->chooseDashboard($masterRoleName);
    }

    private function getAuthenticatedUserMasterRoleName(): string
    {
        $authenticatedUser = auth()->user();
        $masterRole = $authenticatedUser->masterRole;
        if (!empty($masterRole)) {
            return $masterRole->name;
        }
        return '';
    }

    private function chooseDashboard(string $masterRoleName)
    {
        $rolesNamesDashboardControllersNames = config('masterplanmanagement.roles_names_dashboard_controllers_names');
        if (array_key_exists($masterRoleName, $rolesNamesDashboardControllersNames)) {
            return redirect()->action(
                'Dashboard\\'.$rolesNamesDashboardControllersNames[$masterRoleName] . 'DashboardController@home'
            );
        }
        return view('home');
    }
}
