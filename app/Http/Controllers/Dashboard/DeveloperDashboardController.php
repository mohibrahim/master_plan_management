<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\MasterPlanManagement\Services\Models\User\Dashboard\DashboardControllerInterface;

class DeveloperDashboardController extends Controller implements DashboardControllerInterface
{
    public function home()
    {
        return response()->view('dashboard.developer');
    }

    
}
