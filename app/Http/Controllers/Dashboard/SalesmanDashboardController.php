<?php

namespace App\Http\Controllers\Dashboard;

use App\Unit;
use App\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MasterPlanManagement\Services\Models\Unit\UnitStatusStrategy\SuitableUnitStatus;
use App\MasterPlanManagement\Services\Models\User\Dashboard\DashboardControllerInterface;

class SalesmanDashboardController extends Controller implements DashboardControllerInterface
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('salesman_dashboard');
    }

    public function home()
    {
        $authenticatedUser = auth()->user();
        $projectUserAssignments = $authenticatedUser->projectUserAssignments;
        return response()->view('dashboard.salesman', compact('projectUserAssignments'));
    }

    public function getSelectedProject(Request $request)
    {
        $projectId = $request->input('project_id');
        if (!empty($projectId)) {
            $project = Project::find($projectId);
            $unitStatus = resolve(SuitableUnitStatus::class)->getList();
            $text = $this->createProjectView($project, $unitStatus  );
            return ['html'=> $text];
        }
    }

    private function createProjectView($project, $unitStatus)
    {
        $text =
        '<div class="row mt-3">
            <div class="col-sm-12 col-md-12 col-lg-2 col-xl-2 px-1" id="colum-filer-collapse">
                <div class="card">
                    <div class="card-body">
                        <form>
                            <fieldset>
                                <h5 class="text-center"> '.__('project.filtering_units').' </h5>
                                <div class="form-group">
                                    '.\Form::label('zone_id', __('project.zone_id')).'
                                    '.\Form::select('zone_id', $project->zones_names_ids,
                                    null,['class'=>'form-control', 'placeholder'=>__('unit.choose_all'),
                                    'id'=>'zone-id-select-element']) .'
                                </div>
                                <div class="form-group">
                                    '.\Form::label('unit_model_id', __('project.unit_model_id')).'
                                    '.\Form::select('unit_model_id', $project->units_models_names_ids,
                                    null,['class'=>'form-control', 'placeholder'=>__('unit.choose_all'),
                                    'id'=>'unit-model-id-select-element']) .'
                                </div>
                                <div class="form-group">
                                    '.\Form::label('unit_status', __('project.unit_status')).'
                                    '.\Form::select('unit_status', $unitStatus, null,['class'=>'form-control',
                                    'placeholder'=>__('unit.choose_all'),
                                    'id'=>'unit-status-select-element']) .'
                                </div>

                                <div class="form-group">
                                    '.\Form::label('in-block-radio-button', __('project.in_block')).'
                                    '.\Form::checkbox('in_block',"che",false, ['id'=>'in-block-radio-button']) .'
                                </div>

                                <div class="form-group">
                                    <button type="button" id="filtering-btn" class="btn btn-primary btn-block form-control" data-filtered-project-id="'.$project->id.'">'.__('project.filtering').'</button>
                                </div>

                                <div class="form-group">
                                    <input type="reset" class="btn btn-success btn-block form-control" value="'.__('project.reset_filtering').'">
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-10 col-xl-10 px-1" id="colum-master-plan-image-holder">
                <div class="card">
                    <button class="btn btn-outline-dark btn-sm border-0" id="filter-collapse-button">
                        <i class="fas fa-angle-double-right" id="chevron-icon-element"></i>
                    </button>
                    <div class="card-body">
                        <legend class="text-center">'.__('project.master_plan_image').'</legend>
                        <div class="overflow-auto border border-info" id="over-flow-wrapper" >

                            <img src="'.asset($project->master_plan_image).'"
                                alt="" id="map-image" class="m-auto" usemap="#master-plan-test">
                            <map name="master-plan-test" id="map-element">
                                ';
                                $text .= $this->unitsAreas($project);
                    $text .='</map>
                        </div>
                    </div>
                </div>
            </div>
        </div>';
        return $text;
    }

    private function unitsAreas(Project $project): string
    {
        $text = '';
        foreach($project->units as $key=> $unit){
            $text .= '<area class="my-selected-area" id="area-'.$key.'" shape="poly"
                coords="'.$unit->html_area_tag_coords.'"
                data-maphilight=\'{"stroke":true, "strokeColor":"000000","strokeWidth":2,"fillColor":"'.$unit->status_color.'","fillOpacity":0.7,"alwaysOn":true}\'
                href="'.action('UnitController@show', ['id'=>$unit->id]).'" alt=""
                data-toggle="modal" data-target="#unit-modal" data-unit-details=\''.$unit.'\'>';

        }
        return $text;
    }

    public function getUnitRelatedCatalogsLinks(Request $request)
    {
        $unitId = $request->input('unit_id');
        $links = [];
        if (!empty($unitId)) {
            $unit = Unit::findOrFail($unitId);
            $links = $unit->getRelatedCatalogsImagesPdfs();
            return $links;
        }
        return $links;
    }

    public function getUnitsAreasAsHtmlTags(Request $request)
    {
        $projectId = $request->input('project_id');
        $project = Project::findOrFail($projectId);
        $unitAreas = $this->unitsAreas($project);
        return $unitAreas;
    }
}
