<?php

namespace App\Http\Controllers;

use Closure;
use App\Catalog;
use Illuminate\Http\Request;
use App\Http\Requests\CatalogRequest;
use Illuminate\Support\Facades\Storage;
use App\MasterPlanManagement\Services\IndexTenancy\IndexStrategyInterface;
use App\MasterPlanManagement\Services\Models\Catalog\HiddenInputCreator\HiddenInputCreator;
use App\MasterPlanManagement\Services\FlashMessageLanguage\FlashMessageArabicEnglishInterface;
use App\MasterPlanManagement\Services\Models\ProjectFile\UploadFile\DestroyUploadedFile\Destroy;
use App\MasterPlanManagement\Services\Models\Catalog\CreateCatalogStrategy\CreateCatalogInterface;
use App\MasterPlanManagement\Services\Models\Catalog\DeleteCatalogStrategy\DeleteCatalogInterface;
use App\MasterPlanManagement\Services\Models\Catalog\UpdateCatalogStrategy\UpdateCatalogInterface;

class CatalogController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */



    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('catalogs_rbac');
        $this->middleware('catalogs_multi_tenancy')->only(['show', 'edit', 'update', 'destroy']);

        $this->middleware(function ($request, Closure $next) {
            session(['highlighSelectedNav' => 'catalogs']);
            return $next($request);
        });
    }

    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */



    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function index(IndexStrategyInterface $indexByMultiTenancy, Catalog $catalog)
    {
        $authenticatedUser = auth()->user();
        $catalogs = $indexByMultiTenancy->index($catalog, $authenticatedUser, 'index_catalogs_multi_tenancy');
        $catalogs = $catalogs->paginate(20);
        return view('catalogs.index', compact('catalogs'));
    }

    public function create(Catalog $catalog)
    {
        $hiddenInput = resolve(HiddenInputCreator::class)->createHiddenInput(request());
        return view('catalogs.create', compact('catalog', 'hiddenInput'));
    }


    public function store(CatalogRequest $request, CreateCatalogInterface $createCatalog, FlashMessageArabicEnglishInterface $flashMessageArabicEnglish)
    {
        $catalog = $createCatalog->create($request);
        flash()->success(
            $flashMessageArabicEnglish->getSuitableFlashMessage('تم إضافة الكتالوج بنجاح', 'Catalog Added Successfully.')
        );
        return redirect()->action('CatalogController@show', ['id' => $catalog->id]);
    }


    public function show(Catalog $catalog)
    {
        return view('catalogs.show', compact('catalog'));
    }

    public function showReservationCatalog(Catalog $catalog)
    {
        return view('catalogs.show_reservation_catalog', compact('catalog'));
    }

    public function edit(Catalog $catalog)
    {
        return view('catalogs.edit', compact('catalog'));
    }

    public function update(CatalogRequest $request, int $id, UpdateCatalogInterface $updateCatalog, FlashMessageArabicEnglishInterface $flashMessageArabicEnglish)
    {
        $catalog = $updateCatalog->update($id, $request);
        flash()->success(
            $flashMessageArabicEnglish->getSuitableFlashMessage('تم تعديل الكتالوج بنجاح', 'Catalog has been updated successfully.')

        )->important();
        return redirect()->action('CatalogController@show', ['id' => $catalog->id]);
    }

    public function destroy(Catalog $catalog, FlashMessageArabicEnglishInterface $flashMessageArabicEnglish)
    {
        $isDelete = resolve(DeleteCatalogInterface::class)->delete($catalog);
        flash()->success(
            $flashMessageArabicEnglish->getSuitableFlashMessage("تم حذف الكتالوج $catalog->name بنجاح", 'Catalog: ' . $catalog->name . ', has been deleted successfully')
        )->important();
        return redirect('catalogs');
    }

    /*
    |--------------------------------------------------------------------------
    | Miscellaneous
    |--------------------------------------------------------------------------
    */


    /*
    |--------------------------------------------------------------------------
    | Miscellaneous - Ajax
    |--------------------------------------------------------------------------
    */

    public function search(Request $request, Catalog $catalog, IndexStrategyInterface $indexByMultiTenancy)
    {
        $authenticatedUser = auth()->user();
        $builder = $indexByMultiTenancy->index($catalog, $authenticatedUser, 'index_catalogs_multi_tenancy');
        $results = $catalog->search($builder, $request->input('keyword'));
        return $results->get();
    }

    public function deleteFile($projectFileId)
    {
        $destroyProjectFile = new Destroy('project_files', $projectFileId);
        $destroyProjectFile->destroyFile();
        return ['success'];
    }

    public function downloadFile(Request $request)
    {
        $fileName = $request->input('file_name');
        return Storage::download($fileName);
    }

    public function displayFile(Request $request)
    {
        $fileName = $request->input('file_name');
        return response(Storage::get($fileName))->header('Content-Type', Storage::mimeType($fileName));
    }
}
