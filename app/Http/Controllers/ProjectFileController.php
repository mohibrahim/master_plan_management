<?php

namespace App\Http\Controllers;

use Closure;
use App\User;
use App\ProjectFile;
use Illuminate\Http\Request;
use App\Http\Requests\ProjectFileRequest;
use App\MasterPlanManagement\Services\IndexTenancy\IndexStrategyInterface;
use App\MasterPlanManagement\Services\LanguageLocator\LanguageLocatorTrait;
use App\MasterPlanManagement\Services\FlashMessageLanguage\FlashMessageArabicEnglishInterface;
use App\MasterPlanManagement\Services\Models\User\UpdateUserStrategy\UpdateProjectFileInterface;
use App\MasterPlanManagement\Services\Models\ProjectFile\CreateProjectFileStrategy\CreateProjectFileInterface;

class ProjectFileController extends Controller
{
    use LanguageLocatorTrait;
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */



    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('project_files_rbac');
        $this->middleware('project_files_multi_tenancy')->only(['show', 'edit', 'update', 'destroy']);

        $this->middleware(function ($request, Closure $next) {
            session(['highlighSelectedNav' => 'projectFile']);
            return $next($request);
        });
    }

    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */



    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */


    public function index(IndexStrategyInterface $indexByMultiTenancy, ProjectFile $projectFile)
    {
        $authenticatedUser = auth()->user();
        $projectFiles = $indexByMultiTenancy->index($projectFile, $authenticatedUser, 'index_project_files_multi_tenancy');
        $projectFiles = $projectFiles->paginate(20);
        return view('project_files.index', compact('projectFiles'));
    }

    public function create()
    {
        return view('project_files.create');
    }


    public function store(
        ProjectFileRequest $request,
        CreateProjectFileInterface $createProjectFile,
        FlashMessageArabicEnglishInterface $flashMessageArabicEnglish
    ) {
        $projectFile = $createProjectFile->create($request);
        flash()->success(
            $flashMessageArabicEnglish->getSuitableFlashMessage('تم إضافة الملف بنجاح', 'File has been Added Successfully.')
        );
        return redirect()->action('ProjectFileController@show', ['id' => $projectFile->id]);
    }


    public function show($id)
    {
        $projectFile = ProjectFile::findOrFail($id);
        return view('project_files.show', compact('projectFile'));
    }

    public function edit(ProjectFile $projectFile)
    {
        return view('project_files.edit', compact('projectFile'));
    }

    public function update(
        ProjectFileRequest $request,
        ProjectFile $projectFile,
        UpdateProjectFileInterface $updateProjectFile,
        FlashMessageArabicEnglishInterface $flashMessageArabicEnglish
    ) {
        $projectFile = $updateProjectFile->update($projectFile->id, $request);
        flash()->success(
            $flashMessageArabicEnglish->getSuitableFlashMessage('تم تعديل الملف بنجاح', 'File has been updated successfully.')

        )->important();
        return redirect()->action('ProjectFileController@show', ['id' => $projectFile->id]);
    }

    public function destroy(
        ProjectFile $projectFile,
        FlashMessageArabicEnglishInterface $flashMessageArabicEnglish
    ) {
        $projectFile->delete();
        flash()->success(
            $flashMessageArabicEnglish->getSuitableFlashMessage("تم حذف المستخم $projectFile->title بنجاح", 'User: ' . $projectFile->title . ', has been deleted successfully')

        )->important();
        return redirect('project_files');
    }


    public function search(Request $request, User $user)
    {
        $results = $user->search($request->input('keyword'));
        return $results;
    }
}
