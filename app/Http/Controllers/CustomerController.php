<?php

namespace App\Http\Controllers;

use Closure;
use App\Customer;
use App\NetSuiteIntegration;
use Illuminate\Http\Request;
use App\Http\Requests\CustomerRequest;
use App\MasterPlanManagement\Services\IndexTenancy\IndexStrategyInterface;
use App\MasterPlanManagement\Services\FlashMessageLanguage\FlashMessageArabicEnglishInterface;
use App\MasterPlanManagement\Services\Models\IntegrationPushingQueue\OperationType\OperationType;
use App\MasterPlanManagement\Services\Models\Customer\CreateCustomerStrategy\CreateCustomerInterface;
use App\MasterPlanManagement\Services\Models\Customer\DeleteCustomerStrategy\DeleteCustomerInterface;
use App\MasterPlanManagement\Services\Models\Customer\UpdateCustomerStrategy\UpdateCustomerInterface;
use App\MasterPlanManagement\Services\Models\IntegrationPushingQueue\IntegrationType\IntegrationType;
use App\MasterPlanManagement\Services\Integrations\Push\ThirdPartyObjects\Customer\NetSuite\MapEloquentCustomerToNetSuiteAsArrayStore;
use App\MasterPlanManagement\Services\Integrations\Push\ThirdPartyObjects\Customer\NetSuite\MapEloquentCustomerToNetSuiteAsArrayUpdate;
use App\MasterPlanManagement\Services\Integrations\Push\ThirdPartyObjects\Customer\NetSuite\MapEloquentCustomerToNetSuiteAsArrayDestroy;
use App\MasterPlanManagement\Services\Models\IntegrationPushingQueue\CreateIntegrationPushingQueue\CreateIntegrationPushingQueueCustomer;

class CustomerController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */
    private $createIntegrationPushingQueueCustomer;


    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */

    public function __construct(CreateIntegrationPushingQueueCustomer $createIntegrationPushingQueueCustomer)
    {
        $this->middleware('auth');
        $this->middleware('customers_rbac');
        $this->middleware('customers_multi_tenancy')->only(['show', 'edit', 'update', 'destroy']);

        $this->middleware(function ($request, Closure $next) {
            session(['highlighSelectedNav' => 'customer']);
            return $next($request);
        });

        $this->createIntegrationPushingQueueCustomer = $createIntegrationPushingQueueCustomer;
    }

    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */



    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function index(IndexStrategyInterface $indexByMultiTenancy, Customer $customer)
    {
        $authenticatedUser = auth()->user();
        $customer = $indexByMultiTenancy->index($customer, $authenticatedUser, 'index_customers_multi_tenancy');
        $customers = $customer->paginate(20);
        $netSuitNamesIds = NetSuiteIntegration::all()->pluck('name', 'id');
        return view('customers.index', compact('customers', 'netSuitNamesIds'));
    }

    public function create(Customer $customer)
    {
        return view('customers.create', compact('customer'));
    }


    public function store(CustomerRequest $request, CreateCustomerInterface $createCustomer, FlashMessageArabicEnglishInterface $flashMessageArabicEnglish)
    {
        $customer = $createCustomer->create($request);



        //Queue job to third party pushing queue.
        $createIntegrationPushingQueueCustomer = $this->createIntegrationPushingQueueCustomer;
        $createIntegrationPushingQueueCustomer->setIntegrationType(IntegrationType::NETSUITE)
            ->setMapEloquentCustomerToThirdParty(new MapEloquentCustomerToNetSuiteAsArrayStore)
            ->setOperationType(OperationType::CREATE)
            ->setCustomer($customer);
        $createIntegrationPushingQueueCustomer->create();



        flash()->success(
            $flashMessageArabicEnglish->getSuitableFlashMessage('تم إضافة العميل بنجاح', 'Customer Added Successfully.')
        );
        return redirect()->action('CustomerController@show', ['id' => $customer->id]);
    }


    public function show(Customer $customer)
    {
        return view('customers.show', compact('customer'));
    }

    public function edit(Customer $customer)
    {
        return view('customers.edit', compact('customer'));
    }

    public function update(CustomerRequest $request, int $id, UpdateCustomerInterface $updateCustomer, FlashMessageArabicEnglishInterface $flashMessageArabicEnglish)
    {
        $customer = $updateCustomer->update($id, $request);



         //Queue job to third party pushing queue.
         $createIntegrationPushingQueueCustomer = $this->createIntegrationPushingQueueCustomer;
         $createIntegrationPushingQueueCustomer->setIntegrationType(IntegrationType::NETSUITE)
             ->setMapEloquentCustomerToThirdParty(new MapEloquentCustomerToNetSuiteAsArrayUpdate)
             ->setOperationType(OperationType::UPDATE)
             ->setCustomer($customer);
         $createIntegrationPushingQueueCustomer->create();



        flash()->success(
            $flashMessageArabicEnglish->getSuitableFlashMessage('تم تعديل العميل بنجاح', 'Customer has been updated successfully.')

        )->important();
        return redirect()->action('CustomerController@show', ['id' => $customer->id]);
    }

    public function destroy(Customer $customer, FlashMessageArabicEnglishInterface $flashMessageArabicEnglish)
    {
        // Queue job to third party pushing queue.
        $createIntegrationPushingQueueCustomer = $this->createIntegrationPushingQueueCustomer;
        $createIntegrationPushingQueueCustomer->setIntegrationType(IntegrationType::NETSUITE)
            ->setMapEloquentCustomerToThirdParty(new MapEloquentCustomerToNetSuiteAsArrayDestroy)
            ->setOperationType(OperationType::DELETE)
            ->setCustomer($customer);
        $createIntegrationPushingQueueCustomer->create();



        $isDeleted = resolve(DeleteCustomerInterface::class)->delete($customer);
        flash()->success(
            $flashMessageArabicEnglish->getSuitableFlashMessage("تم حذف العميل $customer->name بنجاح", 'Customer: ' . $customer->name . ', has been deleted successfully')

        )->important();
        return redirect('customers');
    }

    /*
    |--------------------------------------------------------------------------
    | Miscellaneous
    |--------------------------------------------------------------------------
    */


    /*
    |--------------------------------------------------------------------------
    | Miscellaneous - Ajax
    |--------------------------------------------------------------------------
    */

    public function search(Request $request, Customer $customer, IndexStrategyInterface $indexByMultiTenancy)
    {
        $authenticatedUser = auth()->user();
        $builder = $indexByMultiTenancy->index($customer, $authenticatedUser, 'index_customers_multi_tenancy');
        $results = $customer->search($builder, $request->input('keyword'));
        return $results->get();
    }

}
