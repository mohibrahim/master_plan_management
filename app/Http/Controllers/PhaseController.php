<?php

namespace App\Http\Controllers;

use Closure;
use App\Phase;
use App\Project;
use App\NetSuiteIntegration;
use Illuminate\Http\Request;
use App\Http\Requests\PhaseRequest;
use App\MasterPlanManagement\Services\IndexTenancy\IndexStrategyInterface;
use App\MasterPlanManagement\Services\Models\Phase\CreatePhaseStrategy\CreatePhaseInterface;
use App\MasterPlanManagement\Services\Models\Phase\DeletePhaseStrategy\DeletePhaseInterface;
use App\MasterPlanManagement\Services\Models\Phase\UpdatePhaseStrategy\UpdatePhaseInterface;
use App\MasterPlanManagement\Services\FlashMessageLanguage\FlashMessageArabicEnglishInterface;
use App\MasterPlanManagement\Services\Models\ProjectFile\UploadFile\DestroyUploadedFile\Destroy;
use App\MasterPlanManagement\Services\Models\Catalog\DeleteCatalogStrategy\DeleteCatalogInterface;

class PhaseController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */



    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('phases_rbac');
        $this->middleware('phases_multi_tenancy')->only(['show', 'edit', 'update', 'destroy']);

        $this->middleware(function ($request, Closure $next) {
            session(['highlighSelectedNav' => 'phases']);
            return $next($request);
        });
    }

    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */



    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function index(IndexStrategyInterface $indexByMultiTenancy, Phase $phase)
    {
        $authenticatedUser = auth()->user();
        $phases = $indexByMultiTenancy->index($phase, $authenticatedUser, 'index_phases_multi_tenancy');
        $phases = $phases->paginate(20);
        $netSuitNamesIds = NetSuiteIntegration::all()->pluck('name', 'id');
        return view('phases.index', compact('phases', 'netSuitNamesIds'));
    }

    public function create(Phase $phase)
    {
        return view('phases.create', compact('phase'));
    }


    public function store(PhaseRequest $request, CreatePhaseInterface $createPhase, FlashMessageArabicEnglishInterface $flashMessageArabicEnglish)
    {
        $phase = $createPhase->create($request);
        flash()->success(
            $flashMessageArabicEnglish->getSuitableFlashMessage('تم إضافة المرحلة بنجاح', 'Phase Added Successfully.')
        );
        return redirect()->action('PhaseController@show', ['id' => $phase->id]);
    }


    public function show(Phase $phase)
    {
        return view('phases.show', compact('phase'));
    }

    public function edit(Phase $phase)
    {
        $selectedProjectNameId = $phase->selected_project_name_id;
        return view('phases.edit', compact('phase', 'selectedProjectNameId'));
    }

    public function update(PhaseRequest $request, int $id, UpdatePhaseInterface $updatePhase, FlashMessageArabicEnglishInterface $flashMessageArabicEnglish)
    {
        $phase = $updatePhase->update($id, $request);
        flash()->success(
            $flashMessageArabicEnglish->getSuitableFlashMessage('تم تعديل المرحلة بنجاح', 'Phase has been updated successfully.')

        )->important();
        return redirect()->action('PhaseController@show', ['id' => $phase->id]);
    }

    public function destroy(Phase $phase, FlashMessageArabicEnglishInterface $flashMessageArabicEnglish, DeletePhaseInterface $deletePhase)
    {
        $deletePhase->delete($phase);
        flash()->success(
            $flashMessageArabicEnglish->getSuitableFlashMessage("تم حذف المرحلة $phase->name بنجاح", 'Phase: ' . $phase->name . ', has been deleted successfully')

        )->important();
        return redirect('phases');
    }

    /*
    |--------------------------------------------------------------------------
    | Miscellaneous
    |--------------------------------------------------------------------------
    */


    /*
    |--------------------------------------------------------------------------
    | Miscellaneous - Ajax
    |--------------------------------------------------------------------------
    */

    public function search(Request $request, Phase $phase, IndexStrategyInterface $indexByMultiTenancy)
    {
        $authenticatedUser = auth()->user();
        $builder = $indexByMultiTenancy->index($phase, $authenticatedUser, 'index_phases_multi_tenancy');
        $results = $phase->search($builder, $request->input('keyword'));
        return $results->get();
    }

    public function deleteImage($projectFileId)
    {
        $destroyProjectFile = new Destroy('project_files', $projectFileId);
        $destroyProjectFile->destroyFile();
        return ['success'];
    }

    public function getProjectNamesIds(Request $request)
    {
        $keyword  = $request->input('term');
        $results = [
            'results' =>
            Project::where('name', 'like', "$keyword%")
                ->limit(6)
                ->get()
                ->map(function ($item, $key) {
                    return ['id' => $item->id, 'text' => $item->name];
                })
        ];
        return $results;
    }
}
