<?php

namespace App\Http\Controllers;

use App\Bank;
use Closure;
use App\Unit;
use App\Customer;
use App\Reservation;
use App\NetSuiteIntegration;
use Illuminate\Http\Request;
use App\Http\Requests\ReservationRequest;
use App\MasterPlanManagement\Services\IndexTenancy\IndexStrategyInterface;
use App\MasterPlanManagement\Services\Integrations\Push\CommunicationStrategy\GuzzlePut;
use App\MasterPlanManagement\Services\Models\Unit\UnitStatusStrategy\UnitStatusAbstract;
use App\MasterPlanManagement\Services\Integrations\Push\CommunicationStrategy\GuzzlePost;
use App\MasterPlanManagement\Services\Integrations\Push\CommunicationStrategy\GuzzleDelete;
use App\MasterPlanManagement\Services\FlashMessageLanguage\FlashMessageArabicEnglishInterface;
use App\MasterPlanManagement\Services\Models\IntegrationPushingQueue\OperationType\OperationType;
use App\MasterPlanManagement\Services\Models\IntegrationPushingQueue\IntegrationType\IntegrationType;
use App\MasterPlanManagement\Services\Models\Reservation\CreateReservationStrategy\CreateReservation;
use App\MasterPlanManagement\Services\Models\Reservation\ReservationStatusStrategy\SuitableReservationStatus;
use App\MasterPlanManagement\Services\Models\Reservation\DeleteReservationStrategy\DeleteReservationInterface;
use App\MasterPlanManagement\Services\Models\Reservation\UpdateReservationStrategy\UpdateReservationInterface;
use App\MasterPlanManagement\Services\Integrations\Push\DataSync\Reservation\Create\CreateReservationThirdPartyInterface;
use App\MasterPlanManagement\Services\Integrations\Push\DataSync\Reservation\Delete\DeleteReservationThirdPartyInterface;
use App\MasterPlanManagement\Services\Integrations\Push\DataSync\Reservation\Update\UpdateReservationThirdPartyInterface;
use App\MasterPlanManagement\Services\Integrations\Push\ThirdPartyObjects\Reservation\NetSuite\MapEloquentReservationToNetSuiteAsArrayStore;
use App\MasterPlanManagement\Services\Models\IntegrationPushingQueue\CreateIntegrationPushingQueue\CreateIntegrationPushingQueueReservation;
use App\MasterPlanManagement\Services\Integrations\Push\ThirdPartyObjects\Reservation\NetSuite\MapEloquentReservationToNetSuiteAsArrayUpdate;
use App\MasterPlanManagement\Services\Integrations\Push\ThirdPartyObjects\Reservation\NetSuite\MapEloquentReservationToNetSuiteAsArrayDestroy;

class ReservationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */



    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('reservations_rbac');
        $this->middleware('reservations_multi_tenancy')->only(['show', 'edit', 'update', 'destroy']);

        $this->middleware(function ($request, Closure $next) {
            session(['highlighSelectedNav' => 'reservation']);
            return $next($request);
        });
    }

    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */



    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function index(IndexStrategyInterface $indexByMultiTenancy, Reservation $reservation)
    {
        $authenticatedUser = auth()->user();
        $reservation = $indexByMultiTenancy->index($reservation, $authenticatedUser, 'index_reservations_multi_tenancy');
        $reservations = $reservation->paginate(20);
        $netSuitNamesIds = NetSuiteIntegration::all()->pluck('name', 'id');
        return view('reservations.index', compact('reservations', 'netSuitNamesIds'));
    }

    public function create(Reservation $reservation)
    {
        $unitId = request()->input('unit_id');
        $reservation->unit_id = $unitId;
        $reservationStatus = resolve(SuitableReservationStatus::class)->getList();
        return view('reservations.create', compact('reservation', 'reservationStatus'));
    }


    public function store(
        ReservationRequest $request,
        CreateReservation $createReservation,
        FlashMessageArabicEnglishInterface $flashMessageArabicEnglish,
        GuzzlePost $guzzlePost,
        NetSuiteIntegration $netSuiteIntegration,
        CreateReservationThirdPartyInterface $createNetSuiteReservation
    ) {

        $reservation = $createReservation->create($request);

        //pushing to netsuite
        // $createNetSuiteReservation->setNetSuiteIntegration($netSuiteIntegration::first());
        // $createNetSuiteReservation->setPostToClientInterface($guzzlePost);
        // $createNetSuiteReservation->store($reservation);

        //adding job to third party pushing queue.
        $createIntegrationPushingQueueReservation = new CreateIntegrationPushingQueueReservation();
        $createIntegrationPushingQueueReservation->setIntegrationType(IntegrationType::NETSUITE)
            ->setMapEloquentReservationToThirdParty(new MapEloquentReservationToNetSuiteAsArrayStore)
            ->setOperationType(OperationType::CREATE)
            ->setReservation($reservation);
        $createIntegrationPushingQueueReservation->create();


        flash()->success(
            $flashMessageArabicEnglish->getSuitableFlashMessage('تم إضافة الحجز بنجاح', 'Reservation Added Successfully.')
        );
        return redirect()->action('ReservationController@show', ['id' => $reservation->id]);
    }


    public function show(Reservation $reservation)
    {
        return view('reservations.show', compact('reservation'));
    }

    public function edit(Reservation $reservation)
    {
        $reservationStatus = resolve(SuitableReservationStatus::class)->getList();
        return view('reservations.edit', compact('reservation', 'reservationStatus'));
    }

    public function update(
        ReservationRequest $request,
        int $id,
        UpdateReservationInterface $updateReservation,
        FlashMessageArabicEnglishInterface $flashMessageArabicEnglish,
        GuzzlePut $guzzlePut,
        NetSuiteIntegration $netSuiteIntegration,
        UpdateReservationThirdPartyInterface $updateNetSuiteReservation

    ) {
        $reservation = $updateReservation->update($id, $request);

        // $updateNetSuiteReservation->setNetSuiteIntegration($netSuiteIntegration::first());
        // $updateNetSuiteReservation->setPostToClientInterface($guzzlePut);
        // $updateNetSuiteReservation->update($reservation);

        //adding job to third party pushing queue.
        $createIntegrationPushingQueueReservation = new CreateIntegrationPushingQueueReservation();
        $createIntegrationPushingQueueReservation->setIntegrationType(IntegrationType::NETSUITE)
            ->setMapEloquentReservationToThirdParty(new MapEloquentReservationToNetSuiteAsArrayUpdate)
            ->setOperationType(OperationType::UPDATE)
            ->setReservation($reservation);
        $createIntegrationPushingQueueReservation->create();


        flash()->success(
            $flashMessageArabicEnglish->getSuitableFlashMessage('تم تعديل الحجز بنجاح', 'Reservation has been updated successfully.')

        )->important();
        return redirect()->action('ReservationController@show', ['id' => $reservation->id]);
    }

    public function destroy(
        Reservation $reservation,
        FlashMessageArabicEnglishInterface $flashMessageArabicEnglish,
        GuzzleDelete $guzzleDelete,
        NetSuiteIntegration $netSuiteIntegration,
        DeleteReservationThirdPartyInterface $deleteNetSuiteReservation
    ) {
        // $deleteNetSuiteReservation->setNetSuiteIntegration($netSuiteIntegration::first());
        // $deleteNetSuiteReservation->setPostToClientInterface($guzzleDelete);
        // $deleteNetSuiteReservation->delete($reservation);

        //adding job to third party pushing queue.
        // $createIntegrationPushingQueueReservation = new CreateIntegrationPushingQueueReservation();
        // $createIntegrationPushingQueueReservation->setIntegrationType(IntegrationType::NETSUITE)
        //     ->setMapEloquentReservationToThirdParty(new MapEloquentReservationToNetSuiteAsArrayDestroy)
        //     ->setOperationType(OperationType::DELETE)
        //     ->setReservation($reservation);
        // $createIntegrationPushingQueueReservation->create();

        $isDeleted = resolve(DeleteReservationInterface::class)->delete($reservation);
        flash()->success(
            $flashMessageArabicEnglish->getSuitableFlashMessage("تم حذف الحجز $reservation->id بنجاح", 'Reservation: ' . $reservation->id . ', has been deleted successfully')

        )->important();
        return redirect('reservations');
    }

    /*
    |--------------------------------------------------------------------------
    | Miscellaneous
    |--------------------------------------------------------------------------
    */


    /*
    |--------------------------------------------------------------------------
    | Miscellaneous - Ajax
    |--------------------------------------------------------------------------
    */

    public function search(Request $request, Reservation $reservation, IndexStrategyInterface $indexByMultiTenancy)
    {
        $authenticatedUser = auth()->user();
        $builder = $indexByMultiTenancy->index($reservation, $authenticatedUser, 'index_reservations_multi_tenancy');
        $results = $reservation->search($builder, $request->input('keyword'));
        return $results->get();
    }

    public function getUnitCodesIds(Request $request)
    {
        $keyword  = $request->input('term');
        $results = [
            'results' =>
            Unit::where('code', 'like', "$keyword%")
                ->where('status', UnitStatusAbstract::AVAILABLE)
                ->limit(6)
                ->get()
                ->map(function ($item, $key) {
                    return ['id' => $item->id, 'text' => optional($item->project)->name . ' | ' . $item->code, 'booking_amount' => $item->booking_amount, 'minimum_booking_amount' => $item->minimum_booking_amount, 'currency' => $item->currency];
                })
        ];
        return $results;
    }

    public function getCustomerNamesIds(Request $request)
    {
        $keyword  = $request->input('term');
        $results = [
            'results' =>
            Customer::where('name', 'like', "$keyword%")
                ->orWhere('national_id', 'like', "$keyword%")
                ->limit(6)
                ->get()
                ->map(function ($item, $key) {
                    return ['id' => $item->id, 'text' => $item->name . ' | ' . $item->phone];
                })
        ];
        return $results;
    }

    public function getBanksNamesIds(Request $request)
    {
        $keyword  = $request->input('term');
        $results = [
            'results' =>
            Bank::where('id', 'like', "%$keyword%")
                ->orWhere('name', 'like', "%$keyword%")
                ->limit(5)
                ->get()
                ->map(function ($bank, $key) {
                    return [
                        'id' => $bank->id,
                        'text' => $bank->name,
                    ];
                })
        ];
        return $results;
    }

}
