<?php

namespace App\Http\Controllers;

use Closure;
use App\UnitModel;
use App\NetSuiteIntegration;
use Illuminate\Http\Request;
use App\Http\Requests\UnitModelRequest;
use App\MasterPlanManagement\Services\IndexTenancy\IndexStrategyInterface;
use App\MasterPlanManagement\Services\FlashMessageLanguage\FlashMessageArabicEnglishInterface;
use App\MasterPlanManagement\Services\Models\UnitModel\UnitModelTypeStrategy\SuitableUnitModelType;
use App\MasterPlanManagement\Services\Models\UnitModel\CreateUnitModelStrategy\CreateUnitModelInterface;
use App\MasterPlanManagement\Services\Models\UnitModel\DeleteUnitModelStrategy\DeleteUnitModelInterface;
use App\MasterPlanManagement\Services\Models\UnitModel\UpdateUnitModelStrategy\UpdateUnitModelInterface;
use App\MasterPlanManagement\Services\Models\UnitModel\UnitModelBusinessTypeStrategy\SuitableUnitModelBusinessType;
use App\MasterPlanManagement\Services\Models\UnitModel\UnitModelFinishingTypeStrategy\SuitableUnitModelFinishingType;

class UnitModelController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */



    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('unit_models_rbac');
        $this->middleware('unit_models_multi_tenancy')->only(['show', 'edit', 'update', 'destroy']);

        $this->middleware(function ($request, Closure $next) {
            session(['highlighSelectedNav' => 'unit_models']);
            return $next($request);
        });
    }

    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */



    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function index(IndexStrategyInterface $indexByMultiTenancy, UnitModel $unitModel)
    {
        $authenticatedUser = auth()->user();
        $unitModel = $indexByMultiTenancy->index($unitModel, $authenticatedUser, 'index_unit_models_multi_tenancy');
        $unitModels = $unitModel->paginate(20);
        $netSuitNamesIds = NetSuiteIntegration::all()->pluck('name', 'id');
        return view('unit_models.index', compact('unitModels', 'netSuitNamesIds'));
    }

    public function create(UnitModel $unitModel)
    {
        $unitModelTypes = resolve(SuitableUnitModelType::class)->getList();
        $unitModelBusinessTypes = resolve(SuitableUnitModelBusinessType::class)->getList();
        $unitModelFinishingTypes = resolve(SuitableUnitModelFinishingType::class)->getList();
        return view('unit_models.create', compact('unitModel', 'unitModelTypes', 'unitModelBusinessTypes', 'unitModelFinishingTypes'));
    }


    public function store(UnitModelRequest $request, CreateUnitModelInterface $createUnitModel, FlashMessageArabicEnglishInterface $flashMessageArabicEnglish)
    {
        $unitModel = $createUnitModel->create($request);
        flash()->success(
            $flashMessageArabicEnglish->getSuitableFlashMessage('تم إضافة نموذج الوحدة بنجاح', 'Unit Model Added Successfully.')
        );
        return redirect()->action('UnitModelController@show', ['id' => $unitModel->id]);
    }


    public function show(UnitModel $unitModel)
    {
        return view('unit_models.show', compact('unitModel'));
    }

    public function edit(UnitModel $unitModel)
    {
        $unitModelTypes = resolve(SuitableUnitModelType::class)->getList();
        $unitModelBusinessTypes = resolve(SuitableUnitModelBusinessType::class)->getList();
        $unitModelFinishingTypes = resolve(SuitableUnitModelFinishingType::class)->getList();
        return view('unit_models.edit', compact('unitModel', 'unitModelTypes', 'unitModelBusinessTypes', 'unitModelFinishingTypes'));
    }

    public function update(UnitModelRequest $request, int $id, UpdateUnitModelInterface $updateUnitModel, FlashMessageArabicEnglishInterface $flashMessageArabicEnglish)
    {
        $unitModel = $updateUnitModel->update($id, $request);
        flash()->success(
            $flashMessageArabicEnglish->getSuitableFlashMessage('تم تعديل نموذج الوحدة بنجاح', 'Unit Model has been updated successfully.')

        )->important();
        return redirect()->action('UnitModelController@show', ['id' => $unitModel->id]);
    }

    public function destroy(UnitModel $unitModel, FlashMessageArabicEnglishInterface $flashMessageArabicEnglish, DeleteUnitModelInterface $deleteUnitModel)
    {
        $deleteUnitModel->delete($unitModel);
        flash()->success(
            $flashMessageArabicEnglish->getSuitableFlashMessage("تم حذف نموذج الوحدة $unitModel->name بنجاح", 'Unit Model: ' . $unitModel->name . ', has been deleted successfully')

        )->important();
        return redirect('unit_models');
    }

    /*
    |--------------------------------------------------------------------------
    | Miscellaneous
    |--------------------------------------------------------------------------
    */


    /*
    |--------------------------------------------------------------------------
    | Miscellaneous - Ajax
    |--------------------------------------------------------------------------
    */

    public function search(Request $request, UnitModel $unitModel, IndexStrategyInterface $indexByMultiTenancy)
    {
        $authenticatedUser = auth()->user();
        $builder = $indexByMultiTenancy->index($unitModel, $authenticatedUser, 'index_unit_models_multi_tenancy');
        $results = $unitModel->search($builder, $request->input('keyword'));
        return $results->get();
    }


}
