<?php

namespace App\Http\Controllers;

use Closure;
use App\Project;
use Illuminate\Http\Request;
use App\Http\Requests\ProjectRequest;
use App\MasterPlanManagement\Services\FileUploader\FileUploaderInterface;
use App\MasterPlanManagement\Services\IndexTenancy\IndexStrategyInterface;
use App\MasterPlanManagement\Services\Models\Unit\FilterUnits\FilterUnitInterface;
use App\MasterPlanManagement\Services\Models\Unit\UnitStatusStrategy\SuitableUnitStatus;
use App\MasterPlanManagement\Services\FlashMessageLanguage\FlashMessageArabicEnglishInterface;
use App\MasterPlanManagement\Services\Models\ProjectFile\UploadFile\DestroyUploadedFile\Destroy;
use App\MasterPlanManagement\Services\Models\Project\CreateProjectStrategy\CreateProjectInterface;
use App\MasterPlanManagement\Services\Models\Project\DeleteProjectStrategy\DeleteProjectInterface;
use App\MasterPlanManagement\Services\Models\Project\UpdateProjectStrategy\UpdateProjectInterface;
use App\NetSuiteIntegration;

class ProjectController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */



    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('projects_rbac');
        $this->middleware('projects_multi_tenancy')->only(['show', 'edit', 'update', 'destroy']);

        $this->middleware(function ($request, Closure $next) {
            session(['highlighSelectedNav' => 'projects']);
            return $next($request);
        });
    }

    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */



    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function index(IndexStrategyInterface $indexByMultiTenancy, Project $project)
    {
        $authenticatedUser = auth()->user();
        $projects = $indexByMultiTenancy->index($project, $authenticatedUser, 'index_projects_multi_tenancy');
        $projects = $projects->paginate(20);
        $netSuitNamesIds = NetSuiteIntegration::all()->pluck('name', 'id');
        return view('projects.index', compact('projects', 'netSuitNamesIds'));
    }

    public function create(Project $project)
    {
        return view('projects.create', compact('project'));
    }


    public function store(ProjectRequest $request, CreateProjectInterface $createProject, FlashMessageArabicEnglishInterface $flashMessageArabicEnglish)
    {
        $project = $createProject->create($request);
        flash()->success(
            $flashMessageArabicEnglish->getSuitableFlashMessage('تم إضافة المشروع بنجاح', 'Project Added Successfully.')
        );
        return redirect()->action('ProjectController@show', ['id' => $project->id]);
    }


    public function show(Project $project)
    {
        $unitStatus = resolve(SuitableUnitStatus::class)->getList();
        return view('projects.show', compact('project', 'unitStatus'));
    }

    public function edit(Project $project)
    {
        return view('projects.edit', compact('project'));
    }

    public function update(ProjectRequest $request, int $id, UpdateProjectInterface $updateProject, FlashMessageArabicEnglishInterface $flashMessageArabicEnglish)
    {
        $project = $updateProject->update($id, $request);
        flash()->success(
            $flashMessageArabicEnglish->getSuitableFlashMessage('تم تعديل المشروع بنجاح', 'Project has been updated successfully.')

        )->important();
        return redirect()->action('ProjectController@show', ['id' => $project->id]);
    }

    public function destroy(Project $project, FlashMessageArabicEnglishInterface $flashMessageArabicEnglish, DeleteProjectInterface $deleteProject)
    {
        $deleteProject->delete($project);
        flash()->success(
            $flashMessageArabicEnglish->getSuitableFlashMessage("تم حذف المشروع $project->name بنجاح", 'Project: ' . $project->name . ', has been deleted successfully')

        )->important();
        return redirect('projects');
    }

    /*
    |--------------------------------------------------------------------------
    | Miscellaneous
    |--------------------------------------------------------------------------
    */


    /*
    |--------------------------------------------------------------------------
    | Miscellaneous - Ajax
    |--------------------------------------------------------------------------
    */

    public function search(Request $request, Project $project, IndexStrategyInterface $indexByMultiTenancy)
    {
        $authenticatedUser = auth()->user();
        $builder = $indexByMultiTenancy->index($project, $authenticatedUser, 'index_projects_multi_tenancy');
        $results = $project->search($builder, $request->input('keyword'));
        return $results->get();
    }

    public function deleteImage($projectFileId)
    {
        $destroyProjectFile = new Destroy('project_files', $projectFileId);
        $destroyProjectFile->destroyFile();
        return ['success'];
    }

    public function filterMasterPlanImage(Request $request, FilterUnitInterface $filterUnit)
    {
        return $filterUnit->filter($request);
    }

    public function deleteLogo(Request $request)
    {
        $logo = $request->input('logo');
        $projects = Project::where('logo', $logo)->get();
        if ($projects->isNotEmpty()) {
            $project = $projects->first();
            $fileUploader = resolve(FileUploaderInterface::class);
            $fileUploader->setFileNameToSkip('images/projects_logos/no_image.png');
            $fileUploader->delete($project->logo, 'images/projects_logos');
            $project->logo = 'images/projects_logos/no_image.png';
            $project->save();
            return response()->json(['process'=>'success'], 200);
        }
    }



}
