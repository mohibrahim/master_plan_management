<?php

namespace App\Http\Controllers;

use Closure;
use App\Block;
use App\Project;
use App\NetSuiteIntegration;
use Illuminate\Http\Request;
use App\Http\Requests\BlockRequest;
use App\MasterPlanManagement\Services\IndexTenancy\IndexStrategyInterface;
use App\MasterPlanManagement\Services\Models\Block\CreateBlockStrategy\CreateBlockInterface;
use App\MasterPlanManagement\Services\Models\Block\DeleteBlockStrategy\DeleteBlockInterface;
use App\MasterPlanManagement\Services\Models\Block\UpdateBlockStrategy\UpdateBlockInterface;
use App\MasterPlanManagement\Services\FlashMessageLanguage\FlashMessageArabicEnglishInterface;

class BlockController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------
    |
    */



    /*
    |--------------------------------------------------------------------------
    | Constructors
    |--------------------------------------------------------------------------
    |
    */

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('blocks_rbac');
        $this->middleware('blocks_multi_tenancy')->only(['show', 'edit', 'update', 'destroy']);

        $this->middleware(function ($request, Closure $next) {
            session(['highlighSelectedNav' => 'block']);
            return $next($request);
        });
    }

    /*
    |--------------------------------------------------------------------------
    | Getters, Setters & Interface implementations
    |--------------------------------------------------------------------------
    |
    */



    /*
    |--------------------------------------------------------------------------
    | Core
    |--------------------------------------------------------------------------
    |
    */

    public function index(IndexStrategyInterface $indexByMultiTenancy, Block $block)
    {
        $authenticatedUser = auth()->user();
        $block = $indexByMultiTenancy->index($block, $authenticatedUser, 'index_blocks_multi_tenancy');
        $blocks = $block->paginate(20);
        $netSuitNamesIds = NetSuiteIntegration::all()->pluck('name', 'id');
        return view('blocks.index', compact('blocks', 'netSuitNamesIds'));
    }

    public function create(Block $block)
    {
        return view('blocks.create', compact('block'));
    }


    public function store(BlockRequest $request, CreateBlockInterface $createBlock, FlashMessageArabicEnglishInterface $flashMessageArabicEnglish)
    {
        $block = $createBlock->create($request);
        flash()->success(
            $flashMessageArabicEnglish->getSuitableFlashMessage('تم إضافة القطعة بنجاح', 'Block Added Successfully.')
        );
        return redirect()->action('BlockController@show', ['id' => $block->id]);
    }


    public function show(Block $block)
    {
        return view('blocks.show', compact('block'));
    }

    public function edit(Block $block)
    {
        return view('blocks.edit', compact('block'));
    }

    public function update(BlockRequest $request, int $id, UpdateBlockInterface $updateBlock, FlashMessageArabicEnglishInterface $flashMessageArabicEnglish)
    {
        $block = $updateBlock->update($id, $request);
        flash()->success(
            $flashMessageArabicEnglish->getSuitableFlashMessage('تم تعديل القطعة بنجاح', 'Block has been updated successfully.')

        )->important();
        return redirect()->action('BlockController@show', ['id' => $block->id]);
    }

    public function destroy(Block $block, FlashMessageArabicEnglishInterface $flashMessageArabicEnglish, DeleteBlockInterface $deleteBlock)
    {
        $deleteBlock->delete($block);
        flash()->success(
            $flashMessageArabicEnglish->getSuitableFlashMessage("تم حذف القطعة $block->name بنجاح", 'Block: ' . $block->name . ', has been deleted successfully')

        )->important();
        return redirect('blocks');
    }

    /*
    |--------------------------------------------------------------------------
    | Miscellaneous
    |--------------------------------------------------------------------------
    */


    /*
    |--------------------------------------------------------------------------
    | Miscellaneous - Ajax
    |--------------------------------------------------------------------------
    */

    public function search(Request $request, Block $block, IndexStrategyInterface $indexByMultiTenancy)
    {
        $authenticatedUser = auth()->user();
        $builder = $indexByMultiTenancy->index($block, $authenticatedUser, 'index_blocks_multi_tenancy');
        $results = $block->search($builder, $request->input('keyword'));
        return $results->get();
    }


    public function getProjectNamesIds(Request $request)
    {
        $keyword  = $request->input('term');
        $results = [
            'results' =>
            Project::where('name', 'like', "$keyword%")
                ->limit(6)
                ->get()
                ->map(function ($project, $key) {
                    return ['id' => $project->id, 'text' => $project->name, 'masterPlanImage' => $project->master_plan_image, 'zones'=>optional($project->zones)->pluck('name', 'id')];
                })
        ];
        return $results;
    }
}
